I'm not actually trying to change or extend PokeAPI here, I just forked the repo so that I could build some of my own tools on top of it.

I'll probably eventually move those tools to their own repo and incorporate PokeAPI as a submodule instead.

I've imported much of the PokeAPI data into a sqlite database, and also added data from [smogon.com].

There are some Python scripts which combine the two and process them into documents.

