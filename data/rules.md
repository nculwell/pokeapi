= Rules for pen-and-paper Pokemon battles

== Setup

1. Materials needed: this guide, blank Pokemon stat cards, blank move cards, blank log sheets.
1. Each player selects Pokemon to battle with. Do this privately so that other players don't know which Pokemon are on your team. You are only allowed to choose Pokemon that are listed in this guide, and only one per team with a given Pokedex number.
2. For each Pokemon on your team, fill out a Pokemon stat sheet. Copy the information from this guide to the stat card.

Pokemon stat card

Pokemon: POKEMON NAME
Ability: ABILITY
HP:  HP   Spe: SPE
Atk: ATK  SpA: SPA
Def: DEF  SpD: SPD
Upscaled HP: SCALEDHP

If you want, you can assign IVs, EVs and nature to stats and calculate the adjusted stat values. Otherwise, just use the base stats for that Pokemon. For HP, the value should be scaled up by a factor of 5/4 (1.25) in order to make our damage formulas work correctly; we call this the "Upscaled HP". The guide already includes upscaled HP values for Pokemon as calculated with 0 IVs and 0 EVs, so if you apply IVs and EVs then you should recalculate this value.

3. Choose moves for each Pokemon. For each move, fill out a move card with the following information.

Pokmon: POKEMON NAME
Move: MOVE NAME
Type: TYPE    Damage class: DC
Power: POW    Accuracy: ACC
PP: PP        Priority: PRIO
[PP CHECKBOXES]
MOVE NOTES

The move card has checkboxes that indicate the number of remaining PPs. Mark these boxes until the right number of PPs is remaining for the move. Use the number of PPs that the move should have with full PP Ups applied.

Place the move cards for each Pokemon underneath that Pokemon's stat card.

4. Begin a new battle log sheet. Write one player's name at the top of each column.

== Beginning the battle

Each player selects one of their Pokemon to go first, and places that Pokemon's stat card face down on the table. Then both players flip over the cards they have chosen.

On each side of the log sheet, record which Pokemon began the battle for that player like this:

LEAD: (Pokemon name)

Calculate the attack and special attack stat ratios for each Pokemon. That is, (My Pokemon's Attack / Opponent Pokemon's Defense) and (My Pokemon's Special Attack / Opponent Pokemon's Special Defense). Record these values on your side of the log sheet like this:

AR: (attack ratio)
SR: (special attack ratio)

== Fighting the battle

At the beginning of the round, each player chooses either a move card or a 
