 offense.......................53.02714%
 weatherless...................42.29667%
 rain..........................35.12867%
 sun...........................29.54401%
 balance.......................25.84114%
 hyperoffense..................20.74973%
 multiweather..................11.68209%
 rainoffense................... 7.31543%
 choice........................ 6.41945%
 sand.......................... 4.59983%
 sunoffense.................... 3.74112%
 dragmag....................... 2.72904%
 batonpass..................... 0.62965%
 sandoffense................... 0.50118%
 monotype...................... 0.38452%
 semistall..................... 0.38198%
 monoelectric.................. 0.21330%
 hail.......................... 0.21330%
 monowater..................... 0.17122%

 Stalliness (mean: -0.440)
 -2.0|###
     |###
 -1.5|##
     |###############
 -1.0|##########################
     |########################
 -0.5|#########################
     |#########################
  0.0|#######################
     |##############################
 +0.5|##
     |######
 +1.0|#
 more negative = more offensive, more positive = more stall
 one # =  0.53%
