 weatherless...................82.08198%
 hyperoffense..................62.71543%
 offense.......................37.28457%
 monotype......................14.35237%
 monopsychic................... 9.05719%
 sand.......................... 8.15826%
 hail.......................... 7.53055%
 monoelectric.................. 5.29519%
 hailoffense................... 3.76528%
 batonpass..................... 3.64239%
 rain.......................... 2.22921%

 Stalliness (mean: -1.154)
     |#################
     |
     |####################
     |
     |
     |######
     |#################
     |
 -1.2|#########
     |
 -1.0|##############################
     |
 -0.8|############
     |#######
 -0.6|######
     |
 -0.4|#############################
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  0.65%
