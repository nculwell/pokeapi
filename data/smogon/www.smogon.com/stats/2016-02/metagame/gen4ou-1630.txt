 weatherless...................51.13954%
 sand..........................47.77784%
 offense.......................40.45570%
 hyperoffense..................34.42150%
 sandoffense...................13.26507%
 balance....................... 8.79431%
 stall......................... 8.54451%
 semistall..................... 7.78399%
 sandstall..................... 6.73482%
 voltturn...................... 5.38381%
 dragmag....................... 2.36368%
 rain.......................... 0.78806%
 choice........................ 0.66987%
 rainoffense................... 0.57010%
 hail.......................... 0.49771%
 trickroom..................... 0.32432%
 multiweather.................. 0.24685%
 batonpass..................... 0.19382%
 hailstall..................... 0.09957%
 sun........................... 0.04370%
 tricksand..................... 0.04088%
 monotype...................... 0.03291%
 monofire...................... 0.02512%
 monograss..................... 0.00222%
 monodark...................... 0.00210%
 monosteel..................... 0.00165%
 monoflying.................... 0.00158%
 monofighting.................. 0.00113%
 monowater..................... 0.00069%
 hailoffense................... 0.00037%
 monobug....................... 0.00007%
 monodragon.................... 0.00000%
 sunstall...................... 0.00000%
 monopoison.................... 0.00000%
 sunoffense.................... 0.00000%
 monopsychic................... 0.00000%
 monorock...................... 0.00000%
 monoghost..................... 0.00000%
 trickhail..................... 0.00000%
 monoelectric.................. 0.00000%
 mononormal.................... 0.00000%

 Stalliness (mean: -0.375)
 -2.0|###
     |######
 -1.5|#############
     |#############################
 -1.0|##############################
     |########################
 -0.5|###################
     |######################
  0.0|##########
     |######
 +0.5|#####
     |###
 +1.0|#########
     |######
 +1.5|###
     |##########
 +2.0|###
     |###
 +2.5|#
 more negative = more offensive, more positive = more stall
 one # =  0.49%
