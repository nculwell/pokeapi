 +----------------------------------------+ 
 | Gastly                                 | 
 +----------------------------------------+ 
 | Raw count: 17                          | 
 | Avg. weight: 0.064371653129            | 
 | Viability Ceiling: 72                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 100.000%                  | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:36/0/36/200/0/200 66.787%        | 
 | Timid:0/0/0/196/76/196 33.213%         | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Sludge Bomb 100.000%                   | 
 | Shadow Ball 100.000%                   | 
 | Hidden Power Ground 66.787%            | 
 | Thunderbolt 66.787%                    | 
 | Explosion 33.213%                      | 
 | Giga Drain 33.213%                     | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Chinchou                               | 
 +----------------------------------------+ 
 | Raw count: 13                          | 
 | Avg. weight: 0.0841783156302           | 
 | Viability Ceiling: 72                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Volt Absorb 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 66.787%                   | 
 | Choice Specs 33.213%                   | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Modest:0/0/52/232/0/220 66.787%        | 
 | Modest:0/0/0/232/228/0 33.213%         | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Thunderbolt 100.000%                   | 
 | Ice Beam 100.000%                      | 
 | Hidden Power Grass 100.000%            | 
 | Surf 66.787%                           | 
 | Hydro Pump 33.213%                     | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Buizel                                 | 
 +----------------------------------------+ 
 | Raw count: 13                          | 
 | Avg. weight: 0.030109735087            | 
 | Viability Ceiling: 72                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Swift Swim 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 92.855%                    | 
 | Lum Berry  7.145%                      | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/236/0/0/36/236 92.855%       | 
 | Timid:76/0/0/0/196/236  7.145%         | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Aqua Jet 92.855%                       | 
 | Ice Punch 92.855%                      | 
 | Waterfall 92.855%                      | 
 | Brick Break 92.855%                    | 
 | Surf  7.145%                           | 
 | Protect  7.145%                        | 
 | Other 14.291%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Cranidos +18.790%                      | 
 | Abra +18.790%                          | 
 | Lickitung +18.790%                     | 
 | Aipom +1.446%                          | 
 | Lileep +1.446%                         | 
 | Elekid +1.446%                         | 
 | Gligar +1.446%                         | 
 | Riolu +1.446%                          | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Abra                                   | 
 +----------------------------------------+ 
 | Raw count: 13                          | 
 | Avg. weight: 0.027958317089            | 
 | Viability Ceiling: 72                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Inner Focus 100.000%                   | 
 | Synchronize  0.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Specs 100.000%                  | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/236/76/196 100.000%        | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Hidden Power Ice 100.000%              | 
 | Shadow Ball 100.000%                   | 
 | Psychic 100.000%                       | 
 | Energy Ball 100.000%                   | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Cranidos +25.935%                      | 
 | Lickitung +25.935%                     | 
 | Buizel +20.236%                        | 
 | Chinchou +5.699%                       | 
 | Gastly +5.699%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Lickitung                              | 
 +----------------------------------------+ 
 | Raw count: 7                           | 
 | Avg. weight: 0.0519225888796           | 
 | Viability Ceiling: 72                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Oblivious 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/236/0/0/236/36 100.000%      | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Return 100.000%                        | 
 | Power Whip 100.000%                    | 
 | Fire Punch 100.000%                    | 
 | Earthquake 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Cranidos +25.935%                      | 
 | Abra +25.935%                          | 
 | Buizel +20.236%                        | 
 | Chinchou +5.699%                       | 
 | Gastly +5.699%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Cranidos                               | 
 +----------------------------------------+ 
 | Raw count: 8                           | 
 | Avg. weight: 0.0454322652697           | 
 | Viability Ceiling: 72                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Mold Breaker 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 100.000%                  | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/236/0/0/36/212 100.000%        | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Earthquake 100.000%                    | 
 | Fire Punch 100.000%                    | 
 | Head Smash 100.000%                    | 
 | Iron Head 100.000%                     | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Abra +25.935%                          | 
 | Lickitung +25.935%                     | 
 | Buizel +20.236%                        | 
 | Chinchou +5.699%                       | 
 | Gastly +5.699%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Diglett                                | 
 +----------------------------------------+ 
 | Raw count: 6                           | 
 | Avg. weight: 0.121809996839            | 
 | Viability Ceiling: 71                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Arena Trap 100.000%                    | 
 | Sand Veil  0.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 100.000%                      | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/240/0/0/0/236 100.000%         | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Hidden Power Ice 100.000%              | 
 | Rock Slide 100.000%                    | 
 | Sucker Punch 100.000%                  | 
 | Earthquake 100.000%                    | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Mankey                                 | 
 +----------------------------------------+ 
 | Raw count: 6                           | 
 | Avg. weight: 0.121809996839            | 
 | Viability Ceiling: 71                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Anger Point 100.000%                   | 
 | Vital Spirit  0.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 100.000%                  | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:116/196/0/0/0/196 100.000%     | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | U-turn 100.000%                        | 
 | Ice Punch 100.000%                     | 
 | Close Combat 100.000%                  | 
 | Payback 100.000%                       | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Dratini                                | 
 +----------------------------------------+ 
 | Raw count: 5                           | 
 | Avg. weight: 0.146171996207            | 
 | Viability Ceiling: 71                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Shed Skin 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 100.000%                      | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:28/244/0/0/36/196 100.000%     | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Outrage 100.000%                       | 
 | Dragon Dance 100.000%                  | 
 | Waterfall 100.000%                     | 
 | Extreme Speed 100.000%                 | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Bronzor                                | 
 +----------------------------------------+ 
 | Raw count: 7                           | 
 | Avg. weight: 0.104408568719            | 
 | Viability Ceiling: 71                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 | Heatproof  0.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Oran Berry 100.000%                    | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Relaxed:220/8/148/4/68/12 100.000%     | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Psychic 100.000%                       | 
 | Reflect 100.000%                       | 
 | Stealth Rock 100.000%                  | 
 | Earthquake 100.000%                    | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Lileep                                 | 
 +----------------------------------------+ 
 | Raw count: 3                           | 
 | Avg. weight: 0.00932281132468          | 
 | Viability Ceiling: 57                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Suction Cups 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Rose Incense 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/188/60/0/220/12 100.000%     | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Confuse Ray 100.000%                   | 
 | Seed Bomb 100.000%                     | 
 | Double-Edge 100.000%                   | 
 | Synthesis 100.000%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Gligar +94.301%                        | 
 | Elekid +94.301%                        | 
 | Riolu +94.301%                         | 
 | Aipom +94.301%                         | 
 | Buizel +20.236%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Elekid                                 | 
 +----------------------------------------+ 
 | Raw count: 8                           | 
 | Avg. weight: 0.00349605424676          | 
 | Viability Ceiling: 59                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Static 100.000%                        | 
 +----------------------------------------+ 
 | Items                                  | 
 | Lum Berry 100.000%                     | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/252/0/0/0/236 100.000%         | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Dynamic Punch 100.000%                 | 
 | Thunder 100.000%                       | 
 | Cross Chop 100.000%                    | 
 | Attract 100.000%                       | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Aipom +94.301%                         | 
 | Lileep +94.301%                        | 
 | Gligar +94.301%                        | 
 | Riolu +94.301%                         | 
 | Buizel +20.236%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Aipom                                  | 
 +----------------------------------------+ 
 | Raw count: 11                          | 
 | Avg. weight: 0.00254258490673          | 
 | Viability Ceiling: 57                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pickup 100.000%                        | 
 | Run Away  0.000%                       | 
 +----------------------------------------+ 
 | Items                                  | 
 | Nothing 100.000%                       | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:76/196/0/0/0/236 100.000%        | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Dynamic Punch 100.000%                 | 
 | Gunk Shot 100.000%                     | 
 | Double-Edge 100.000%                   | 
 | Thunder 100.000%                       | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Lileep +94.301%                        | 
 | Elekid +94.301%                        | 
 | Gligar +94.301%                        | 
 | Riolu +94.301%                         | 
 | Buizel +20.236%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Gligar                                 | 
 +----------------------------------------+ 
 | Raw count: 10                          | 
 | Avg. weight: 0.0027968433974           | 
 | Viability Ceiling: 57                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Hyper Cutter 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/236/0/0/236/0 100.000%       | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Roost 100.000%                         | 
 | Stone Edge 100.000%                    | 
 | Swords Dance 100.000%                  | 
 | Earthquake 100.000%                    | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Aipom +94.301%                         | 
 | Lileep +94.301%                        | 
 | Elekid +94.301%                        | 
 | Riolu +94.301%                         | 
 | Buizel +20.236%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Riolu                                  | 
 +----------------------------------------+ 
 | Raw count: 9                           | 
 | Avg. weight: 0.00310760377489          | 
 | Viability Ceiling: 57                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Inner Focus 100.000%                   | 
 | Steadfast  0.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/196/116/0/196/0 90.176%      | 
 | Adamant:0/196/36/0/196/13  9.824%      | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Copycat 100.000%                       | 
 | Cross Chop 100.000%                    | 
 | Earthquake 100.000%                    | 
 | High Jump Kick 100.000%                | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Aipom +94.301%                         | 
 | Lileep +94.301%                        | 
 | Gligar +94.301%                        | 
 | Elekid +94.301%                        | 
 | Buizel +20.236%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
