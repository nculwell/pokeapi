 Total leads: 25694
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Azelf              | 12.25668% | 2857   | 11.119% | 
 | 2    | Metagross          | 10.89771% | 2247   |  8.745% | 
 | 3    | Machamp            | 10.80859% | 2646   | 10.298% | 
 | 4    | Jirachi            |  6.98064% | 1326   |  5.161% | 
 | 5    | Aerodactyl         |  4.53780% | 1141   |  4.441% | 
 | 6    | Infernape          |  4.18738% | 1108   |  4.312% | 
 | 7    | Zapdos             |  3.41730% | 733    |  2.853% | 
 | 8    | Hippowdon          |  3.33091% | 735    |  2.861% | 
 | 9    | Heatran            |  3.15796% | 775    |  3.016% | 
 | 10   | Tyranitar          |  2.74224% | 678    |  2.639% | 
 | 11   | Swampert           |  2.46427% | 654    |  2.545% | 
 | 12   | Dragonite          |  2.16510% | 589    |  2.292% | 
 | 13   | Empoleon           |  2.09340% | 488    |  1.899% | 
 | 14   | Roserade           |  2.08764% | 518    |  2.016% | 
 | 15   | Starmie            |  1.95029% | 559    |  2.176% | 
 | 16   | Uxie               |  1.94741% | 383    |  1.491% | 
 | 17   | Smeargle           |  1.86565% | 470    |  1.829% | 
 | 18   | Mamoswine          |  1.48764% | 341    |  1.327% | 
 | 19   | Flygon             |  1.30565% | 351    |  1.366% | 
 | 20   | Skarmory           |  1.26980% | 394    |  1.533% | 
 | 21   | Gliscor            |  1.24345% | 388    |  1.510% | 
 | 22   | Gengar             |  1.19321% | 346    |  1.347% | 
 | 23   | Crobat             |  1.05863% | 282    |  1.098% | 
 | 24   | Celebi             |  1.03142% | 266    |  1.035% | 
 | 25   | Gyarados           |  0.94620% | 291    |  1.133% | 
 | 26   | Forretress         |  0.79909% | 309    |  1.203% | 
 | 27   | Bronzong           |  0.74699% | 261    |  1.016% | 
 | 28   | Breloom            |  0.70221% | 260    |  1.012% | 
 | 29   | Scizor             |  0.60885% | 197    |  0.767% | 
 | 30   | Abomasnow          |  0.54792% | 120    |  0.467% | 
 | 31   | Weavile            |  0.47604% | 237    |  0.922% | 
 | 32   | Butterfree         |  0.47451% | 80     |  0.311% | 
 | 33   | Jynx               |  0.44904% | 79     |  0.307% | 
 | 34   | Staraptor          |  0.43973% | 129    |  0.502% | 
 | 35   | Ninjask            |  0.43069% | 162    |  0.630% | 
 | 36   | Yanmega            |  0.36217% | 78     |  0.304% | 
 | 37   | Lucario            |  0.35759% | 145    |  0.564% | 
 | 38   | Raikou             |  0.35501% | 73     |  0.284% | 
 | 39   | Rotom-Heat         |  0.35299% | 77     |  0.300% | 
 | 40   | Ambipom            |  0.33378% | 114    |  0.444% | 
 | 41   | Electrode          |  0.31080% | 57     |  0.222% | 
 | 42   | Nidoqueen          |  0.29700% | 59     |  0.230% | 
 | 43   | Gallade            |  0.29326% | 93     |  0.362% | 
 | 44   | Electivire         |  0.26464% | 127    |  0.494% | 
 | 45   | Alakazam           |  0.24151% | 81     |  0.315% | 
 | 46   | Porygon-Z          |  0.21921% | 62     |  0.241% | 
 | 47   | Mesprit            |  0.21139% | 36     |  0.140% | 
 | 48   | Blissey            |  0.19949% | 91     |  0.354% | 
 | 49   | Medicham           |  0.19125% | 52     |  0.202% | 
 | 50   | Rotom-Wash         |  0.18723% | 63     |  0.245% | 
 | 51   | Jolteon            |  0.18417% | 71     |  0.276% | 
 | 52   | Hariyama           |  0.17424% | 85     |  0.331% | 
 | 53   | Pikachu            |  0.15885% | 80     |  0.311% | 
 | 54   | Cresselia          |  0.15279% | 49     |  0.191% | 
 | 55   | Torterra           |  0.15040% | 76     |  0.296% | 
 | 56   | Heracross          |  0.14559% | 40     |  0.156% | 
 | 57   | Dusknoir           |  0.13056% | 75     |  0.292% | 
 | 58   | Slaking            |  0.12477% | 62     |  0.241% | 
 | 59   | Cloyster           |  0.10974% | 38     |  0.148% | 
 | 60   | Spiritomb          |  0.10564% | 34     |  0.132% | 
 | 61   | Togekiss           |  0.09284% | 97     |  0.378% | 
 | 62   | Honchkrow          |  0.08258% | 39     |  0.152% | 
 | 63   | Magmortar          |  0.08057% | 51     |  0.198% | 
 | 64   | Charizard          |  0.07949% | 48     |  0.187% | 
 | 65   | Typhlosion         |  0.07662% | 52     |  0.202% | 
 | 66   | Suicune            |  0.07217% | 33     |  0.128% | 
 | 67   | Aggron             |  0.06584% | 23     |  0.090% | 
 | 68   | Venusaur           |  0.06554% | 38     |  0.148% | 
 | 69   | Kingdra            |  0.06470% | 113    |  0.440% | 
 | 70   | Magnezone          |  0.05574% | 31     |  0.121% | 
 | 71   | Snorlax            |  0.05237% | 60     |  0.234% | 
 | 72   | Steelix            |  0.05225% | 25     |  0.097% | 
 | 73   | Feraligatr         |  0.05135% | 22     |  0.086% | 
 | 74   | Umbreon            |  0.05057% | 34     |  0.132% | 
 | 75   | Swellow            |  0.04761% | 20     |  0.078% | 
 | 76   | Moltres            |  0.04751% | 12     |  0.047% | 
 | 77   | Claydol            |  0.04726% | 36     |  0.140% | 
 | 78   | Blaziken           |  0.04545% | 19     |  0.074% | 
 | 79   | Shaymin            |  0.04169% | 14     |  0.054% | 
 | 80   | Poliwrath          |  0.04011% | 18     |  0.070% | 
 | 81   | Tentacruel         |  0.03843% | 18     |  0.070% | 
 | 82   | Venomoth           |  0.03542% | 7      |  0.027% | 
 | 83   | Arcanine           |  0.03520% | 16     |  0.062% | 
 | 84   | Rhyperior          |  0.03369% | 15     |  0.058% | 
 | 85   | Houndoom           |  0.03306% | 9      |  0.035% | 
 | 86   | Ursaring           |  0.02887% | 11     |  0.043% | 
 | 87   | Rotom-Fan          |  0.02885% | 7      |  0.027% | 
 | 88   | Sceptile           |  0.02828% | 33     |  0.128% | 
 | 89   | Glalie             |  0.02743% | 5      |  0.019% | 
 | 90   | Drapion            |  0.02693% | 21     |  0.082% | 
 | 91   | Raichu             |  0.02594% | 27     |  0.105% | 
 | 92   | Entei              |  0.02581% | 16     |  0.062% | 
 | 93   | Blastoise          |  0.02485% | 12     |  0.047% | 
 | 94   | Vaporeon           |  0.02438% | 15     |  0.058% | 
 | 95   | Jumpluff           |  0.02270% | 7      |  0.027% | 
 | 96   | Ludicolo           |  0.02098% | 9      |  0.035% | 
 | 97   | Politoed           |  0.01939% | 8      |  0.031% | 
 | 98   | Milotic            |  0.01891% | 9      |  0.035% | 
 | 99   | Espeon             |  0.01842% | 34     |  0.132% | 
 | 100  | Gardevoir          |  0.01827% | 8      |  0.031% | 
 | 101  | Lapras             |  0.01728% | 23     |  0.090% | 
 | 102  | Registeel          |  0.01661% | 12     |  0.047% | 
 | 103  | Weezing            |  0.01652% | 16     |  0.062% | 
 | 104  | Tauros             |  0.01541% | 6      |  0.023% | 
 | 105  | Lopunny            |  0.01474% | 8      |  0.031% | 
 | 106  | Whiscash           |  0.01427% | 12     |  0.047% | 
 | 107  | Ampharos           |  0.01411% | 10     |  0.039% | 
 | 108  | Toxicroak          |  0.01385% | 4      |  0.016% | 
 | 109  | Torkoal            |  0.01310% | 11     |  0.043% | 
 | 110  | Gastrodon          |  0.01291% | 5      |  0.019% | 
 | 111  | Dugtrio            |  0.01250% | 2      |  0.008% | 
 | 112  | Clefable           |  0.01242% | 4      |  0.016% | 
 | 113  | Mightyena          |  0.01234% | 5      |  0.019% | 
 | 114  | Misdreavus         |  0.01102% | 8      |  0.031% | 
 | 115  | Luxray             |  0.01096% | 7      |  0.027% | 
 | 116  | Donphan            |  0.01086% | 10     |  0.039% | 
 | 117  | Slowbro            |  0.01038% | 4      |  0.016% | 
 | 118  | Shuckle            |  0.00970% | 23     |  0.090% | 
 | 119  | Manectric          |  0.00962% | 4      |  0.016% | 
 | 120  | Rotom-Frost        |  0.00930% | 4      |  0.016% | 
 | 121  | Shedinja           |  0.00854% | 5      |  0.019% | 
 | 122  | Azumarill          |  0.00840% | 9      |  0.035% | 
 | 123  | Turtwig            |  0.00826% | 12     |  0.047% | 
 | 124  | Absol              |  0.00796% | 7      |  0.027% | 
 | 125  | Rattata            |  0.00793% | 3      |  0.012% | 
 | 126  | Purugly            |  0.00735% | 6      |  0.023% | 
 | 127  | Tangrowth          |  0.00720% | 5      |  0.019% | 
 | 128  | Nidoking           |  0.00714% | 4      |  0.016% | 
 | 129  | Mismagius          |  0.00703% | 5      |  0.019% | 
 | 130  | Graveler           |  0.00659% | 4      |  0.016% | 
 | 131  | Altaria            |  0.00649% | 5      |  0.019% | 
 | 132  | Lanturn            |  0.00633% | 2      |  0.008% | 
 | 133  | Poochyena          |  0.00625% | 1      |  0.004% | 
 | 134  | Qwilfish           |  0.00624% | 2      |  0.008% | 
 | 135  | Omastar            |  0.00579% | 11     |  0.043% | 
 | 136  | Bastiodon          |  0.00571% | 4      |  0.016% | 
 | 137  | Meganium           |  0.00570% | 5      |  0.019% | 
 | 138  | Porygon2           |  0.00565% | 3      |  0.012% | 
 | 139  | Tropius            |  0.00552% | 1      |  0.004% | 
 | 140  | Pachirisu          |  0.00546% | 4      |  0.016% | 
 | 141  | Pidgeot            |  0.00535% | 11     |  0.043% | 
 | 142  | Drifblim           |  0.00530% | 4      |  0.016% | 
 | 143  | Exeggutor          |  0.00494% | 3      |  0.012% | 
 | 144  | Articuno           |  0.00481% | 2      |  0.008% | 
 | 145  | Sableye            |  0.00470% | 2      |  0.008% | 
 | 146  | Leafeon            |  0.00465% | 2      |  0.008% | 
 | 147  | Kabutops           |  0.00465% | 2      |  0.008% | 
 | 148  | Chatot             |  0.00465% | 2      |  0.008% | 
 | 149  | Magikarp           |  0.00451% | 2      |  0.008% | 
 | 150  | Crawdaunt          |  0.00444% | 2      |  0.008% | 
 | 151  | Aron               |  0.00437% | 2      |  0.008% | 
 | 152  | Jigglypuff         |  0.00437% | 2      |  0.008% | 
 | 153  | Dusclops           |  0.00424% | 1      |  0.004% | 
 | 154  | Hitmonchan         |  0.00421% | 2      |  0.008% | 
 | 155  | Elekid             |  0.00400% | 2      |  0.008% | 
 | 156  | Hitmontop          |  0.00396% | 2      |  0.008% | 
 | 157  | Regirock           |  0.00392% | 30     |  0.117% | 
 | 158  | Walrein            |  0.00370% | 2      |  0.008% | 
 | 159  | Grumpig            |  0.00316% | 3      |  0.012% | 
 | 160  | Ariados            |  0.00280% | 1      |  0.004% | 
 | 161  | Kangaskhan         |  0.00270% | 8      |  0.031% | 
 | 162  | Mawile             |  0.00267% | 1      |  0.004% | 
 | 163  | Grovyle            |  0.00257% | 1      |  0.004% | 
 | 164  | Noctowl            |  0.00254% | 2      |  0.008% | 
 | 165  | Vigoroth           |  0.00252% | 1      |  0.004% | 
 | 166  | Bulbasaur          |  0.00248% | 3      |  0.012% | 
 | 167  | Onix               |  0.00235% | 4      |  0.016% | 
 | 168  | Kricketune         |  0.00235% | 1      |  0.004% | 
 | 169  | Rampardos          |  0.00233% | 1      |  0.004% | 
 | 170  | Eevee              |  0.00233% | 1      |  0.004% | 
 | 171  | Chansey            |  0.00233% | 1      |  0.004% | 
 | 172  | Teddiursa          |  0.00233% | 1      |  0.004% | 
 | 173  | Combusken          |  0.00233% | 1      |  0.004% | 
 | 174  | Ninetales          |  0.00233% | 1      |  0.004% | 
 | 175  | Hitmonlee          |  0.00208% | 1      |  0.004% | 
 | 176  | Mr. Mime           |  0.00208% | 1      |  0.004% | 
 | 177  | Muk                |  0.00207% | 1      |  0.004% | 
 | 178  | Rotom              |  0.00205% | 2      |  0.008% | 
 | 179  | Grotle             |  0.00205% | 3      |  0.012% | 
 | 180  | Rhydon             |  0.00195% | 1      |  0.004% | 
 | 181  | Luxio              |  0.00192% | 1      |  0.004% | 
 | 182  | Hippopotas         |  0.00167% | 1      |  0.004% | 
 | 183  | Voltorb            |  0.00126% | 1      |  0.004% | 
 | 184  | Marowak            |  0.00112% | 3      |  0.012% | 
 | 185  | Shiftry            |  0.00088% | 2      |  0.008% | 
 | 186  | Miltank            |  0.00087% | 1      |  0.004% | 
 | 187  | Ditto              |  0.00074% | 1      |  0.004% | 
 | 188  | Kingler            |  0.00067% | 1      |  0.004% | 
 | 189  | Magnemite          |  0.00003% | 1      |  0.004% | 
 | 190  | Golem              |  0.00000% | 1      |  0.004% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
