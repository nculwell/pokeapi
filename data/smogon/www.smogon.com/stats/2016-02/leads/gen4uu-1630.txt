 Total leads: 130
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Blaziken           | 51.97075% | 16     | 12.308% | 
 | 2    | Rhyperior          | 25.76846% | 13     | 10.000% | 
 | 3    | Registeel          |  8.27179% | 6      |  4.615% | 
 | 4    | Jumpluff           |  3.10749% | 3      |  2.308% | 
 | 5    | Claydol            |  2.88918% | 3      |  2.308% | 
 | 6    | Arcanine           |  1.99843% | 2      |  1.538% | 
 | 7    | Hariyama           |  1.34202% | 5      |  3.846% | 
 | 8    | Weezing            |  1.26888% | 1      |  0.769% | 
 | 9    | Mesprit            |  1.25002% | 4      |  3.077% | 
 | 10   | Kangaskhan         |  1.13377% | 1      |  0.769% | 
 | 11   | Nidoqueen          |  0.97593% | 7      |  5.385% | 
 | 12   | Ambipom            |  0.02328% | 12     |  9.231% | 
 | 13   | Gastrodon          |  0.00000% | 2      |  1.538% | 
 | 14   | Spinda             |  0.00000% | 1      |  0.769% | 
 | 15   | Cloyster           |  0.00000% | 1      |  0.769% | 
 | 16   | Beedrill           |  0.00000% | 2      |  1.538% | 
 | 17   | Spiritomb          |  0.00000% | 1      |  0.769% | 
 | 18   | Uxie               |  0.00000% | 9      |  6.923% | 
 | 19   | Dragonair          |  0.00000% | 1      |  0.769% | 
 | 20   | Raichu             |  0.00000% | 1      |  0.769% | 
 | 21   | Crawdaunt          |  0.00000% | 1      |  0.769% | 
 | 22   | Lopunny            |  0.00000% | 3      |  2.308% | 
 | 23   | Gligar             |  0.00000% | 1      |  0.769% | 
 | 24   | Exeggutor          |  0.00000% | 1      |  0.769% | 
 | 25   | Absol              |  0.00000% | 3      |  2.308% | 
 | 26   | Drapion            |  0.00000% | 1      |  0.769% | 
 | 27   | Buizel             |  0.00000% | 1      |  0.769% | 
 | 28   | Mismagius          |  0.00000% | 3      |  2.308% | 
 | 29   | Kabutops           |  0.00000% | 1      |  0.769% | 
 | 30   | Milotic            |  0.00000% | 1      |  0.769% | 
 | 31   | Sceptile           |  0.00000% | 1      |  0.769% | 
 | 32   | Linoone            |  0.00000% | 1      |  0.769% | 
 | 33   | Pidgeot            |  0.00000% | 1      |  0.769% | 
 | 34   | Tangrowth          |  0.00000% | 3      |  2.308% | 
 | 35   | Snover             |  0.00000% | 4      |  3.077% | 
 | 36   | Chatot             |  0.00000% | 1      |  0.769% | 
 | 37   | Armaldo            |  0.00000% | 1      |  0.769% | 
 | 38   | Zangoose           |  0.00000% | 1      |  0.769% | 
 | 39   | Azumarill          |  0.00000% | 1      |  0.769% | 
 | 40   | Lanturn            |  0.00000% | 1      |  0.769% | 
 | 41   | Torterra           |  0.00000% | 1      |  0.769% | 
 | 42   | Aggron             |  0.00000% | 1      |  0.769% | 
 | 43   | Ampharos           |  0.00000% | 1      |  0.769% | 
 | 44   | Omastar            |  0.00000% | 1      |  0.769% | 
 | 45   | Alakazam           |  0.00000% | 3      |  2.308% | 
 | 46   | Bibarel            |  0.00000% | 1      |  0.769% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
