 Total battles: 11
 Avg. weight/team: 0.511
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Abra               | 38.37429% | 7      | 31.818% | 6      | 36.735% | 
 | 2    | Cranidos           | 31.64922% | 6      | 27.273% | 5      | 30.612% | 
 | 3    | Bagon              | 30.50648% | 5      | 22.727% | 3      | 18.367% | 
 | 4    | Aron               | 24.92078% | 4      | 18.182% | 0      |  0.000% | 
 | 5    | Tentacool          | 24.92078% | 4      | 18.182% | 3      | 18.367% | 
 | 6    | Chimchar           | 19.95302% | 5      | 22.727% | 4      | 24.490% | 
 | 7    | Duskull            | 19.33507% | 3      | 13.636% | 2      | 12.245% | 
 | 8    | Buizel             | 18.92475% | 4      | 18.182% | 4      | 24.490% | 
 | 9    | Munchlax           | 18.82638% | 4      | 18.182% | 4      | 24.490% | 
 | 10   | Charmander         | 16.23571% | 5      | 22.727% | 4      | 24.490% | 
 | 11   | Gastly             | 15.40493% | 3      | 13.636% | 3      | 18.367% | 
 | 12   | Wynaut             | 13.95700% | 5      | 22.727% | 4      | 24.490% | 
 | 13   | Snover             | 13.12621% | 3      | 13.636% | 3      | 18.367% | 
 | 14   | Piplup             | 12.92872% | 4      | 18.182% | 4      | 24.490% | 
 | 15   | Diglett            | 12.09794% | 2      |  9.091% | 1      |  6.122% | 
 | 16   | Shellder           | 11.17141% | 2      |  9.091% | 1      |  6.122% | 
 | 17   | Corphish           | 11.17141% | 2      |  9.091% | 2      | 12.245% | 
 | 18   | Croagunk           | 11.17141% | 2      |  9.091% | 2      | 12.245% | 
 | 19   | Treecko            | 10.03205% | 2      |  9.091% | 1      |  6.122% | 
 | 20   | Aipom              | 10.03205% | 2      |  9.091% | 1      |  6.122% | 
 | 21   | Gligar             | 10.03205% | 2      |  9.091% | 2      | 12.245% | 
 | 22   | Onix               |  9.00716% | 2      |  9.091% | 2      | 12.245% | 
 | 23   | Carvanha           |  8.89270% | 2      |  9.091% | 1      |  6.122% | 
 | 24   | Anorith            |  8.89270% | 2      |  9.091% | 1      |  6.122% | 
 | 25   | Bronzor            |  8.89270% | 2      |  9.091% | 2      | 12.245% | 
 | 26   | Eevee              |  8.89270% | 2      |  9.091% | 2      | 12.245% | 
 | 27   | Larvitar           |  7.34301% | 3      | 13.636% | 2      | 12.245% | 
 | 28   | Chikorita          |  7.34301% | 3      | 13.636% | 3      | 18.367% | 
 | 29   | Shinx              |  7.34301% | 3      | 13.636% | 3      | 18.367% | 
 | 30   | Gible              |  6.61398% | 2      |  9.091% | 2      | 12.245% | 
 | 31   | Taillow            |  6.51223% | 1      |  4.545% | 1      |  6.122% | 
 | 32   | Dratini            |  6.51223% | 1      |  4.545% | 1      |  6.122% | 
 | 33   | Magby              |  5.58570% | 1      |  4.545% | 1      |  6.122% | 
 | 34   | Beldum             |  5.58570% | 1      |  4.545% | 1      |  6.122% | 
 | 35   | Growlithe          |  5.58570% | 1      |  4.545% | 1      |  6.122% | 
 | 36   | Elekid             |  5.58570% | 1      |  4.545% | 0      |  0.000% | 
 | 37   | Doduo              |  5.58570% | 1      |  4.545% | 0      |  0.000% | 
 | 38   | Drowzee            |  5.58570% | 1      |  4.545% | 0      |  0.000% | 
 | 39   | Meowth             |  5.58570% | 1      |  4.545% | 1      |  6.122% | 
 | 40   | Shroomish          |  5.58570% | 1      |  4.545% | 1      |  6.122% | 
 | 41   | Voltorb            |  3.42146% | 1      |  4.545% | 1      |  6.122% | 
 | 42   | Mantyke            |  3.42146% | 1      |  4.545% | 1      |  6.122% | 
 | 43   | Togepi             |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 44   | Meditite           |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 45   | Slowpoke           |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 46   | Wooper             |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 47   | Electrike          |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 48   | Mankey             |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 49   | Teddiursa          |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 50   | Riolu              |  3.30699% | 1      |  4.545% | 0      |  0.000% | 
 | 51   | Snubbull           |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 52   | Ponyta             |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 53   | Glameow            |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 54   | Cubone             |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 | 55   | Shellos            |  3.30699% | 1      |  4.545% | 1      |  6.122% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
