 weatherless...................91.01861%
 balance.......................45.82986%
 offense.......................34.29742%
 hyperoffense..................10.87598%
 rain.......................... 7.29184%
 semistall..................... 7.09556%
 rainoffense................... 5.42551%
 stall......................... 1.90119%
 monotype...................... 1.68838%
 choice........................ 1.30528%
 hail.......................... 1.12675%
 monobug....................... 1.12559%
 monoghost..................... 1.12559%
 sun........................... 0.56279%
 batonpass..................... 0.56279%
 mononormal.................... 0.56279%
 monoflying.................... 0.56279%

 Stalliness (mean:  0.082)
     |#
 -2.0|#
     |#
 -1.5|
     |############
 -1.0|############
     |###############
 -0.5|##############
     |####################
  0.0|######################
     |##################
 +0.5|###################
     |##############################
 +1.0|##########
     |############
 +1.5|##
     |
 +2.0|
 more negative = more offensive, more positive = more stall
 one # =  0.52%
