 Total battles: 2
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Snover             | 50.00000% | 2      | 50.000% | 1      | 46.154% | 
 | 2    | Gastly             | 50.00000% | 2      | 50.000% | 2      | 92.308% | 
 | 3    | Munchlax           | 50.00000% | 2      | 50.000% | 2      | 92.308% | 
 | 4    | Bronzor            | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 | 5    | Machop             | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 | 6    | Elekid             | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 | 7    | Gligar             | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 | 8    | Porygon            | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 | 9    | Igglybuff          | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 | 10   | Mankey             | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 | 11   | Sunkern            | 25.00000% | 1      | 25.000% | 1      | 46.154% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
