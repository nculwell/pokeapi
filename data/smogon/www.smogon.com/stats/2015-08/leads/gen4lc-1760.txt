 Total leads: 4
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Igglybuff          |  0.00000% | 1      | 25.000% | 
 | 2    | Bronzor            |  0.00000% | 1      | 25.000% | 
 | 3    | Machop             |  0.00000% | 1      | 25.000% | 
 | 4    | Sunkern            |  0.00000% | 1      | 25.000% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
