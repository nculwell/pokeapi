 Total leads: 158
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Sharpedo           | 87.84961% | 19     | 12.025% | 
 | 2    | Sandslash          |  8.03411% | 5      |  3.165% | 
 | 3    | Nidoqueen          |  2.96796% | 2      |  1.266% | 
 | 4    | Cacturne           |  1.11241% | 3      |  1.899% | 
 | 5    | Solrock            |  0.03591% | 7      |  4.430% | 
 | 6    | Quagsire           |  0.00000% | 1      |  0.633% | 
 | 7    | Gastrodon          |  0.00000% | 2      |  1.266% | 
 | 8    | Pinsir             |  0.00000% | 1      |  0.633% | 
 | 9    | Tauros             |  0.00000% | 2      |  1.266% | 
 | 10   | Manectric          |  0.00000% | 1      |  0.633% | 
 | 11   | Beedrill           |  0.00000% | 2      |  1.266% | 
 | 12   | Regirock           |  0.00000% | 11     |  6.962% | 
 | 13   | Chatot             |  0.00000% | 1      |  0.633% | 
 | 14   | Arbok              |  0.00000% | 1      |  0.633% | 
 | 15   | Gorebyss           |  0.00000% | 1      |  0.633% | 
 | 16   | Gardevoir          |  0.00000% | 1      |  0.633% | 
 | 17   | Kecleon            |  0.00000% | 2      |  1.266% | 
 | 18   | Marowak            |  0.00000% | 2      |  1.266% | 
 | 19   | Glaceon            |  0.00000% | 1      |  0.633% | 
 | 20   | Wigglytuff         |  0.00000% | 1      |  0.633% | 
 | 21   | Charizard          |  0.00000% | 8      |  5.063% | 
 | 22   | Crawdaunt          |  0.00000% | 1      |  0.633% | 
 | 23   | Slowking           |  0.00000% | 1      |  0.633% | 
 | 24   | Drifblim           |  0.00000% | 1      |  0.633% | 
 | 25   | Jynx               |  0.00000% | 9      |  5.696% | 
 | 26   | Shuckle            |  0.00000% | 3      |  1.899% | 
 | 27   | Magmortar          |  0.00000% | 1      |  0.633% | 
 | 28   | Rhydon             |  0.00000% | 4      |  2.532% | 
 | 29   | Machoke            |  0.00000% | 1      |  0.633% | 
 | 30   | Gligar             |  0.00000% | 5      |  3.165% | 
 | 31   | Jumpluff           |  0.00000% | 1      |  0.633% | 
 | 32   | Bastiodon          |  0.00000% | 3      |  1.899% | 
 | 33   | Skuntank           |  0.00000% | 2      |  1.266% | 
 | 34   | Probopass          |  0.00000% | 2      |  1.266% | 
 | 35   | Gabite             |  0.00000% | 1      |  0.633% | 
 | 36   | Delibird           |  0.00000% | 1      |  0.633% | 
 | 37   | Sudowoodo          |  0.00000% | 1      |  0.633% | 
 | 38   | Linoone            |  0.00000% | 1      |  0.633% | 
 | 39   | Butterfree         |  0.00000% | 2      |  1.266% | 
 | 40   | Medicham           |  0.00000% | 11     |  6.962% | 
 | 41   | Dunsparce          |  0.00000% | 1      |  0.633% | 
 | 42   | Vileplume          |  0.00000% | 1      |  0.633% | 
 | 43   | Glalie             |  0.00000% | 7      |  4.430% | 
 | 44   | Camerupt           |  0.00000% | 2      |  1.266% | 
 | 45   | Dusclops           |  0.00000% | 5      |  3.165% | 
 | 46   | Typhlosion         |  0.00000% | 1      |  0.633% | 
 | 47   | Dodrio             |  0.00000% | 1      |  0.633% | 
 | 48   | Shedinja           |  0.00000% | 1      |  0.633% | 
 | 49   | Ampharos           |  0.00000% | 1      |  0.633% | 
 | 50   | Persian            |  0.00000% | 5      |  3.165% | 
 | 51   | Sneasel            |  0.00000% | 2      |  1.266% | 
 | 52   | Floatzel           |  0.00000% | 1      |  0.633% | 
 | 53   | Electrode          |  0.00000% | 1      |  0.633% | 
 | 54   | Articuno           |  0.00000% | 3      |  1.899% | 
 | 55   | Roselia            |  0.00000% | 1      |  0.633% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
