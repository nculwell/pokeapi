 Total leads: 25860
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Azelf              |  8.10131% | 2095   |  8.101% | 
 | 2    | Metagross          |  4.67131% | 1208   |  4.671% | 
 | 3    | Aerodactyl         |  4.43542% | 1147   |  4.435% | 
 | 4    | Heatran            |  4.19180% | 1084   |  4.192% | 
 | 5    | Hippowdon          |  3.83991% | 993    |  3.840% | 
 | 6    | Infernape          |  3.52282% | 911    |  3.523% | 
 | 7    | Machamp            |  3.34107% | 864    |  3.341% | 
 | 8    | Dragonite          |  3.27920% | 848    |  3.279% | 
 | 9    | Jirachi            |  3.22119% | 833    |  3.221% | 
 | 10   | Gengar             |  2.90410% | 751    |  2.904% | 
 | 11   | Tyranitar          |  2.76875% | 716    |  2.769% | 
 | 12   | Roserade           |  2.69142% | 696    |  2.691% | 
 | 13   | Zapdos             |  2.52127% | 652    |  2.521% | 
 | 14   | Skarmory           |  2.51353% | 650    |  2.514% | 
 | 15   | Starmie            |  2.32405% | 601    |  2.324% | 
 | 16   | Swampert           |  2.27765% | 589    |  2.278% | 
 | 17   | Scizor             |  2.08817% | 540    |  2.088% | 
 | 18   | Gliscor            |  2.02243% | 523    |  2.022% | 
 | 19   | Empoleon           |  1.95282% | 505    |  1.953% | 
 | 20   | Crobat             |  1.82135% | 471    |  1.821% | 
 | 21   | Gyarados           |  1.81361% | 469    |  1.814% | 
 | 22   | Forretress         |  1.76721% | 457    |  1.767% | 
 | 23   | Smeargle           |  1.48105% | 383    |  1.481% | 
 | 24   | Ninjask            |  1.42691% | 369    |  1.427% | 
 | 25   | Celebi             |  1.41531% | 366    |  1.415% | 
 | 26   | Bronzong           |  1.36891% | 354    |  1.369% | 
 | 27   | Uxie               |  1.27224% | 329    |  1.272% | 
 | 28   | Flygon             |  1.21810% | 315    |  1.218% | 
 | 29   | Mamoswine          |  1.03248% | 267    |  1.032% | 
 | 30   | Claydol            |  0.90487% | 234    |  0.905% | 
 | 31   | Gallade            |  0.86620% | 224    |  0.866% | 
 | 32   | Lucario            |  0.81593% | 211    |  0.816% | 
 | 33   | Electivire         |  0.76566% | 198    |  0.766% | 
 | 34   | Breloom            |  0.70766% | 183    |  0.708% | 
 | 35   | Weavile            |  0.69992% | 181    |  0.700% | 
 | 36   | Jolteon            |  0.58005% | 150    |  0.580% | 
 | 37   | Ambipom            |  0.55298% | 143    |  0.553% | 
 | 38   | Yanmega            |  0.54911% | 142    |  0.549% | 
 | 39   | Kingdra            |  0.54911% | 142    |  0.549% | 
 | 40   | Blissey            |  0.54524% | 141    |  0.545% | 
 | 41   | Abomasnow          |  0.51817% | 134    |  0.518% | 
 | 42   | Nidoqueen          |  0.46017% | 119    |  0.460% | 
 | 43   | Torterra           |  0.45630% | 118    |  0.456% | 
 | 44   | Togekiss           |  0.45244% | 117    |  0.452% | 
 | 45   | Alakazam           |  0.45244% | 117    |  0.452% | 
 | 46   | Staraptor          |  0.44857% | 116    |  0.449% | 
 | 47   | Spiritomb          |  0.44470% | 115    |  0.445% | 
 | 48   | Dusknoir           |  0.41377% | 107    |  0.414% | 
 | 49   | Donphan            |  0.39830% | 103    |  0.398% | 
 | 50   | Pikachu            |  0.39443% | 102    |  0.394% | 
 | 51   | Registeel          |  0.37896% | 98     |  0.379% | 
 | 52   | Golem              |  0.35576% | 92     |  0.356% | 
 | 53   | Heracross          |  0.33643% | 87     |  0.336% | 
 | 54   | Umbreon            |  0.33643% | 87     |  0.336% | 
 | 55   | Arcanine           |  0.32483% | 84     |  0.325% | 
 | 56   | Feraligatr         |  0.32483% | 84     |  0.325% | 
 | 57   | Cloyster           |  0.32096% | 83     |  0.321% | 
 | 58   | Charizard          |  0.30936% | 80     |  0.309% | 
 | 59   | Typhlosion         |  0.30162% | 78     |  0.302% | 
 | 60   | Pinsir             |  0.29389% | 76     |  0.294% | 
 | 61   | Hitmontop          |  0.23589% | 61     |  0.236% | 
 | 62   | Snorlax            |  0.23202% | 60     |  0.232% | 
 | 63   | Rotom-Wash         |  0.22428% | 58     |  0.224% | 
 | 64   | Blaziken           |  0.21655% | 56     |  0.217% | 
 | 65   | Ursaring           |  0.21268% | 55     |  0.213% | 
 | 66   | Rhyperior          |  0.18948% | 49     |  0.189% | 
 | 67   | Magnezone          |  0.17788% | 46     |  0.178% | 
 | 68   | Kangaskhan         |  0.17015% | 44     |  0.170% | 
 | 69   | Houndoom           |  0.17015% | 44     |  0.170% | 
 | 70   | Zangoose           |  0.16241% | 42     |  0.162% | 
 | 71   | Tentacruel         |  0.16241% | 42     |  0.162% | 
 | 72   | Aggron             |  0.15468% | 40     |  0.155% | 
 | 73   | Scyther            |  0.15468% | 40     |  0.155% | 
 | 74   | Raikou             |  0.15081% | 39     |  0.151% | 
 | 75   | Dugtrio            |  0.15081% | 39     |  0.151% | 
 | 76   | Hariyama           |  0.14308% | 37     |  0.143% | 
 | 77   | Mesprit            |  0.13534% | 35     |  0.135% | 
 | 78   | Mismagius          |  0.11601% | 30     |  0.116% | 
 | 79   | Omastar            |  0.11214% | 29     |  0.112% | 
 | 80   | Primeape           |  0.11214% | 29     |  0.112% | 
 | 81   | Jynx               |  0.10441% | 27     |  0.104% | 
 | 82   | Milotic            |  0.10054% | 26     |  0.101% | 
 | 83   | Blastoise          |  0.08894% | 23     |  0.089% | 
 | 84   | Cresselia          |  0.08507% | 22     |  0.085% | 
 | 85   | Gardevoir          |  0.08507% | 22     |  0.085% | 
 | 86   | Vaporeon           |  0.08507% | 22     |  0.085% | 
 | 87   | Shuckle            |  0.08507% | 22     |  0.085% | 
 | 88   | Drapion            |  0.08121% | 21     |  0.081% | 
 | 89   | Miltank            |  0.08121% | 21     |  0.081% | 
 | 90   | Porygon-Z          |  0.07734% | 20     |  0.077% | 
 | 91   | Electrode          |  0.07734% | 20     |  0.077% | 
 | 92   | Exeggutor          |  0.07347% | 19     |  0.073% | 
 | 93   | Absol              |  0.06961% | 18     |  0.070% | 
 | 94   | Sceptile           |  0.06961% | 18     |  0.070% | 
 | 95   | Linoone            |  0.06961% | 18     |  0.070% | 
 | 96   | Suicune            |  0.06961% | 18     |  0.070% | 
 | 97   | Armaldo            |  0.06961% | 18     |  0.070% | 
 | 98   | Rampardos          |  0.06574% | 17     |  0.066% | 
 | 99   | Tauros             |  0.06574% | 17     |  0.066% | 
 | 100  | Steelix            |  0.06187% | 16     |  0.062% | 
 | 101  | Azumarill          |  0.06187% | 16     |  0.062% | 
 | 102  | Meganium           |  0.06187% | 16     |  0.062% | 
 | 103  | Venusaur           |  0.05800% | 15     |  0.058% | 
 | 104  | Raichu             |  0.05800% | 15     |  0.058% | 
 | 105  | Luxray             |  0.05800% | 15     |  0.058% | 
 | 106  | Shaymin            |  0.05800% | 15     |  0.058% | 
 | 107  | Banette            |  0.05414% | 14     |  0.054% | 
 | 108  | Beedrill           |  0.04640% | 12     |  0.046% | 
 | 109  | Nidoking           |  0.04640% | 12     |  0.046% | 
 | 110  | Lapras             |  0.04640% | 12     |  0.046% | 
 | 111  | Medicham           |  0.04640% | 12     |  0.046% | 
 | 112  | Corsola            |  0.04254% | 11     |  0.043% | 
 | 113  | Hypno              |  0.04254% | 11     |  0.043% | 
 | 114  | Clefable           |  0.04254% | 11     |  0.043% | 
 | 115  | Ludicolo           |  0.04254% | 11     |  0.043% | 
 | 116  | Rotom-Heat         |  0.04254% | 11     |  0.043% | 
 | 117  | Shedinja           |  0.04254% | 11     |  0.043% | 
 | 118  | Swellow            |  0.03867% | 10     |  0.039% | 
 | 119  | Arbok              |  0.03867% | 10     |  0.039% | 
 | 120  | Honchkrow          |  0.03867% | 10     |  0.039% | 
 | 121  | Swinub             |  0.03867% | 10     |  0.039% | 
 | 122  | Moltres            |  0.03867% | 10     |  0.039% | 
 | 123  | Poliwrath          |  0.03480% | 9      |  0.035% | 
 | 124  | Gastrodon          |  0.03480% | 9      |  0.035% | 
 | 125  | Pichu              |  0.03480% | 9      |  0.035% | 
 | 126  | Espeon             |  0.03480% | 9      |  0.035% | 
 | 127  | Glalie             |  0.03480% | 9      |  0.035% | 
 | 128  | Weezing            |  0.03094% | 8      |  0.031% | 
 | 129  | Rotom              |  0.03094% | 8      |  0.031% | 
 | 130  | Slaking            |  0.03094% | 8      |  0.031% | 
 | 131  | Solrock            |  0.03094% | 8      |  0.031% | 
 | 132  | Tangrowth          |  0.03094% | 8      |  0.031% | 
 | 133  | Rhydon             |  0.02707% | 7      |  0.027% | 
 | 134  | Marowak            |  0.02707% | 7      |  0.027% | 
 | 135  | Lanturn            |  0.02707% | 7      |  0.027% | 
 | 136  | Toxicroak          |  0.02707% | 7      |  0.027% | 
 | 137  | Rotom-Mow          |  0.02707% | 7      |  0.027% | 
 | 138  | Victreebel         |  0.02707% | 7      |  0.027% | 
 | 139  | Buizel             |  0.02707% | 7      |  0.027% | 
 | 140  | Onix               |  0.02707% | 7      |  0.027% | 
 | 141  | Magmortar          |  0.02707% | 7      |  0.027% | 
 | 142  | Purugly            |  0.02320% | 6      |  0.023% | 
 | 143  | Regirock           |  0.02320% | 6      |  0.023% | 
 | 144  | Butterfree         |  0.02320% | 6      |  0.023% | 
 | 145  | Graveler           |  0.02320% | 6      |  0.023% | 
 | 146  | Croagunk           |  0.02320% | 6      |  0.023% | 
 | 147  | Magcargo           |  0.02320% | 6      |  0.023% | 
 | 148  | Lopunny            |  0.02320% | 6      |  0.023% | 
 | 149  | Sharpedo           |  0.01933% | 5      |  0.019% | 
 | 150  | Slowbro            |  0.01933% | 5      |  0.019% | 
 | 151  | Kabutops           |  0.01933% | 5      |  0.019% | 
 | 152  | Articuno           |  0.01933% | 5      |  0.019% | 
 | 153  | Cradily            |  0.01933% | 5      |  0.019% | 
 | 154  | Sableye            |  0.01933% | 5      |  0.019% | 
 | 155  | Dodrio             |  0.01933% | 5      |  0.019% | 
 | 156  | Sudowoodo          |  0.01933% | 5      |  0.019% | 
 | 157  | Magikarp           |  0.01547% | 4      |  0.015% | 
 | 158  | Granbull           |  0.01547% | 4      |  0.015% | 
 | 159  | Squirtle           |  0.01547% | 4      |  0.015% | 
 | 160  | Wailord            |  0.01547% | 4      |  0.015% | 
 | 161  | Drifblim           |  0.01547% | 4      |  0.015% | 
 | 162  | Jumpluff           |  0.01547% | 4      |  0.015% | 
 | 163  | Bastiodon          |  0.01547% | 4      |  0.015% | 
 | 164  | Bibarel            |  0.01547% | 4      |  0.015% | 
 | 165  | Wormadam-Trash     |  0.01547% | 4      |  0.015% | 
 | 166  | Floatzel           |  0.01547% | 4      |  0.015% | 
 | 167  | Gorebyss           |  0.01160% | 3      |  0.012% | 
 | 168  | Ampharos           |  0.01160% | 3      |  0.012% | 
 | 169  | Girafarig          |  0.01160% | 3      |  0.012% | 
 | 170  | Politoed           |  0.01160% | 3      |  0.012% | 
 | 171  | Dunsparce          |  0.01160% | 3      |  0.012% | 
 | 172  | Venomoth           |  0.01160% | 3      |  0.012% | 
 | 173  | Rotom-Fan          |  0.01160% | 3      |  0.012% | 
 | 174  | Qwilfish           |  0.01160% | 3      |  0.012% | 
 | 175  | Entei              |  0.01160% | 3      |  0.012% | 
 | 176  | Magneton           |  0.01160% | 3      |  0.012% | 
 | 177  | Mightyena          |  0.00773% | 2      |  0.008% | 
 | 178  | Mudkip             |  0.00773% | 2      |  0.008% | 
 | 179  | Riolu              |  0.00773% | 2      |  0.008% | 
 | 180  | Leafeon            |  0.00773% | 2      |  0.008% | 
 | 181  | Eevee              |  0.00773% | 2      |  0.008% | 
 | 182  | Skuntank           |  0.00773% | 2      |  0.008% | 
 | 183  | Rattata            |  0.00773% | 2      |  0.008% | 
 | 184  | Phione             |  0.00773% | 2      |  0.008% | 
 | 185  | Slowking           |  0.00773% | 2      |  0.008% | 
 | 186  | Machoke            |  0.00773% | 2      |  0.008% | 
 | 187  | Torkoal            |  0.00773% | 2      |  0.008% | 
 | 188  | Pachirisu          |  0.00773% | 2      |  0.008% | 
 | 189  | Lunatone           |  0.00773% | 2      |  0.008% | 
 | 190  | Rapidash           |  0.00773% | 2      |  0.008% | 
 | 191  | Persian            |  0.00773% | 2      |  0.008% | 
 | 192  | Vespiquen          |  0.00773% | 2      |  0.008% | 
 | 193  | Chatot             |  0.00773% | 2      |  0.008% | 
 | 194  | Ninetales          |  0.00773% | 2      |  0.008% | 
 | 195  | Quilava            |  0.00773% | 2      |  0.008% | 
 | 196  | Mr. Mime           |  0.00773% | 2      |  0.008% | 
 | 197  | Prinplup           |  0.00773% | 2      |  0.008% | 
 | 198  | Gligar             |  0.00387% | 1      |  0.004% | 
 | 199  | Budew              |  0.00387% | 1      |  0.004% | 
 | 200  | Luxio              |  0.00387% | 1      |  0.004% | 
 | 201  | Hippopotas         |  0.00387% | 1      |  0.004% | 
 | 202  | Ledian             |  0.00387% | 1      |  0.004% | 
 | 203  | Muk                |  0.00387% | 1      |  0.004% | 
 | 204  | Relicanth          |  0.00387% | 1      |  0.004% | 
 | 205  | Wigglytuff         |  0.00387% | 1      |  0.004% | 
 | 206  | Porygon2           |  0.00387% | 1      |  0.004% | 
 | 207  | Crawdaunt          |  0.00387% | 1      |  0.004% | 
 | 208  | Exploud            |  0.00387% | 1      |  0.004% | 
 | 209  | Walrein            |  0.00387% | 1      |  0.004% | 
 | 210  | Beautifly          |  0.00387% | 1      |  0.004% | 
 | 211  | Golduck            |  0.00387% | 1      |  0.004% | 
 | 212  | Monferno           |  0.00387% | 1      |  0.004% | 
 | 213  | Gastly             |  0.00387% | 1      |  0.004% | 
 | 214  | Magnemite          |  0.00387% | 1      |  0.004% | 
 | 215  | Ivysaur            |  0.00387% | 1      |  0.004% | 
 | 216  | Pidgeot            |  0.00387% | 1      |  0.004% | 
 | 217  | Regigigas          |  0.00387% | 1      |  0.004% | 
 | 218  | Kingler            |  0.00387% | 1      |  0.004% | 
 | 219  | Bronzor            |  0.00387% | 1      |  0.004% | 
 | 220  | Charmander         |  0.00387% | 1      |  0.004% | 
 | 221  | Togepi             |  0.00387% | 1      |  0.004% | 
 | 222  | Stantler           |  0.00387% | 1      |  0.004% | 
 | 223  | Xatu               |  0.00387% | 1      |  0.004% | 
 | 224  | Noctowl            |  0.00387% | 1      |  0.004% | 
 | 225  | Tropius            |  0.00387% | 1      |  0.004% | 
 | 226  | Kakuna             |  0.00387% | 1      |  0.004% | 
 | 227  | Horsea             |  0.00387% | 1      |  0.004% | 
 | 228  | Hitmonchan         |  0.00387% | 1      |  0.004% | 
 | 229  | Snover             |  0.00387% | 1      |  0.004% | 
 | 230  | Shinx              |  0.00387% | 1      |  0.004% | 
 | 231  | Dusclops           |  0.00387% | 1      |  0.004% | 
 | 232  | Sneasel            |  0.00387% | 1      |  0.004% | 
 | 233  | Quagsire           |  0.00387% | 1      |  0.004% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
