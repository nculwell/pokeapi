 weatherless...................92.00000%
 offense.......................55.00000%
 balance.......................31.00000%
 hyperoffense..................10.00000%
 sun........................... 4.00000%
 rain.......................... 4.00000%
 semistall..................... 3.00000%
 choice........................ 1.00000%
 stall......................... 1.00000%
 monofighting.................. 1.00000%
 monotype...................... 1.00000%

 Stalliness (mean: -0.215)
     |#####
 -1.0|##############
     |##############
 -0.5|##############################
     |##########################
  0.0|####################
     |#######################
 +0.5|###############
     |
 +1.0|##
     |###
 more negative = more offensive, more positive = more stall
 one # =  0.67%
