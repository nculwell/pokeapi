 weatherless...................66.96442%
 offense.......................47.91957%
 sand..........................27.62568%
 balance.......................23.31013%
 hyperoffense..................21.19490%
 sandoffense................... 5.50657%
 semistall..................... 4.92653%
 rain.......................... 3.02398%
 stall......................... 2.64888%
 hail.......................... 1.99923%
 voltturn...................... 1.88322%
 choice........................ 1.58159%
 sandstall..................... 1.46172%
 trickroom..................... 1.18329%
 batonpass..................... 1.18329%
 rainoffense................... 1.14076%
 monotype...................... 1.02862%
 dragmag....................... 0.95128%
 sun........................... 0.66125%
 tricksand..................... 0.44857%
 multiweather.................. 0.27456%
 monosteel..................... 0.23589%
 monowater..................... 0.16628%
 monofire...................... 0.15855%
 monodark...................... 0.11988%
 hailstall..................... 0.08894%
 monofighting.................. 0.06961%
 hailoffense................... 0.06187%
 monoelectric.................. 0.06187%
 monopoison.................... 0.05800%
 monoflying.................... 0.04254%
 mononormal.................... 0.03480%
 gravity....................... 0.03094%
 monograss..................... 0.03094%
 sunoffense.................... 0.02707%
 monobug....................... 0.02707%
 trapper....................... 0.02707%
 monopsychic................... 0.02320%
 monodragon.................... 0.01933%
 tailwind...................... 0.01933%
 monorock...................... 0.01933%
 monoghost..................... 0.01547%
 monoground.................... 0.01160%
 monofairy..................... 0.00773%

 Stalliness (mean: -0.315)
 -2.0|##
     |######
 -1.5|##########
     |###################
 -1.0|########################
     |#########################
 -0.5|############################
     |##############################
  0.0|#####################
     |##################
 +0.5|#############
     |#########
 +1.0|#########
     |###
 +1.5|####
     |##
 +2.0|#
     |#
 +2.5|#
 more negative = more offensive, more positive = more stall
 one # =  0.44%
