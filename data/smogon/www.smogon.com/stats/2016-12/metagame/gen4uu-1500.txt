 weatherless...................100.00000%
 offense.......................43.13844%
 balance.......................34.09048%
 hyperoffense..................16.86315%
 semistall..................... 5.90793%

 Stalliness (mean: -0.210)
 -1.5|############
     |#####
 -1.0|########
     |##########
 -0.5|##############################
     |####################
  0.0|########
     |##########################
 +0.5|####
     |####
 +1.0|##############
 more negative = more offensive, more positive = more stall
 one # =  0.67%
