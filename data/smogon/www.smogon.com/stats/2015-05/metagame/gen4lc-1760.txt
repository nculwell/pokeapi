 weatherless...................13.40489%
 hyperoffense..................10.77718%
 offense....................... 2.62771%

 Stalliness (mean: -1.224)
     |
     |
     |
     |
     |
     |
     |
     |
     |
     |##############################
 -1.2|
     |
 -1.0|
     |#######
 -0.8|
     |
 -0.6|
     |
 -0.4|
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  2.68%
