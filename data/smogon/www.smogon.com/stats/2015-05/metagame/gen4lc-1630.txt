 weatherless...................100.00000%
 hyperoffense..................77.12184%
 offense.......................22.87816%

 Stalliness (mean: -1.210)
     |
     |
     |
     |
     |
     |
     |
     |
     |
     |##############################
 -1.2|
     |
 -1.0|
     |#########
 -0.8|
     |
 -0.6|
     |
 -0.4|
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  2.57%
