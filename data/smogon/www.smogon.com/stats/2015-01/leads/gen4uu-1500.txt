 Total leads: 6
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Scyther            | 41.87485% | 2      | 33.333% | 
 | 2    | Shiftry            | 20.93743% | 1      | 16.667% | 
 | 3    | Electrode          | 12.39591% | 1      | 16.667% | 
 | 4    | Torterra           | 12.39591% | 1      | 16.667% | 
 | 5    | Absol              | 12.39591% | 1      | 16.667% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
