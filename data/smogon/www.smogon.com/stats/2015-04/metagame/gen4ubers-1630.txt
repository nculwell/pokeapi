 weatherless...................100.00000%
 offense.......................54.01627%
 stall.........................44.77401%
 semistall..................... 1.20972%
 sun........................... 0.00000%
 sunoffense.................... 0.00000%
 rain.......................... 0.00000%
 hyperoffense.................. 0.00000%
 sand.......................... 0.00000%
 mononormal.................... 0.00000%
 balance....................... 0.00000%
 monotype...................... 0.00000%

 Stalliness (mean:  1.022)
 -1.0|
     |
 -0.5|#
     |##############################
  0.0|
     |
 +0.5|
     |
 +1.0|
     |
 +1.5|#
     |
 +2.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  1.76%
