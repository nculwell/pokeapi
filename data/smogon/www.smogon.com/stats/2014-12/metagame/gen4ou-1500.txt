 weatherless...................60.44413%
 offense.......................43.40687%
 sand..........................36.30153%
 hyperoffense..................26.52980%
 balance.......................20.16338%
 sandoffense................... 7.97686%
 semistall..................... 6.70288%
 stall......................... 3.19707%
 sandstall..................... 2.35527%
 choice........................ 2.31412%
 rain.......................... 1.96461%
 voltturn...................... 1.77299%
 hail.......................... 1.35713%
 rainoffense................... 1.10554%
 dragmag....................... 0.87058%
 hailstall..................... 0.50247%
 monotype...................... 0.32991%
 multiweather.................. 0.28550%
 batonpass..................... 0.28236%
 sun........................... 0.21809%
 monowater..................... 0.08887%
 monoflying.................... 0.06619%
 trickroom..................... 0.05469%
 tricksand..................... 0.05469%
 monobug....................... 0.05096%
 monodark...................... 0.03908%
 monopsychic................... 0.03056%
 monosteel..................... 0.03056%
 monopoison.................... 0.02751%
 monoghost..................... 0.02751%
 monofairy..................... 0.01824%
 monofighting.................. 0.01809%
 monofire...................... 0.01809%
 monodragon.................... 0.01809%
 monorock...................... 0.00864%
 monoground.................... 0.00864%

 Stalliness (mean: -0.346)
     |#########
 -1.5|###########
     |#############
 -1.0|##############################
     |################
 -0.5|############################
     |##################
  0.0|############
     |#############
 +0.5|###########
     |#######
 +1.0|#######
     |######
 +1.5|###
     |#
 +2.0|#
     |##
 +2.5|#
 more negative = more offensive, more positive = more stall
 one # =  0.52%
