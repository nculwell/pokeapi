 weatherless...................48.27062%
 sand..........................46.87048%
 offense.......................34.05765%
 hyperoffense..................32.82827%
 balance.......................19.20243%
 sandoffense...................15.69177%
 stall......................... 7.02733%
 semistall..................... 6.88432%
 sandstall..................... 3.26613%
 hail.......................... 3.22155%
 dragmag....................... 2.86784%
 voltturn...................... 2.76502%
 rain.......................... 1.68649%
 hailstall..................... 1.42540%
 rainoffense................... 1.24230%
 choice........................ 0.94520%
 trickroom..................... 0.44649%
 tricksand..................... 0.30232%
 batonpass..................... 0.30045%
 multiweather.................. 0.27403%
 sun........................... 0.22489%
 sunoffense.................... 0.03984%
 hailoffense................... 0.03786%
 monotype...................... 0.03536%
 monofighting.................. 0.01848%
 monoelectric.................. 0.00988%
 monodragon.................... 0.00342%
 monopoison.................... 0.00253%
 tricksun...................... 0.00239%
 trapper....................... 0.00054%
 monoice....................... 0.00041%
 monodark...................... 0.00034%
 monosteel..................... 0.00016%
 monorock...................... 0.00008%
 monowater..................... 0.00003%
 mononormal.................... 0.00003%
 monoflying.................... 0.00000%
 sunstall...................... 0.00000%
 monobug....................... 0.00000%
 monoground.................... 0.00000%
 monofire...................... 0.00000%

 Stalliness (mean: -0.317)
     |############
 -1.5|#####################
     |########################
 -1.0|##############################
     |########################
 -0.5|##############
     |###################
  0.0|################
     |#################
 +0.5|#########
     |#######
 +1.0|########
     |####
 +1.5|##########
     |#######
 +2.0|####
     |##
 +2.5|#
 more negative = more offensive, more positive = more stall
 one # =  0.43%
