 hyperoffense.................. 4.91122%
 rainoffense................... 4.91122%
 rain.......................... 4.91122%
 offense....................... 0.00000%
 weatherless................... 0.00000%

 Stalliness (mean: -1.491)
     |
     |
     |
     |##############################
     |
     |
 -1.2|
     |
 -1.0|
     |
 -0.8|
     |
 -0.6|
     |
 -0.4|
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  3.33%
