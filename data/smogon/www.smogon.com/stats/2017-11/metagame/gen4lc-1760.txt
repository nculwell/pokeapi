 hyperoffense.................. 0.01019%
 rainoffense................... 0.01019%
 rain.......................... 0.01019%
 offense....................... 0.00000%
 weatherless................... 0.00000%

 Stalliness (mean: -1.491)
     |
     |
     |
     |##############################
     |
     |
 -1.2|
     |
 -1.0|
     |
 -0.8|
     |
 -0.6|
     |
 -0.4|
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  3.33%
