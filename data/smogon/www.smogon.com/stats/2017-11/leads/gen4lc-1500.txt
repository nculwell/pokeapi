 Total leads: 14
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Chimchar           | 28.26298% | 3      | 21.429% | 
 | 2    | Wailmer            | 14.99865% | 2      | 14.286% | 
 | 3    | Aipom              | 14.35616% | 2      | 14.286% | 
 | 4    | Riolu              | 12.38492% | 3      | 21.429% | 
 | 5    | Houndour           |  9.42099% | 1      |  7.143% | 
 | 6    | Bulbasaur          |  9.42099% | 1      |  7.143% | 
 | 7    | Caterpie           |  5.57766% | 1      |  7.143% | 
 | 8    | Turtwig            |  5.57766% | 1      |  7.143% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
