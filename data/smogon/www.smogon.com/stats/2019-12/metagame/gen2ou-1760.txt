 weatherless...................98.10010%
 balance.......................55.45014%
 offense.......................23.20859%
 semistall.....................12.36357%
 stall......................... 8.97770%
 sun........................... 1.64757%
 batonpass..................... 0.35387%
 sandstall..................... 0.25233%
 sand.......................... 0.25233%
 monotype...................... 0.00357%
 mononormal.................... 0.00356%
 dragmag....................... 0.00222%
 monowater..................... 0.00000%
 rain.......................... 0.00000%
 trapper....................... 0.00000%
 monograss..................... 0.00000%
 monopoison.................... 0.00000%
 monobug....................... 0.00000%
 monopsychic................... 0.00000%
 monorock...................... 0.00000%
 monoghost..................... 0.00000%
 hyperoffense.................. 0.00000%
 monoground.................... 0.00000%
 monoice....................... 0.00000%
 monodark...................... 0.00000%
 monoflying.................... 0.00000%
 monoelectric.................. 0.00000%
 monofire...................... 0.00000%
 monodragon.................... 0.00000%

 Stalliness (mean:  0.572)
     |#
 -0.5|####
     |#################
  0.0|##############################
     |############################
 +0.5|##############
     |############
 +1.0|##############################
     |#########
 +1.5|########
     |#####
 +2.0|#########
     |#
 +2.5|
 more negative = more offensive, more positive = more stall
 one # =  0.59%
