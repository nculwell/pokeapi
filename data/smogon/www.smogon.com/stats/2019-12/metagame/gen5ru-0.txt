 weatherless...................100.00000%
 offense.......................75.00000%
 stall.........................25.00000%

 Stalliness (mean:  0.402)
     |##############################
  0.0|
     |
 +0.5|
     |
 +1.0|
     |
 +1.5|
     |
 +2.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  2.50%
