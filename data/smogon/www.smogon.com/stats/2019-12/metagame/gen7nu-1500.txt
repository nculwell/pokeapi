 weatherless...................86.93749%
 offense.......................49.34848%
 balance.......................31.37278%
 hail..........................11.46202%
 hyperoffense..................10.92406%
 semistall..................... 5.84494%
 voltturn...................... 5.50019%
 stall......................... 2.50974%
 hailoffense................... 1.98358%
 sun........................... 1.09775%
 monotype...................... 0.81966%
 rain.......................... 0.78342%
 choice........................ 0.75270%
 trickroom..................... 0.50060%
 multiweather.................. 0.23604%
 monoelectric.................. 0.21400%
 trickhail..................... 0.17618%
 sunoffense.................... 0.17015%
 monofighting.................. 0.12522%
 monowater..................... 0.12186%
 sand.......................... 0.08009%
 monoice....................... 0.07546%
 mononormal.................... 0.05821%
 monorock...................... 0.05659%
 monograss..................... 0.05590%
 trapper....................... 0.04729%
 rainoffense................... 0.04642%
 monobug....................... 0.04112%
 tailwind...................... 0.03886%
 monofire...................... 0.03601%
 monopoison.................... 0.02495%
 monofairy..................... 0.02407%
 monoflying.................... 0.01636%
 monopsychic................... 0.01126%
 monoghost..................... 0.00873%
 monoground.................... 0.00822%
 monodark...................... 0.00790%

 Stalliness (mean: -0.093)
     |#
 -1.5|####
     |###########
 -1.0|############
     |################
 -0.5|##########################
     |##############################
  0.0|##############################
     |#####################
 +0.5|############
     |##########
 +1.0|#######
     |######
 +1.5|###
     |#
 +2.0|###
     |
 +2.5|
     |
 +3.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  0.51%
