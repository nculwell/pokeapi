 weatherless...................83.15135%
 offense.......................52.59519%
 balance.......................24.57123%
 hyperoffense..................17.76859%
 sand.......................... 8.66843%
 sun........................... 8.05011%
 stall......................... 3.57193%
 sandoffense................... 3.13447%
 monotype...................... 2.58580%
 semistall..................... 1.49305%
 sunoffense.................... 1.38032%
 monoflying.................... 0.89754%
 monofairy..................... 0.66130%
 mononormal.................... 0.61726%
 monowater..................... 0.45641%
 monodark...................... 0.35669%
 monofighting.................. 0.30996%
 monopoison.................... 0.25749%
 hail.......................... 0.19365%
 monoghost..................... 0.18563%
 monograss..................... 0.16953%
 monoground.................... 0.08788%
 multiweather.................. 0.06429%
 dragmag....................... 0.05743%
 hailoffense................... 0.03397%
 monosteel..................... 0.02520%
 monorock...................... 0.02408%
 monopsychic................... 0.02074%
 monobug....................... 0.02066%
 monodragon.................... 0.00502%
 monofire...................... 0.00407%
 monoelectric.................. 0.00167%
 rain.......................... 0.00074%
 hailstall..................... 0.00074%
 monoice....................... 0.00047%
 rainoffense................... 0.00000%
 sunstall...................... 0.00000%
 batonpass..................... 0.00000%
 sandstall..................... 0.00000%
 trapper....................... 0.00000%

 Stalliness (mean: -0.312)
 -3.0|
     |
 -2.0|#
     |###########
 -1.0|######################
     |##############################
  0.0|#########
     |#####################
 +1.0|#
     |#
 +2.0|##
     |#
 +3.0|
     |
 +4.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  1.00%
