 weatherless...................61.75581%
 offense.......................59.97065%
 balance.......................32.06198%
 sand..........................21.02283%
 sun...........................10.16175%
 hyperoffense.................. 6.59782%
 trickroom..................... 5.90831%
 hail.......................... 4.17136%
 tricksun...................... 3.88372%
 rain.......................... 3.59905%
 tailwind...................... 3.07821%
 semistall..................... 1.35134%
 hailoffense................... 0.98488%
 multiweather.................. 0.70969%
 tricksand..................... 0.35851%
 sandoffense................... 0.31438%
 voltturn...................... 0.27443%
 rainoffense................... 0.22265%
 sunoffense.................... 0.13836%
 monotype...................... 0.10668%
 trickhail..................... 0.05876%
 mononormal.................... 0.03385%
 dragmag....................... 0.02373%
 monowater..................... 0.02110%
 stall......................... 0.01821%
 monosteel..................... 0.01779%
 gravity....................... 0.01186%
 trickrain..................... 0.00763%
 monoelectric.................. 0.00673%
 batonpass..................... 0.00618%
 monobug....................... 0.00616%
 monoghost..................... 0.00612%
 monoice....................... 0.00551%
 sunstall...................... 0.00430%
 monodragon.................... 0.00372%
 sandstall..................... 0.00183%
 monofairy..................... 0.00152%
 trapper....................... 0.00139%
 monofire...................... 0.00109%
 monopsychic................... 0.00091%
 monofighting.................. 0.00084%
 monoflying.................... 0.00062%
 monodark...................... 0.00039%
 allweather.................... 0.00021%
 monorock...................... 0.00015%
 monoground.................... 0.00013%
 monopoison.................... 0.00006%
 rainstall..................... 0.00001%
 hailstall..................... 0.00000%
 monograss..................... 0.00000%
 swagplay...................... 0.00000%

 Stalliness (mean: -0.211)
 -1.5|###
     |####
 -1.0|##########
     |#################
 -0.5|########################
     |##############################
  0.0|#############################
     |####################
 +0.5|##########
     |######
 +1.0|###
     |
 more negative = more offensive, more positive = more stall
 one # =  0.64%
