 weatherless...................100.00000%
 offense.......................50.00000%
 balance.......................50.00000%

 Stalliness (mean:  0.041)
     |##############################
  0.0|
     |
 +0.1|
     |
 more negative = more offensive, more positive = more stall
 one # =  1.67%
