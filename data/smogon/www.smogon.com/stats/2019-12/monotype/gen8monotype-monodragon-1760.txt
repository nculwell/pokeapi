 Total battles: 213067
 Avg. weight/team: 0.001
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Dragapult          | 98.98015% | 35898  | 94.683% | 26156  | 96.010% | 
 | 2    | Duraludon          | 93.07496% | 30248  | 79.781% | 24943  | 91.557% | 
 | 3    | Hydreigon          | 91.05137% | 28293  | 74.624% | 19806  | 72.701% | 
 | 4    | Dracovish          | 80.54568% | 27600  | 72.796% | 19889  | 73.006% | 
 | 5    | Kommo-o            | 74.68009% | 19742  | 52.070% | 13644  | 50.083% | 
 | 6    | Haxorus            | 42.78569% | 14087  | 37.155% | 9490   | 34.835% | 
 | 7    | Goodra             | 38.87267% | 14036  | 37.021% | 9952   | 36.530% | 
 | 8    | Dracozolt          | 31.91927% | 14231  | 37.535% | 9794   | 35.951% | 
 | 9    | Noivern            | 18.48669% | 11309  | 29.828% | 8134   | 29.857% | 
 | 10   | Turtonator         | 10.44372% | 7065   | 18.634% | 5309   | 19.488% | 
 | 11   | Flygon             |  5.98583% | 6733   | 17.759% | 4788   | 17.575% | 
 | 12   | Flapple            |  5.47331% | 5560   | 14.665% | 3657   | 13.424% | 
 | 13   | Appletun           |  4.43893% | 8716   | 22.989% | 5846   | 21.459% | 
 | 14   | Drampa             |  2.08839% | 1253   |  3.305% | 876    |  3.216% | 
 | 15   | Silvally-Dragon    |  0.92321% | 1370   |  3.613% | 941    |  3.454% | 
 | 16   | Noibat             |  0.16628% | 18     |  0.047% | 14     |  0.051% | 
 | 17   | Drakloak           |  0.02574% | 60     |  0.158% | 53     |  0.195% | 
 | 18   | Zweilous           |  0.01277% | 80     |  0.211% | 59     |  0.217% | 
 | 19   | Hakamo-o           |  0.01084% | 36     |  0.095% | 26     |  0.095% | 
 | 20   | Fraxure            |  0.00945% | 18     |  0.047% | 11     |  0.040% | 
 | 21   | Sliggoo            |  0.00301% | 90     |  0.237% | 56     |  0.206% | 
 | 22   | Vibrava            |  0.00023% | 3      |  0.008% | 2      |  0.007% | 
 | 23   | Applin             |  0.00002% | 8      |  0.021% | 6      |  0.022% | 
 | 24   | Goomy              |  0.00000% | 3      |  0.008% | 2      |  0.007% | 
 | 25   | Axew               |  0.00000% | 7      |  0.018% | 4      |  0.015% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
