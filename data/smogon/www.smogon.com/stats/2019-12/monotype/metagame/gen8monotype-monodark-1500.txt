 monotype......................100.00000%
 monodark......................100.00000%
 sand..........................68.74040%
 offense.......................59.36658%
 weatherless...................31.04732%
 balance.......................28.58704%
 hyperoffense..................11.44870%
 sandoffense................... 5.31835%
 semistall..................... 0.55865%
 choice........................ 0.24100%
 sun........................... 0.20069%
 sunoffense.................... 0.08469%
 stall......................... 0.03902%
 dragmag....................... 0.03422%
 voltturn...................... 0.02447%
 sandstall..................... 0.02293%
 hail.......................... 0.01159%
 monoelectric.................. 0.00953%
 monofairy..................... 0.00439%
 monoghost..................... 0.00399%
 mononormal.................... 0.00395%
 monorock...................... 0.00255%
 monofighting.................. 0.00209%
 monosteel..................... 0.00106%
 monoice....................... 0.00006%
 monopoison.................... 0.00000%

 Stalliness (mean: -0.283)
 -1.5|####
     |#######
 -1.0|#########
     |##############
 -0.5|#####################
     |##############################
  0.0|#####################
     |################
 +0.5|########
     |#####
 +1.0|##
     |
 more negative = more offensive, more positive = more stall
 one # =  0.72%
