 monograss.....................100.00000%
 monotype......................100.00000%
 weatherless...................90.75489%
 balance.......................64.46110%
 offense.......................29.80045%
 sun........................... 7.92921%
 voltturn...................... 4.92185%
 semistall..................... 3.25563%
 hyperoffense.................. 1.84471%
 hail.......................... 1.58487%
 sunoffense.................... 0.94462%
 stall......................... 0.63810%
 multiweather.................. 0.32240%
 rain.......................... 0.10967%
 trickroom..................... 0.09302%
 hailoffense................... 0.03638%
 trickhail..................... 0.03028%
 choice........................ 0.00992%
 rainstall..................... 0.00252%
 sunstall...................... 0.00179%
 monodragon.................... 0.00154%
 hailstall..................... 0.00000%
 monopoison.................... 0.00000%
 monosteel..................... 0.00000%
 monofairy..................... 0.00000%

 Stalliness (mean:  0.206)
 -1.5|
     |#
 -1.0|##
     |######
 -0.5|####
     |##########################
  0.0|#################
     |##############################
 +0.5|#########################
     |#########################
 +1.0|###
     |###
 +1.5|
     |
 +2.0|
     |
 +2.5|
 more negative = more offensive, more positive = more stall
 one # =  0.69%
