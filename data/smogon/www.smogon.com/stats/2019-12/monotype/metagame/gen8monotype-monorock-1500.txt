 monotype......................100.00000%
 monorock......................100.00000%
 sand..........................94.70209%
 offense.......................53.46235%
 balance.......................37.26734%
 hyperoffense.................. 7.88909%
 sandoffense................... 7.56638%
 weatherless................... 5.29791%
 semistall..................... 1.19281%
 multiweather.................. 0.64584%
 sun........................... 0.64584%
 trickroom..................... 0.40110%
 tricksand..................... 0.40110%
 monobug....................... 0.23082%
 stall......................... 0.18841%
 monopsychic................... 0.08942%
 monofire...................... 0.06568%
 monowater..................... 0.05961%
 choice........................ 0.05498%
 sandstall..................... 0.02718%
 monodark...................... 0.01821%
 monoground.................... 0.01570%

 Stalliness (mean: -0.174)
 -1.5|###
     |#######
 -1.0|#########
     |############
 -0.5|#########################
     |#############################
  0.0|##############################
     |##############################
 +0.5|#############
     |#####
 +1.0|###
     |#
 +1.5|
     |
 +2.0|
     |
 +2.5|
     |
 +3.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  0.60%
