 Total leads: 426134
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Galvantula         | 20.79189% | 4243   | 20.792% | 
 | 2    | Shuckle            | 16.31793% | 3330   | 16.318% | 
 | 3    | Crustle            | 10.57480% | 2158   | 10.575% | 
 | 4    | Orbeetle           |  8.33538% | 1701   |  8.335% | 
 | 5    | Araquanid          |  6.84569% | 1397   |  6.846% | 
 | 6    | Ribombee           |  6.82609% | 1393   |  6.826% | 
 | 7    | Centiskorch        |  4.79247% | 978    |  4.792% | 
 | 8    | Frosmoth           |  3.50370% | 715    |  3.504% | 
 | 9    | Durant             |  3.12638% | 638    |  3.126% | 
 | 10   | Accelgor           |  3.11168% | 635    |  3.112% | 
 | 11   | Vikavolt           |  2.94997% | 602    |  2.950% | 
 | 12   | Golisopod          |  2.79806% | 571    |  2.798% | 
 | 13   | Butterfree         |  2.51384% | 513    |  2.514% | 
 | 14   | Escavalier         |  2.02382% | 413    |  2.024% | 
 | 15   | Ninjask            |  1.85721% | 379    |  1.857% | 
 | 16   | Shedinja           |  1.32798% | 271    |  1.328% | 
 | 17   | Dottler            |  0.58313% | 119    |  0.583% | 
 | 18   | Silvally-Bug       |  0.58313% | 119    |  0.583% | 
 | 19   | Vespiquen          |  0.57823% | 118    |  0.578% | 
 | 20   | Skorupi            |  0.17151% | 35     |  0.172% | 
 | 21   | Charjabug          |  0.11271% | 23     |  0.113% | 
 | 22   | Shelmet            |  0.05880% | 12     |  0.059% | 
 | 23   | Snom               |  0.05880% | 12     |  0.059% | 
 | 24   | Wimpod             |  0.05390% | 11     |  0.054% | 
 | 25   | Dwebble            |  0.03920% | 8      |  0.039% | 
 | 26   | Blipbug            |  0.02940% | 6      |  0.029% | 
 | 27   | Metapod            |  0.00980% | 2      |  0.010% | 
 | 28   | Cutiefly           |  0.00980% | 2      |  0.010% | 
 | 29   | Sizzlipede         |  0.00490% | 1      |  0.005% | 
 | 30   | Caterpie           |  0.00490% | 1      |  0.005% | 
 | 31   | Grubbin            |  0.00490% | 1      |  0.005% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
