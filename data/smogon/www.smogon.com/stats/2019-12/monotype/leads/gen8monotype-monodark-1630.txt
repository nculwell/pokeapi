 Total leads: 426134
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Tyranitar          | 18.86359% | 4883   | 15.736% | 
 | 2    | Grimmsnarl         | 18.56577% | 5712   | 18.407% | 
 | 3    | Hydreigon          | 15.42117% | 3072   |  9.900% | 
 | 4    | Bisharp            | 11.26482% | 2507   |  8.079% | 
 | 5    | Drapion            | 10.94442% | 2172   |  6.999% | 
 | 6    | Sableye            |  5.71238% | 2416   |  7.786% | 
 | 7    | Crawdaunt          |  4.19107% | 990    |  3.190% | 
 | 8    | Obstagoon          |  3.80742% | 1950   |  6.284% | 
 | 9    | Mandibuzz          |  3.02724% | 1208   |  3.893% | 
 | 10   | Morpeko            |  2.61043% | 1363   |  4.392% | 
 | 11   | Weavile            |  2.37070% | 1352   |  4.357% | 
 | 12   | Malamar            |  0.90616% | 830    |  2.675% | 
 | 13   | Scrafty            |  0.54422% | 574    |  1.850% | 
 | 14   | Skuntank           |  0.35638% | 203    |  0.654% | 
 | 15   | Shiftry            |  0.34386% | 172    |  0.554% | 
 | 16   | Pangoro            |  0.26813% | 223    |  0.719% | 
 | 17   | Silvally-Dark      |  0.21942% | 158    |  0.509% | 
 | 18   | Umbreon            |  0.20269% | 261    |  0.841% | 
 | 19   | Thievul            |  0.20055% | 479    |  1.544% | 
 | 20   | Pawniard           |  0.10053% | 44     |  0.142% | 
 | 21   | Liepard            |  0.06421% | 183    |  0.590% | 
 | 22   | Linoone-Galar      |  0.00561% | 59     |  0.190% | 
 | 23   | Morgrem            |  0.00492% | 172    |  0.554% | 
 | 24   | Vullaby            |  0.00427% | 20     |  0.064% | 
 | 25   | Deino              |  0.00007% | 3      |  0.010% | 
 | 26   | Sneasel            |  0.00000% | 10     |  0.032% | 
 | 27   | Nuzleaf            |  0.00000% | 4      |  0.013% | 
 | 28   | Zweilous           |  0.00000% | 8      |  0.026% | 
 | 29   | Scraggy            |  0.00000% | 3      |  0.010% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
