 Total leads: 426134
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Obstagoon          | 15.91097% | 2887   | 15.076% | 
 | 2    | Heliolisk          | 14.37549% | 2449   | 12.789% | 
 | 3    | Diggersby          | 11.31371% | 1871   |  9.770% | 
 | 4    | Cinccino           |  9.03520% | 1672   |  8.731% | 
 | 5    | Indeedee           |  7.39024% | 1277   |  6.668% | 
 | 6    | Braviary           |  5.77546% | 1039   |  5.426% | 
 | 7    | Snorlax            |  5.45864% | 1214   |  6.339% | 
 | 8    | Dubwool            |  5.23661% | 1149   |  6.000% | 
 | 9    | Ditto              |  5.05695% | 1084   |  5.661% | 
 | 10   | Bewear             |  3.79314% | 736    |  3.843% | 
 | 11   | Oranguru           |  2.67918% | 569    |  2.971% | 
 | 12   | Drampa             |  2.53771% | 594    |  3.102% | 
 | 13   | Unfezant           |  2.22828% | 431    |  2.251% | 
 | 14   | Indeedee-F         |  2.15152% | 464    |  2.423% | 
 | 15   | Greedent           |  1.98509% | 487    |  2.543% | 
 | 16   | Silvally           |  1.32177% | 291    |  1.520% | 
 | 17   | Type: Null         |  1.28751% | 271    |  1.415% | 
 | 18   | Persian            |  1.00375% | 305    |  1.593% | 
 | 19   | Noctowl            |  0.54816% | 137    |  0.715% | 
 | 20   | Linoone-Galar      |  0.25191% | 64     |  0.334% | 
 | 21   | Bunnelby           |  0.24254% | 52     |  0.272% | 
 | 22   | Eevee              |  0.13278% | 38     |  0.198% | 
 | 23   | Meowth             |  0.12023% | 31     |  0.162% | 
 | 24   | Munchlax           |  0.08540% | 18     |  0.094% | 
 | 25   | Wooloo             |  0.03364% | 11     |  0.057% | 
 | 26   | Stufful            |  0.02422% | 5      |  0.026% | 
 | 27   | Rufflet            |  0.01065% | 2      |  0.010% | 
 | 28   | Tranquill          |  0.00927% | 2      |  0.010% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
