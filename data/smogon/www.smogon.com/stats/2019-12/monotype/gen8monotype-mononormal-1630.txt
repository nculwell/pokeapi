 Total battles: 213067
 Avg. weight/team: 0.004
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Ditto              | 87.68669% | 13752  | 71.812% | 10154  | 71.417% | 
 | 2    | Snorlax            | 76.08217% | 12497  | 65.258% | 9360   | 65.833% | 
 | 3    | Obstagoon          | 70.44415% | 13864  | 72.397% | 10480  | 73.710% | 
 | 4    | Indeedee           | 67.44598% | 9007   | 47.034% | 6467   | 45.485% | 
 | 5    | Heliolisk          | 64.27945% | 10164  | 53.076% | 7665   | 53.911% | 
 | 6    | Diggersby          | 50.42793% | 8193   | 42.783% | 6256   | 44.001% | 
 | 7    | Bewear             | 42.76980% | 6273   | 32.757% | 4730   | 33.268% | 
 | 8    | Braviary           | 42.61001% | 7657   | 39.984% | 5613   | 39.479% | 
 | 9    | Dubwool            | 26.89700% | 6655   | 34.752% | 5178   | 36.419% | 
 | 10   | Cinccino           | 18.54326% | 6446   | 33.661% | 4775   | 33.585% | 
 | 11   | Drampa             |  9.08891% | 4141   | 21.624% | 2914   | 20.495% | 
 | 12   | Unfezant           |  8.96623% | 2387   | 12.465% | 1803   | 12.681% | 
 | 13   | Indeedee-F         |  7.46074% | 2417   | 12.621% | 1783   | 12.541% | 
 | 14   | Type: Null         |  6.03066% | 1807   |  9.436% | 1341   |  9.432% | 
 | 15   | Greedent           |  5.44892% | 2358   | 12.313% | 1756   | 12.351% | 
 | 16   | Noctowl            |  5.41806% | 1359   |  7.097% | 946    |  6.654% | 
 | 17   | Silvally           |  4.71783% | 1891   |  9.875% | 1313   |  9.235% | 
 | 18   | Oranguru           |  2.99030% | 1606   |  8.386% | 1295   |  9.108% | 
 | 19   | Munchlax           |  1.48770% | 144    |  0.752% | 106    |  0.746% | 
 | 20   | Persian            |  0.54074% | 813    |  4.245% | 664    |  4.670% | 
 | 21   | Eevee              |  0.40675% | 315    |  1.645% | 241    |  1.695% | 
 | 22   | Linoone-Galar      |  0.14410% | 237    |  1.238% | 193    |  1.357% | 
 | 23   | Meowth             |  0.06446% | 129    |  0.674% | 101    |  0.710% | 
 | 24   | Tranquill          |  0.01512% | 19     |  0.099% | 15     |  0.106% | 
 | 25   | Wooloo             |  0.00702% | 96     |  0.501% | 74     |  0.520% | 
 | 26   | Rufflet            |  0.00498% | 18     |  0.094% | 11     |  0.077% | 
 | 27   | Stufful            |  0.00316% | 14     |  0.073% | 12     |  0.084% | 
 | 28   | Bunnelby           |  0.00035% | 56     |  0.292% | 55     |  0.387% | 
 | 29   | Hoothoot           |  0.00000% | 9      |  0.047% | 2      |  0.014% | 
 | 30   | Minccino           |  0.00000% | 4      |  0.021% | 3      |  0.021% | 
 | 31   | Skwovet            |  0.00000% | 2      |  0.010% | 1      |  0.007% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
