 +----------------------------------------+ 
 | Cinderace                              | 
 +----------------------------------------+ 
 | Raw count: 14956                       | 
 | Avg. weight: 0.00610915342554          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Blaze 100.000%                         | 
 +----------------------------------------+ 
 | Items                                  | 
 | Heavy-Duty Boots 45.142%               | 
 | Life Orb 26.554%                       | 
 | Choice Scarf 12.558%                   | 
 | Choice Band  8.384%                    | 
 | Flame Plate  1.478%                    | 
 | Leftovers  1.071%                      | 
 | Other  4.812%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 63.709%          | 
 | Jolly:0/252/0/0/0/252 12.675%          | 
 | Jolly:4/252/0/0/0/252  9.000%          | 
 | Jolly:0/252/4/0/0/252  8.529%          | 
 | Adamant:0/252/0/0/4/252  1.799%        | 
 | Other  4.289%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Pyro Ball 98.064%                      | 
 | High Jump Kick 77.391%                 | 
 | Court Change 59.946%                   | 
 | U-turn 56.169%                         | 
 | Sucker Punch 48.459%                   | 
 | Gunk Shot 31.188%                      | 
 | Zen Headbutt  8.351%                   | 
 | Bounce  3.559%                         | 
 | Other 16.873%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Charizard                              | 
 +----------------------------------------+ 
 | Raw count: 12037                       | 
 | Avg. weight: 0.00629934557757          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Solar Power 63.286%                    | 
 | Blaze 36.714%                          | 
 +----------------------------------------+ 
 | Items                                  | 
 | Heavy-Duty Boots 46.729%               | 
 | Choice Scarf 17.712%                   | 
 | Choice Specs 17.012%                   | 
 | Life Orb 10.007%                       | 
 | Salac Berry  1.934%                    | 
 | Assault Vest  1.420%                   | 
 | Leftovers  1.287%                      | 
 | Other  3.899%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 58.541%          | 
 | Jolly:0/252/0/0/4/252 13.514%          | 
 | Jolly:0/252/0/0/0/252 11.553%          | 
 | Timid:0/0/4/252/0/252  4.941%          | 
 | Adamant:0/252/0/0/4/252  1.789%        | 
 | Hasty:0/4/0/252/0/252  1.318%          | 
 | Other  8.346%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Solar Beam 60.433%                     | 
 | Fire Blast 50.171%                     | 
 | Air Slash 49.821%                      | 
 | Focus Blast 32.744%                    | 
 | Dragon Pulse 27.971%                   | 
 | Earthquake 27.956%                     | 
 | Dragon Dance 27.210%                   | 
 | Flamethrower 21.488%                   | 
 | Hurricane 19.745%                      | 
 | Flare Blitz 18.137%                    | 
 | Dragon Claw 13.803%                    | 
 | Thunder Punch 12.969%                  | 
 | Blaze Kick  8.738%                     | 
 | Ancient Power  8.349%                  | 
 | Substitute  3.702%                     | 
 | Other 16.764%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +1.507%                      | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Rotom-Heat                             | 
 +----------------------------------------+ 
 | Raw count: 13157                       | 
 | Avg. weight: 0.00562743593693          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 49.962%                   | 
 | Leftovers 27.034%                      | 
 | Light Clay  8.656%                     | 
 | Heavy-Duty Boots  3.888%               | 
 | Choice Specs  3.297%                   | 
 | Chesto Berry  2.489%                   | 
 | Other  4.674%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 69.972%          | 
 | Modest:248/0/0/252/0/8  5.293%         | 
 | Timid:236/0/0/20/0/252  4.643%         | 
 | Calm:248/0/8/0/252/0  4.295%           | 
 | Calm:252/0/4/0/252/0  2.326%           | 
 | Bold:252/0/252/0/4/0  1.446%           | 
 | Other 12.026%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Volt Switch 78.718%                    | 
 | Overheat 70.931%                       | 
 | Thunderbolt 66.396%                    | 
 | Trick 46.951%                          | 
 | Will-O-Wisp 28.836%                    | 
 | Nasty Plot 24.952%                     | 
 | Substitute 15.039%                     | 
 | Light Screen 12.031%                   | 
 | Thunder Wave 10.676%                   | 
 | Reflect  9.442%                        | 
 | Shadow Ball  8.424%                    | 
 | Dark Pulse  8.119%                     | 
 | Other 19.484%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Ninetales +4.162%                      | 
 | Torkoal +0.823%                        | 
 | Silvally-Fire +0.821%                  | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Torkoal                                | 
 +----------------------------------------+ 
 | Raw count: 10396                       | 
 | Avg. weight: 0.00590741214274          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drought 99.891%                        | 
 | White Smoke  0.086%                    | 
 | Shell Armor  0.023%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Heat Rock 42.159%                      | 
 | Leftovers 32.236%                      | 
 | Heavy-Duty Boots  9.998%               | 
 | Rocky Helmet  9.144%                   | 
 | Eject Pack  1.320%                     | 
 | Charcoal  1.118%                       | 
 | Other  4.025%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Bold:252/0/252/4/0/0 29.276%           | 
 | Bold:248/0/252/8/0/0 12.782%           | 
 | Bold:248/0/252/0/8/0  8.355%           | 
 | Calm:248/0/0/0/252/0  7.212%           | 
 | Relaxed:248/8/252/0/0/0  6.676%        | 
 | Calm:252/0/0/4/252/0  4.810%           | 
 | Other 30.889%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Rapid Spin 92.840%                     | 
 | Stealth Rock 88.322%                   | 
 | Lava Plume 82.511%                     | 
 | Yawn 62.781%                           | 
 | Earth Power 16.746%                    | 
 | Solar Beam 10.499%                     | 
 | Will-O-Wisp 10.485%                    | 
 | Clear Smog  9.553%                     | 
 | Fire Blast  7.164%                     | 
 | Other 19.099%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Silvally-Fire +1.823%                  | 
 | Rotom-Heat +1.205%                     | 
 | Chandelure +0.800%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Chandelure                             | 
 +----------------------------------------+ 
 | Raw count: 9438                        | 
 | Avg. weight: 0.0061667099111           | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Infiltrator 64.162%                    | 
 | Flash Fire 34.049%                     | 
 | Flame Body  1.789%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 66.490%                   | 
 | Choice Specs 25.248%                   | 
 | Air Balloon  2.285%                    | 
 | Leftovers  1.589%                      | 
 | Other  4.389%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 67.951%          | 
 | Modest:4/0/0/252/0/252 16.135%         | 
 | Timid:0/0/4/252/0/252  9.124%          | 
 | Modest:0/0/0/252/4/252  4.273%         | 
 | Other  2.517%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Shadow Ball 98.718%                    | 
 | Energy Ball 80.703%                    | 
 | Fire Blast 73.940%                     | 
 | Psychic 42.581%                        | 
 | Trick 42.513%                          | 
 | Flamethrower 20.647%                   | 
 | Dark Pulse 15.272%                     | 
 | Solar Beam  7.238%                     | 
 | Other 18.388%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Torkoal +1.065%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Ninetales                              | 
 +----------------------------------------+ 
 | Raw count: 6469                        | 
 | Avg. weight: 0.00703208114595          | 
 | Viability Ceiling: 78                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drought 96.082%                        | 
 | Flash Fire  3.918%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 24.868%                      | 
 | Heat Rock 23.491%                      | 
 | Choice Specs 17.704%                   | 
 | Focus Sash 10.662%                     | 
 | Life Orb 10.583%                       | 
 | Choice Scarf  8.521%                   | 
 | Other  4.171%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 69.613%          | 
 | Timid:92/0/0/252/4/160 14.042%         | 
 | Timid:102/0/0/252/4/150  6.393%        | 
 | Timid:4/0/0/252/0/252  1.997%          | 
 | Calm:252/0/0/4/252/0  1.096%           | 
 | Timid:0/0/0/8/248/252  1.085%          | 
 | Other  5.773%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Solar Beam 67.346%                     | 
 | Fire Blast 59.136%                     | 
 | Nasty Plot 39.726%                     | 
 | Energy Ball 35.109%                    | 
 | Dark Pulse 34.188%                     | 
 | Flamethrower 31.916%                   | 
 | Extrasensory 23.859%                   | 
 | Calm Mind 22.439%                      | 
 | Substitute 20.625%                     | 
 | Psyshock 15.112%                       | 
 | Shadow Ball 10.556%                    | 
 | Will-O-Wisp  9.400%                    | 
 | Weather Ball  6.151%                   | 
 | Hypnosis  5.756%                       | 
 | Other 18.683%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Rotom-Heat +7.826%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Salazzle                               | 
 +----------------------------------------+ 
 | Raw count: 6725                        | 
 | Avg. weight: 0.00640079699802          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Corrosion 98.569%                      | 
 | Oblivious  1.431%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Black Sludge 33.090%                   | 
 | Leftovers 31.902%                      | 
 | Choice Scarf 11.616%                   | 
 | Choice Specs  7.164%                   | 
 | Focus Sash  7.037%                     | 
 | Air Balloon  5.622%                    | 
 | Other  3.569%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 28.187%          | 
 | Timid:0/0/0/252/0/252 20.019%          | 
 | Timid:248/0/0/8/0/252 17.993%          | 
 | Timid:248/0/8/0/0/252 11.327%          | 
 | Hasty:0/4/0/252/0/252  9.230%          | 
 | Timid:0/0/4/252/0/252  3.658%          | 
 | Other  9.587%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Toxic 83.318%                          | 
 | Sludge Wave 66.852%                    | 
 | Flamethrower 62.998%                   | 
 | Substitute 62.388%                     | 
 | Protect 28.641%                        | 
 | Fire Blast 23.289%                     | 
 | Dragon Pulse 15.082%                   | 
 | Disable 12.790%                        | 
 | Nasty Plot 11.487%                     | 
 | Foul Play  6.482%                      | 
 | Acrobatics  6.257%                     | 
 | Fake Out  4.339%                       | 
 | Other 16.077%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +9.607%                      | 
 | Centiskorch +6.984%                    | 
 | Turtonator +1.017%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Arcanine                               | 
 +----------------------------------------+ 
 | Raw count: 9458                        | 
 | Avg. weight: 0.00468985700509          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Intimidate 97.320%                     | 
 | Justified  1.397%                      | 
 | Flash Fire  1.283%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 36.788%                    | 
 | Leftovers 23.397%                      | 
 | Life Orb 14.245%                       | 
 | Assault Vest 13.326%                   | 
 | Rocky Helmet  6.094%                   | 
 | Heavy-Duty Boots  1.873%               | 
 | Other  4.277%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 38.364%          | 
 | Adamant:0/252/0/0/4/252 20.207%        | 
 | Jolly:0/252/4/0/0/252 12.366%          | 
 | Bold:252/0/252/4/0/0  6.130%           | 
 | Impish:248/0/68/0/0/192  3.734%        | 
 | Hasty:0/252/0/4/0/252  2.257%          | 
 | Other 16.942%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Flare Blitz 86.734%                    | 
 | Extreme Speed 83.242%                  | 
 | Wild Charge 63.424%                    | 
 | Close Combat 53.846%                   | 
 | Morning Sun 35.214%                    | 
 | Will-O-Wisp 23.867%                    | 
 | Play Rough 22.490%                     | 
 | Roar  8.264%                           | 
 | Flamethrower  6.806%                   | 
 | Other 16.113%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Coalossal                              | 
 +----------------------------------------+ 
 | Raw count: 7130                        | 
 | Avg. weight: 0.00392339545949          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Steam Engine 57.893%                   | 
 | Flame Body 39.169%                     | 
 | Flash Fire  2.937%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Heavy-Duty Boots 33.340%               | 
 | Focus Sash 25.167%                     | 
 | Weakness Policy 14.387%                | 
 | Assault Vest  8.011%                   | 
 | Leftovers  7.646%                      | 
 | Passho Berry  2.876%                   | 
 | Mago Berry  2.692%                     | 
 | Air Balloon  2.070%                    | 
 | Other  3.811%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Bold:248/0/252/0/8/0 33.401%           | 
 | Adamant:248/252/0/0/0/8 10.512%        | 
 | Adamant:0/252/0/0/0/252  9.789%        | 
 | Adamant:248/252/0/0/8/0  9.444%        | 
 | Impish:248/8/252/0/0/0  4.951%         | 
 | Modest:248/0/0/252/8/0  4.328%         | 
 | Other 27.574%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Stealth Rock 74.255%                   | 
 | Rapid Spin 64.297%                     | 
 | Spikes 59.646%                         | 
 | Flamethrower 38.763%                   | 
 | Earthquake 25.130%                     | 
 | Flare Blitz 24.407%                    | 
 | Stone Edge 17.628%                     | 
 | Earth Power 13.161%                    | 
 | Explosion 10.373%                      | 
 | Scald  8.335%                          | 
 | Ancient Power  7.027%                  | 
 | Fire Blast  5.447%                     | 
 | Iron Head  4.950%                      | 
 | Heat Wave  4.824%                      | 
 | Rock Slide  4.766%                     | 
 | Fire Punch  4.730%                     | 
 | Tar Shot  4.459%                       | 
 | Protect  3.582%                        | 
 | Gyro Ball  3.243%                      | 
 | Endure  2.779%                         | 
 | Other 18.198%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Centiskorch +20.070%                   | 
 | Salazzle +14.987%                      | 
 | Charizard +4.178%                      | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Centiskorch                            | 
 +----------------------------------------+ 
 | Raw count: 8137                        | 
 | Avg. weight: 0.0033384458565           | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Flash Fire 72.157%                     | 
 | Flame Body 19.712%                     | 
 | White Smoke  8.130%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 48.256%                      | 
 | Heavy-Duty Boots 31.453%               | 
 | Focus Sash  3.668%                     | 
 | Choice Scarf  3.013%                   | 
 | Life Orb  2.344%                       | 
 | Assault Vest  2.317%                   | 
 | Charti Berry  1.908%                   | 
 | Choice Band  1.711%                    | 
 | Aguav Berry  1.634%                    | 
 | Other  3.695%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:252/0/0/0/4/252 34.239%          | 
 | Adamant:248/252/0/0/8/0 19.753%        | 
 | Jolly:0/252/0/0/4/252  8.757%          | 
 | Adamant:252/252/0/0/4/0  6.737%        | 
 | Adamant:180/252/0/0/0/76  6.572%       | 
 | Lonely:248/252/0/8/0/0  3.399%         | 
 | Other 20.544%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Fire Lash 89.303%                      | 
 | Coil 71.434%                           | 
 | Knock Off 67.286%                      | 
 | Leech Life 59.725%                     | 
 | Power Whip 49.734%                     | 
 | Substitute 34.760%                     | 
 | Flare Blitz  6.863%                    | 
 | Crunch  4.829%                         | 
 | Other 16.067%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +20.191%                     | 
 | Salazzle +10.202%                      | 
 | Turtonator +1.302%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Turtonator                             | 
 +----------------------------------------+ 
 | Raw count: 4045                        | 
 | Avg. weight: 0.00219795567004          | 
 | Viability Ceiling: 76                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Shell Armor 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | White Herb 24.389%                     | 
 | Leftovers 23.916%                      | 
 | Bright Powder 15.533%                  | 
 | Choice Specs 12.840%                   | 
 | Rocky Helmet  4.666%                   | 
 | Assault Vest  4.618%                   | 
 | Heavy-Duty Boots  4.600%               | 
 | Life Orb  2.774%                       | 
 | Weakness Policy  2.620%                | 
 | Other  4.045%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Modest:0/0/0/252/4/252 22.373%         | 
 | Bold:248/0/8/252/0/0 16.868%           | 
 | Hardy:252/0/0/252/0/0  8.007%          | 
 | Rash:0/4/0/252/0/252  5.833%           | 
 | Modest:248/0/0/252/0/0  4.644%         | 
 | Modest:252/0/0/252/4/0  4.490%         | 
 | Other 37.785%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Draco Meteor 47.213%                   | 
 | Shell Smash 44.553%                    | 
 | Fire Blast 40.885%                     | 
 | Flash Cannon 38.996%                   | 
 | Dragon Pulse 38.898%                   | 
 | Shell Trap 29.159%                     | 
 | Body Press 23.829%                     | 
 | Earthquake 23.280%                     | 
 | Rapid Spin 18.497%                     | 
 | Flamethrower 18.491%                   | 
 | Iron Defense 16.875%                   | 
 | Solar Beam 14.041%                     | 
 | Outrage  9.057%                        | 
 | Focus Blast  6.904%                    | 
 | Explosion  5.425%                      | 
 | Dragon Claw  4.883%                    | 
 | Other 19.015%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Flareon +3.906%                        | 
 | Centiskorch +2.839%                    | 
 | Salazzle +1.660%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Silvally-Fire                          | 
 +----------------------------------------+ 
 | Raw count: 1567                        | 
 | Avg. weight: 0.00436419445132          | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | RKS System 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Fire Memory 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 51.341%          | 
 | Timid:0/0/0/252/4/252 39.712%          | 
 | Modest:6/0/0/252/0/252  6.288%         | 
 | Other  2.659%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Multi-Attack 52.659%                   | 
 | Parting Shot 48.183%                   | 
 | Thunderbolt 46.762%                    | 
 | Flamethrower 46.276%                   | 
 | Grass Pledge 44.681%                   | 
 | Swords Dance 37.641%                   | 
 | Iron Head 37.623%                      | 
 | Psychic Fangs 36.019%                  | 
 | Outrage 11.327%                        | 
 | U-turn  8.722%                         | 
 | Ice Beam  7.980%                       | 
 | Crunch  7.732%                         | 
 | Other 14.395%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Torkoal +18.355%                       | 
 | Rotom-Heat +11.989%                    | 
 | Arcanine +2.499%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Flareon                                | 
 +----------------------------------------+ 
 | Raw count: 1417                        | 
 | Avg. weight: 0.000834940298228         | 
 | Viability Ceiling: 69                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Guts 79.172%                           | 
 | Flash Fire 20.828%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Assault Vest 37.190%                   | 
 | Toxic Orb 20.124%                      | 
 | Leftovers 15.270%                      | 
 | Flame Orb 13.277%                      | 
 | Choice Band  9.949%                    | 
 | Other  4.191%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:252/252/0/0/4/0 52.878%        | 
 | Careful:252/4/0/0/252/0  8.814%        | 
 | Adamant:248/252/0/0/8/0  8.041%        | 
 | Jolly:4/252/0/0/0/252  7.436%          | 
 | Calm:252/0/4/0/252/0  4.465%           | 
 | Careful:0/0/252/0/4/252  3.902%        | 
 | Other 14.465%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Flare Blitz 80.166%                    | 
 | Superpower 70.146%                     | 
 | Facade 65.801%                         | 
 | Quick Attack 63.001%                   | 
 | Wish 27.884%                           | 
 | Protect 18.101%                        | 
 | Body Slam 10.434%                      | 
 | Substitute  8.894%                     | 
 | Will-O-Wisp  7.771%                    | 
 | Fire Spin  7.106%                      | 
 | Yawn  6.866%                           | 
 | Fire Fang  6.476%                      | 
 | Flamethrower  4.068%                   | 
 | Rest  3.904%                           | 
 | Other 19.382%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Turtonator +28.947%                    | 
 | Torkoal +6.618%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Carkol                                 | 
 +----------------------------------------+ 
 | Raw count: 154                         | 
 | Avg. weight: 0.000927205987862         | 
 | Viability Ceiling: 62                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Flame Body 89.698%                     | 
 | Steam Engine 10.302%                   | 
 | Flash Fire  0.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Eviolite 99.973%                       | 
 | Other  0.027%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Impish:248/8/252/0/0/0 88.667%         | 
 | Careful:248/8/0/0/252/0  3.818%        | 
 | Hardy:0/0/0/4/252/252  3.766%          | 
 | Other  3.750%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Rapid Spin 96.138%                     | 
 | Protect 92.426%                        | 
 | Stealth Rock 92.412%                   | 
 | Rock Slide 88.661%                     | 
 | Will-O-Wisp  8.614%                    | 
 | Scald  6.520%                          | 
 | Other 15.227%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Centiskorch +62.717%                   | 
 | Arcanine +47.052%                      | 
 | Ninetales +42.118%                     | 
 | Charizard +13.647%                     | 
 | Rotom-Heat +10.358%                    | 
 | Heatmor +3.697%                        | 
 | Flareon +2.731%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Lampent                                | 
 +----------------------------------------+ 
 | Raw count: 58                          | 
 | Avg. weight: 0.00129470117496          | 
 | Viability Ceiling: 60                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Infiltrator 97.897%                    | 
 | Flash Fire  2.103%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Eviolite 100.000%                      | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Calm:252/0/168/0/88/0 97.897%          | 
 | Other  2.103%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Trick Room 100.000%                    | 
 | Clear Smog 100.000%                    | 
 | Pain Split 100.000%                    | 
 | Flamethrower 97.897%                   | 
 | Other  2.103%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +71.701%                     | 
 | Salazzle +49.808%                      | 
 | Ninetales +48.666%                     | 
 | Torkoal +34.208%                       | 
 | Rotom-Heat +19.515%                    | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Heatmor                                | 
 +----------------------------------------+ 
 | Raw count: 396                         | 
 | Avg. weight: 0.000206211081009         | 
 | Viability Ceiling: 66                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Gluttony 59.452%                       | 
 | Flash Fire 37.784%                     | 
 | White Smoke  2.764%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Aguav Berry 58.853%                    | 
 | Rocky Helmet 27.143%                   | 
 | Choice Specs  6.783%                   | 
 | Assault Vest  3.039%                   | 
 | Other  4.182%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:252/252/0/0/4/0 71.756%        | 
 | Quiet:252/68/0/188/0/0  9.932%         | 
 | Adamant:0/4/252/0/252/0  7.006%        | 
 | Modest:252/0/0/252/4/0  6.784%         | 
 | Other  4.522%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Fire Lash 79.392%                      | 
 | Drain Punch 70.189%                    | 
 | Hone Claws 69.315%                     | 
 | Shadow Claw 44.479%                    | 
 | Throat Chop 34.292%                    | 
 | Giga Drain 20.093%                     | 
 | Flamethrower 16.905%                   | 
 | Sucker Punch 13.134%                   | 
 | Belch  9.934%                          | 
 | Focus Blast  9.917%                    | 
 | Curse  7.006%                          | 
 | Amnesia  7.006%                        | 
 | Other 18.336%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Arcanine +26.587%                      | 
 | Centiskorch +26.088%                   | 
 | Rotom-Heat +9.314%                     | 
 | Carkol +6.873%                         | 
 | Flareon +6.046%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
