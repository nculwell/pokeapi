 +----------------------------------------+ 
 | Cinderace                              | 
 +----------------------------------------+ 
 | Raw count: 14956                       | 
 | Avg. weight: 0.114359820389            | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Blaze 100.000%                         | 
 +----------------------------------------+ 
 | Items                                  | 
 | Heavy-Duty Boots 48.010%               | 
 | Life Orb 20.991%                       | 
 | Choice Scarf 12.973%                   | 
 | Choice Band  8.772%                    | 
 | Leftovers  2.923%                      | 
 | Focus Sash  1.456%                     | 
 | Other  4.875%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 72.047%          | 
 | Jolly:4/252/0/0/0/252  9.326%          | 
 | Jolly:0/252/4/0/0/252  6.144%          | 
 | Jolly:0/252/0/0/0/252  4.854%          | 
 | Adamant:0/252/0/0/4/252  2.104%        | 
 | Adamant:0/252/252/0/4/0  1.683%        | 
 | Other  3.842%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Pyro Ball 97.770%                      | 
 | High Jump Kick 80.768%                 | 
 | Court Change 66.711%                   | 
 | U-turn 63.674%                         | 
 | Sucker Punch 35.474%                   | 
 | Gunk Shot 20.559%                      | 
 | Zen Headbutt 12.850%                   | 
 | Bulk Up  4.052%                        | 
 | Other 18.142%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Charizard                              | 
 +----------------------------------------+ 
 | Raw count: 12037                       | 
 | Avg. weight: 0.125184160517            | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Solar Power 78.983%                    | 
 | Blaze 21.017%                          | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 32.680%                   | 
 | Heavy-Duty Boots 26.234%               | 
 | Choice Specs 20.477%                   | 
 | Life Orb  8.643%                       | 
 | Leftovers  2.988%                      | 
 | Expert Belt  1.940%                    | 
 | Assault Vest  1.743%                   | 
 | Charti Berry  1.029%                   | 
 | Other  4.266%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 72.735%          | 
 | Jolly:0/252/0/0/4/252  6.532%          | 
 | Timid:4/0/0/252/0/252  3.603%          | 
 | Modest:4/0/0/252/0/252  3.277%         | 
 | Timid:0/0/4/252/0/252  2.990%          | 
 | Adamant:0/252/0/0/4/252  2.272%        | 
 | Other  8.592%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Air Slash 68.858%                      | 
 | Solar Beam 67.259%                     | 
 | Fire Blast 51.085%                     | 
 | Dragon Pulse 45.849%                   | 
 | Focus Blast 44.069%                    | 
 | Flamethrower 36.211%                   | 
 | Hurricane 16.596%                      | 
 | Earthquake 12.318%                     | 
 | Dragon Dance 11.423%                   | 
 | Dragon Claw  5.875%                    | 
 | Flare Blitz  5.650%                    | 
 | Ancient Power  5.636%                  | 
 | Blaze Kick  4.675%                     | 
 | Thunder Punch  3.906%                  | 
 | Heat Wave  2.879%                      | 
 | Other 17.708%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Rotom-Heat                             | 
 +----------------------------------------+ 
 | Raw count: 13157                       | 
 | Avg. weight: 0.109608504207            | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 35.238%                   | 
 | Leftovers 34.129%                      | 
 | Light Clay 12.111%                     | 
 | Choice Specs  7.230%                   | 
 | Assault Vest  3.107%                   | 
 | Aguav Berry  2.193%                    | 
 | Heavy-Duty Boots  1.710%               | 
 | Other  4.282%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 64.532%          | 
 | Timid:236/0/0/20/0/252  5.708%         | 
 | Calm:248/0/8/0/252/0  4.300%           | 
 | Timid:4/0/0/252/0/252  2.005%          | 
 | Hardy:252/0/0/0/252/4  1.980%          | 
 | Calm:252/0/4/0/252/0  1.967%           | 
 | Other 19.507%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Volt Switch 70.029%                    | 
 | Overheat 66.897%                       | 
 | Thunderbolt 61.723%                    | 
 | Trick 35.078%                          | 
 | Nasty Plot 26.318%                     | 
 | Will-O-Wisp 24.686%                    | 
 | Dark Pulse 22.955%                     | 
 | Substitute 22.173%                     | 
 | Light Screen 16.822%                   | 
 | Reflect 14.371%                        | 
 | Thunder Wave 11.426%                   | 
 | Discharge 10.386%                      | 
 | Other 17.135%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Torkoal                                | 
 +----------------------------------------+ 
 | Raw count: 10396                       | 
 | Avg. weight: 0.119230223709            | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drought 99.617%                        | 
 | White Smoke  0.210%                    | 
 | Shell Armor  0.173%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Heat Rock 48.960%                      | 
 | Leftovers 24.670%                      | 
 | Heavy-Duty Boots  7.568%               | 
 | Rocky Helmet  7.427%                   | 
 | Aguav Berry  2.870%                    | 
 | Eject Pack  2.662%                     | 
 | Mental Herb  1.598%                    | 
 | Other  4.244%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Bold:252/0/252/4/0/0 19.647%           | 
 | Bold:248/0/252/8/0/0 12.683%           | 
 | Relaxed:248/8/252/0/0/0 11.965%        | 
 | Bold:248/0/252/0/8/0  9.710%           | 
 | Bold:252/0/252/0/4/0  5.903%           | 
 | Quiet:252/4/0/252/0/0  4.371%          | 
 | Other 35.721%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Rapid Spin 88.604%                     | 
 | Stealth Rock 83.597%                   | 
 | Lava Plume 79.973%                     | 
 | Yawn 57.019%                           | 
 | Clear Smog 16.214%                     | 
 | Earth Power 15.464%                    | 
 | Will-O-Wisp 14.166%                    | 
 | Solar Beam 13.667%                     | 
 | Flamethrower  5.604%                   | 
 | Body Press  5.185%                     | 
 | Fire Blast  3.863%                     | 
 | Other 16.644%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Chandelure +4.037%                     | 
 | Silvally-Fire +0.944%                  | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Arcanine                               | 
 +----------------------------------------+ 
 | Raw count: 9458                        | 
 | Avg. weight: 0.126127161917            | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Intimidate 97.350%                     | 
 | Justified  1.935%                      | 
 | Flash Fire  0.715%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 46.294%                    | 
 | Leftovers 13.319%                      | 
 | Life Orb 11.611%                       | 
 | Assault Vest  9.766%                   | 
 | Rocky Helmet  6.247%                   | 
 | Heavy-Duty Boots  4.952%               | 
 | Choice Scarf  3.696%                   | 
 | Other  4.115%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 49.850%          | 
 | Adamant:0/252/0/0/4/252 14.952%        | 
 | Jolly:0/252/4/0/0/252  8.573%          | 
 | Adamant:4/252/0/0/0/252  3.763%        | 
 | Jolly:4/252/0/0/0/252  2.641%          | 
 | Impish:252/4/252/0/0/0  2.548%         | 
 | Other 17.673%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Flare Blitz 93.021%                    | 
 | Extreme Speed 87.492%                  | 
 | Wild Charge 70.680%                    | 
 | Close Combat 50.215%                   | 
 | Play Rough 37.794%                     | 
 | Morning Sun 24.840%                    | 
 | Will-O-Wisp 13.595%                    | 
 | Crunch  4.654%                         | 
 | Other 17.709%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Chandelure                             | 
 +----------------------------------------+ 
 | Raw count: 9438                        | 
 | Avg. weight: 0.0985053221277           | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Infiltrator 68.671%                    | 
 | Flash Fire 28.051%                     | 
 | Flame Body  3.278%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 71.428%                   | 
 | Choice Specs 16.786%                   | 
 | Leftovers  3.215%                      | 
 | Focus Sash  2.096%                     | 
 | Life Orb  1.783%                       | 
 | Other  4.692%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 80.360%          | 
 | Timid:0/0/4/252/0/252  5.091%          | 
 | Modest:4/0/0/252/0/252  4.804%         | 
 | Timid:4/0/0/252/0/252  3.425%          | 
 | Modest:0/0/0/252/4/252  2.580%         | 
 | Other  3.741%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Shadow Ball 98.703%                    | 
 | Energy Ball 85.560%                    | 
 | Fire Blast 54.129%                     | 
 | Psychic 41.141%                        | 
 | Trick 38.195%                          | 
 | Flamethrower 37.888%                   | 
 | Dark Pulse 12.556%                     | 
 | Calm Mind  4.654%                      | 
 | Substitute  4.582%                     | 
 | Clear Smog  4.261%                     | 
 | Other 18.329%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Torkoal +4.598%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Ninetales                              | 
 +----------------------------------------+ 
 | Raw count: 6469                        | 
 | Avg. weight: 0.133977324732            | 
 | Viability Ceiling: 78                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drought 98.726%                        | 
 | Flash Fire  1.274%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Specs 26.876%                   | 
 | Heat Rock 24.740%                      | 
 | Leftovers 17.626%                      | 
 | Life Orb  9.785%                       | 
 | Heavy-Duty Boots  5.629%               | 
 | Focus Sash  5.314%                     | 
 | Choice Scarf  3.900%                   | 
 | Air Balloon  2.309%                    | 
 | Other  3.822%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 77.719%          | 
 | Timid:92/0/0/252/4/160  7.035%         | 
 | Timid:102/0/0/252/4/150  2.420%        | 
 | Calm:252/0/0/4/252/0  1.699%           | 
 | Timid:0/0/4/252/0/252  1.340%          | 
 | Hasty:0/4/0/252/0/252  1.115%          | 
 | Other  8.671%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Solar Beam 67.597%                     | 
 | Fire Blast 65.265%                     | 
 | Nasty Plot 42.264%                     | 
 | Extrasensory 35.475%                   | 
 | Energy Ball 32.117%                    | 
 | Flamethrower 31.770%                   | 
 | Shadow Ball 22.057%                    | 
 | Dark Pulse 21.851%                     | 
 | Psyshock 15.744%                       | 
 | Will-O-Wisp 13.426%                    | 
 | Calm Mind 13.341%                      | 
 | Substitute 10.358%                     | 
 | Hypnosis  4.072%                       | 
 | Memento  3.025%                        | 
 | Encore  2.918%                         | 
 | Other 18.721%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Salazzle                               | 
 +----------------------------------------+ 
 | Raw count: 6725                        | 
 | Avg. weight: 0.121290722144            | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Corrosion 97.301%                      | 
 | Oblivious  2.699%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Black Sludge 37.288%                   | 
 | Leftovers 25.554%                      | 
 | Focus Sash  8.890%                     | 
 | Choice Scarf  8.294%                   | 
 | Choice Specs  7.233%                   | 
 | Air Balloon  6.952%                    | 
 | Life Orb  4.008%                       | 
 | Other  1.781%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 28.968%          | 
 | Timid:248/0/0/8/0/252 27.764%          | 
 | Timid:248/0/8/0/0/252 19.198%          | 
 | Hasty:0/4/0/252/0/252  6.413%          | 
 | Timid:0/0/0/252/0/252  4.685%          | 
 | Timid:4/0/0/252/0/252  2.730%          | 
 | Other 10.243%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Toxic 76.146%                          | 
 | Flamethrower 69.280%                   | 
 | Substitute 61.703%                     | 
 | Protect 46.969%                        | 
 | Sludge Wave 46.006%                    | 
 | Fire Blast 19.499%                     | 
 | Dragon Pulse 19.276%                   | 
 | Nasty Plot 14.362%                     | 
 | Disable  9.519%                        | 
 | Sludge Bomb  5.934%                    | 
 | Taunt  5.187%                          | 
 | Fake Out  4.698%                       | 
 | Overheat  4.243%                       | 
 | Other 17.179%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +2.014%                      | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Centiskorch                            | 
 +----------------------------------------+ 
 | Raw count: 8137                        | 
 | Avg. weight: 0.0538357210241           | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Flash Fire 56.774%                     | 
 | Flame Body 22.865%                     | 
 | White Smoke 20.361%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Heavy-Duty Boots 39.897%               | 
 | Leftovers 26.581%                      | 
 | Assault Vest 11.449%                   | 
 | Choice Band  4.489%                    | 
 | Life Orb  4.177%                       | 
 | Choice Scarf  2.480%                   | 
 | Focus Sash  2.394%                     | 
 | Aguav Berry  1.725%                    | 
 | Charti Berry  1.554%                   | 
 | Rocky Helmet  1.133%                   | 
 | Other  4.123%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:248/252/0/0/8/0 25.871%        | 
 | Jolly:252/0/0/0/4/252  9.099%          | 
 | Adamant:252/252/0/0/4/0  8.229%        | 
 | Jolly:0/252/0/0/4/252  5.891%          | 
 | Adamant:180/252/0/0/0/76  4.165%       | 
 | Adamant:64/252/56/0/136/0  4.163%      | 
 | Other 42.583%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Fire Lash 87.084%                      | 
 | Leech Life 76.309%                     | 
 | Power Whip 66.419%                     | 
 | Coil 59.467%                           | 
 | Knock Off 51.359%                      | 
 | Flare Blitz 10.618%                    | 
 | Substitute  9.877%                     | 
 | Lunge  9.063%                          | 
 | Crunch  6.961%                         | 
 | Thunder Fang  4.103%                   | 
 | Other 18.740%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +14.053%                     | 
 | Turtonator +3.173%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Coalossal                              | 
 +----------------------------------------+ 
 | Raw count: 7130                        | 
 | Avg. weight: 0.0499672726239           | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Steam Engine 76.290%                   | 
 | Flame Body 20.816%                     | 
 | Flash Fire  2.894%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Focus Sash 26.304%                     | 
 | Weakness Policy 19.512%                | 
 | Leftovers 14.453%                      | 
 | Heavy-Duty Boots 13.507%               | 
 | Assault Vest 10.614%                   | 
 | Passho Berry  5.580%                   | 
 | Air Balloon  3.746%                    | 
 | Rocky Helmet  2.106%                   | 
 | Other  4.179%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:248/252/0/0/8/0 19.129%        | 
 | Bold:248/0/252/0/8/0 11.323%           | 
 | Adamant:0/252/0/0/0/252  9.522%        | 
 | Adamant:248/252/0/0/0/8  8.898%        | 
 | Jolly:0/252/0/0/4/252  4.806%          | 
 | Impish:248/8/252/0/0/0  3.099%         | 
 | Other 43.223%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Stealth Rock 59.250%                   | 
 | Rapid Spin 51.269%                     | 
 | Spikes 37.518%                         | 
 | Stone Edge 36.255%                     | 
 | Earthquake 36.071%                     | 
 | Flare Blitz 35.720%                    | 
 | Flamethrower 20.111%                   | 
 | Explosion 13.602%                      | 
 | Earth Power 10.018%                    | 
 | Scald  9.614%                          | 
 | Iron Head  9.594%                      | 
 | Rock Slide  8.207%                     | 
 | Tar Shot  7.381%                       | 
 | Fire Punch  7.123%                     | 
 | Ancient Power  6.585%                  | 
 | Body Press  6.551%                     | 
 | Heavy Slam  5.274%                     | 
 | Endure  4.437%                         | 
 | Fire Blast  4.299%                     | 
 | Heat Crash  3.847%                     | 
 | Solar Beam  3.743%                     | 
 | Gyro Ball  3.578%                      | 
 | Other 19.953%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Centiskorch +17.273%                   | 
 | Salazzle +3.427%                       | 
 | Heatmor +0.927%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Turtonator                             | 
 +----------------------------------------+ 
 | Raw count: 4045                        | 
 | Avg. weight: 0.0491712519859           | 
 | Viability Ceiling: 76                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Shell Armor 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | White Herb 46.927%                     | 
 | Leftovers 16.646%                      | 
 | Assault Vest 10.058%                   | 
 | Choice Specs  6.642%                   | 
 | Rocky Helmet  4.026%                   | 
 | Heavy-Duty Boots  2.652%               | 
 | Bright Powder  2.550%                  | 
 | Air Balloon  2.405%                    | 
 | Weakness Policy  1.875%                | 
 | Focus Sash  1.773%                     | 
 | Other  4.446%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Modest:0/0/0/252/4/252 13.314%         | 
 | Jolly:0/0/0/252/4/252 10.184%          | 
 | Timid:0/0/0/252/4/252  8.201%          | 
 | Modest:248/0/8/252/0/0  6.005%         | 
 | Adamant:252/252/4/0/0/0  4.275%        | 
 | Modest:248/0/0/252/0/0  3.915%         | 
 | Other 54.107%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Shell Smash 49.441%                    | 
 | Dragon Pulse 46.236%                   | 
 | Flash Cannon 42.103%                   | 
 | Draco Meteor 39.728%                   | 
 | Flamethrower 38.471%                   | 
 | Rapid Spin 27.138%                     | 
 | Fire Blast 27.104%                     | 
 | Earthquake 19.364%                     | 
 | Body Press 17.895%                     | 
 | Solar Beam 16.411%                     | 
 | Shell Trap 13.679%                     | 
 | Outrage  9.626%                        | 
 | Focus Blast  8.975%                    | 
 | Bulk Up  6.426%                        | 
 | Dragon Claw  6.037%                    | 
 | Explosion  5.811%                      | 
 | Iron Defense  5.514%                   | 
 | Overheat  2.955%                       | 
 | Other 17.085%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Centiskorch +6.754%                    | 
 | Flareon +3.785%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Silvally-Fire                          | 
 +----------------------------------------+ 
 | Raw count: 1567                        | 
 | Avg. weight: 0.0720466788303           | 
 | Viability Ceiling: 80                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | RKS System 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Fire Memory 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 33.008%          | 
 | Calm:0/0/0/4/252/252 31.317%           | 
 | Timid:0/0/0/252/4/252 16.041%          | 
 | Timid:0/0/4/252/0/252  7.926%          | 
 | Modest:6/0/0/252/0/252  6.569%         | 
 | Adamant:0/252/20/0/12/224  1.054%      | 
 | Other  4.085%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Thunderbolt 61.731%                    | 
 | Parting Shot 61.241%                   | 
 | Ice Beam 49.850%                       | 
 | U-turn 36.183%                         | 
 | Multi-Attack 36.180%                   | 
 | Flamethrower 22.574%                   | 
 | Iron Head 20.804%                      | 
 | Swords Dance 19.356%                   | 
 | Grass Pledge 18.066%                   | 
 | Psychic Fangs 16.751%                  | 
 | Surf 13.060%                           | 
 | Crunch  9.574%                         | 
 | Outrage  7.761%                        | 
 | Ice Fang  6.213%                       | 
 | Shadow Claw  3.548%                    | 
 | Other 17.106%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Torkoal +10.711%                       | 
 | Rotom-Heat +5.529%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Flareon                                | 
 +----------------------------------------+ 
 | Raw count: 1417                        | 
 | Avg. weight: 0.0266495630023           | 
 | Viability Ceiling: 69                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Guts 71.563%                           | 
 | Flash Fire 28.437%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Toxic Orb 32.980%                      | 
 | Assault Vest 23.137%                   | 
 | Leftovers 22.244%                      | 
 | Flame Orb  9.781%                      | 
 | Choice Band  5.992%                    | 
 | Choice Scarf  2.146%                   | 
 | Other  3.719%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:252/252/0/0/4/0 42.731%        | 
 | Careful:252/4/0/0/252/0 17.505%        | 
 | Jolly:0/252/0/0/4/252  5.099%          | 
 | Jolly:4/252/0/0/0/252  4.962%          | 
 | Adamant:0/252/0/0/4/252  4.506%        | 
 | Adamant:248/252/0/0/8/0  3.606%        | 
 | Other 21.591%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Flare Blitz 83.474%                    | 
 | Superpower 56.084%                     | 
 | Facade 50.972%                         | 
 | Quick Attack 50.623%                   | 
 | Wish 28.977%                           | 
 | Protect 21.174%                        | 
 | Will-O-Wisp 17.625%                    | 
 | Substitute 17.538%                     | 
 | Nothing 12.965%                        | 
 | Body Slam 11.423%                      | 
 | Last Resort  7.306%                    | 
 | Rest  5.639%                           | 
 | Sleep Talk  5.596%                     | 
 | Fire Fang  5.225%                      | 
 | Double-Edge  4.535%                    | 
 | Fire Spin  3.474%                      | 
 | Other 17.370%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Turtonator +19.707%                    | 
 | Torkoal +5.083%                        | 
 | Rotom-Heat +1.112%                     | 
 | Carkol +0.626%                         | 
 | Heatmor +0.536%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Heatmor                                | 
 +----------------------------------------+ 
 | Raw count: 396                         | 
 | Avg. weight: 0.0295999430772           | 
 | Viability Ceiling: 66                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | White Smoke 49.956%                    | 
 | Gluttony 32.549%                       | 
 | Flash Fire 17.495%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 47.687%                       | 
 | Aguav Berry 29.142%                    | 
 | Assault Vest  5.886%                   | 
 | Rocky Helmet  5.192%                   | 
 | Choice Band  4.298%                    | 
 | Choice Specs  2.818%                   | 
 | Other  4.977%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Rash:0/164/0/232/0/112 47.294%         | 
 | Adamant:252/252/0/0/4/0 28.102%        | 
 | Quiet:252/4/0/252/0/0  6.655%          | 
 | Adamant:0/252/0/0/4/252  4.298%        | 
 | Bold:4/0/252/0/252/0  3.389%           | 
 | Adamant:0/4/252/0/252/0  3.017%        | 
 | Other  7.244%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Fire Lash 82.793%                      | 
 | Giga Drain 63.929%                     | 
 | Sucker Punch 55.578%                   | 
 | Low Kick 51.639%                       | 
 | Drain Punch 31.373%                    | 
 | Hone Claws 27.030%                     | 
 | Shadow Claw 23.073%                    | 
 | Flamethrower  9.736%                   | 
 | Focus Blast  9.113%                    | 
 | Throat Chop  9.042%                    | 
 | Fire Blast  6.763%                     | 
 | Thunder Punch  4.752%                  | 
 | Stockpile  3.488%                      | 
 | Spit Up  3.389%                        | 
 | Other 18.302%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +34.926%                     | 
 | Salazzle +25.316%                      | 
 | Arcanine +10.899%                      | 
 | Centiskorch +7.457%                    | 
 | Carkol +2.883%                         | 
 | Flareon +2.303%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Carkol                                 | 
 +----------------------------------------+ 
 | Raw count: 154                         | 
 | Avg. weight: 0.0180280527645           | 
 | Viability Ceiling: 62                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Flame Body 72.521%                     | 
 | Steam Engine 27.479%                   | 
 | Flash Fire  0.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Eviolite 99.276%                       | 
 | Other  0.724%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Impish:248/8/252/0/0/0 63.173%         | 
 | Hardy:0/0/0/4/252/252 11.973%          | 
 | Careful:248/8/0/0/252/0 10.159%        | 
 | Calm:248/0/8/0/252/0  8.954%           | 
 | Modest:248/0/0/252/8/0  4.936%         | 
 | Other  0.806%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Rapid Spin 84.005%                     | 
 | Stealth Rock 78.182%                   | 
 | Protect 67.321%                        | 
 | Rock Slide 55.349%                     | 
 | Will-O-Wisp 31.087%                    | 
 | Scald 19.812%                          | 
 | Spikes 19.797%                         | 
 | Reflect 18.719%                        | 
 | Explosion 11.452%                      | 
 | Other 14.276%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Centiskorch +45.285%                   | 
 | Ninetales +12.028%                     | 
 | Heatmor +11.456%                       | 
 | Flareon +10.100%                       | 
 | Arcanine +5.042%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Lampent                                | 
 +----------------------------------------+ 
 | Raw count: 58                          | 
 | Avg. weight: 0.0250636262483           | 
 | Viability Ceiling: 60                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Infiltrator 90.722%                    | 
 | Flash Fire  9.278%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Eviolite 100.000%                      | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Calm:252/0/168/0/88/0 90.538%          | 
 | Calm:252/0/4/0/252/0  6.649%           | 
 | Other  2.813%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Trick Room 100.000%                    | 
 | Clear Smog 99.816%                     | 
 | Pain Split 99.816%                     | 
 | Flamethrower 90.538%                   | 
 | Other  9.830%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +81.507%                     | 
 | Salazzle +43.439%                      | 
 | Ninetales +42.028%                     | 
 | Torkoal +30.334%                       | 
 | Rotom-Heat +13.127%                    | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Litwick                                | 
 +----------------------------------------+ 
 | Raw count: 11                          | 
 | Avg. weight: 0.430444068334            | 
 | Viability Ceiling: 66                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Flame Body 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Focus Sash 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Relaxed:0/36/236/0/236/0 100.000%      | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Taunt 100.000%                         | 
 | Trick Room 100.000%                    | 
 | Memento 100.000%                       | 
 | Pain Split 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Coalossal +81.606%                     | 
 | Centiskorch +77.329%                   | 
 | Chandelure +50.435%                    | 
 | Torkoal +30.573%                       | 
 | Rotom-Heat +20.059%                    | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
