 Total battles: 213067
 Avg. weight/team: 0.015
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Ferrothorn         | 88.82352% | 10388  | 84.662% | 9316   | 99.930% | 
 | 2    | Whimsicott         | 64.96216% | 7065   | 57.579% | 5653   | 60.638% | 
 | 3    | Appletun           | 61.46712% | 7099   | 57.857% | 5262   | 56.444% | 
 | 4    | Rotom-Mow          | 59.18529% | 6439   | 52.478% | 5083   | 54.524% | 
 | 5    | Roserade           | 51.45239% | 6220   | 50.693% | 4653   | 49.912% | 
 | 6    | Rillaboom          | 49.34227% | 5838   | 47.579% | 4195   | 44.999% | 
 | 7    | Ludicolo           | 34.29145% | 4474   | 36.463% | 3295   | 35.345% | 
 | 8    | Flapple            | 33.52331% | 4310   | 35.126% | 2990   | 32.073% | 
 | 9    | Shiftry            | 24.64594% | 2905   | 23.676% | 2091   | 22.430% | 
 | 10   | Dhelmise           | 20.57051% | 2777   | 22.632% | 2044   | 21.925% | 
 | 11   | Tsareena           | 18.02491% | 2548   | 20.766% | 1767   | 18.954% | 
 | 12   | Vileplume          | 17.00462% | 2109   | 17.188% | 1498   | 16.069% | 
 | 13   | Eldegoss           | 16.49174% | 2270   | 18.500% | 1607   | 17.238% | 
 | 14   | Leafeon            | 12.25204% | 1636   | 13.333% | 1124   | 12.057% | 
 | 15   | Silvally-Grass     |  7.32700% | 997    |  8.126% | 681    |  7.305% | 
 | 16   | Trevenant          |  7.31160% | 1247   | 10.163% | 897    |  9.622% | 
 | 17   | Gourgeist-Super    |  6.72171% | 893    |  7.278% | 682    |  7.316% | 
 | 18   | Bellossom          |  6.49394% | 903    |  7.359% | 639    |  6.854% | 
 | 19   | Abomasnow          |  6.42489% | 1075   |  8.761% | 814    |  8.732% | 
 | 20   | Shiinotic          |  4.88347% | 873    |  7.115% | 603    |  6.468% | 
 | 21   | Maractus           |  2.59649% | 469    |  3.822% | 357    |  3.829% | 
 | 22   | Cherrim            |  1.77295% | 319    |  2.600% | 225    |  2.414% | 
 | 23   | Gourgeist-Small    |  1.40570% | 201    |  1.638% | 140    |  1.502% | 
 | 24   | Gourgeist          |  0.60168% | 133    |  1.084% | 93     |  0.998% | 
 | 25   | Roselia            |  0.56456% | 90     |  0.733% | 78     |  0.837% | 
 | 26   | Cottonee           |  0.34873% | 50     |  0.407% | 39     |  0.418% | 
 | 27   | Gloom              |  0.34506% | 50     |  0.407% | 34     |  0.365% | 
 | 28   | Ferroseed          |  0.12572% | 18     |  0.147% | 17     |  0.182% | 
 | 29   | Gourgeist-Large    |  0.10990% | 16     |  0.130% | 14     |  0.150% | 
 | 30   | Gossifleur         |  0.07256% | 19     |  0.155% | 17     |  0.182% | 
 | 31   | Pumpkaboo-Super    |  0.06905% | 11     |  0.090% | 10     |  0.107% | 
 | 32   | Thwackey           |  0.04853% | 8      |  0.065% | 6      |  0.064% | 
 | 33   | Applin             |  0.04319% | 9      |  0.073% | 8      |  0.086% | 
 | 34   | Budew              |  0.01181% | 2      |  0.016% | 2      |  0.021% | 
 | 35   | Lombre             |  0.00591% | 1      |  0.008% | 1      |  0.011% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
