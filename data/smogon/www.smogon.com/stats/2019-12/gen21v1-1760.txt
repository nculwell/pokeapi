 Total battles: 111
 Avg. weight/team: 0.003
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Heracross          | 75.64534% | 59     | 26.577% | 25     | 67.568% | 
 | 2    | Porygon2           | 39.03584% | 14     |  6.306% | 12     | 32.432% | 
 | 3    | Starmie            | 39.03584% | 30     | 13.514% | 4      | 10.811% | 
 | 4    | Suicune            | 36.60950% | 26     | 11.712% | 9      | 24.324% | 
 | 5    | Umbreon            | 36.60950% | 28     | 12.613% | 9      | 24.324% | 
 | 6    | Crobat             | 13.81931% | 8      |  3.604% | 1      |  2.703% | 
 | 7    | Persian            | 13.81931% | 8      |  3.604% | 4      | 10.811% | 
 | 8    | Jumpluff           | 13.81931% | 7      |  3.153% | 2      |  5.405% | 
 | 9    | Tauros             | 10.53534% | 10     |  4.505% | 2      |  5.405% | 
 | 10   | Venusaur           | 10.53534% | 14     |  6.306% | 6      | 16.216% | 
 | 11   | Electrode          | 10.53534% | 9      |  4.054% | 4      | 10.811% | 
 | 12   | Golem              |  0.00000% | 3      |  1.351% | 1      |  2.703% | 
 | 13   | Pinsir             |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 14   | Lapras             |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 15   | Quagsire           |  0.00000% | 3      |  1.351% | 2      |  5.405% | 
 | 16   | Cloyster           |  0.00000% | 5      |  2.252% | 2      |  5.405% | 
 | 17   | Dragonite          |  0.00000% | 7      |  3.153% | 1      |  2.703% | 
 | 18   | Jynx               |  0.00000% | 7      |  3.153% | 4      | 10.811% | 
 | 19   | Vaporeon           |  0.00000% | 9      |  4.054% | 3      |  8.108% | 
 | 20   | Piloswine          |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 21   | Snorlax            |  0.00000% | 4      |  1.802% | 0      |  0.000% | 
 | 22   | Raikou             |  0.00000% | 33     | 14.865% | 11     | 29.730% | 
 | 23   | Kangaskhan         |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 24   | Arbok              |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 25   | Ursaring           |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 26   | Xatu               |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 27   | Muk                |  0.00000% | 4      |  1.802% | 2      |  5.405% | 
 | 28   | Poliwrath          |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 29   | Machamp            |  0.00000% | 27     | 12.162% | 11     | 29.730% | 
 | 30   | Marowak            |  0.00000% | 17     |  7.658% | 6      | 16.216% | 
 | 31   | Haunter            |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 32   | Electabuzz         |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 33   | Wigglytuff         |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 34   | Sudowoodo          |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 35   | Arcanine           |  0.00000% | 2      |  0.901% | 0      |  0.000% | 
 | 36   | Scizor             |  0.00000% | 9      |  4.054% | 4      | 10.811% | 
 | 37   | Croconaw           |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 38   | Spinarak           |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 39   | Shuckle            |  0.00000% | 8      |  3.604% | 2      |  5.405% | 
 | 40   | Rhydon             |  0.00000% | 3      |  1.351% | 1      |  2.703% | 
 | 41   | Exeggutor          |  0.00000% | 8      |  3.604% | 1      |  2.703% | 
 | 42   | Entei              |  0.00000% | 3      |  1.351% | 1      |  2.703% | 
 | 43   | Blastoise          |  0.00000% | 2      |  0.901% | 2      |  5.405% | 
 | 44   | Alakazam           |  0.00000% | 13     |  5.856% | 4      | 10.811% | 
 | 45   | Raticate           |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 46   | Blissey            |  0.00000% | 23     | 10.360% | 9      | 24.324% | 
 | 47   | Qwilfish           |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 48   | Articuno           |  0.00000% | 2      |  0.901% | 1      |  2.703% | 
 | 49   | Furret             |  0.00000% | 2      |  0.901% | 1      |  2.703% | 
 | 50   | Typhlosion         |  0.00000% | 4      |  1.802% | 3      |  8.108% | 
 | 51   | Ditto              |  0.00000% | 5      |  2.252% | 4      | 10.811% | 
 | 52   | Vulpix             |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 53   | Cyndaquil          |  0.00000% | 2      |  0.901% | 1      |  2.703% | 
 | 54   | Aerodactyl         |  0.00000% | 3      |  1.351% | 2      |  5.405% | 
 | 55   | Zapdos             |  0.00000% | 15     |  6.757% | 8      | 21.622% | 
 | 56   | Gyarados           |  0.00000% | 3      |  1.351% | 0      |  0.000% | 
 | 57   | Tentacruel         |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 58   | Steelix            |  0.00000% | 6      |  2.703% | 1      |  2.703% | 
 | 59   | Miltank            |  0.00000% | 15     |  6.757% | 11     | 29.730% | 
 | 60   | Misdreavus         |  0.00000% | 6      |  2.703% | 3      |  8.108% | 
 | 61   | Wartortle          |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 62   | Espeon             |  0.00000% | 2      |  0.901% | 0      |  0.000% | 
 | 63   | Houndoom           |  0.00000% | 3      |  1.351% | 2      |  5.405% | 
 | 64   | Gengar             |  0.00000% | 19     |  8.559% | 8      | 21.622% | 
 | 65   | Rattata            |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 66   | Lanturn            |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 67   | Totodile           |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 68   | Meowth             |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 69   | Wobbuffet          |  0.00000% | 2      |  0.901% | 1      |  2.703% | 
 | 70   | Ampharos           |  0.00000% | 3      |  1.351% | 2      |  5.405% | 
 | 71   | Vileplume          |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 72   | Scyther            |  0.00000% | 2      |  0.901% | 1      |  2.703% | 
 | 73   | Dodrio             |  0.00000% | 1      |  0.450% | 1      |  2.703% | 
 | 74   | Wooper             |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 75   | Forretress         |  0.00000% | 7      |  3.153% | 5      | 13.514% | 
 | 76   | Togetic            |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 77   | Tyranitar          |  0.00000% | 19     |  8.559% | 5      | 13.514% | 
 | 78   | Skarmory           |  0.00000% | 5      |  2.252% | 2      |  5.405% | 
 | 79   | Nidoking           |  0.00000% | 8      |  3.604% | 2      |  5.405% | 
 | 80   | Meganium           |  0.00000% | 4      |  1.802% | 1      |  2.703% | 
 | 81   | Feraligatr         |  0.00000% | 3      |  1.351% | 0      |  0.000% | 
 | 82   | Mr. Mime           |  0.00000% | 2      |  0.901% | 1      |  2.703% | 
 | 83   | Gastly             |  0.00000% | 1      |  0.450% | 0      |  0.000% | 
 | 84   | Charizard          |  0.00000% | 7      |  3.153% | 1      |  2.703% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
