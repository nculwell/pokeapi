 Total battles: 1
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Metang             | 50.00000% | 1      | 50.000% | 1      | 300.000% | 
 | 2    | Smeargle           | 50.00000% | 1      | 50.000% | 0      |  0.000% | 
 | 3    | Metagross          | 50.00000% | 1      | 50.000% | 0      |  0.000% | 
 | 4    | Porygon2           | 50.00000% | 1      | 50.000% | 1      | 300.000% | 
 | 5    | Charmander         | 50.00000% | 1      | 50.000% | 0      |  0.000% | 
 | 6    | Grovyle            | 50.00000% | 1      | 50.000% | 0      |  0.000% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
