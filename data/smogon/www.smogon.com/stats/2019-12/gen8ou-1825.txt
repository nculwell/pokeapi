 Total battles: 2647791
 Avg. weight/team: 0.001
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Corviknight        | 39.23359% | 1317788 | 24.885% | 999295 | 24.717% | 
 | 2    | Dragapult          | 37.04172% | 1932908 | 36.500% | 1416369 | 35.034% | 
 | 3    | Seismitoad         | 31.23876% | 777745 | 14.687% | 659863 | 16.322% | 
 | 4    | Darmanitan-Galar   | 27.62771% | 1645980 | 31.082% | 1244641 | 30.786% | 
 | 5    | Clefable           | 27.42748% | 587516 | 11.094% | 464336 | 11.485% | 
 | 6    | Excadrill          | 25.54023% | 1278523 | 24.143% | 998171 | 24.690% | 
 | 7    | Toxapex            | 24.65085% | 797134 | 15.053% | 633583 | 15.672% | 
 | 8    | Rotom-Heat         | 23.14916% | 517069 |  9.764% | 433149 | 10.714% | 
 | 9    | Ditto              | 23.11415% | 844214 | 15.942% | 567347 | 14.033% | 
 | 10   | Aegislash          | 20.63743% | 613642 | 11.588% | 457947 | 11.327% | 
 | 11   | Hydreigon          | 20.60999% | 703179 | 13.279% | 534675 | 13.225% | 
 | 12   | Ferrothorn         | 19.06008% | 1273153 | 24.042% | 1070596 | 26.481% | 
 | 13   | Mandibuzz          | 16.57349% | 422072 |  7.970% | 327354 |  8.097% | 
 | 14   | Hatterene          | 15.10292% | 741914 | 14.010% | 571392 | 14.133% | 
 | 15   | Cinderace          | 13.65277% | 1029905 | 19.448% | 780210 | 19.298% | 
 | 16   | Tyranitar          | 11.20848% | 564750 | 10.665% | 449396 | 11.116% | 
 | 17   | Dugtrio            | 10.94590% | 259644 |  4.903% | 199674 |  4.939% | 
 | 18   | Conkeldurr         | 10.85594% | 387795 |  7.323% | 300128 |  7.424% | 
 | 19   | Dracovish          | 10.29605% | 1024461 | 19.346% | 755685 | 18.692% | 
 | 20   | Grimmsnarl         |  9.84346% | 891322 | 16.831% | 690057 | 17.068% | 
 | 21   | Rotom-Wash         |  9.83338% | 640016 | 12.086% | 534764 | 13.227% | 
 | 22   | Sylveon            |  9.10866% | 138971 |  2.624% | 105261 |  2.604% | 
 | 23   | Bisharp            |  8.73496% | 291311 |  5.501% | 214713 |  5.311% | 
 | 24   | Toxtricity         |  7.14304% | 770364 | 14.547% | 556104 | 13.755% | 
 | 25   | Kommo-o            |  6.98928% | 169579 |  3.202% | 124636 |  3.083% | 
 | 26   | Hippowdon          |  6.49960% | 92724  |  1.751% | 79139  |  1.957% | 
 | 27   | Corsola-Galar      |  6.15665% | 784852 | 14.821% | 618664 | 15.303% | 
 | 28   | Gyarados           |  5.53916% | 560817 | 10.590% | 404042 |  9.994% | 
 | 29   | Togekiss           |  5.40812% | 452341 |  8.542% | 337346 |  8.344% | 
 | 30   | Gastrodon          |  5.38343% | 158371 |  2.991% | 124709 |  3.085% | 
 | 31   | Mew                |  5.15139% | 117791 |  2.224% | 95466  |  2.361% | 
 | 32   | Mimikyu            |  4.72795% | 332280 |  6.275% | 239486 |  5.924% | 
 | 33   | Vaporeon           |  4.45194% | 47850  |  0.904% | 37833  |  0.936% | 
 | 34   | Snorlax            |  4.42215% | 120446 |  2.274% | 90051  |  2.227% | 
 | 35   | Jellicent          |  4.10628% | 129754 |  2.450% | 104324 |  2.580% | 
 | 36   | Obstagoon          |  4.04276% | 239151 |  4.516% | 185304 |  4.583% | 
 | 37   | Crawdaunt          |  3.93568% | 118422 |  2.236% | 89131  |  2.205% | 
 | 38   | Hawlucha           |  3.83608% | 262449 |  4.956% | 183224 |  4.532% | 
 | 39   | Cloyster           |  3.73483% | 130457 |  2.464% | 97968  |  2.423% | 
 | 40   | Rotom-Mow          |  3.44924% | 67703  |  1.278% | 54834  |  1.356% | 
 | 41   | Dracozolt          |  3.38345% | 289520 |  5.467% | 215214 |  5.323% | 
 | 42   | Pelipper           |  3.21824% | 395943 |  7.477% | 356642 |  8.821% | 
 | 43   | Gengar             |  2.85371% | 207187 |  3.912% | 147975 |  3.660% | 
 | 44   | Reuniclus          |  2.57300% | 82187  |  1.552% | 58946  |  1.458% | 
 | 45   | Avalugg            |  2.40194% | 70349  |  1.328% | 52110  |  1.289% | 
 | 46   | Barraskewda        |  2.29530% | 393261 |  7.426% | 273205 |  6.758% | 
 | 47   | Milotic            |  1.67017% | 93621  |  1.768% | 73016  |  1.806% | 
 | 48   | Glalie             |  1.65658% | 20862  |  0.394% | 16092  |  0.398% | 
 | 49   | Sigilyph           |  1.62998% | 21321  |  0.403% | 15785  |  0.390% | 
 | 50   | Centiskorch        |  1.62039% | 170275 |  3.215% | 130397 |  3.225% | 
 | 51   | Lucario            |  1.51005% | 85888  |  1.622% | 61809  |  1.529% | 
 | 52   | Xatu               |  1.45068% | 28862  |  0.545% | 22544  |  0.558% | 
 | 53   | Polteageist        |  1.44617% | 195727 |  3.696% | 137645 |  3.405% | 
 | 54   | Mamoswine          |  1.44305% | 68900  |  1.301% | 55276  |  1.367% | 
 | 55   | Sirfetch'd         |  1.38799% | 115650 |  2.184% | 86949  |  2.151% | 
 | 56   | Gigalith           |  1.38427% | 19396  |  0.366% | 16161  |  0.400% | 
 | 57   | Ribombee           |  1.37172% | 158528 |  2.994% | 138499 |  3.426% | 
 | 58   | Diggersby          |  1.25426% | 33418  |  0.631% | 25231  |  0.624% | 
 | 59   | Arcanine           |  1.10939% | 146642 |  2.769% | 114169 |  2.824% | 
 | 60   | Chandelure         |  1.10402% | 137109 |  2.589% | 104051 |  2.574% | 
 | 61   | Coalossal          |  0.99778% | 103696 |  1.958% | 82649  |  2.044% | 
 | 62   | Flareon            |  0.97114% | 11311  |  0.214% | 8748   |  0.216% | 
 | 63   | Araquanid          |  0.97030% | 87751  |  1.657% | 75332  |  1.863% | 
 | 64   | Charizard          |  0.90212% | 139851 |  2.641% | 105091 |  2.599% | 
 | 65   | Drednaw            |  0.83379% | 104710 |  1.977% | 74050  |  1.832% | 
 | 66   | Duraludon          |  0.81370% | 128902 |  2.434% | 98575  |  2.438% | 
 | 67   | Pyukumuku          |  0.80877% | 41288  |  0.780% | 32371  |  0.801% | 
 | 68   | Weezing-Galar      |  0.78458% | 137202 |  2.591% | 108469 |  2.683% | 
 | 69   | Rhyperior          |  0.75729% | 35333  |  0.667% | 27030  |  0.669% | 
 | 70   | Copperajah         |  0.74684% | 133003 |  2.512% | 106887 |  2.644% | 
 | 71   | Durant             |  0.73285% | 48312  |  0.912% | 38016  |  0.940% | 
 | 72   | Quagsire           |  0.66233% | 61545  |  1.162% | 47286  |  1.170% | 
 | 73   | Golisopod          |  0.64895% | 80193  |  1.514% | 62133  |  1.537% | 
 | 74   | Gardevoir          |  0.63884% | 81035  |  1.530% | 57981  |  1.434% | 
 | 75   | Dhelmise           |  0.63346% | 25651  |  0.484% | 19188  |  0.475% | 
 | 76   | Shiftry            |  0.60346% | 37574  |  0.710% | 28265  |  0.699% | 
 | 77   | Steelix            |  0.60321% | 34370  |  0.649% | 28144  |  0.696% | 
 | 78   | Ninetales          |  0.58755% | 37847  |  0.715% | 30444  |  0.753% | 
 | 79   | Mantine            |  0.57994% | 42499  |  0.803% | 32765  |  0.810% | 
 | 80   | Eldegoss           |  0.56509% | 70560  |  1.332% | 52476  |  1.298% | 
 | 81   | Runerigus          |  0.56373% | 123122 |  2.325% | 103748 |  2.566% | 
 | 82   | Salazzle           |  0.55388% | 91768  |  1.733% | 69966  |  1.731% | 
 | 83   | Indeedee           |  0.54451% | 56725  |  1.071% | 44231  |  1.094% | 
 | 84   | Haxorus            |  0.53108% | 83535  |  1.577% | 57855  |  1.431% | 
 | 85   | Noivern            |  0.50033% | 44679  |  0.844% | 33718  |  0.834% | 
 | 86   | Shuckle            |  0.46157% | 81528  |  1.540% | 73017  |  1.806% | 
 | 87   | Heliolisk          |  0.45654% | 60217  |  1.137% | 45576  |  1.127% | 
 | 88   | Appletun           |  0.42131% | 132057 |  2.494% | 94340  |  2.333% | 
 | 89   | Galvantula         |  0.40411% | 161149 |  3.043% | 143401 |  3.547% | 
 | 90   | Whimsicott         |  0.40195% | 81713  |  1.543% | 66098  |  1.635% | 
 | 91   | Flygon             |  0.40190% | 52973  |  1.000% | 39257  |  0.971% | 
 | 92   | Torkoal            |  0.38606% | 71568  |  1.351% | 65193  |  1.613% | 
 | 93   | Pangoro            |  0.37943% | 8503   |  0.161% | 6502   |  0.161% | 
 | 94   | Bewear             |  0.35739% | 28116  |  0.531% | 20638  |  0.510% | 
 | 95   | Goodra             |  0.34587% | 89957  |  1.699% | 65223  |  1.613% | 
 | 96   | Flapple            |  0.34066% | 57997  |  1.095% | 40398  |  0.999% | 
 | 97   | Vikavolt           |  0.33377% | 41599  |  0.786% | 31936  |  0.790% | 
 | 98   | Octillery          |  0.33052% | 8371   |  0.158% | 6350   |  0.157% | 
 | 99   | Umbreon            |  0.32769% | 41701  |  0.787% | 31022  |  0.767% | 
 | 100  | Alcremie           |  0.32450% | 55864  |  1.055% | 39306  |  0.972% | 
 | 101  | Toxicroak          |  0.32008% | 29364  |  0.554% | 21449  |  0.531% | 
 | 102  | Wishiwashi         |  0.31450% | 3563   |  0.067% | 2651   |  0.066% | 
 | 103  | Bronzong           |  0.30256% | 41045  |  0.775% | 34593  |  0.856% | 
 | 104  | Sandaconda         |  0.29338% | 61999  |  1.171% | 47438  |  1.173% | 
 | 105  | Orbeetle           |  0.28109% | 93935  |  1.774% | 78225  |  1.935% | 
 | 106  | Machamp            |  0.26830% | 45872  |  0.866% | 36021  |  0.891% | 
 | 107  | Eiscue             |  0.26745% | 124084 |  2.343% | 87590  |  2.167% | 
 | 108  | Braviary           |  0.25936% | 36686  |  0.693% | 26062  |  0.645% | 
 | 109  | Vanilluxe          |  0.25325% | 28085  |  0.530% | 23445  |  0.580% | 
 | 110  | Froslass           |  0.24883% | 20073  |  0.379% | 17108  |  0.423% | 
 | 111  | Pincurchin         |  0.24770% | 65807  |  1.243% | 56137  |  1.389% | 
 | 112  | Rillaboom          |  0.24173% | 87140  |  1.646% | 64415  |  1.593% | 
 | 113  | Morpeko            |  0.23848% | 80151  |  1.514% | 60826  |  1.505% | 
 | 114  | Roserade           |  0.23635% | 44862  |  0.847% | 33746  |  0.835% | 
 | 115  | Weavile            |  0.19903% | 40571  |  0.766% | 29537  |  0.731% | 
 | 116  | Lapras             |  0.19554% | 71322  |  1.347% | 54559  |  1.350% | 
 | 117  | Tsareena           |  0.19454% | 38436  |  0.726% | 27968  |  0.692% | 
 | 118  | Falinks            |  0.17317% | 83913  |  1.585% | 56333  |  1.393% | 
 | 119  | Frosmoth           |  0.17223% | 96207  |  1.817% | 64479  |  1.595% | 
 | 120  | Jolteon            |  0.16017% | 52810  |  0.997% | 40116  |  0.992% | 
 | 121  | Inteleon           |  0.15198% | 105083 |  1.984% | 74096  |  1.833% | 
 | 122  | Indeedee-F         |  0.15095% | 18137  |  0.342% | 13819  |  0.342% | 
 | 123  | Dubwool            |  0.14996% | 41234  |  0.779% | 30173  |  0.746% | 
 | 124  | Cursola            |  0.14804% | 54664  |  1.032% | 39305  |  0.972% | 
 | 125  | Hitmontop          |  0.14574% | 38118  |  0.720% | 29495  |  0.730% | 
 | 126  | Boltund            |  0.14296% | 59455  |  1.123% | 44028  |  1.089% | 
 | 127  | Ludicolo           |  0.14203% | 42684  |  0.806% | 31063  |  0.768% | 
 | 128  | Espeon             |  0.12930% | 68297  |  1.290% | 49869  |  1.233% | 
 | 129  | Shedinja           |  0.12827% | 13930  |  0.263% | 9329   |  0.231% | 
 | 130  | Rotom-Fan          |  0.12712% | 7731   |  0.146% | 6190   |  0.153% | 
 | 131  | Vileplume          |  0.12113% | 16566  |  0.313% | 12214  |  0.302% | 
 | 132  | Malamar            |  0.12058% | 27801  |  0.525% | 20154  |  0.499% | 
 | 133  | Mr. Rime           |  0.11585% | 42447  |  0.802% | 29634  |  0.733% | 
 | 134  | Arctozolt          |  0.11445% | 22883  |  0.432% | 16549  |  0.409% | 
 | 135  | Raichu             |  0.11102% | 11425  |  0.216% | 8189   |  0.203% | 
 | 136  | Mudsdale           |  0.10503% | 16764  |  0.317% | 13039  |  0.323% | 
 | 137  | Golurk             |  0.09155% | 9472   |  0.179% | 7164   |  0.177% | 
 | 138  | Arctovish          |  0.09119% | 20462  |  0.386% | 15207  |  0.376% | 
 | 139  | Gallade            |  0.08775% | 23404  |  0.442% | 16535  |  0.409% | 
 | 140  | Rapidash-Galar     |  0.08032% | 47906  |  0.905% | 31923  |  0.790% | 
 | 141  | Passimian          |  0.07854% | 5703   |  0.108% | 4385   |  0.108% | 
 | 142  | Cramorant          |  0.07506% | 60604  |  1.144% | 46130  |  1.141% | 
 | 143  | Rotom              |  0.07463% | 2121   |  0.040% | 1639   |  0.041% | 
 | 144  | Drapion            |  0.07352% | 28657  |  0.541% | 22383  |  0.554% | 
 | 145  | Claydol            |  0.07044% | 34239  |  0.647% | 26747  |  0.662% | 
 | 146  | Garbodor           |  0.06790% | 10564  |  0.199% | 8140   |  0.201% | 
 | 147  | Sableye            |  0.06676% | 25127  |  0.474% | 20335  |  0.503% | 
 | 148  | Stonjourner        |  0.06496% | 12480  |  0.236% | 9651   |  0.239% | 
 | 149  | Dusclops           |  0.06332% | 22023  |  0.416% | 17657  |  0.437% | 
 | 150  | Accelgor           |  0.06097% | 34032  |  0.643% | 26433  |  0.654% | 
 | 151  | Grapploct          |  0.06071% | 35554  |  0.671% | 26670  |  0.660% | 
 | 152  | Scrafty            |  0.05648% | 24036  |  0.454% | 18086  |  0.447% | 
 | 153  | Glaceon            |  0.05228% | 8105   |  0.153% | 5859   |  0.145% | 
 | 154  | Hitmonlee          |  0.05137% | 19943  |  0.377% | 14146  |  0.350% | 
 | 155  | Togedemaru         |  0.05098% | 12604  |  0.238% | 10106  |  0.250% | 
 | 156  | Cofagrigus         |  0.04222% | 22361  |  0.422% | 18240  |  0.451% | 
 | 157  | Cinccino           |  0.04199% | 27802  |  0.525% | 20838  |  0.515% | 
 | 158  | Greedent           |  0.04172% | 28495  |  0.538% | 21476  |  0.531% | 
 | 159  | Kingler            |  0.04159% | 14539  |  0.275% | 10318  |  0.255% | 
 | 160  | Butterfree         |  0.03906% | 29210  |  0.552% | 20892  |  0.517% | 
 | 161  | Musharna           |  0.03756% | 11691  |  0.221% | 8297   |  0.205% | 
 | 162  | Abomasnow          |  0.03568% | 20897  |  0.395% | 16988  |  0.420% | 
 | 163  | Drifblim           |  0.03521% | 14190  |  0.268% | 10498  |  0.260% | 
 | 164  | Leafeon            |  0.03327% | 17204  |  0.325% | 12506  |  0.309% | 
 | 165  | Pikachu            |  0.03309% | 9361   |  0.177% | 6834   |  0.169% | 
 | 166  | Drampa             |  0.03089% | 15210  |  0.287% | 10934  |  0.270% | 
 | 167  | Type: Null         |  0.03063% | 4572   |  0.086% | 3496   |  0.086% | 
 | 168  | Aromatisse         |  0.02983% | 5654   |  0.107% | 4174   |  0.103% | 
 | 169  | Klinklang          |  0.02966% | 7222   |  0.136% | 5464   |  0.135% | 
 | 170  | Perrserker         |  0.02786% | 24626  |  0.465% | 18171  |  0.449% | 
 | 171  | Barbaracle         |  0.02711% | 17527  |  0.331% | 12230  |  0.303% | 
 | 172  | Escavalier         |  0.02646% | 15036  |  0.284% | 11276  |  0.279% | 
 | 173  | Slurpuff           |  0.02610% | 10925  |  0.206% | 9197   |  0.227% | 
 | 174  | Maractus           |  0.02589% | 8156   |  0.154% | 6167   |  0.153% | 
 | 175  | Sawk               |  0.02514% | 3847   |  0.073% | 2807   |  0.069% | 
 | 176  | Roselia            |  0.02488% | 922    |  0.017% | 710    |  0.018% | 
 | 177  | Gourgeist-Super    |  0.02467% | 6260   |  0.118% | 4875   |  0.121% | 
 | 178  | Rotom-Frost        |  0.02189% | 4552   |  0.086% | 3397   |  0.084% | 
 | 179  | Diglett            |  0.02187% | 3305   |  0.062% | 2595   |  0.064% | 
 | 180  | Shiinotic          |  0.02119% | 10555  |  0.199% | 7867   |  0.195% | 
 | 181  | Silvally-Steel     |  0.02045% | 5838   |  0.110% | 4410   |  0.109% | 
 | 182  | Silvally-Rock      |  0.02032% | 549    |  0.010% | 382    |  0.009% | 
 | 183  | Doublade           |  0.01956% | 11544  |  0.218% | 8742   |  0.216% | 
 | 184  | Mr. Mime-Galar     |  0.01915% | 14028  |  0.265% | 10080  |  0.249% | 
 | 185  | Hitmonchan         |  0.01825% | 10064  |  0.190% | 7394   |  0.183% | 
 | 186  | Lanturn            |  0.01626% | 9070   |  0.171% | 6993   |  0.173% | 
 | 187  | Crustle            |  0.01564% | 9452   |  0.178% | 7239   |  0.179% | 
 | 188  | Piloswine          |  0.01554% | 2755   |  0.052% | 2294   |  0.057% | 
 | 189  | Solrock            |  0.01512% | 2049   |  0.039% | 1671   |  0.041% | 
 | 190  | Manectric          |  0.01490% | 10984  |  0.207% | 8486   |  0.210% | 
 | 191  | Silvally-Poison    |  0.01486% | 1568   |  0.030% | 1178   |  0.029% | 
 | 192  | Qwilfish           |  0.01476% | 7214   |  0.136% | 6080   |  0.150% | 
 | 193  | Silvally-Fairy     |  0.01451% | 5340   |  0.101% | 3991   |  0.099% | 
 | 194  | Ninjask            |  0.01286% | 13406  |  0.253% | 10125  |  0.250% | 
 | 195  | Silvally-Electric  |  0.01275% | 3692   |  0.070% | 2674   |  0.066% | 
 | 196  | Trevenant          |  0.01251% | 13327  |  0.252% | 9867   |  0.244% | 
 | 197  | Dusknoir           |  0.01178% | 15231  |  0.288% | 11619  |  0.287% | 
 | 198  | Silvally-Water     |  0.01126% | 1654   |  0.031% | 1269   |  0.031% | 
 | 199  | Silvally           |  0.01035% | 6548   |  0.124% | 5071   |  0.125% | 
 | 200  | Beartic            |  0.00995% | 7819   |  0.148% | 5366   |  0.133% | 
 | 201  | Thievul            |  0.00906% | 12481  |  0.236% | 9094   |  0.225% | 
 | 202  | Turtonator         |  0.00870% | 15161  |  0.286% | 11558  |  0.286% | 
 | 203  | Silvally-Ghost     |  0.00837% | 2129   |  0.040% | 1614   |  0.040% | 
 | 204  | Rhydon             |  0.00745% | 6084   |  0.115% | 4733   |  0.117% | 
 | 205  | Darumaka-Galar     |  0.00726% | 266    |  0.005% | 188    |  0.005% | 
 | 206  | Oranguru           |  0.00671% | 3656   |  0.069% | 2829   |  0.070% | 
 | 207  | Drilbur            |  0.00650% | 112    |  0.002% | 85     |  0.002% | 
 | 208  | Bellossom          |  0.00633% | 8947   |  0.169% | 6489   |  0.161% | 
 | 209  | Mawile             |  0.00612% | 6304   |  0.119% | 4793   |  0.119% | 
 | 210  | Trapinch           |  0.00549% | 725    |  0.014% | 510    |  0.013% | 
 | 211  | Delibird           |  0.00544% | 3320   |  0.063% | 2571   |  0.064% | 
 | 212  | Swoobat            |  0.00541% | 3850   |  0.073% | 2971   |  0.073% | 
 | 213  | Silvally-Fire      |  0.00532% | 3157   |  0.060% | 2311   |  0.057% | 
 | 214  | Stunfisk-Galar     |  0.00506% | 7854   |  0.148% | 6413   |  0.159% | 
 | 215  | Skuntank           |  0.00505% | 3748   |  0.071% | 2787   |  0.069% | 
 | 216  | Noctowl            |  0.00468% | 3342   |  0.063% | 2432   |  0.060% | 
 | 217  | Gothitelle         |  0.00428% | 5419   |  0.102% | 3813   |  0.094% | 
 | 218  | Silvally-Flying    |  0.00423% | 1094   |  0.021% | 822    |  0.020% | 
 | 219  | Wobbuffet          |  0.00346% | 3751   |  0.071% | 2788   |  0.069% | 
 | 220  | Duosion            |  0.00317% | 586    |  0.011% | 424    |  0.010% | 
 | 221  | Unfezant           |  0.00291% | 4842   |  0.091% | 3588   |  0.089% | 
 | 222  | Mr. Mime           |  0.00271% | 2185   |  0.041% | 1688   |  0.042% | 
 | 223  | Silvally-Grass     |  0.00235% | 2315   |  0.044% | 1649   |  0.041% | 
 | 224  | Meowstic           |  0.00216% | 3049   |  0.058% | 2317   |  0.057% | 
 | 225  | Sudowoodo          |  0.00210% | 3083   |  0.058% | 2361   |  0.058% | 
 | 226  | Linoone-Galar      |  0.00203% | 2002   |  0.038% | 1606   |  0.040% | 
 | 227  | Dottler            |  0.00203% | 1728   |  0.033% | 1369   |  0.034% | 
 | 228  | Scorbunny          |  0.00192% | 307    |  0.006% | 250    |  0.006% | 
 | 229  | Drakloak           |  0.00187% | 542    |  0.010% | 400    |  0.010% | 
 | 230  | Liepard            |  0.00181% | 2852   |  0.054% | 2168   |  0.054% | 
 | 231  | Persian            |  0.00167% | 2564   |  0.048% | 1972   |  0.049% | 
 | 232  | Basculin           |  0.00162% | 2473   |  0.047% | 1826   |  0.045% | 
 | 233  | Magikarp           |  0.00143% | 663    |  0.013% | 532    |  0.013% | 
 | 234  | Gourgeist          |  0.00126% | 1017   |  0.019% | 760    |  0.019% | 
 | 235  | Onix               |  0.00122% | 2537   |  0.048% | 2184   |  0.054% | 
 | 236  | Silvally-Ground    |  0.00119% | 2393   |  0.045% | 1762   |  0.044% | 
 | 237  | Whiscash           |  0.00114% | 2026   |  0.038% | 1471   |  0.036% | 
 | 238  | Morgrem            |  0.00113% | 1723   |  0.033% | 1395   |  0.035% | 
 | 239  | Hattrem            |  0.00103% | 1334   |  0.025% | 1042   |  0.026% | 
 | 240  | Silvally-Dark      |  0.00102% | 1102   |  0.021% | 840    |  0.021% | 
 | 241  | Ferroseed          |  0.00102% | 691    |  0.013% | 584    |  0.014% | 
 | 242  | Raboot             |  0.00098% | 749    |  0.014% | 583    |  0.014% | 
 | 243  | Beheeyem           |  0.00091% | 2061   |  0.039% | 1456   |  0.036% | 
 | 244  | Throh              |  0.00089% | 1347   |  0.025% | 1039   |  0.026% | 
 | 245  | Swirlix            |  0.00082% | 1203   |  0.023% | 1094   |  0.027% | 
 | 246  | Clobbopus          |  0.00077% | 174    |  0.003% | 136    |  0.003% | 
 | 247  | Eevee              |  0.00077% | 1787   |  0.034% | 1260   |  0.031% | 
 | 248  | Charjabug          |  0.00072% | 1082   |  0.020% | 989    |  0.024% | 
 | 249  | Silvally-Fighting  |  0.00068% | 731    |  0.014% | 501    |  0.012% | 
 | 250  | Gurdurr            |  0.00067% | 958    |  0.018% | 710    |  0.018% | 
 | 251  | Cottonee           |  0.00067% | 1395   |  0.026% | 1120   |  0.028% | 
 | 252  | Togetic            |  0.00059% | 1535   |  0.029% | 1204   |  0.030% | 
 | 253  | Klang              |  0.00054% | 297    |  0.006% | 236    |  0.006% | 
 | 254  | Shellder           |  0.00051% | 132    |  0.002% | 86     |  0.002% | 
 | 255  | Cherrim            |  0.00050% | 1676   |  0.032% | 1212   |  0.030% | 
 | 256  | Wooloo             |  0.00047% | 744    |  0.014% | 584    |  0.014% | 
 | 257  | Shelmet            |  0.00046% | 247    |  0.005% | 205    |  0.005% | 
 | 258  | Wailord            |  0.00046% | 2625   |  0.050% | 1979   |  0.049% | 
 | 259  | Ponyta-Galar       |  0.00045% | 463    |  0.009% | 324    |  0.008% | 
 | 260  | Heatmor            |  0.00045% | 2688   |  0.051% | 2011   |  0.050% | 
 | 261  | Carkol             |  0.00044% | 1595   |  0.030% | 1337   |  0.033% | 
 | 262  | Vespiquen          |  0.00042% | 1788   |  0.034% | 1287   |  0.032% | 
 | 263  | Gourgeist-Small    |  0.00041% | 1126   |  0.021% | 798    |  0.020% | 
 | 264  | Cutiefly           |  0.00039% | 460    |  0.009% | 423    |  0.010% | 
 | 265  | Clefairy           |  0.00036% | 825    |  0.016% | 629    |  0.016% | 
 | 266  | Seaking            |  0.00031% | 568    |  0.011% | 372    |  0.009% | 
 | 267  | Lunatone           |  0.00031% | 1702   |  0.032% | 1193   |  0.030% | 
 | 268  | Sneasel            |  0.00028% | 659    |  0.012% | 484    |  0.012% | 
 | 269  | Solosis            |  0.00027% | 458    |  0.009% | 393    |  0.010% | 
 | 270  | Silvally-Psychic   |  0.00027% | 399    |  0.008% | 265    |  0.007% | 
 | 271  | Riolu              |  0.00023% | 1113   |  0.021% | 755    |  0.019% | 
 | 272  | Silvally-Dragon    |  0.00023% | 822    |  0.016% | 598    |  0.015% | 
 | 273  | Munchlax           |  0.00023% | 933    |  0.018% | 622    |  0.015% | 
 | 274  | Impidimp           |  0.00022% | 164    |  0.003% | 134    |  0.003% | 
 | 275  | Wooper             |  0.00020% | 104    |  0.002% | 89     |  0.002% | 
 | 276  | Gloom              |  0.00018% | 1118   |  0.021% | 768    |  0.019% | 
 | 277  | Mareanie           |  0.00017% | 205    |  0.004% | 159    |  0.004% | 
 | 278  | Silvally-Bug       |  0.00017% | 296    |  0.006% | 198    |  0.005% | 
 | 279  | Meowth             |  0.00017% | 657    |  0.012% | 493    |  0.012% | 
 | 280  | Mantyke            |  0.00016% | 233    |  0.004% | 172    |  0.004% | 
 | 281  | Gothorita          |  0.00016% | 148    |  0.003% | 101    |  0.002% | 
 | 282  | Pupitar            |  0.00015% | 170    |  0.003% | 141    |  0.003% | 
 | 283  | Snom               |  0.00015% | 1042   |  0.020% | 762    |  0.019% | 
 | 284  | Haunter            |  0.00014% | 1034   |  0.020% | 771    |  0.019% | 
 | 285  | Gourgeist-Large    |  0.00012% | 325    |  0.006% | 255    |  0.006% | 
 | 286  | Nuzleaf            |  0.00012% | 49     |  0.001% | 37     |  0.001% | 
 | 287  | Thwackey           |  0.00012% | 296    |  0.006% | 222    |  0.005% | 
 | 288  | Silvally-Ice       |  0.00010% | 494    |  0.009% | 322    |  0.008% | 
 | 289  | Blipbug            |  0.00008% | 1201   |  0.023% | 1163   |  0.029% | 
 | 290  | Farfetch'd-Galar   |  0.00007% | 1053   |  0.020% | 795    |  0.020% | 
 | 291  | Woobat             |  0.00006% | 77     |  0.001% | 62     |  0.002% | 
 | 292  | Charmeleon         |  0.00006% | 117    |  0.002% | 105    |  0.003% | 
 | 293  | Palpitoad          |  0.00006% | 408    |  0.008% | 346    |  0.009% | 
 | 294  | Espurr             |  0.00006% | 59     |  0.001% | 44     |  0.001% | 
 | 295  | Wynaut             |  0.00006% | 201    |  0.004% | 173    |  0.004% | 
 | 296  | Arrokuda           |  0.00006% | 84     |  0.002% | 61     |  0.002% | 
 | 297  | Gossifleur         |  0.00005% | 239    |  0.005% | 165    |  0.004% | 
 | 298  | Rufflet            |  0.00005% | 132    |  0.002% | 94     |  0.002% | 
 | 299  | Vulpix             |  0.00004% | 130    |  0.002% | 91     |  0.002% | 
 | 300  | Metapod            |  0.00004% | 150    |  0.003% | 120    |  0.003% | 
 | 301  | Scraggy            |  0.00004% | 132    |  0.002% | 105    |  0.003% | 
 | 302  | Bonsly             |  0.00004% | 146    |  0.003% | 109    |  0.003% | 
 | 303  | Milcery            |  0.00004% | 68     |  0.001% | 54     |  0.001% | 
 | 304  | Shellos            |  0.00004% | 191    |  0.004% | 136    |  0.003% | 
 | 305  | Koffing            |  0.00004% | 248    |  0.005% | 201    |  0.005% | 
 | 306  | Spritzee           |  0.00004% | 229    |  0.004% | 178    |  0.004% | 
 | 307  | Cufant             |  0.00004% | 111    |  0.002% | 71     |  0.002% | 
 | 308  | Yamask-Galar       |  0.00003% | 344    |  0.006% | 276    |  0.007% | 
 | 309  | Gastly             |  0.00003% | 348    |  0.007% | 271    |  0.007% | 
 | 310  | Krabby             |  0.00003% | 161    |  0.003% | 134    |  0.003% | 
 | 311  | Boldore            |  0.00003% | 253    |  0.005% | 199    |  0.005% | 
 | 312  | Bronzor            |  0.00003% | 249    |  0.005% | 226    |  0.006% | 
 | 313  | Yamper             |  0.00002% | 281    |  0.005% | 216    |  0.005% | 
 | 314  | Grookey            |  0.00002% | 353    |  0.007% | 293    |  0.007% | 
 | 315  | Fraxure            |  0.00002% | 170    |  0.003% | 119    |  0.003% | 
 | 316  | Tyrogue            |  0.00002% | 63     |  0.001% | 51     |  0.001% | 
 | 317  | Vullaby            |  0.00002% | 457    |  0.009% | 271    |  0.007% | 
 | 318  | Snorunt            |  0.00002% | 287    |  0.005% | 160    |  0.004% | 
 | 319  | Munna              |  0.00002% | 53     |  0.001% | 42     |  0.001% | 
 | 320  | Dwebble            |  0.00001% | 259    |  0.005% | 193    |  0.005% | 
 | 321  | Cleffa             |  0.00001% | 163    |  0.003% | 98     |  0.002% | 
 | 322  | Zweilous           |  0.00001% | 499    |  0.009% | 369    |  0.009% | 
 | 323  | Wingull            |  0.00001% | 56     |  0.001% | 51     |  0.001% | 
 | 324  | Sliggoo            |  0.00001% | 312    |  0.006% | 229    |  0.006% | 
 | 325  | Budew              |  0.00001% | 28     |  0.001% | 18     |  0.000% | 
 | 326  | Natu               |  0.00001% | 216    |  0.004% | 183    |  0.005% | 
 | 327  | Remoraid           |  0.00001% | 290    |  0.005% | 169    |  0.004% | 
 | 328  | Stufful            |  0.00001% | 46     |  0.001% | 35     |  0.001% | 
 | 329  | Frillish           |  0.00001% | 118    |  0.002% | 93     |  0.002% | 
 | 330  | Machoke            |  0.00001% | 318    |  0.006% | 245    |  0.006% | 
 | 331  | Hippopotas         |  0.00001% | 74     |  0.001% | 64     |  0.002% | 
 | 332  | Mime Jr.           |  0.00001% | 63     |  0.001% | 53     |  0.001% | 
 | 333  | Timburr            |  0.00001% | 26     |  0.000% | 17     |  0.000% | 
 | 334  | Lotad              |  0.00001% | 48     |  0.001% | 35     |  0.001% | 
 | 335  | Noibat             |  0.00001% | 53     |  0.001% | 40     |  0.001% | 
 | 336  | Grubbin            |  0.00001% | 180    |  0.003% | 154    |  0.004% | 
 | 337  | Mudbray            |  0.00001% | 70     |  0.001% | 59     |  0.001% | 
 | 338  | Goomy              |  0.00001% | 68     |  0.001% | 51     |  0.001% | 
 | 339  | Growlithe          |  0.00001% | 77     |  0.001% | 61     |  0.002% | 
 | 340  | Hatenna            |  0.00001% | 69     |  0.001% | 49     |  0.001% | 
 | 341  | Hakamo-o           |  0.00001% | 283    |  0.005% | 221    |  0.005% | 
 | 342  | Sinistea           |  0.00001% | 222    |  0.004% | 194    |  0.005% | 
 | 343  | Silicobra          |  0.00001% | 68     |  0.001% | 56     |  0.001% | 
 | 344  | Rookidee           |  0.00001% | 66     |  0.001% | 55     |  0.001% | 
 | 345  | Stunky             |  0.00001% | 26     |  0.000% | 21     |  0.001% | 
 | 346  | Combee             |  0.00001% | 27     |  0.001% | 24     |  0.001% | 
 | 347  | Sizzlipede         |  0.00001% | 72     |  0.001% | 60     |  0.001% | 
 | 348  | Zigzagoon-Galar    |  0.00001% | 107    |  0.002% | 84     |  0.002% | 
 | 349  | Barboach           |  0.00001% | 55     |  0.001% | 36     |  0.001% | 
 | 350  | Corphish           |  0.00001% | 132    |  0.002% | 102    |  0.003% | 
 | 351  | Meowth-Galar       |  0.00001% | 121    |  0.002% | 85     |  0.002% | 
 | 352  | Duskull            |  0.00001% | 78     |  0.001% | 68     |  0.002% | 
 | 353  | Cubchoo            |  0.00000% | 59     |  0.001% | 51     |  0.001% | 
 | 354  | Charmander         |  0.00000% | 154    |  0.003% | 118    |  0.003% | 
 | 355  | Togepi             |  0.00000% | 400    |  0.008% | 217    |  0.005% | 
 | 356  | Ralts              |  0.00000% | 696    |  0.013% | 599    |  0.015% | 
 | 357  | Snover             |  0.00000% | 61     |  0.001% | 53     |  0.001% | 
 | 358  | Drizzile           |  0.00000% | 233    |  0.004% | 169    |  0.004% | 
 | 359  | Applin             |  0.00000% | 313    |  0.006% | 213    |  0.005% | 
 | 360  | Skorupi            |  0.00000% | 53     |  0.001% | 45     |  0.001% | 
 | 361  | Salandit           |  0.00000% | 239    |  0.005% | 195    |  0.005% | 
 | 362  | Bergmite           |  0.00000% | 48     |  0.001% | 32     |  0.001% | 
 | 363  | Feebas             |  0.00000% | 143    |  0.003% | 108    |  0.003% | 
 | 364  | Yamask             |  0.00000% | 132    |  0.002% | 94     |  0.002% | 
 | 365  | Dewpider           |  0.00000% | 74     |  0.001% | 62     |  0.002% | 
 | 366  | Purrloin           |  0.00000% | 32     |  0.001% | 23     |  0.001% | 
 | 367  | Dreepy             |  0.00000% | 116    |  0.002% | 94     |  0.002% | 
 | 368  | Axew               |  0.00000% | 104    |  0.002% | 79     |  0.002% | 
 | 369  | Vibrava            |  0.00000% | 58     |  0.001% | 49     |  0.001% | 
 | 370  | Skwovet            |  0.00000% | 117    |  0.002% | 82     |  0.002% | 
 | 371  | Chewtle            |  0.00000% | 51     |  0.001% | 45     |  0.001% | 
 | 372  | Tranquill          |  0.00000% | 46     |  0.001% | 40     |  0.001% | 
 | 373  | Machop             |  0.00000% | 29     |  0.001% | 24     |  0.001% | 
 | 374  | Lampent            |  0.00000% | 219    |  0.004% | 165    |  0.004% | 
 | 375  | Electrike          |  0.00000% | 38     |  0.001% | 29     |  0.001% | 
 | 376  | Rhyhorn            |  0.00000% | 51     |  0.001% | 38     |  0.001% | 
 | 377  | Kirlia             |  0.00000% | 265    |  0.005% | 207    |  0.005% | 
 | 378  | Pawniard           |  0.00000% | 72     |  0.001% | 60     |  0.001% | 
 | 379  | Binacle            |  0.00000% | 18     |  0.000% | 12     |  0.000% | 
 | 380  | Swinub             |  0.00000% | 56     |  0.001% | 46     |  0.001% | 
 | 381  | Sobble             |  0.00000% | 199    |  0.004% | 166    |  0.004% | 
 | 382  | Joltik             |  0.00000% | 74     |  0.001% | 61     |  0.002% | 
 | 383  | Elgyem             |  0.00000% | 11     |  0.000% | 7      |  0.000% | 
 | 384  | Chinchou           |  0.00000% | 53     |  0.001% | 42     |  0.001% | 
 | 385  | Litwick            |  0.00000% | 689    |  0.013% | 576    |  0.014% | 
 | 386  | Phantump           |  0.00000% | 106    |  0.002% | 84     |  0.002% | 
 | 387  | Honedge            |  0.00000% | 66     |  0.001% | 49     |  0.001% | 
 | 388  | Nincada            |  0.00000% | 40     |  0.001% | 33     |  0.001% | 
 | 389  | Lombre             |  0.00000% | 99     |  0.002% | 86     |  0.002% | 
 | 390  | Bunnelby           |  0.00000% | 82     |  0.002% | 69     |  0.002% | 
 | 391  | Corvisquire        |  0.00000% | 123    |  0.002% | 85     |  0.002% | 
 | 392  | Roggenrola         |  0.00000% | 32     |  0.001% | 26     |  0.001% | 
 | 393  | Oddish             |  0.00000% | 81     |  0.002% | 54     |  0.001% | 
 | 394  | Nickit             |  0.00000% | 71     |  0.001% | 55     |  0.001% | 
 | 395  | Rolycoly           |  0.00000% | 80     |  0.002% | 56     |  0.001% | 
 | 396  | Caterpie           |  0.00000% | 116    |  0.002% | 94     |  0.002% | 
 | 397  | Steenee            |  0.00000% | 23     |  0.000% | 17     |  0.000% | 
 | 398  | Trubbish           |  0.00000% | 87     |  0.002% | 76     |  0.002% | 
 | 399  | Klink              |  0.00000% | 31     |  0.001% | 25     |  0.001% | 
 | 400  | Wailmer            |  0.00000% | 133    |  0.003% | 105    |  0.003% | 
 | 401  | Toxel              |  0.00000% | 138    |  0.003% | 111    |  0.003% | 
 | 402  | Pancham            |  0.00000% | 72     |  0.001% | 49     |  0.001% | 
 | 403  | Minccino           |  0.00000% | 21     |  0.000% | 16     |  0.000% | 
 | 404  | Pidove             |  0.00000% | 49     |  0.001% | 33     |  0.001% | 
 | 405  | Jangmo-o           |  0.00000% | 26     |  0.000% | 23     |  0.001% | 
 | 406  | Helioptile         |  0.00000% | 22     |  0.000% | 17     |  0.000% | 
 | 407  | Drifloon           |  0.00000% | 88     |  0.002% | 62     |  0.002% | 
 | 408  | Pumpkaboo-Super    |  0.00000% | 45     |  0.001% | 35     |  0.001% | 
 | 409  | Deino              |  0.00000% | 63     |  0.001% | 45     |  0.001% | 
 | 410  | Pichu              |  0.00000% | 201    |  0.004% | 146    |  0.004% | 
 | 411  | Wimpod             |  0.00000% | 37     |  0.001% | 26     |  0.001% | 
 | 412  | Morelull           |  0.00000% | 64     |  0.001% | 59     |  0.001% | 
 | 413  | Inkay              |  0.00000% | 67     |  0.001% | 57     |  0.001% | 
 | 414  | Cherubi            |  0.00000% | 82     |  0.002% | 50     |  0.001% | 
 | 415  | Larvitar           |  0.00000% | 28     |  0.001% | 19     |  0.000% | 
 | 416  | Goldeen            |  0.00000% | 57     |  0.001% | 48     |  0.001% | 
 | 417  | Baltoy             |  0.00000% | 77     |  0.001% | 59     |  0.001% | 
 | 418  | Karrablast         |  0.00000% | 24     |  0.000% | 15     |  0.000% | 
 | 419  | Bounsweet          |  0.00000% | 20     |  0.000% | 14     |  0.000% | 
 | 420  | Tympole            |  0.00000% | 29     |  0.001% | 19     |  0.000% | 
 | 421  | Gothita            |  0.00000% | 6      |  0.000% | 6      |  0.000% | 
 | 422  | Pumpkaboo-Large    |  0.00000% | 6      |  0.000% | 5      |  0.000% | 
 | 423  | Croagunk           |  0.00000% | 30     |  0.001% | 19     |  0.000% | 
 | 424  | Pumpkaboo-Small    |  0.00000% | 9      |  0.000% | 6      |  0.000% | 
 | 425  | Vanillish          |  0.00000% | 53     |  0.001% | 33     |  0.001% | 
 | 426  | Hoothoot           |  0.00000% | 13     |  0.000% | 13     |  0.000% | 
 | 427  | Pumpkaboo          |  0.00000% | 17     |  0.000% | 8      |  0.000% | 
 | 428  | Seedot             |  0.00000% | 20     |  0.000% | 17     |  0.000% | 
 | 429  | Vanillite          |  0.00000% | 23     |  0.000% | 16     |  0.000% | 
 | 430  | Golett             |  0.00000% | 23     |  0.000% | 19     |  0.000% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
