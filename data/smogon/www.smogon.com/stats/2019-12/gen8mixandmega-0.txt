 Total battles: 7972
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Dragapult          | 32.86503% | 5240   | 32.865% | 3777   | 32.034% | 
 | 2    | Corviknight        | 32.32564% | 5154   | 32.326% | 4158   | 35.265% | 
 | 3    | Arcanine           | 22.84872% | 3643   | 22.849% | 2821   | 23.926% | 
 | 4    | Cinderace          | 21.30582% | 3397   | 21.306% | 2506   | 21.254% | 
 | 5    | Eternatus          | 18.99147% | 3028   | 18.991% | 2311   | 19.600% | 
 | 6    | Toxtricity         | 17.95033% | 2862   | 17.950% | 2139   | 18.141% | 
 | 7    | Toxapex            | 14.80808% | 2361   | 14.808% | 1886   | 15.996% | 
 | 8    | Ditto              | 13.37180% | 2132   | 13.372% | 1417   | 12.018% | 
 | 9    | Dracovish          | 12.97040% | 2068   | 12.970% | 1467   | 12.442% | 
 | 10   | Barraskewda        | 12.55645% | 2002   | 12.556% | 1384   | 11.738% | 
 | 11   | Excadrill          | 12.48746% | 1991   | 12.487% | 1427   | 12.103% | 
 | 12   | Noivern            | 10.07903% | 1607   | 10.079% | 1227   | 10.407% | 
 | 13   | Hydreigon          |  9.94104% | 1585   |  9.941% | 1119   |  9.491% | 
 | 14   | Mimikyu            |  9.89087% | 1577   |  9.891% | 1185   | 10.050% | 
 | 15   | Gengar             |  9.41420% | 1501   |  9.414% | 1016   |  8.617% | 
 | 16   | Kommo-o            |  9.39538% | 1498   |  9.395% | 1086   |  9.211% | 
 | 17   | Dracozolt          |  8.40442% | 1340   |  8.404% | 958    |  8.125% | 
 | 18   | Grimmsnarl         |  8.15981% | 1301   |  8.160% | 963    |  8.167% | 
 | 19   | Cursola            |  7.86503% | 1254   |  7.865% | 880    |  7.464% | 
 | 20   | Lucario            |  7.80231% | 1244   |  7.802% | 867    |  7.353% | 
 | 21   | Darmanitan-Galar   |  6.97441% | 1112   |  6.974% | 814    |  6.904% | 
 | 22   | Hatterene          |  6.89915% | 1100   |  6.899% | 786    |  6.666% | 
 | 23   | Mew                |  6.62318% | 1056   |  6.623% | 783    |  6.641% | 
 | 24   | Inteleon           |  6.55419% | 1045   |  6.554% | 785    |  6.658% | 
 | 25   | Mandibuzz          |  6.39112% | 1019   |  6.391% | 752    |  6.378% | 
 | 26   | Weavile            |  6.37858% | 1017   |  6.379% | 765    |  6.488% | 
 | 27   | Tyranitar          |  6.24059% | 995    |  6.241% | 755    |  6.403% | 
 | 28   | Ferrothorn         |  6.22805% | 993    |  6.228% | 781    |  6.624% | 
 | 29   | Shuckle            |  5.95208% | 949    |  5.952% | 861    |  7.302% | 
 | 30   | Chandelure         |  5.89563% | 940    |  5.896% | 680    |  5.767% | 
 | 31   | Hippowdon          |  5.80783% | 926    |  5.808% | 762    |  6.463% | 
 | 32   | Rhyperior          |  5.72629% | 913    |  5.726% | 706    |  5.988% | 
 | 33   | Gyarados           |  5.53813% | 883    |  5.538% | 664    |  5.632% | 
 | 34   | Zamazenta-Crowned  |  5.28098% | 842    |  5.281% | 629    |  5.335% | 
 | 35   | Jellicent          |  5.16809% | 824    |  5.168% | 654    |  5.547% | 
 | 36   | Aegislash          |  5.01129% | 799    |  5.011% | 621    |  5.267% | 
 | 37   | Charizard          |  4.73532% | 755    |  4.735% | 609    |  5.165% | 
 | 38   | Duraludon          |  4.64752% | 741    |  4.648% | 562    |  4.766% | 
 | 39   | Mamoswine          |  4.53462% | 723    |  4.535% | 564    |  4.783% | 
 | 40   | Indeedee           |  4.08931% | 652    |  4.089% | 449    |  3.808% | 
 | 41   | Haxorus            |  3.90743% | 623    |  3.907% | 426    |  3.613% | 
 | 42   | Rillaboom          |  3.73181% | 595    |  3.732% | 451    |  3.825% | 
 | 43   | Centiskorch        |  3.54992% | 566    |  3.550% | 416    |  3.528% | 
 | 44   | Corsola-Galar      |  3.41821% | 545    |  3.418% | 418    |  3.545% | 
 | 45   | Clefable           |  3.31159% | 528    |  3.312% | 409    |  3.469% | 
 | 46   | Orbeetle           |  3.31159% | 528    |  3.312% | 455    |  3.859% | 
 | 47   | Vikavolt           |  3.07953% | 491    |  3.080% | 409    |  3.469% | 
 | 48   | Bisharp            |  2.86001% | 456    |  2.860% | 334    |  2.833% | 
 | 49   | Milotic            |  2.84119% | 453    |  2.841% | 348    |  2.951% | 
 | 50   | Gastrodon          |  2.80983% | 448    |  2.810% | 378    |  3.206% | 
 | 51   | Coalossal          |  2.75339% | 439    |  2.753% | 346    |  2.935% | 
 | 52   | Cloyster           |  2.73457% | 436    |  2.735% | 319    |  2.706% | 
 | 53   | Reuniclus          |  2.60286% | 415    |  2.603% | 289    |  2.451% | 
 | 54   | Vaporeon           |  2.59659% | 414    |  2.597% | 335    |  2.841% | 
 | 55   | Pelipper           |  2.48996% | 397    |  2.490% | 350    |  2.968% | 
 | 56   | Runerigus          |  2.48996% | 397    |  2.490% | 320    |  2.714% | 
 | 57   | Conkeldurr         |  2.46488% | 393    |  2.465% | 287    |  2.434% | 
 | 58   | Snorlax            |  2.45861% | 392    |  2.459% | 300    |  2.544% | 
 | 59   | Steelix            |  2.40843% | 384    |  2.408% | 294    |  2.493% | 
 | 60   | Salazzle           |  2.39589% | 382    |  2.396% | 273    |  2.315% | 
 | 61   | Jolteon            |  2.36453% | 377    |  2.365% | 299    |  2.536% | 
 | 62   | Dugtrio            |  2.30181% | 367    |  2.302% | 286    |  2.426% | 
 | 63   | Togekiss           |  2.23281% | 356    |  2.233% | 249    |  2.112% | 
 | 64   | Golisopod          |  2.14501% | 342    |  2.145% | 266    |  2.256% | 
 | 65   | Zacian             |  2.08856% | 333    |  2.089% | 214    |  1.815% | 
 | 66   | Rotom-Wash         |  2.06974% | 330    |  2.070% | 254    |  2.154% | 
 | 67   | Falinks            |  2.05720% | 328    |  2.057% | 215    |  1.823% | 
 | 68   | Polteageist        |  2.05093% | 327    |  2.051% | 222    |  1.883% | 
 | 69   | Boltund            |  2.03838% | 325    |  2.038% | 237    |  2.010% | 
 | 70   | Copperajah         |  2.01957% | 322    |  2.020% | 247    |  2.095% | 
 | 71   | Charizard-Mega-X   |  2.01330% | 321    |  2.013% | 271    |  2.298% | 
 | 72   | Goodra             |  2.00702% | 320    |  2.007% | 241    |  2.044% | 
 | 73   | Appletun           |  1.99448% | 318    |  1.994% | 225    |  1.908% | 
 | 74   | Sirfetch'd         |  1.97566% | 315    |  1.976% | 240    |  2.036% | 
 | 75   | Alcremie           |  1.95058% | 311    |  1.951% | 227    |  1.925% | 
 | 76   | Lapras             |  1.91295% | 305    |  1.913% | 246    |  2.086% | 
 | 77   | Morpeko            |  1.84395% | 294    |  1.844% | 245    |  2.078% | 
 | 78   | Frosmoth           |  1.78123% | 284    |  1.781% | 185    |  1.569% | 
 | 79   | Obstagoon          |  1.77496% | 283    |  1.775% | 209    |  1.773% | 
 | 80   | Flygon             |  1.73733% | 277    |  1.737% | 224    |  1.900% | 
 | 81   | Tyranitar-Mega     |  1.55544% | 248    |  1.555% | 195    |  1.654% | 
 | 82   | Weezing-Galar      |  1.53036% | 244    |  1.530% | 193    |  1.637% | 
 | 83   | Gourgeist-Super    |  1.51154% | 241    |  1.512% | 175    |  1.484% | 
 | 84   | Rotom-Heat         |  1.47391% | 235    |  1.474% | 202    |  1.713% | 
 | 85   | Charizard-Mega-Y   |  1.46764% | 234    |  1.468% | 187    |  1.586% | 
 | 86   | Seismitoad         |  1.40492% | 224    |  1.405% | 196    |  1.662% | 
 | 87   | Gardevoir          |  1.35474% | 216    |  1.355% | 156    |  1.323% | 
 | 88   | Rapidash-Galar     |  1.34220% | 214    |  1.342% | 157    |  1.332% | 
 | 89   | Gyarados-Mega      |  1.19167% | 190    |  1.192% | 165    |  1.399% | 
 | 90   | Ribombee           |  1.17913% | 188    |  1.179% | 145    |  1.230% | 
 | 91   | Drednaw            |  1.17285% | 187    |  1.173% | 152    |  1.289% | 
 | 92   | Ninetales          |  1.16658% | 186    |  1.167% | 137    |  1.162% | 
 | 93   | Hawlucha           |  1.16658% | 186    |  1.167% | 135    |  1.145% | 
 | 94   | Tsareena           |  1.16658% | 186    |  1.167% | 129    |  1.094% | 
 | 95   | Gardevoir-Mega     |  1.14150% | 182    |  1.141% | 140    |  1.187% | 
 | 96   | Galvantula         |  1.12895% | 180    |  1.129% | 166    |  1.408% | 
 | 97   | Torkoal            |  1.12268% | 179    |  1.123% | 134    |  1.136% | 
 | 98   | Mr. Rime           |  1.11641% | 178    |  1.116% | 121    |  1.026% | 
 | 99   | Dusknoir           |  1.07250% | 171    |  1.073% | 139    |  1.179% | 
 | 100  | Avalugg            |  1.06623% | 170    |  1.066% | 128    |  1.086% | 
 | 101  | Klinklang          |  1.05996% | 169    |  1.060% | 129    |  1.094% | 
 | 102  | Sylveon            |  1.04742% | 167    |  1.047% | 128    |  1.086% | 
 | 103  | Lucario-Mega       |  1.04114% | 166    |  1.041% | 136    |  1.153% | 
 | 104  | Turtonator         |  1.03487% | 165    |  1.035% | 138    |  1.170% | 
 | 105  | Zamazenta          |  1.02860% | 164    |  1.029% | 120    |  1.018% | 
 | 106  | Wobbuffet          |  1.00978% | 161    |  1.010% | 101    |  0.857% | 
 | 107  | Espeon             |  0.92825% | 148    |  0.928% | 102    |  0.865% | 
 | 108  | Machamp            |  0.89689% | 143    |  0.897% | 112    |  0.950% | 
 | 109  | Drampa             |  0.89689% | 143    |  0.897% | 111    |  0.941% | 
 | 110  | Cramorant          |  0.87180% | 139    |  0.872% | 102    |  0.865% | 
 | 111  | Grapploct          |  0.87180% | 139    |  0.872% | 88     |  0.746% | 
 | 112  | Pikachu            |  0.87180% | 139    |  0.872% | 112    |  0.950% | 
 | 113  | Umbreon            |  0.84671% | 135    |  0.847% | 93     |  0.789% | 
 | 114  | Gallade-Mega       |  0.82790% | 132    |  0.828% | 104    |  0.882% | 
 | 115  | Shedinja           |  0.82163% | 131    |  0.822% | 93     |  0.789% | 
 | 116  | Dubwool            |  0.80281% | 128    |  0.803% | 98     |  0.831% | 
 | 117  | Roserade           |  0.80281% | 128    |  0.803% | 95     |  0.806% | 
 | 118  | Gallade            |  0.79654% | 127    |  0.797% | 104    |  0.882% | 
 | 119  | Escavalier         |  0.76518% | 122    |  0.765% | 98     |  0.831% | 
 | 120  | Claydol            |  0.76518% | 122    |  0.765% | 91     |  0.772% | 
 | 121  | Hitmontop          |  0.76518% | 122    |  0.765% | 94     |  0.797% | 
 | 122  | Eiscue             |  0.74636% | 119    |  0.746% | 91     |  0.772% | 
 | 123  | Durant             |  0.74009% | 118    |  0.740% | 89     |  0.755% | 
 | 124  | Bronzong           |  0.73382% | 117    |  0.734% | 106    |  0.899% | 
 | 125  | Flapple            |  0.70873% | 113    |  0.709% | 81     |  0.687% | 
 | 126  | Gigalith           |  0.70873% | 113    |  0.709% | 85     |  0.721% | 
 | 127  | Arctovish          |  0.70246% | 112    |  0.702% | 96     |  0.814% | 
 | 128  | Dhelmise           |  0.68991% | 110    |  0.690% | 81     |  0.687% | 
 | 129  | Arctozolt          |  0.68991% | 110    |  0.690% | 80     |  0.679% | 
 | 130  | Toxicroak          |  0.68364% | 109    |  0.684% | 78     |  0.662% | 
 | 131  | Braviary           |  0.67737% | 108    |  0.677% | 84     |  0.712% | 
 | 132  | Pincurchin         |  0.66483% | 106    |  0.665% | 89     |  0.755% | 
 | 133  | Accelgor           |  0.66483% | 106    |  0.665% | 78     |  0.662% | 
 | 134  | Sandaconda         |  0.64601% | 103    |  0.646% | 86     |  0.729% | 
 | 135  | Heliolisk          |  0.63974% | 102    |  0.640% | 75     |  0.636% | 
 | 136  | Abomasnow-Mega     |  0.63347% | 101    |  0.633% | 78     |  0.662% | 
 | 137  | Drapion            |  0.62720% | 100    |  0.627% | 79     |  0.670% | 
 | 138  | Silvally           |  0.62092% | 99     |  0.621% | 68     |  0.577% | 
 | 139  | Cinccino           |  0.58956% | 94     |  0.590% | 77     |  0.653% | 
 | 140  | Aromatisse         |  0.58329% | 93     |  0.583% | 70     |  0.594% | 
 | 141  | Vileplume          |  0.58329% | 93     |  0.583% | 70     |  0.594% | 
 | 142  | Barbaracle         |  0.57702% | 92     |  0.577% | 71     |  0.602% | 
 | 143  | Xatu               |  0.56448% | 90     |  0.564% | 78     |  0.662% | 
 | 144  | Hitmonlee          |  0.55193% | 88     |  0.552% | 78     |  0.662% | 
 | 145  | Pyukumuku          |  0.52057% | 83     |  0.521% | 69     |  0.585% | 
 | 146  | Butterfree         |  0.51430% | 82     |  0.514% | 69     |  0.585% | 
 | 147  | Garbodor           |  0.51430% | 82     |  0.514% | 64     |  0.543% | 
 | 148  | Drifblim           |  0.50803% | 81     |  0.508% | 56     |  0.475% | 
 | 149  | Cofagrigus         |  0.50176% | 80     |  0.502% | 59     |  0.500% | 
 | 150  | Musharna           |  0.48294% | 77     |  0.483% | 64     |  0.543% | 
 | 151  | Stonjourner        |  0.48294% | 77     |  0.483% | 49     |  0.416% | 
 | 152  | Leafeon            |  0.47667% | 76     |  0.477% | 58     |  0.492% | 
 | 153  | Raichu             |  0.47040% | 75     |  0.470% | 49     |  0.416% | 
 | 154  | Hitmonchan         |  0.47040% | 75     |  0.470% | 61     |  0.517% | 
 | 155  | Kingler            |  0.47040% | 75     |  0.470% | 50     |  0.424% | 
 | 156  | Indeedee-F         |  0.45158% | 72     |  0.452% | 50     |  0.424% | 
 | 157  | Ninjask            |  0.45158% | 72     |  0.452% | 56     |  0.475% | 
 | 158  | Crustle            |  0.44531% | 71     |  0.445% | 55     |  0.466% | 
 | 159  | Glaceon            |  0.43276% | 69     |  0.433% | 61     |  0.517% | 
 | 160  | Abomasnow          |  0.43276% | 69     |  0.433% | 59     |  0.500% | 
 | 161  | Glalie             |  0.42022% | 67     |  0.420% | 60     |  0.509% | 
 | 162  | Araquanid          |  0.42022% | 67     |  0.420% | 58     |  0.492% | 
 | 163  | Pangoro            |  0.37632% | 60     |  0.376% | 45     |  0.382% | 
 | 164  | Shiinotic          |  0.37632% | 60     |  0.376% | 46     |  0.390% | 
 | 165  | Steelix-Mega       |  0.37632% | 60     |  0.376% | 44     |  0.373% | 
 | 166  | Bewear             |  0.36377% | 58     |  0.364% | 40     |  0.339% | 
 | 167  | Perrserker         |  0.35750% | 57     |  0.358% | 41     |  0.348% | 
 | 168  | Stunfisk-Galar     |  0.35123% | 56     |  0.351% | 39     |  0.331% | 
 | 169  | Greedent           |  0.34496% | 55     |  0.345% | 44     |  0.373% | 
 | 170  | Bellossom          |  0.33241% | 53     |  0.332% | 36     |  0.305% | 
 | 171  | Eevee              |  0.32614% | 52     |  0.326% | 45     |  0.382% | 
 | 172  | Sudowoodo          |  0.32614% | 52     |  0.326% | 38     |  0.322% | 
 | 173  | Whimsicott         |  0.31987% | 51     |  0.320% | 41     |  0.348% | 
 | 174  | Rotom-Fan          |  0.30105% | 48     |  0.301% | 36     |  0.305% | 
 | 175  | Flareon            |  0.29478% | 47     |  0.295% | 40     |  0.339% | 
 | 176  | Wishiwashi         |  0.29478% | 47     |  0.295% | 44     |  0.373% | 
 | 177  | Trevenant          |  0.27597% | 44     |  0.276% | 37     |  0.314% | 
 | 178  | Mawile             |  0.27597% | 44     |  0.276% | 30     |  0.254% | 
 | 179  | Eldegoss           |  0.26969% | 43     |  0.270% | 31     |  0.263% | 
 | 180  | Golurk             |  0.26969% | 43     |  0.270% | 37     |  0.314% | 
 | 181  | Mr. Mime-Galar     |  0.26342% | 42     |  0.263% | 25     |  0.212% | 
 | 182  | Quagsire           |  0.26342% | 42     |  0.263% | 33     |  0.280% | 
 | 183  | Mudsdale           |  0.25715% | 41     |  0.257% | 35     |  0.297% | 
 | 184  | Manectric-Mega     |  0.25088% | 40     |  0.251% | 33     |  0.280% | 
 | 185  | Vanilluxe          |  0.25088% | 40     |  0.251% | 27     |  0.229% | 
 | 186  | Manectric          |  0.23833% | 38     |  0.238% | 30     |  0.254% | 
 | 187  | Thievul            |  0.23833% | 38     |  0.238% | 29     |  0.246% | 
 | 188  | Froslass           |  0.23206% | 37     |  0.232% | 27     |  0.229% | 
 | 189  | Shiftry            |  0.23206% | 37     |  0.232% | 29     |  0.246% | 
 | 190  | Ludicolo           |  0.22579% | 36     |  0.226% | 27     |  0.229% | 
 | 191  | Scrafty            |  0.20697% | 33     |  0.207% | 24     |  0.204% | 
 | 192  | Wailord            |  0.20697% | 33     |  0.207% | 29     |  0.246% | 
 | 193  | Mantine            |  0.19443% | 31     |  0.194% | 24     |  0.204% | 
 | 194  | Lanturn            |  0.19443% | 31     |  0.194% | 23     |  0.195% | 
 | 195  | Blastoise-Mega     |  0.18189% | 29     |  0.182% | 24     |  0.204% | 
 | 196  | Beartic            |  0.18189% | 29     |  0.182% | 19     |  0.161% | 
 | 197  | Unfezant           |  0.17561% | 28     |  0.176% | 16     |  0.136% | 
 | 198  | Crawdaunt          |  0.16934% | 27     |  0.169% | 21     |  0.178% | 
 | 199  | Noctowl            |  0.15680% | 25     |  0.157% | 20     |  0.170% | 
 | 200  | Malamar            |  0.15680% | 25     |  0.157% | 19     |  0.161% | 
 | 201  | Slurpuff           |  0.15053% | 24     |  0.151% | 22     |  0.187% | 
 | 202  | Cherrim            |  0.15053% | 24     |  0.151% | 11     |  0.093% | 
 | 203  | Doublade           |  0.14425% | 23     |  0.144% | 20     |  0.170% | 
 | 204  | Type: Null         |  0.14425% | 23     |  0.144% | 19     |  0.161% | 
 | 205  | Vespiquen          |  0.14425% | 23     |  0.144% | 14     |  0.119% | 
 | 206  | Passimian          |  0.13798% | 22     |  0.138% | 16     |  0.136% | 
 | 207  | Sableye            |  0.12544% | 20     |  0.125% | 14     |  0.119% | 
 | 208  | Gourgeist-Small    |  0.12544% | 20     |  0.125% | 11     |  0.093% | 
 | 209  | Sigilyph           |  0.11917% | 19     |  0.119% | 11     |  0.093% | 
 | 210  | Octillery          |  0.11917% | 19     |  0.119% | 14     |  0.119% | 
 | 211  | Sableye-Mega       |  0.11290% | 18     |  0.113% | 15     |  0.127% | 
 | 212  | Rotom              |  0.11290% | 18     |  0.113% | 14     |  0.119% | 
 | 213  | Venusaur-Mega      |  0.11290% | 18     |  0.113% | 11     |  0.093% | 
 | 214  | Silvally-Fire      |  0.10662% | 17     |  0.107% | 13     |  0.110% | 
 | 215  | Silvally-Dragon    |  0.10035% | 16     |  0.100% | 14     |  0.119% | 
 | 216  | Meowstic           |  0.10035% | 16     |  0.100% | 15     |  0.127% | 
 | 217  | Rotom-Mow          |  0.10035% | 16     |  0.100% | 16     |  0.136% | 
 | 218  | Qwilfish           |  0.10035% | 16     |  0.100% | 14     |  0.119% | 
 | 219  | Skuntank           |  0.10035% | 16     |  0.100% | 8      |  0.068% | 
 | 220  | Liepard            |  0.09408% | 15     |  0.094% | 8      |  0.068% | 
 | 221  | Silvally-Ghost     |  0.09408% | 15     |  0.094% | 15     |  0.127% | 
 | 222  | Beheeyem           |  0.09408% | 15     |  0.094% | 10     |  0.085% | 
 | 223  | Rotom-Frost        |  0.09408% | 15     |  0.094% | 12     |  0.102% | 
 | 224  | Heatmor            |  0.08781% | 14     |  0.088% | 10     |  0.085% | 
 | 225  | Dusclops           |  0.08781% | 14     |  0.088% | 8      |  0.068% | 
 | 226  | Solrock            |  0.08781% | 14     |  0.088% | 10     |  0.085% | 
 | 227  | Glalie-Mega        |  0.08154% | 13     |  0.082% | 10     |  0.085% | 
 | 228  | Togedemaru         |  0.08154% | 13     |  0.082% | 12     |  0.102% | 
 | 229  | Rhydon             |  0.08154% | 13     |  0.082% | 6      |  0.051% | 
 | 230  | Lunatone           |  0.07526% | 12     |  0.075% | 8      |  0.068% | 
 | 231  | Swoobat            |  0.06899% | 11     |  0.069% | 8      |  0.068% | 
 | 232  | Silvally-Dark      |  0.06899% | 11     |  0.069% | 3      |  0.025% | 
 | 233  | Persian            |  0.06899% | 11     |  0.069% | 9      |  0.076% | 
 | 234  | Grookey            |  0.06272% | 10     |  0.063% | 8      |  0.068% | 
 | 235  | Scorbunny          |  0.06272% | 10     |  0.063% | 10     |  0.085% | 
 | 236  | Onix               |  0.06272% | 10     |  0.063% | 7      |  0.059% | 
 | 237  | Magikarp           |  0.05645% | 9      |  0.056% | 8      |  0.068% | 
 | 238  | Croagunk           |  0.05645% | 9      |  0.056% | 5      |  0.042% | 
 | 239  | Silvally-Water     |  0.05645% | 9      |  0.056% | 8      |  0.068% | 
 | 240  | Silvally-Fairy     |  0.05645% | 9      |  0.056% | 7      |  0.059% | 
 | 241  | Silvally-Electric  |  0.05645% | 9      |  0.056% | 9      |  0.076% | 
 | 242  | Linoone-Galar      |  0.05018% | 8      |  0.050% | 8      |  0.068% | 
 | 243  | Yamper             |  0.05018% | 8      |  0.050% | 7      |  0.059% | 
 | 244  | Whiscash           |  0.04390% | 7      |  0.044% | 4      |  0.034% | 
 | 245  | Maractus           |  0.04390% | 7      |  0.044% | 6      |  0.051% | 
 | 246  | Dewpider           |  0.04390% | 7      |  0.044% | 7      |  0.059% | 
 | 247  | Carkol             |  0.03763% | 6      |  0.038% | 5      |  0.042% | 
 | 248  | Piloswine          |  0.03763% | 6      |  0.038% | 6      |  0.051% | 
 | 249  | Rhyhorn            |  0.03763% | 6      |  0.038% | 6      |  0.051% | 
 | 250  | Diggersby          |  0.03136% | 5      |  0.031% | 4      |  0.034% | 
 | 251  | Drakloak           |  0.03136% | 5      |  0.031% | 5      |  0.042% | 
 | 252  | Meowth             |  0.03136% | 5      |  0.031% | 4      |  0.034% | 
 | 253  | Raboot             |  0.03136% | 5      |  0.031% | 3      |  0.025% | 
 | 254  | Silvally-Flying    |  0.03136% | 5      |  0.031% | 3      |  0.025% | 
 | 255  | Baltoy             |  0.03136% | 5      |  0.031% | 5      |  0.042% | 
 | 256  | Sobble             |  0.03136% | 5      |  0.031% | 4      |  0.034% | 
 | 257  | Gurdurr            |  0.03136% | 5      |  0.031% | 5      |  0.042% | 
 | 258  | Silvally-Psychic   |  0.03136% | 5      |  0.031% | 3      |  0.025% | 
 | 259  | Riolu              |  0.03136% | 5      |  0.031% | 3      |  0.025% | 
 | 260  | Haunter            |  0.02509% | 4      |  0.025% | 3      |  0.025% | 
 | 261  | Silvally-Fighting  |  0.02509% | 4      |  0.025% | 4      |  0.034% | 
 | 262  | Silvally-Rock      |  0.02509% | 4      |  0.025% | 3      |  0.025% | 
 | 263  | Blipbug            |  0.02509% | 4      |  0.025% | 4      |  0.034% | 
 | 264  | Mareanie           |  0.02509% | 4      |  0.025% | 3      |  0.025% | 
 | 265  | Caterpie           |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 266  | Silvally-Steel     |  0.01882% | 3      |  0.019% | 2      |  0.017% | 
 | 267  | Yamask             |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 268  | Metapod            |  0.01882% | 3      |  0.019% | 2      |  0.017% | 
 | 269  | Ponyta-Galar       |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 270  | Charjabug          |  0.01882% | 3      |  0.019% | 2      |  0.017% | 
 | 271  | Zigzagoon-Galar    |  0.01882% | 3      |  0.019% | 2      |  0.017% | 
 | 272  | Oranguru           |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 273  | Hakamo-o           |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 274  | Sawk               |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 275  | Silvally-Poison    |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 276  | Charmander         |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 277  | Wooloo             |  0.01882% | 3      |  0.019% | 3      |  0.025% | 
 | 278  | Mr. Mime           |  0.01882% | 3      |  0.019% | 2      |  0.017% | 
 | 279  | Farfetch'd-Galar   |  0.01254% | 2      |  0.013% | 1      |  0.008% | 
 | 280  | Silicobra          |  0.01254% | 2      |  0.013% | 0      |  0.000% | 
 | 281  | Dottler            |  0.01254% | 2      |  0.013% | 1      |  0.008% | 
 | 282  | Meowth-Galar       |  0.01254% | 2      |  0.013% | 0      |  0.000% | 
 | 283  | Throh              |  0.01254% | 2      |  0.013% | 1      |  0.008% | 
 | 284  | Pawniard           |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 285  | Thwackey           |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 286  | Delibird           |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 287  | Snom               |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 288  | Gastly             |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 289  | Milcery            |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 290  | Cutiefly           |  0.01254% | 2      |  0.013% | 1      |  0.008% | 
 | 291  | Gourgeist          |  0.01254% | 2      |  0.013% | 0      |  0.000% | 
 | 292  | Seaking            |  0.01254% | 2      |  0.013% | 1      |  0.008% | 
 | 293  | Bunnelby           |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 294  | Chinchou           |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 295  | Cleffa             |  0.01254% | 2      |  0.013% | 1      |  0.008% | 
 | 296  | Rookidee           |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 297  | Sneasel            |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 298  | Silvally-Grass     |  0.01254% | 2      |  0.013% | 2      |  0.017% | 
 | 299  | Seedot             |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 300  | Growlithe          |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 301  | Drizzile           |  0.00627% | 1      |  0.006% | 0      |  0.000% | 
 | 302  | Diglett            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 303  | Morgrem            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 304  | Wynaut             |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 305  | Charmeleon         |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 306  | Ferroseed          |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 307  | Silvally-Ice       |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 308  | Basculin           |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 309  | Noibat             |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 310  | Vulpix             |  0.00627% | 1      |  0.006% | 0      |  0.000% | 
 | 311  | Stufful            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 312  | Impidimp           |  0.00627% | 1      |  0.006% | 0      |  0.000% | 
 | 313  | Togetic            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 314  | Roselia            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 315  | Axew               |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 316  | Swirlix            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 317  | Zweilous           |  0.00627% | 1      |  0.006% | 0      |  0.000% | 
 | 318  | Applin             |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 319  | Silvally-Ground    |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 320  | Sinistea           |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 321  | Barboach           |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 322  | Hatenna            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 323  | Dwebble            |  0.00627% | 1      |  0.006% | 0      |  0.000% | 
 | 324  | Machop             |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 325  | Munchlax           |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 326  | Nuzleaf            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 327  | Pichu              |  0.00627% | 1      |  0.006% | 0      |  0.000% | 
 | 328  | Toxel              |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 329  | Arrokuda           |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 330  | Togepi             |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 331  | Cherubi            |  0.00627% | 1      |  0.006% | 0      |  0.000% | 
 | 332  | Sizzlipede         |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 333  | Chewtle            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 334  | Yamask-Galar       |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 335  | Sliggoo            |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 336  | Darumaka-Galar     |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 | 337  | Clefairy           |  0.00627% | 1      |  0.006% | 1      |  0.008% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
