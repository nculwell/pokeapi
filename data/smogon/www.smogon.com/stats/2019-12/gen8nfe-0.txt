 Total battles: 5286
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Mr. Mime-Galar     | 50.65267% | 5355   | 50.653% | 3841   | 45.823% | 
 | 2    | Machoke            | 41.12751% | 4348   | 41.128% | 3610   | 43.068% | 
 | 3    | Haunter            | 33.36171% | 3527   | 33.362% | 2660   | 31.734% | 
 | 4    | Corsola-Galar      | 29.74839% | 3145   | 29.748% | 2592   | 30.923% | 
 | 5    | Linoone-Galar      | 28.38630% | 3001   | 28.386% | 2497   | 29.789% | 
 | 6    | Piloswine          | 27.26069% | 2882   | 27.261% | 2458   | 29.324% | 
 | 7    | Gurdurr            | 21.09345% | 2230   | 21.093% | 1790   | 21.355% | 
 | 8    | Klang              | 19.89217% | 2103   | 19.892% | 1694   | 20.210% | 
 | 9    | Drakloak           | 18.62467% | 1969   | 18.625% | 1494   | 17.824% | 
 | 10   | Hattrem            | 15.08702% | 1595   | 15.087% | 1307   | 15.593% | 
 | 11   | Pawniard           | 14.79379% | 1564   | 14.794% | 1255   | 14.972% | 
 | 12   | Ferroseed          | 14.12221% | 1493   | 14.122% | 1316   | 15.700% | 
 | 13   | Fraxure            | 13.80061% | 1459   | 13.801% | 984    | 11.739% | 
 | 14   | Rufflet            | 13.62089% | 1440   | 13.621% | 1107   | 13.207% | 
 | 15   | Pikachu            | 12.76958% | 1350   | 12.770% | 1048   | 12.503% | 
 | 16   | Clefairy           | 12.51419% | 1323   | 12.514% | 1014   | 12.097% | 
 | 17   | Carkol             | 12.42906% | 1314   | 12.429% | 1128   | 13.457% | 
 | 18   | Morgrem            | 12.27771% | 1298   | 12.278% | 1083   | 12.920% | 
 | 19   | Dusclops           | 12.06016% | 1275   | 12.060% | 1011   | 12.061% | 
 | 20   | Sneasel            | 11.05751% | 1169   | 11.058% | 938    | 11.190% | 
 | 21   | Raboot             | 10.76428% | 1138   | 10.764% | 909    | 10.844% | 
 | 22   | Palpitoad          | 10.20621% | 1079   | 10.206% | 890    | 10.618% | 
 | 23   | Roselia            | 10.11162% | 1069   | 10.112% | 835    |  9.962% | 
 | 24   | Togetic            | 10.06432% | 1064   | 10.064% | 855    | 10.200% | 
 | 25   | Mareanie           |  9.80893% | 1037   |  9.809% | 819    |  9.771% | 
 | 26   | Gloom              |  9.09005% | 961    |  9.090% | 767    |  9.150% | 
 | 27   | Duosion            |  8.95762% | 947    |  8.958% | 693    |  8.268% | 
 | 28   | Charjabug          |  6.22399% | 658    |  6.224% | 599    |  7.146% | 
 | 29   | Farfetch'd-Galar   |  6.14832% | 650    |  6.148% | 515    |  6.144% | 
 | 30   | Honedge            |  5.83617% | 617    |  5.836% | 471    |  5.619% | 
 | 31   | Thwackey           |  5.40106% | 571    |  5.401% | 466    |  5.559% | 
 | 32   | Zweilous           |  5.19296% | 549    |  5.193% | 442    |  5.273% | 
 | 33   | Boldore            |  4.62543% | 489    |  4.625% | 420    |  5.011% | 
 | 34   | Charmeleon         |  4.50246% | 476    |  4.502% | 367    |  4.378% | 
 | 35   | Corvisquire        |  4.45516% | 471    |  4.455% | 358    |  4.271% | 
 | 36   | Dottler            |  4.39841% | 465    |  4.398% | 400    |  4.772% | 
 | 37   | Lampent            |  4.09572% | 433    |  4.096% | 347    |  4.140% | 
 | 38   | Hakamo-o           |  3.99168% | 422    |  3.992% | 320    |  3.818% | 
 | 39   | Vulpix             |  3.86871% | 409    |  3.869% | 379    |  4.522% | 
 | 40   | Drizzile           |  3.78358% | 400    |  3.784% | 316    |  3.770% | 
 | 41   | Trapinch           |  3.73628% | 395    |  3.736% | 321    |  3.830% | 
 | 42   | Sliggoo            |  3.37684% | 357    |  3.377% | 240    |  2.863% | 
 | 43   | Vullaby            |  3.12145% | 330    |  3.121% | 266    |  3.173% | 
 | 44   | Lombre             |  1.88233% | 199    |  1.882% | 157    |  1.873% | 
 | 45   | Drilbur            |  1.74991% | 185    |  1.750% | 142    |  1.694% | 
 | 46   | Natu               |  1.61748% | 171    |  1.617% | 140    |  1.670% | 
 | 47   | Krabby             |  1.57964% | 167    |  1.580% | 131    |  1.563% | 
 | 48   | Cutiefly           |  1.55127% | 164    |  1.551% | 152    |  1.813% | 
 | 49   | Munchlax           |  1.48505% | 157    |  1.485% | 113    |  1.348% | 
 | 50   | Scraggy            |  1.45668% | 154    |  1.457% | 131    |  1.563% | 
 | 51   | Bronzor            |  1.37155% | 145    |  1.372% | 144    |  1.718% | 
 | 52   | Diglett            |  1.30533% | 138    |  1.305% | 120    |  1.432% | 
 | 53   | Onix               |  1.25804% | 133    |  1.258% | 104    |  1.241% | 
 | 54   | Nuzleaf            |  1.24858% | 132    |  1.249% | 100    |  1.193% | 
 | 55   | Vibrava            |  1.12561% | 119    |  1.126% | 107    |  1.277% | 
 | 56   | Pupitar            |  1.02157% | 108    |  1.022% | 89     |  1.062% | 
 | 57   | Shellos            |  0.96481% | 102    |  0.965% | 82     |  0.978% | 
 | 58   | Kirlia             |  0.96481% | 102    |  0.965% | 71     |  0.847% | 
 | 59   | Gothorita          |  0.91752% | 97     |  0.918% | 80     |  0.954% | 
 | 60   | Darumaka-Galar     |  0.84185% | 89     |  0.842% | 61     |  0.728% | 
 | 61   | Dewpider           |  0.82293% | 87     |  0.823% | 79     |  0.942% | 
 | 62   | Ponyta-Galar       |  0.79455% | 84     |  0.795% | 68     |  0.811% | 
 | 63   | Sinistea           |  0.79455% | 84     |  0.795% | 60     |  0.716% | 
 | 64   | Dwebble            |  0.75672% | 80     |  0.757% | 72     |  0.859% | 
 | 65   | Mime Jr.           |  0.68104% | 72     |  0.681% | 52     |  0.620% | 
 | 66   | Steenee            |  0.68104% | 72     |  0.681% | 56     |  0.668% | 
 | 67   | Corphish           |  0.66213% | 70     |  0.662% | 48     |  0.573% | 
 | 68   | Tranquill          |  0.59591% | 63     |  0.596% | 51     |  0.608% | 
 | 69   | Wailmer            |  0.52024% | 55     |  0.520% | 41     |  0.489% | 
 | 70   | Croagunk           |  0.50132% | 53     |  0.501% | 42     |  0.501% | 
 | 71   | Remoraid           |  0.47295% | 50     |  0.473% | 40     |  0.477% | 
 | 72   | Vanillish          |  0.47295% | 50     |  0.473% | 40     |  0.477% | 
 | 73   | Swirlix            |  0.46349% | 49     |  0.463% | 43     |  0.513% | 
 | 74   | Cufant             |  0.43511% | 46     |  0.435% | 38     |  0.453% | 
 | 75   | Shellder           |  0.42565% | 45     |  0.426% | 32     |  0.382% | 
 | 76   | Mantyke            |  0.40673% | 43     |  0.407% | 34     |  0.406% | 
 | 77   | Salandit           |  0.33106% | 35     |  0.331% | 27     |  0.322% | 
 | 78   | Rhyhorn            |  0.33106% | 35     |  0.331% | 30     |  0.358% | 
 | 79   | Gossifleur         |  0.32160% | 34     |  0.322% | 21     |  0.251% | 
 | 80   | Mudbray            |  0.29323% | 31     |  0.293% | 23     |  0.274% | 
 | 81   | Arrokuda           |  0.28377% | 30     |  0.284% | 22     |  0.262% | 
 | 82   | Frillish           |  0.27431% | 29     |  0.274% | 23     |  0.274% | 
 | 83   | Chinchou           |  0.24593% | 26     |  0.246% | 21     |  0.251% | 
 | 84   | Hippopotas         |  0.23647% | 25     |  0.236% | 22     |  0.262% | 
 | 85   | Binacle            |  0.23647% | 25     |  0.236% | 18     |  0.215% | 
 | 86   | Goldeen            |  0.23647% | 25     |  0.236% | 17     |  0.203% | 
 | 87   | Riolu              |  0.23647% | 25     |  0.236% | 21     |  0.251% | 
 | 88   | Meowth-Galar       |  0.22701% | 24     |  0.227% | 21     |  0.251% | 
 | 89   | Koffing            |  0.21756% | 23     |  0.218% | 20     |  0.239% | 
 | 90   | Cottonee           |  0.21756% | 23     |  0.218% | 19     |  0.227% | 
 | 91   | Skwovet            |  0.20810% | 22     |  0.208% | 15     |  0.179% | 
 | 92   | Clobbopus          |  0.20810% | 22     |  0.208% | 16     |  0.191% | 
 | 93   | Snover             |  0.20810% | 22     |  0.208% | 22     |  0.262% | 
 | 94   | Baltoy             |  0.18918% | 20     |  0.189% | 17     |  0.203% | 
 | 95   | Drifloon           |  0.17972% | 19     |  0.180% | 12     |  0.143% | 
 | 96   | Wingull            |  0.17026% | 18     |  0.170% | 17     |  0.203% | 
 | 97   | Metapod            |  0.16080% | 17     |  0.161% | 15     |  0.179% | 
 | 98   | Morelull           |  0.16080% | 17     |  0.161% | 17     |  0.203% | 
 | 99   | Sizzlipede         |  0.15134% | 16     |  0.151% | 15     |  0.179% | 
 | 100  | Scorbunny          |  0.14188% | 15     |  0.142% | 14     |  0.167% | 
 | 101  | Snorunt            |  0.14188% | 15     |  0.142% | 12     |  0.143% | 
 | 102  | Golett             |  0.14188% | 15     |  0.142% | 12     |  0.143% | 
 | 103  | Trubbish           |  0.13243% | 14     |  0.132% | 14     |  0.167% | 
 | 104  | Inkay              |  0.12297% | 13     |  0.123% | 10     |  0.119% | 
 | 105  | Yamask-Galar       |  0.12297% | 13     |  0.123% | 13     |  0.155% | 
 | 106  | Growlithe          |  0.10405% | 11     |  0.104% | 6      |  0.072% | 
 | 107  | Toxel              |  0.10405% | 11     |  0.104% | 9      |  0.107% | 
 | 108  | Spritzee           |  0.10405% | 11     |  0.104% | 7      |  0.084% | 
 | 109  | Woobat             |  0.09459% | 10     |  0.095% | 6      |  0.072% | 
 | 110  | Gastly             |  0.09459% | 10     |  0.095% | 7      |  0.084% | 
 | 111  | Cherubi            |  0.09459% | 10     |  0.095% | 9      |  0.107% | 
 | 112  | Rolycoly           |  0.08513% | 9      |  0.085% | 9      |  0.107% | 
 | 113  | Phantump           |  0.08513% | 9      |  0.085% | 6      |  0.072% | 
 | 114  | Larvitar           |  0.08513% | 9      |  0.085% | 6      |  0.072% | 
 | 115  | Eevee              |  0.07567% | 8      |  0.076% | 8      |  0.095% | 
 | 116  | Helioptile         |  0.07567% | 8      |  0.076% | 6      |  0.072% | 
 | 117  | Pumpkaboo-Super    |  0.06621% | 7      |  0.066% | 6      |  0.072% | 
 | 118  | Chewtle            |  0.06621% | 7      |  0.066% | 5      |  0.060% | 
 | 119  | Wooper             |  0.06621% | 7      |  0.066% | 3      |  0.036% | 
 | 120  | Yamper             |  0.06621% | 7      |  0.066% | 6      |  0.072% | 
 | 121  | Wynaut             |  0.05675% | 6      |  0.057% | 3      |  0.036% | 
 | 122  | Shelmet            |  0.05675% | 6      |  0.057% | 6      |  0.072% | 
 | 123  | Snom               |  0.05675% | 6      |  0.057% | 3      |  0.036% | 
 | 124  | Wooloo             |  0.05675% | 6      |  0.057% | 5      |  0.060% | 
 | 125  | Grookey            |  0.05675% | 6      |  0.057% | 4      |  0.048% | 
 | 126  | Blipbug            |  0.05675% | 6      |  0.057% | 6      |  0.072% | 
 | 127  | Bunnelby           |  0.05675% | 6      |  0.057% | 6      |  0.072% | 
 | 128  | Applin             |  0.04729% | 5      |  0.047% | 5      |  0.060% | 
 | 129  | Minccino           |  0.04729% | 5      |  0.047% | 3      |  0.036% | 
 | 130  | Togepi             |  0.04729% | 5      |  0.047% | 4      |  0.048% | 
 | 131  | Electrike          |  0.03784% | 4      |  0.038% | 4      |  0.048% | 
 | 132  | Tyrogue            |  0.03784% | 4      |  0.038% | 3      |  0.036% | 
 | 133  | Meowth             |  0.03784% | 4      |  0.038% | 4      |  0.048% | 
 | 134  | Sobble             |  0.03784% | 4      |  0.038% | 4      |  0.048% | 
 | 135  | Charmander         |  0.03784% | 4      |  0.038% | 4      |  0.048% | 
 | 136  | Elgyem             |  0.03784% | 4      |  0.038% | 2      |  0.024% | 
 | 137  | Hatenna            |  0.03784% | 4      |  0.038% | 3      |  0.036% | 
 | 138  | Magikarp           |  0.02838% | 3      |  0.028% | 2      |  0.024% | 
 | 139  | Silicobra          |  0.02838% | 3      |  0.028% | 2      |  0.024% | 
 | 140  | Skorupi            |  0.02838% | 3      |  0.028% | 3      |  0.036% | 
 | 141  | Bonsly             |  0.02838% | 3      |  0.028% | 3      |  0.036% | 
 | 142  | Noibat             |  0.02838% | 3      |  0.028% | 3      |  0.036% | 
 | 143  | Stufful            |  0.02838% | 3      |  0.028% | 3      |  0.036% | 
 | 144  | Milcery            |  0.02838% | 3      |  0.028% | 3      |  0.036% | 
 | 145  | Rookidee           |  0.02838% | 3      |  0.028% | 2      |  0.024% | 
 | 146  | Pichu              |  0.02838% | 3      |  0.028% | 3      |  0.036% | 
 | 147  | Nickit             |  0.02838% | 3      |  0.028% | 1      |  0.012% | 
 | 148  | Cleffa             |  0.02838% | 3      |  0.028% | 3      |  0.036% | 
 | 149  | Feebas             |  0.01892% | 2      |  0.019% | 2      |  0.024% | 
 | 150  | Axew               |  0.01892% | 2      |  0.019% | 2      |  0.024% | 
 | 151  | Solosis            |  0.01892% | 2      |  0.019% | 1      |  0.012% | 
 | 152  | Zigzagoon-Galar    |  0.01892% | 2      |  0.019% | 2      |  0.024% | 
 | 153  | Stunky             |  0.01892% | 2      |  0.019% | 2      |  0.024% | 
 | 154  | Nincada            |  0.01892% | 2      |  0.019% | 2      |  0.024% | 
 | 155  | Seedot             |  0.01892% | 2      |  0.019% | 2      |  0.024% | 
 | 156  | Goomy              |  0.01892% | 2      |  0.019% | 2      |  0.024% | 
 | 157  | Munna              |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 158  | Swinub             |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 159  | Roggenrola         |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 160  | Bergmite           |  0.00946% | 1      |  0.009% | 0      |  0.000% | 
 | 161  | Hoothoot           |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 162  | Litwick            |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 163  | Pumpkaboo-Large    |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 164  | Pancham            |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 165  | Impidimp           |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 166  | Budew              |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 167  | Pumpkaboo          |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 168  | Machop             |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 169  | Caterpie           |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 170  | Lotad              |  0.00946% | 1      |  0.009% | 0      |  0.000% | 
 | 171  | Bounsweet          |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 172  | Dreepy             |  0.00946% | 1      |  0.009% | 1      |  0.012% | 
 | 173  | Espurr             |  0.00946% | 1      |  0.009% | 0      |  0.000% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
