 Total battles: 106
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Groudon-Primal     | 32.54717% | 69     | 32.547% | 54     | 33.575% | 
 | 2    | Kyogre-Primal      | 21.69811% | 46     | 21.698% | 32     | 19.896% | 
 | 3    | Darkrai            | 20.75472% | 44     | 20.755% | 31     | 19.275% | 
 | 4    | Ferrothorn         | 20.28302% | 43     | 20.283% | 29     | 18.031% | 
 | 5    | Xerneas            | 17.92453% | 38     | 17.925% | 25     | 15.544% | 
 | 6    | Ho-Oh              | 16.98113% | 36     | 16.981% | 25     | 15.544% | 
 | 7    | Giratina-Origin    | 16.50943% | 35     | 16.509% | 22     | 13.679% | 
 | 8    | Sableye-Mega       | 14.62264% | 31     | 14.623% | 30     | 18.653% | 
 | 9    | Lugia              | 13.20755% | 28     | 13.208% | 22     | 13.679% | 
 | 10   | Greninja           | 12.73585% | 27     | 12.736% | 23     | 14.301% | 
 | 11   | Aegislash          | 11.32075% | 24     | 11.321% | 17     | 10.570% | 
 | 12   | Rayquaza           | 10.37736% | 22     | 10.377% | 17     | 10.570% | 
 | 13   | Giratina           | 10.37736% | 22     | 10.377% | 17     | 10.570% | 
 | 14   | Arceus-Fairy       | 10.37736% | 22     | 10.377% | 12     |  7.461% | 
 | 15   | Salamence-Mega     | 10.37736% | 22     | 10.377% | 13     |  8.083% | 
 | 16   | Mewtwo-Mega-Y      |  9.90566% | 21     |  9.906% | 19     | 11.813% | 
 | 17   | Gigalith           |  8.96226% | 19     |  8.962% | 18     | 11.192% | 
 | 18   | Tyranitar-Mega     |  8.49057% | 18     |  8.491% | 15     |  9.326% | 
 | 19   | Yveltal            |  8.49057% | 18     |  8.491% | 12     |  7.461% | 
 | 20   | Throh              |  7.54717% | 16     |  7.547% | 12     |  7.461% | 
 | 21   | Tangrowth          |  7.54717% | 16     |  7.547% | 9      |  5.596% | 
 | 22   | Aromatisse         |  7.07547% | 15     |  7.075% | 10     |  6.218% | 
 | 23   | Arceus             |  6.60377% | 14     |  6.604% | 12     |  7.461% | 
 | 24   | Gliscor            |  6.60377% | 14     |  6.604% | 10     |  6.218% | 
 | 25   | Kyurem-White       |  6.60377% | 14     |  6.604% | 11     |  6.839% | 
 | 26   | Deoxys-Speed       |  6.60377% | 14     |  6.604% | 14     |  8.705% | 
 | 27   | Infernape          |  6.13208% | 13     |  6.132% | 10     |  6.218% | 
 | 28   | Heatran            |  5.66038% | 12     |  5.660% | 8      |  4.974% | 
 | 29   | Ursaring           |  5.66038% | 12     |  5.660% | 9      |  5.596% | 
 | 30   | Mewtwo-Mega-X      |  5.66038% | 12     |  5.660% | 7      |  4.352% | 
 | 31   | Whimsicott         |  5.66038% | 12     |  5.660% | 12     |  7.461% | 
 | 32   | Gastrodon          |  5.66038% | 12     |  5.660% | 9      |  5.596% | 
 | 33   | Genesect           |  5.18868% | 11     |  5.189% | 10     |  6.218% | 
 | 34   | Gengar-Mega        |  5.18868% | 11     |  5.189% | 8      |  4.974% | 
 | 35   | Gardevoir          |  5.18868% | 11     |  5.189% | 8      |  4.974% | 
 | 36   | Charizard-Mega-X   |  5.18868% | 11     |  5.189% | 9      |  5.596% | 
 | 37   | Hariyama           |  5.18868% | 11     |  5.189% | 9      |  5.596% | 
 | 38   | Dialga             |  5.18868% | 11     |  5.189% | 8      |  4.974% | 
 | 39   | Carracosta         |  5.18868% | 11     |  5.189% | 10     |  6.218% | 
 | 40   | Snorlax            |  4.71698% | 10     |  4.717% | 8      |  4.974% | 
 | 41   | Mewtwo             |  4.71698% | 10     |  4.717% | 10     |  6.218% | 
 | 42   | Scizor-Mega        |  4.71698% | 10     |  4.717% | 7      |  4.352% | 
 | 43   | Florges            |  4.71698% | 10     |  4.717% | 9      |  5.596% | 
 | 44   | Dragonite          |  4.71698% | 10     |  4.717% | 8      |  4.974% | 
 | 45   | Chandelure         |  4.71698% | 10     |  4.717% | 5      |  3.109% | 
 | 46   | Kangaskhan-Mega    |  4.71698% | 10     |  4.717% | 6      |  3.731% | 
 | 47   | Blaziken-Mega      |  4.24528% | 9      |  4.245% | 6      |  3.731% | 
 | 48   | Raikou             |  4.24528% | 9      |  4.245% | 8      |  4.974% | 
 | 49   | Rotom-Wash         |  4.24528% | 9      |  4.245% | 7      |  4.352% | 
 | 50   | Gengar             |  4.24528% | 9      |  4.245% | 8      |  4.974% | 
 | 51   | Blaziken           |  3.77358% | 8      |  3.774% | 6      |  3.731% | 
 | 52   | Zekrom             |  3.77358% | 8      |  3.774% | 6      |  3.731% | 
 | 53   | Palkia             |  3.77358% | 8      |  3.774% | 4      |  2.487% | 
 | 54   | Hoopa-Unbound      |  3.30189% | 7      |  3.302% | 5      |  3.109% | 
 | 55   | Klefki             |  3.30189% | 7      |  3.302% | 7      |  4.352% | 
 | 56   | Lucario-Mega       |  3.30189% | 7      |  3.302% | 6      |  3.731% | 
 | 57   | Breloom            |  3.30189% | 7      |  3.302% | 3      |  1.865% | 
 | 58   | Ambipom            |  2.83019% | 6      |  2.830% | 6      |  3.731% | 
 | 59   | Deoxys-Attack      |  2.83019% | 6      |  2.830% | 6      |  3.731% | 
 | 60   | Metagross          |  2.83019% | 6      |  2.830% | 4      |  2.487% | 
 | 61   | Garchomp-Mega      |  2.83019% | 6      |  2.830% | 5      |  3.109% | 
 | 62   | Sylveon            |  2.83019% | 6      |  2.830% | 6      |  3.731% | 
 | 63   | Umbreon            |  2.83019% | 6      |  2.830% | 5      |  3.109% | 
 | 64   | Shuckle            |  2.83019% | 6      |  2.830% | 6      |  3.731% | 
 | 65   | Arceus-Water       |  2.83019% | 6      |  2.830% | 3      |  1.865% | 
 | 66   | Milotic            |  2.35849% | 5      |  2.358% | 3      |  1.865% | 
 | 67   | Azumarill          |  2.35849% | 5      |  2.358% | 4      |  2.487% | 
 | 68   | Blissey            |  2.35849% | 5      |  2.358% | 4      |  2.487% | 
 | 69   | Arceus-Poison      |  2.35849% | 5      |  2.358% | 4      |  2.487% | 
 | 70   | Deoxys-Defense     |  2.35849% | 5      |  2.358% | 4      |  2.487% | 
 | 71   | Chimecho           |  2.35849% | 5      |  2.358% | 5      |  3.109% | 
 | 72   | Ninjask            |  2.35849% | 5      |  2.358% | 5      |  3.109% | 
 | 73   | Shedinja           |  2.35849% | 5      |  2.358% | 5      |  3.109% | 
 | 74   | Weezing            |  1.88679% | 4      |  1.887% | 4      |  2.487% | 
 | 75   | Houndoom-Mega      |  1.88679% | 4      |  1.887% | 2      |  1.244% | 
 | 76   | Latias             |  1.88679% | 4      |  1.887% | 3      |  1.865% | 
 | 77   | Arceus-Ghost       |  1.88679% | 4      |  1.887% | 2      |  1.244% | 
 | 78   | Absol-Mega         |  1.88679% | 4      |  1.887% | 4      |  2.487% | 
 | 79   | Arceus-Ground      |  1.88679% | 4      |  1.887% | 2      |  1.244% | 
 | 80   | Talonflame         |  1.88679% | 4      |  1.887% | 4      |  2.487% | 
 | 81   | Darmanitan         |  1.88679% | 4      |  1.887% | 4      |  2.487% | 
 | 82   | Arceus-Steel       |  1.88679% | 4      |  1.887% | 3      |  1.865% | 
 | 83   | Excadrill          |  1.88679% | 4      |  1.887% | 2      |  1.244% | 
 | 84   | Clefable           |  1.88679% | 4      |  1.887% | 4      |  2.487% | 
 | 85   | Garchomp           |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 86   | Lapras             |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 87   | Deoxys             |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 88   | Mismagius          |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 89   | Pachirisu          |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 90   | Gyarados-Mega      |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 91   | Scizor             |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 92   | Dusknoir           |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 93   | Chansey            |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 94   | Magnezone          |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 95   | Meowstic           |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 96   | Wobbuffet          |  1.41509% | 3      |  1.415% | 1      |  0.622% | 
 | 97   | Glaceon            |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 98   | Chesnaught         |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 99   | Tyranitar          |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 100  | Trevenant          |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 101  | Claydol            |  1.41509% | 3      |  1.415% | 3      |  1.865% | 
 | 102  | Skarmory           |  1.41509% | 3      |  1.415% | 2      |  1.244% | 
 | 103  | Machamp            |  0.94340% | 2      |  0.943% | 2      |  1.244% | 
 | 104  | Altaria-Mega       |  0.94340% | 2      |  0.943% | 0      |  0.000% | 
 | 105  | Gardevoir-Mega     |  0.94340% | 2      |  0.943% | 0      |  0.000% | 
 | 106  | Landorus-Therian   |  0.94340% | 2      |  0.943% | 1      |  0.622% | 
 | 107  | Bisharp            |  0.94340% | 2      |  0.943% | 2      |  1.244% | 
 | 108  | Venusaur           |  0.94340% | 2      |  0.943% | 0      |  0.000% | 
 | 109  | Swampert-Mega      |  0.94340% | 2      |  0.943% | 1      |  0.622% | 
 | 110  | Moltres            |  0.94340% | 2      |  0.943% | 1      |  0.622% | 
 | 111  | Cloyster           |  0.94340% | 2      |  0.943% | 2      |  1.244% | 
 | 112  | Salamence          |  0.94340% | 2      |  0.943% | 2      |  1.244% | 
 | 113  | Metagross-Mega     |  0.94340% | 2      |  0.943% | 2      |  1.244% | 
 | 114  | Slowbro-Mega       |  0.94340% | 2      |  0.943% | 1      |  0.622% | 
 | 115  | Weavile            |  0.94340% | 2      |  0.943% | 1      |  0.622% | 
 | 116  | Galvantula         |  0.94340% | 2      |  0.943% | 1      |  0.622% | 
 | 117  | Latios             |  0.94340% | 2      |  0.943% | 2      |  1.244% | 
 | 118  | Yanmega            |  0.94340% | 2      |  0.943% | 1      |  0.622% | 
 | 119  | Archeops           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 120  | Beedrill           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 121  | Alakazam-Mega      |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 122  | Tangela            |  0.47170% | 1      |  0.472% | 0      |  0.000% | 
 | 123  | Mew                |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 124  | Aggron-Mega        |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 125  | Sceptile           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 126  | Krookodile         |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 127  | Mawile             |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 128  | Manectric-Mega     |  0.47170% | 1      |  0.472% | 0      |  0.000% | 
 | 129  | Gallade-Mega       |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 130  | Togekiss           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 131  | Serperior          |  0.47170% | 1      |  0.472% | 0      |  0.000% | 
 | 132  | Arcanine           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 133  | Staraptor          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 134  | Ampharos           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 135  | Crawdaunt          |  0.47170% | 1      |  0.472% | 0      |  0.000% | 
 | 136  | Steelix-Mega       |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 137  | Magikarp           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 138  | Politoed           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 139  | Granbull           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 140  | Lanturn            |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 141  | Jolteon            |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 142  | Heliolisk          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 143  | Alakazam           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 144  | Reuniclus          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 145  | Tyrantrum          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 146  | Pangoro            |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 147  | Medicham-Mega      |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 148  | Conkeldurr         |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 149  | Medicham           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 150  | Swampert           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 151  | Electivire         |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 152  | Goodra             |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 153  | Sceptile-Mega      |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 154  | Gourgeist          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 155  | Arceus-Dragon      |  0.47170% | 1      |  0.472% | 0      |  0.000% | 
 | 156  | Noivern            |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 157  | Charizard          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 158  | Pikachu            |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 159  | Reshiram           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 160  | Kyogre             |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 161  | Diancie-Mega       |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 162  | Mienshao           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 163  | Clawitzer          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 164  | Steelix            |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 165  | Arceus-Fighting    |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 166  | Ninetales          |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 167  | Empoleon           |  0.47170% | 1      |  0.472% | 0      |  0.000% | 
 | 168  | Dusclops           |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 169  | Forretress         |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 | 170  | Kingdra            |  0.47170% | 1      |  0.472% | 1      |  0.622% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
