 Total leads: 24142
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Dragapult          |  7.66714% | 1851   |  7.667% | 
 | 2    | Toxtricity         |  4.61851% | 1115   |  4.619% | 
 | 3    | Cinderace          |  4.43211% | 1070   |  4.432% | 
 | 4    | Excadrill          |  3.47527% | 839    |  3.475% | 
 | 5    | Pelipper           |  2.81253% | 679    |  2.813% | 
 | 6    | Mew                |  2.80838% | 678    |  2.808% | 
 | 7    | Hydreigon          |  2.77110% | 669    |  2.771% | 
 | 8    | Dracovish          |  2.72554% | 658    |  2.726% | 
 | 9    | Corsola-Galar      |  2.58471% | 624    |  2.585% | 
 | 10   | Coalossal          |  2.46044% | 594    |  2.460% | 
 | 11   | Shuckle            |  2.26576% | 547    |  2.266% | 
 | 12   | Vikavolt           |  2.09179% | 505    |  2.092% | 
 | 13   | Ferrothorn         |  1.86811% | 451    |  1.868% | 
 | 14   | Darmanitan-Galar   |  1.68586% | 407    |  1.686% | 
 | 15   | Polteageist        |  1.62373% | 392    |  1.624% | 
 | 16   | Haxorus            |  1.57816% | 381    |  1.578% | 
 | 17   | Inteleon           |  1.35863% | 328    |  1.359% | 
 | 18   | Tyranitar          |  1.32963% | 321    |  1.330% | 
 | 19   | Orbeetle           |  1.29235% | 312    |  1.292% | 
 | 20   | Runerigus          |  1.17223% | 283    |  1.172% | 
 | 21   | Sirfetch'd         |  1.14738% | 277    |  1.147% | 
 | 22   | Kommo-o            |  1.12253% | 271    |  1.123% | 
 | 23   | Ribombee           |  1.11838% | 270    |  1.118% | 
 | 24   | Grimmsnarl         |  1.03968% | 251    |  1.040% | 
 | 25   | Boltund            |  1.03140% | 249    |  1.031% | 
 | 26   | Araquanid          |  1.01897% | 246    |  1.019% | 
 | 27   | Toxapex            |  0.98998% | 239    |  0.990% | 
 | 28   | Gyarados           |  0.98998% | 239    |  0.990% | 
 | 29   | Slurpuff           |  0.97341% | 235    |  0.973% | 
 | 30   | Hatterene          |  0.93613% | 226    |  0.936% | 
 | 31   | Cinccino           |  0.87400% | 211    |  0.874% | 
 | 32   | Weezing-Galar      |  0.87400% | 211    |  0.874% | 
 | 33   | Clefable           |  0.82843% | 200    |  0.828% | 
 | 34   | Copperajah         |  0.82015% | 198    |  0.820% | 
 | 35   | Obstagoon          |  0.81601% | 197    |  0.816% | 
 | 36   | Claydol            |  0.80358% | 194    |  0.804% | 
 | 37   | Barraskewda        |  0.79115% | 191    |  0.791% | 
 | 38   | Goodra             |  0.77873% | 188    |  0.779% | 
 | 39   | Cloyster           |  0.74559% | 180    |  0.746% | 
 | 40   | Torkoal            |  0.67517% | 163    |  0.675% | 
 | 41   | Noivern            |  0.65860% | 159    |  0.659% | 
 | 42   | Barbaracle         |  0.64618% | 156    |  0.646% | 
 | 43   | Diggersby          |  0.62132% | 150    |  0.621% | 
 | 44   | Seismitoad         |  0.61304% | 148    |  0.613% | 
 | 45   | Ditto              |  0.59647% | 144    |  0.596% | 
 | 46   | Galvantula         |  0.59647% | 144    |  0.596% | 
 | 47   | Rillaboom          |  0.58404% | 141    |  0.584% | 
 | 48   | Arcanine           |  0.57162% | 138    |  0.572% | 
 | 49   | Conkeldurr         |  0.56333% | 136    |  0.563% | 
 | 50   | Accelgor           |  0.53848% | 130    |  0.538% | 
 | 51   | Steelix            |  0.53848% | 130    |  0.538% | 
 | 52   | Gengar             |  0.53020% | 128    |  0.530% | 
 | 53   | Dracozolt          |  0.51777% | 125    |  0.518% | 
 | 54   | Corviknight        |  0.50949% | 123    |  0.509% | 
 | 55   | Rotom-Wash         |  0.50534% | 122    |  0.505% | 
 | 56   | Frosmoth           |  0.50120% | 121    |  0.501% | 
 | 57   | Avalugg            |  0.49706% | 120    |  0.497% | 
 | 58   | Aegislash          |  0.49706% | 120    |  0.497% | 
 | 59   | Ninetales          |  0.49706% | 120    |  0.497% | 
 | 60   | Mimikyu            |  0.47221% | 114    |  0.472% | 
 | 61   | Chandelure         |  0.45564% | 110    |  0.456% | 
 | 62   | Onix               |  0.43078% | 104    |  0.431% | 
 | 63   | Dugtrio            |  0.43078% | 104    |  0.431% | 
 | 64   | Lucario            |  0.39351% | 95     |  0.394% | 
 | 65   | Togekiss           |  0.39351% | 95     |  0.394% | 
 | 66   | Rhyperior          |  0.39351% | 95     |  0.394% | 
 | 67   | Bisharp            |  0.37279% | 90     |  0.373% | 
 | 68   | Falinks            |  0.34380% | 83     |  0.344% | 
 | 69   | Centiskorch        |  0.33551% | 81     |  0.336% | 
 | 70   | Mandibuzz          |  0.33137% | 80     |  0.331% | 
 | 71   | Vanilluxe          |  0.33137% | 80     |  0.331% | 
 | 72   | Ninjask            |  0.33137% | 80     |  0.331% | 
 | 73   | Perrserker         |  0.32309% | 78     |  0.323% | 
 | 74   | Dhelmise           |  0.31480% | 76     |  0.315% | 
 | 75   | Hawlucha           |  0.28995% | 70     |  0.290% | 
 | 76   | Glalie             |  0.28581% | 69     |  0.286% | 
 | 77   | Hippowdon          |  0.28167% | 68     |  0.282% | 
 | 78   | Pincurchin         |  0.27338% | 66     |  0.273% | 
 | 79   | Espeon             |  0.26510% | 64     |  0.265% | 
 | 80   | Cofagrigus         |  0.26096% | 63     |  0.261% | 
 | 81   | Jolteon            |  0.26096% | 63     |  0.261% | 
 | 82   | Gardevoir          |  0.26096% | 63     |  0.261% | 
 | 83   | Durant             |  0.25681% | 62     |  0.257% | 
 | 84   | Thievul            |  0.25681% | 62     |  0.257% | 
 | 85   | Indeedee           |  0.25267% | 61     |  0.253% | 
 | 86   | Bronzong           |  0.24853% | 60     |  0.249% | 
 | 87   | Flygon             |  0.24439% | 59     |  0.244% | 
 | 88   | Grapploct          |  0.24025% | 58     |  0.240% | 
 | 89   | Sandaconda         |  0.24025% | 58     |  0.240% | 
 | 90   | Gallade            |  0.24025% | 58     |  0.240% | 
 | 91   | Braviary           |  0.23610% | 57     |  0.236% | 
 | 92   | Abomasnow          |  0.23610% | 57     |  0.236% | 
 | 93   | Snorlax            |  0.23196% | 56     |  0.232% | 
 | 94   | Eiscue             |  0.21953% | 53     |  0.220% | 
 | 95   | Appletun           |  0.21125% | 51     |  0.211% | 
 | 96   | Passimian          |  0.20711% | 50     |  0.207% | 
 | 97   | Reuniclus          |  0.20297% | 49     |  0.203% | 
 | 98   | Weavile            |  0.20297% | 49     |  0.203% | 
 | 99   | Cramorant          |  0.19054% | 46     |  0.191% | 
 | 100  | Gigalith           |  0.19054% | 46     |  0.191% | 
 | 101  | Rotom-Heat         |  0.18640% | 45     |  0.186% | 
 | 102  | Flapple            |  0.17397% | 42     |  0.174% | 
 | 103  | Dubwool            |  0.16569% | 40     |  0.166% | 
 | 104  | Sigilyph           |  0.14498% | 35     |  0.145% | 
 | 105  | Butterfree         |  0.14083% | 34     |  0.141% | 
 | 106  | Charizard          |  0.14083% | 34     |  0.141% | 
 | 107  | Malamar            |  0.13669% | 33     |  0.137% | 
 | 108  | Crawdaunt          |  0.12841% | 31     |  0.128% | 
 | 109  | Jellicent          |  0.12426% | 30     |  0.124% | 
 | 110  | Dottler            |  0.12012% | 29     |  0.120% | 
 | 111  | Mamoswine          |  0.12012% | 29     |  0.120% | 
 | 112  | Whimsicott         |  0.11598% | 28     |  0.116% | 
 | 113  | Octillery          |  0.11598% | 28     |  0.116% | 
 | 114  | Mr. Rime           |  0.11598% | 28     |  0.116% | 
 | 115  | Duraludon          |  0.10770% | 26     |  0.108% | 
 | 116  | Froslass           |  0.10770% | 26     |  0.108% | 
 | 117  | Cursola            |  0.10355% | 25     |  0.104% | 
 | 118  | Hitmontop          |  0.10355% | 25     |  0.104% | 
 | 119  | Sylveon            |  0.09941% | 24     |  0.099% | 
 | 120  | Rotom-Fan          |  0.09941% | 24     |  0.099% | 
 | 121  | Tsareena           |  0.09941% | 24     |  0.099% | 
 | 122  | Lapras             |  0.09527% | 23     |  0.095% | 
 | 123  | Indeedee-F         |  0.09113% | 22     |  0.091% | 
 | 124  | Stonjourner        |  0.09113% | 22     |  0.091% | 
 | 125  | Drampa             |  0.08699% | 21     |  0.087% | 
 | 126  | Bewear             |  0.08284% | 20     |  0.083% | 
 | 127  | Mudsdale           |  0.08284% | 20     |  0.083% | 
 | 128  | Pyukumuku          |  0.07870% | 19     |  0.079% | 
 | 129  | Morpeko            |  0.07870% | 19     |  0.079% | 
 | 130  | Machamp            |  0.07456% | 18     |  0.075% | 
 | 131  | Mr. Mime-Galar     |  0.07042% | 17     |  0.070% | 
 | 132  | Milotic            |  0.07042% | 17     |  0.070% | 
 | 133  | Quagsire           |  0.07042% | 17     |  0.070% | 
 | 134  | Drednaw            |  0.07042% | 17     |  0.070% | 
 | 135  | Swoobat            |  0.06627% | 16     |  0.066% | 
 | 136  | Golurk             |  0.06627% | 16     |  0.066% | 
 | 137  | Sudowoodo          |  0.06627% | 16     |  0.066% | 
 | 138  | Gourgeist-Super    |  0.06213% | 15     |  0.062% | 
 | 139  | Heliolisk          |  0.06213% | 15     |  0.062% | 
 | 140  | Greedent           |  0.06213% | 15     |  0.062% | 
 | 141  | Rotom-Frost        |  0.05799% | 14     |  0.058% | 
 | 142  | Golisopod          |  0.05799% | 14     |  0.058% | 
 | 143  | Lanturn            |  0.05385% | 13     |  0.054% | 
 | 144  | Vaporeon           |  0.05385% | 13     |  0.054% | 
 | 145  | Linoone-Galar      |  0.05385% | 13     |  0.054% | 
 | 146  | Trevenant          |  0.05385% | 13     |  0.054% | 
 | 147  | Pikachu            |  0.05385% | 13     |  0.054% | 
 | 148  | Salazzle           |  0.05385% | 13     |  0.054% | 
 | 149  | Lunatone           |  0.04971% | 12     |  0.050% | 
 | 150  | Bellossom          |  0.04971% | 12     |  0.050% | 
 | 151  | Basculin           |  0.04971% | 12     |  0.050% | 
 | 152  | Alcremie           |  0.04971% | 12     |  0.050% | 
 | 153  | Crustle            |  0.04971% | 12     |  0.050% | 
 | 154  | Raichu             |  0.04556% | 11     |  0.046% | 
 | 155  | Pangoro            |  0.04556% | 11     |  0.046% | 
 | 156  | Type: Null         |  0.04556% | 11     |  0.046% | 
 | 157  | Sableye            |  0.04556% | 11     |  0.046% | 
 | 158  | Gastrodon          |  0.04556% | 11     |  0.046% | 
 | 159  | Vespiquen          |  0.04556% | 11     |  0.046% | 
 | 160  | Rhydon             |  0.04142% | 10     |  0.041% | 
 | 161  | Doublade           |  0.04142% | 10     |  0.041% | 
 | 162  | Arctozolt          |  0.04142% | 10     |  0.041% | 
 | 163  | Maractus           |  0.04142% | 10     |  0.041% | 
 | 164  | Xatu               |  0.04142% | 10     |  0.041% | 
 | 165  | Silvally           |  0.04142% | 10     |  0.041% | 
 | 166  | Umbreon            |  0.03728% | 9      |  0.037% | 
 | 167  | Beartic            |  0.03728% | 9      |  0.037% | 
 | 168  | Beheeyem           |  0.03728% | 9      |  0.037% | 
 | 169  | Aromatisse         |  0.03728% | 9      |  0.037% | 
 | 170  | Kingler            |  0.03728% | 9      |  0.037% | 
 | 171  | Roserade           |  0.03728% | 9      |  0.037% | 
 | 172  | Hitmonchan         |  0.03728% | 9      |  0.037% | 
 | 173  | Scrafty            |  0.03314% | 8      |  0.033% | 
 | 174  | Rapidash-Galar     |  0.03314% | 8      |  0.033% | 
 | 175  | Drifblim           |  0.03314% | 8      |  0.033% | 
 | 176  | Unfezant           |  0.03314% | 8      |  0.033% | 
 | 177  | Vileplume          |  0.03314% | 8      |  0.033% | 
 | 178  | Eevee              |  0.02900% | 7      |  0.029% | 
 | 179  | Mantine            |  0.02900% | 7      |  0.029% | 
 | 180  | Carkol             |  0.02900% | 7      |  0.029% | 
 | 181  | Toxicroak          |  0.02900% | 7      |  0.029% | 
 | 182  | Drapion            |  0.02900% | 7      |  0.029% | 
 | 183  | Sawk               |  0.02900% | 7      |  0.029% | 
 | 184  | Mawile             |  0.02900% | 7      |  0.029% | 
 | 185  | Glaceon            |  0.02485% | 6      |  0.025% | 
 | 186  | Klinklang          |  0.02485% | 6      |  0.025% | 
 | 187  | Flareon            |  0.02485% | 6      |  0.025% | 
 | 188  | Escavalier         |  0.02071% | 5      |  0.021% | 
 | 189  | Leafeon            |  0.02071% | 5      |  0.021% | 
 | 190  | Dusclops           |  0.02071% | 5      |  0.021% | 
 | 191  | Silvally-Water     |  0.01657% | 4      |  0.017% | 
 | 192  | Gothitelle         |  0.01657% | 4      |  0.017% | 
 | 193  | Cherrim            |  0.01657% | 4      |  0.017% | 
 | 194  | Shiinotic          |  0.01657% | 4      |  0.017% | 
 | 195  | Persian            |  0.01657% | 4      |  0.017% | 
 | 196  | Eldegoss           |  0.01657% | 4      |  0.017% | 
 | 197  | Solrock            |  0.01657% | 4      |  0.017% | 
 | 198  | Garbodor           |  0.01657% | 4      |  0.017% | 
 | 199  | Wobbuffet          |  0.01243% | 3      |  0.012% | 
 | 200  | Diglett            |  0.01243% | 3      |  0.012% | 
 | 201  | Roselia            |  0.01243% | 3      |  0.012% | 
 | 202  | Dusknoir           |  0.01243% | 3      |  0.012% | 
 | 203  | Meowstic           |  0.01243% | 3      |  0.012% | 
 | 204  | Meowth             |  0.01243% | 3      |  0.012% | 
 | 205  | Mr. Mime           |  0.01243% | 3      |  0.012% | 
 | 206  | Wailord            |  0.01243% | 3      |  0.012% | 
 | 207  | Stunfisk-Galar     |  0.01243% | 3      |  0.012% | 
 | 208  | Gourgeist-Small    |  0.01243% | 3      |  0.012% | 
 | 209  | Hitmonlee          |  0.00828% | 2      |  0.008% | 
 | 210  | Togedemaru         |  0.00828% | 2      |  0.008% | 
 | 211  | Ponyta-Galar       |  0.00828% | 2      |  0.008% | 
 | 212  | Arctovish          |  0.00828% | 2      |  0.008% | 
 | 213  | Silvally-Ghost     |  0.00828% | 2      |  0.008% | 
 | 214  | Honedge            |  0.00828% | 2      |  0.008% | 
 | 215  | Silvally-Grass     |  0.00828% | 2      |  0.008% | 
 | 216  | Clefairy           |  0.00828% | 2      |  0.008% | 
 | 217  | Liepard            |  0.00414% | 1      |  0.004% | 
 | 218  | Farfetch'd-Galar   |  0.00414% | 1      |  0.004% | 
 | 219  | Silvally-Steel     |  0.00414% | 1      |  0.004% | 
 | 220  | Rotom              |  0.00414% | 1      |  0.004% | 
 | 221  | Hakamo-o           |  0.00414% | 1      |  0.004% | 
 | 222  | Silvally-Fairy     |  0.00414% | 1      |  0.004% | 
 | 223  | Turtonator         |  0.00414% | 1      |  0.004% | 
 | 224  | Sobble             |  0.00414% | 1      |  0.004% | 
 | 225  | Swirlix            |  0.00414% | 1      |  0.004% | 
 | 226  | Whiscash           |  0.00414% | 1      |  0.004% | 
 | 227  | Gastly             |  0.00414% | 1      |  0.004% | 
 | 228  | Zigzagoon-Galar    |  0.00414% | 1      |  0.004% | 
 | 229  | Hattrem            |  0.00414% | 1      |  0.004% | 
 | 230  | Grookey            |  0.00414% | 1      |  0.004% | 
 | 231  | Delibird           |  0.00414% | 1      |  0.004% | 
 | 232  | Silvally-Electric  |  0.00414% | 1      |  0.004% | 
 | 233  | Shiftry            |  0.00414% | 1      |  0.004% | 
 | 234  | Qwilfish           |  0.00414% | 1      |  0.004% | 
 | 235  | Ludicolo           |  0.00414% | 1      |  0.004% | 
 | 236  | Sliggoo            |  0.00414% | 1      |  0.004% | 
 | 237  | Yamper             |  0.00414% | 1      |  0.004% | 
 | 238  | Wooloo             |  0.00414% | 1      |  0.004% | 
 | 239  | Riolu              |  0.00414% | 1      |  0.004% | 
 | 240  | Mareanie           |  0.00414% | 1      |  0.004% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
