 Total leads: 245176
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Sableye            | 12.44321% | 13165  |  5.370% | 
 | 2    | Mimikyu            |  7.59057% | 15114  |  6.165% | 
 | 3    | Aegislash          |  5.66536% | 8695   |  3.546% | 
 | 4    | Mew                |  5.14591% | 14944  |  6.095% | 
 | 5    | Corviknight        |  4.11286% | 6412   |  2.615% | 
 | 6    | Dragapult          |  3.77672% | 7258   |  2.960% | 
 | 7    | Corsola-Galar      |  3.73249% | 8860   |  3.614% | 
 | 8    | Dracovish          |  3.70228% | 6967   |  2.842% | 
 | 9    | Darmanitan-Galar   |  3.65206% | 5909   |  2.410% | 
 | 10   | Togekiss           |  3.25012% | 6089   |  2.484% | 
 | 11   | Dubwool            |  2.21001% | 2696   |  1.100% | 
 | 12   | Whimsicott         |  2.17679% | 6237   |  2.544% | 
 | 13   | Grimmsnarl         |  2.15776% | 6331   |  2.582% | 
 | 14   | Cinderace          |  1.70350% | 4365   |  1.780% | 
 | 15   | Crustle            |  1.37798% | 2451   |  1.000% | 
 | 16   | Gastrodon          |  1.25629% | 2464   |  1.005% | 
 | 17   | Sylveon            |  1.21151% | 3328   |  1.357% | 
 | 18   | Arcanine           |  1.11299% | 1660   |  0.677% | 
 | 19   | Ferrothorn         |  1.09074% | 3035   |  1.238% | 
 | 20   | Rotom-Heat         |  1.08569% | 1636   |  0.667% | 
 | 21   | Togedemaru         |  0.98580% | 2603   |  1.062% | 
 | 22   | Weezing-Galar      |  0.97213% | 2370   |  0.967% | 
 | 23   | Excadrill          |  0.94385% | 1888   |  0.770% | 
 | 24   | Tyranitar          |  0.86880% | 1630   |  0.665% | 
 | 25   | Conkeldurr         |  0.85936% | 1944   |  0.793% | 
 | 26   | Milotic            |  0.83544% | 1646   |  0.671% | 
 | 27   | Golisopod          |  0.82538% | 2319   |  0.946% | 
 | 28   | Pyukumuku          |  0.82481% | 3070   |  1.252% | 
 | 29   | Hydreigon          |  0.79929% | 1632   |  0.666% | 
 | 30   | Toxapex            |  0.78011% | 2833   |  1.155% | 
 | 31   | Rotom-Wash         |  0.73609% | 1790   |  0.730% | 
 | 32   | Aromatisse         |  0.65770% | 831    |  0.339% | 
 | 33   | Shuckle            |  0.63588% | 3557   |  1.451% | 
 | 34   | Hatterene          |  0.60981% | 1762   |  0.719% | 
 | 35   | Haxorus            |  0.60438% | 1661   |  0.677% | 
 | 36   | Alcremie           |  0.58405% | 1092   |  0.445% | 
 | 37   | Sawk               |  0.53368% | 1670   |  0.681% | 
 | 38   | Charizard          |  0.51649% | 1033   |  0.421% | 
 | 39   | Ditto              |  0.51607% | 2744   |  1.119% | 
 | 40   | Toxtricity         |  0.50455% | 1893   |  0.772% | 
 | 41   | Inteleon           |  0.47550% | 1926   |  0.786% | 
 | 42   | Obstagoon          |  0.46365% | 1974   |  0.805% | 
 | 43   | Weavile            |  0.42986% | 629    |  0.257% | 
 | 44   | Bewear             |  0.42371% | 763    |  0.311% | 
 | 45   | Cramorant          |  0.40883% | 1744   |  0.711% | 
 | 46   | Vaporeon           |  0.38668% | 838    |  0.342% | 
 | 47   | Barraskewda        |  0.38380% | 581    |  0.237% | 
 | 48   | Rhyperior          |  0.38090% | 1060   |  0.432% | 
 | 49   | Glalie             |  0.37638% | 1120   |  0.457% | 
 | 50   | Hawlucha           |  0.36807% | 950    |  0.387% | 
 | 51   | Appletun           |  0.35742% | 1192   |  0.486% | 
 | 52   | Rillaboom          |  0.35687% | 1423   |  0.580% | 
 | 53   | Chandelure         |  0.34942% | 709    |  0.289% | 
 | 54   | Duraludon          |  0.34901% | 1454   |  0.593% | 
 | 55   | Gyarados           |  0.34737% | 1327   |  0.541% | 
 | 56   | Bisharp            |  0.34598% | 1166   |  0.476% | 
 | 57   | Vikavolt           |  0.32982% | 292    |  0.119% | 
 | 58   | Snorlax            |  0.31715% | 2022   |  0.825% | 
 | 59   | Dracozolt          |  0.31627% | 1210   |  0.494% | 
 | 60   | Mandibuzz          |  0.28962% | 666    |  0.272% | 
 | 61   | Durant             |  0.28660% | 795    |  0.324% | 
 | 62   | Eiscue             |  0.28638% | 2454   |  1.001% | 
 | 63   | Clefable           |  0.27843% | 1247   |  0.509% | 
 | 64   | Ninetales          |  0.26192% | 289    |  0.118% | 
 | 65   | Cloyster           |  0.25744% | 1134   |  0.463% | 
 | 66   | Gigalith           |  0.25453% | 552    |  0.225% | 
 | 67   | Kommo-o            |  0.24800% | 711    |  0.290% | 
 | 68   | Gengar             |  0.23275% | 1165   |  0.475% | 
 | 69   | Lapras             |  0.20728% | 838    |  0.342% | 
 | 70   | Roserade           |  0.19238% | 237    |  0.097% | 
 | 71   | Butterfree         |  0.19152% | 389    |  0.159% | 
 | 72   | Falinks            |  0.18479% | 931    |  0.380% | 
 | 73   | Sirfetch'd         |  0.18203% | 909    |  0.371% | 
 | 74   | Polteageist        |  0.17657% | 516    |  0.210% | 
 | 75   | Salazzle           |  0.17081% | 844    |  0.344% | 
 | 76   | Seismitoad         |  0.16744% | 628    |  0.256% | 
 | 77   | Indeedee-F         |  0.16548% | 364    |  0.148% | 
 | 78   | Steelix            |  0.16460% | 829    |  0.338% | 
 | 79   | Avalugg            |  0.16444% | 439    |  0.179% | 
 | 80   | Mudsdale           |  0.16110% | 198    |  0.081% | 
 | 81   | Copperajah         |  0.16035% | 495    |  0.202% | 
 | 82   | Arctovish          |  0.15505% | 333    |  0.136% | 
 | 83   | Araquanid          |  0.15135% | 512    |  0.209% | 
 | 84   | Sudowoodo          |  0.15103% | 276    |  0.113% | 
 | 85   | Wobbuffet          |  0.14003% | 668    |  0.272% | 
 | 86   | Jellicent          |  0.13928% | 501    |  0.204% | 
 | 87   | Gloom              |  0.13805% | 122    |  0.050% | 
 | 88   | Quagsire           |  0.13610% | 632    |  0.258% | 
 | 89   | Lucario            |  0.13113% | 503    |  0.205% | 
 | 90   | Type: Null         |  0.12986% | 551    |  0.225% | 
 | 91   | Goodra             |  0.12792% | 663    |  0.270% | 
 | 92   | Centiskorch        |  0.11975% | 989    |  0.403% | 
 | 93   | Reuniclus          |  0.10987% | 497    |  0.203% | 
 | 94   | Frosmoth           |  0.10861% | 301    |  0.123% | 
 | 95   | Espeon             |  0.10683% | 305    |  0.124% | 
 | 96   | Drapion            |  0.10326% | 685    |  0.279% | 
 | 97   | Gardevoir          |  0.10254% | 688    |  0.281% | 
 | 98   | Wailord            |  0.10248% | 152    |  0.062% | 
 | 99   | Cinccino           |  0.09798% | 430    |  0.175% | 
 | 100  | Scrafty            |  0.09732% | 805    |  0.328% | 
 | 101  | Runerigus          |  0.09606% | 371    |  0.151% | 
 | 102  | Dusclops           |  0.09489% | 706    |  0.288% | 
 | 103  | Cofagrigus         |  0.08769% | 209    |  0.085% | 
 | 104  | Umbreon            |  0.08704% | 990    |  0.404% | 
 | 105  | Doublade           |  0.08424% | 278    |  0.113% | 
 | 106  | Hitmonlee          |  0.08173% | 1026   |  0.418% | 
 | 107  | Machamp            |  0.07004% | 418    |  0.170% | 
 | 108  | Garbodor           |  0.06748% | 272    |  0.111% | 
 | 109  | Ludicolo           |  0.06286% | 379    |  0.155% | 
 | 110  | Meowstic           |  0.06251% | 107    |  0.044% | 
 | 111  | Malamar            |  0.06107% | 418    |  0.170% | 
 | 112  | Crawdaunt          |  0.05811% | 259    |  0.106% | 
 | 113  | Gallade            |  0.05611% | 352    |  0.144% | 
 | 114  | Grapploct          |  0.05321% | 405    |  0.165% | 
 | 115  | Greedent           |  0.05273% | 952    |  0.388% | 
 | 116  | Trevenant          |  0.05157% | 565    |  0.230% | 
 | 117  | Barbaracle         |  0.05110% | 267    |  0.109% | 
 | 118  | Coalossal          |  0.04811% | 375    |  0.153% | 
 | 119  | Dhelmise           |  0.04711% | 135    |  0.055% | 
 | 120  | Maractus           |  0.04176% | 89     |  0.036% | 
 | 121  | Mr. Mime           |  0.03835% | 169    |  0.069% | 
 | 122  | Ribombee           |  0.03727% | 269    |  0.110% | 
 | 123  | Orbeetle           |  0.03660% | 517    |  0.211% | 
 | 124  | Heatmor            |  0.03542% | 15     |  0.006% | 
 | 125  | Golurk             |  0.03415% | 166    |  0.068% | 
 | 126  | Wishiwashi         |  0.03363% | 178    |  0.073% | 
 | 127  | Beheeyem           |  0.03330% | 54     |  0.022% | 
 | 128  | Abomasnow          |  0.03258% | 77     |  0.031% | 
 | 129  | Hippowdon          |  0.03247% | 277    |  0.113% | 
 | 130  | Pelipper           |  0.03041% | 173    |  0.071% | 
 | 131  | Flygon             |  0.03029% | 134    |  0.055% | 
 | 132  | Heliolisk          |  0.03019% | 53     |  0.022% | 
 | 133  | Onix               |  0.02981% | 160    |  0.065% | 
 | 134  | Morpeko            |  0.02949% | 250    |  0.102% | 
 | 135  | Mamoswine          |  0.02943% | 175    |  0.071% | 
 | 136  | Throh              |  0.02660% | 75     |  0.031% | 
 | 137  | Rhydon             |  0.02639% | 99     |  0.040% | 
 | 138  | Shiinotic          |  0.02582% | 193    |  0.079% | 
 | 139  | Machoke            |  0.02578% | 21     |  0.009% | 
 | 140  | Escavalier         |  0.02576% | 228    |  0.093% | 
 | 141  | Cursola            |  0.02439% | 477    |  0.195% | 
 | 142  | Pangoro            |  0.02331% | 92     |  0.038% | 
 | 143  | Manectric          |  0.02326% | 26     |  0.011% | 
 | 144  | Eldegoss           |  0.02325% | 287    |  0.117% | 
 | 145  | Pincurchin         |  0.02310% | 327    |  0.133% | 
 | 146  | Accelgor           |  0.02290% | 331    |  0.135% | 
 | 147  | Leafeon            |  0.02287% | 199    |  0.081% | 
 | 148  | Gourgeist-Super    |  0.02117% | 516    |  0.210% | 
 | 149  | Stonjourner        |  0.02056% | 91     |  0.037% | 
 | 150  | Snom               |  0.02051% | 184    |  0.075% | 
 | 151  | Jolteon            |  0.02026% | 154    |  0.063% | 
 | 152  | Dusknoir           |  0.01982% | 540    |  0.220% | 
 | 153  | Perrserker         |  0.01967% | 362    |  0.148% | 
 | 154  | Boltund            |  0.01850% | 198    |  0.081% | 
 | 155  | Rotom              |  0.01726% | 16     |  0.007% | 
 | 156  | Shedinja           |  0.01613% | 318    |  0.130% | 
 | 157  | Linoone-Galar      |  0.01545% | 22     |  0.009% | 
 | 158  | Froslass           |  0.01492% | 70     |  0.029% | 
 | 159  | Vanilluxe          |  0.01473% | 113    |  0.046% | 
 | 160  | Pikachu            |  0.01464% | 310    |  0.126% | 
 | 161  | Torkoal            |  0.01446% | 252    |  0.103% | 
 | 162  | Sigilyph           |  0.01405% | 167    |  0.068% | 
 | 163  | Flapple            |  0.01398% | 292    |  0.119% | 
 | 164  | Riolu              |  0.01389% | 379    |  0.155% | 
 | 165  | Drampa             |  0.01333% | 135    |  0.055% | 
 | 166  | Musharna           |  0.01282% | 62     |  0.025% | 
 | 167  | Rotom-Mow          |  0.01274% | 117    |  0.048% | 
 | 168  | Kingler            |  0.01234% | 139    |  0.057% | 
 | 169  | Bellossom          |  0.01229% | 185    |  0.075% | 
 | 170  | Duosion            |  0.01194% | 11     |  0.004% | 
 | 171  | Noivern            |  0.01155% | 153    |  0.062% | 
 | 172  | Turtonator         |  0.01130% | 193    |  0.079% | 
 | 173  | Diggersby          |  0.01128% | 213    |  0.087% | 
 | 174  | Silvally           |  0.01067% | 90     |  0.037% | 
 | 175  | Flareon            |  0.01065% | 79     |  0.032% | 
 | 176  | Gothitelle         |  0.01035% | 132    |  0.054% | 
 | 177  | Braviary           |  0.01007% | 131    |  0.053% | 
 | 178  | Arctozolt          |  0.00960% | 153    |  0.062% | 
 | 179  | Silvally-Water     |  0.00947% | 14     |  0.006% | 
 | 180  | Silvally-Ground    |  0.00917% | 12     |  0.005% | 
 | 181  | Drednaw            |  0.00810% | 168    |  0.069% | 
 | 182  | Silvally-Fairy     |  0.00797% | 67     |  0.027% | 
 | 183  | Whiscash           |  0.00782% | 39     |  0.016% | 
 | 184  | Silvally-Dark      |  0.00758% | 85     |  0.035% | 
 | 185  | Mr. Mime-Galar     |  0.00755% | 150    |  0.061% | 
 | 186  | Lombre             |  0.00743% | 8      |  0.003% | 
 | 187  | Octillery          |  0.00704% | 496    |  0.202% | 
 | 188  | Drifblim           |  0.00691% | 204    |  0.083% | 
 | 189  | Eevee              |  0.00555% | 145    |  0.059% | 
 | 190  | Mr. Rime           |  0.00519% | 188    |  0.077% | 
 | 191  | Xatu               |  0.00502% | 113    |  0.046% | 
 | 192  | Tsareena           |  0.00500% | 251    |  0.102% | 
 | 193  | Indeedee           |  0.00497% | 433    |  0.177% | 
 | 194  | Sandaconda         |  0.00471% | 335    |  0.137% | 
 | 195  | Unfezant           |  0.00469% | 66     |  0.027% | 
 | 196  | Silvally-Steel     |  0.00462% | 71     |  0.029% | 
 | 197  | Galvantula         |  0.00438% | 230    |  0.094% | 
 | 198  | Klang              |  0.00433% | 27     |  0.011% | 
 | 199  | Raichu             |  0.00356% | 76     |  0.031% | 
 | 200  | Hitmontop          |  0.00354% | 157    |  0.064% | 
 | 201  | Rapidash-Galar     |  0.00343% | 173    |  0.071% | 
 | 202  | Dugtrio            |  0.00303% | 122    |  0.050% | 
 | 203  | Bronzong           |  0.00301% | 369    |  0.151% | 
 | 204  | Thievul            |  0.00298% | 78     |  0.032% | 
 | 205  | Stunfisk-Galar     |  0.00273% | 84     |  0.034% | 
 | 206  | Morgrem            |  0.00261% | 55     |  0.022% | 
 | 207  | Carkol             |  0.00252% | 33     |  0.013% | 
 | 208  | Liepard            |  0.00230% | 50     |  0.020% | 
 | 209  | Scraggy            |  0.00216% | 18     |  0.007% | 
 | 210  | Wooloo             |  0.00212% | 88     |  0.036% | 
 | 211  | Sneasel            |  0.00196% | 39     |  0.016% | 
 | 212  | Lanturn            |  0.00184% | 145    |  0.059% | 
 | 213  | Bonsly             |  0.00181% | 23     |  0.009% | 
 | 214  | Silvally-Fire      |  0.00176% | 77     |  0.031% | 
 | 215  | Bronzor            |  0.00173% | 32     |  0.013% | 
 | 216  | Silvally-Dragon    |  0.00158% | 24     |  0.010% | 
 | 217  | Oranguru           |  0.00144% | 17     |  0.007% | 
 | 218  | Munchlax           |  0.00142% | 51     |  0.021% | 
 | 219  | Skuntank           |  0.00141% | 59     |  0.024% | 
 | 220  | Toxicroak          |  0.00139% | 98     |  0.040% | 
 | 221  | Remoraid           |  0.00133% | 8      |  0.003% | 
 | 222  | Slurpuff           |  0.00132% | 51     |  0.021% | 
 | 223  | Mawile             |  0.00131% | 117    |  0.048% | 
 | 224  | Dwebble            |  0.00122% | 58     |  0.024% | 
 | 225  | Persian            |  0.00122% | 182    |  0.074% | 
 | 226  | Ninjask            |  0.00104% | 187    |  0.076% | 
 | 227  | Klinklang          |  0.00103% | 214    |  0.087% | 
 | 228  | Meowth-Galar       |  0.00102% | 9      |  0.004% | 
 | 229  | Togetic            |  0.00097% | 39     |  0.016% | 
 | 230  | Claydol            |  0.00078% | 61     |  0.025% | 
 | 231  | Magikarp           |  0.00076% | 144    |  0.059% | 
 | 232  | Clefairy           |  0.00069% | 45     |  0.018% | 
 | 233  | Hitmonchan         |  0.00069% | 74     |  0.030% | 
 | 234  | Raboot             |  0.00062% | 90     |  0.037% | 
 | 235  | Dreepy             |  0.00061% | 30     |  0.012% | 
 | 236  | Cottonee           |  0.00061% | 72     |  0.029% | 
 | 237  | Yamper             |  0.00058% | 59     |  0.024% | 
 | 238  | Growlithe          |  0.00055% | 13     |  0.005% | 
 | 239  | Impidimp           |  0.00051% | 103    |  0.042% | 
 | 240  | Hattrem            |  0.00050% | 113    |  0.046% | 
 | 241  | Dottler            |  0.00041% | 92     |  0.038% | 
 | 242  | Snorunt            |  0.00028% | 7      |  0.003% | 
 | 243  | Cherrim            |  0.00028% | 35     |  0.014% | 
 | 244  | Passimian          |  0.00024% | 54     |  0.022% | 
 | 245  | Shiftry            |  0.00023% | 62     |  0.025% | 
 | 246  | Silvally-Poison    |  0.00019% | 57     |  0.023% | 
 | 247  | Lunatone           |  0.00019% | 22     |  0.009% | 
 | 248  | Hatenna            |  0.00018% | 24     |  0.010% | 
 | 249  | Basculin           |  0.00016% | 27     |  0.011% | 
 | 250  | Beartic            |  0.00015% | 40     |  0.016% | 
 | 251  | Vulpix             |  0.00012% | 12     |  0.005% | 
 | 252  | Charmander         |  0.00012% | 10     |  0.004% | 
 | 253  | Vileplume          |  0.00012% | 132    |  0.054% | 
 | 254  | Grookey            |  0.00011% | 23     |  0.009% | 
 | 255  | Vullaby            |  0.00008% | 14     |  0.006% | 
 | 256  | Solrock            |  0.00008% | 40     |  0.016% | 
 | 257  | Drakloak           |  0.00008% | 54     |  0.022% | 
 | 258  | Pichu              |  0.00007% | 90     |  0.037% | 
 | 259  | Swoobat            |  0.00006% | 62     |  0.025% | 
 | 260  | Salandit           |  0.00005% | 17     |  0.007% | 
 | 261  | Toxel              |  0.00004% | 7      |  0.003% | 
 | 262  | Glaceon            |  0.00004% | 83     |  0.034% | 
 | 263  | Litwick            |  0.00004% | 6      |  0.002% | 
 | 264  | Haunter            |  0.00003% | 18     |  0.007% | 
 | 265  | Spritzee           |  0.00003% | 6      |  0.002% | 
 | 266  | Deino              |  0.00003% | 6      |  0.002% | 
 | 267  | Vespiquen          |  0.00003% | 22     |  0.009% | 
 | 268  | Honedge            |  0.00002% | 36     |  0.015% | 
 | 269  | Delibird           |  0.00002% | 71     |  0.029% | 
 | 270  | Pawniard           |  0.00001% | 6      |  0.002% | 
 | 271  | Mantine            |  0.00001% | 32     |  0.013% | 
 | 272  | Milcery            |  0.00001% | 12     |  0.005% | 
 | 273  | Thwackey           |  0.00001% | 29     |  0.012% | 
 | 274  | Stunky             |  0.00001% | 12     |  0.005% | 
 | 275  | Trubbish           |  0.00001% | 5      |  0.002% | 
 | 276  | Cleffa             |  0.00001% | 14     |  0.006% | 
 | 277  | Gothita            |  0.00000% | 44     |  0.018% | 
 | 278  | Baltoy             |  0.00000% | 4      |  0.002% | 
 | 279  | Swirlix            |  0.00000% | 8      |  0.003% | 
 | 280  | Metapod            |  0.00000% | 38     |  0.015% | 
 | 281  | Cufant             |  0.00000% | 12     |  0.005% | 
 | 282  | Silvally-Electric  |  0.00000% | 11     |  0.004% | 
 | 283  | Gastly             |  0.00000% | 16     |  0.007% | 
 | 284  | Sobble             |  0.00000% | 12     |  0.005% | 
 | 285  | Qwilfish           |  0.00000% | 45     |  0.018% | 
 | 286  | Applin             |  0.00000% | 33     |  0.013% | 
 | 287  | Ralts              |  0.00000% | 32     |  0.013% | 
 | 288  | Roselia            |  0.00000% | 11     |  0.004% | 
 | 289  | Pancham            |  0.00000% | 6      |  0.002% | 
 | 290  | Woobat             |  0.00000% | 3      |  0.001% | 
 | 291  | Mime Jr.           |  0.00000% | 39     |  0.016% | 
 | 292  | Gourgeist-Small    |  0.00000% | 80     |  0.033% | 
 | 293  | Skwovet            |  0.00000% | 4      |  0.002% | 
 | 294  | Gourgeist          |  0.00000% | 16     |  0.007% | 
 | 295  | Oddish             |  0.00000% | 43     |  0.018% | 
 | 296  | Rufflet            |  0.00000% | 19     |  0.008% | 
 | 297  | Piloswine          |  0.00000% | 39     |  0.016% | 
 | 298  | Pumpkaboo          |  0.00000% | 6      |  0.002% | 
 | 299  | Rookidee           |  0.00000% | 13     |  0.005% | 
 | 300  | Drizzile           |  0.00000% | 28     |  0.011% | 
 | 301  | Noibat             |  0.00000% | 21     |  0.009% | 
 | 302  | Rotom-Fan          |  0.00000% | 18     |  0.007% | 
 | 303  | Charjabug          |  0.00000% | 14     |  0.006% | 
 | 304  | Koffing            |  0.00000% | 7      |  0.003% | 
 | 305  | Silvally-Flying    |  0.00000% | 8      |  0.003% | 
 | 306  | Farfetch'd-Galar   |  0.00000% | 15     |  0.006% | 
 | 307  | Wynaut             |  0.00000% | 18     |  0.007% | 
 | 308  | Gurdurr            |  0.00000% | 54     |  0.022% | 
 | 309  | Karrablast         |  0.00000% | 14     |  0.006% | 
 | 310  | Rotom-Frost        |  0.00000% | 15     |  0.006% | 
 | 311  | Wailmer            |  0.00000% | 24     |  0.010% | 
 | 312  | Feebas             |  0.00000% | 2      |  0.001% | 
 | 313  | Corphish           |  0.00000% | 7      |  0.003% | 
 | 314  | Caterpie           |  0.00000% | 9      |  0.004% | 
 | 315  | Silvally-Fighting  |  0.00000% | 5      |  0.002% | 
 | 316  | Togepi             |  0.00000% | 19     |  0.008% | 
 | 317  | Chinchou           |  0.00000% | 9      |  0.004% | 
 | 318  | Ponyta-Galar       |  0.00000% | 24     |  0.010% | 
 | 319  | Corvisquire        |  0.00000% | 4      |  0.002% | 
 | 320  | Silvally-Ghost     |  0.00000% | 13     |  0.005% | 
 | 321  | Sliggoo            |  0.00000% | 2      |  0.001% | 
 | 322  | Diglett            |  0.00000% | 1      |  0.000% | 
 | 323  | Clobbopus          |  0.00000% | 14     |  0.006% | 
 | 324  | Noctowl            |  0.00000% | 29     |  0.012% | 
 | 325  | Hakamo-o           |  0.00000% | 28     |  0.011% | 
 | 326  | Scorbunny          |  0.00000% | 16     |  0.007% | 
 | 327  | Phantump           |  0.00000% | 36     |  0.015% | 
 | 328  | Rolycoly           |  0.00000% | 26     |  0.011% | 
 | 329  | Shelmet            |  0.00000% | 5      |  0.002% | 
 | 330  | Krabby             |  0.00000% | 11     |  0.004% | 
 | 331  | Jangmo-o           |  0.00000% | 5      |  0.002% | 
 | 332  | Mudbray            |  0.00000% | 17     |  0.007% | 
 | 333  | Roggenrola         |  0.00000% | 4      |  0.002% | 
 | 334  | Silicobra          |  0.00000% | 40     |  0.016% | 
 | 335  | Gothorita          |  0.00000% | 10     |  0.004% | 
 | 336  | Mantyke            |  0.00000% | 7      |  0.003% | 
 | 337  | Binacle            |  0.00000% | 2      |  0.001% | 
 | 338  | Wimpod             |  0.00000% | 5      |  0.002% | 
 | 339  | Wooper             |  0.00000% | 15     |  0.006% | 
 | 340  | Minccino           |  0.00000% | 5      |  0.002% | 
 | 341  | Vanillish          |  0.00000% | 2      |  0.001% | 
 | 342  | Kirlia             |  0.00000% | 15     |  0.006% | 
 | 343  | Espurr             |  0.00000% | 2      |  0.001% | 
 | 344  | Cutiefly           |  0.00000% | 2      |  0.001% | 
 | 345  | Bunnelby           |  0.00000% | 1      |  0.000% | 
 | 346  | Joltik             |  0.00000% | 2      |  0.001% | 
 | 347  | Wingull            |  0.00000% | 1      |  0.000% | 
 | 348  | Pidove             |  0.00000% | 9      |  0.004% | 
 | 349  | Purrloin           |  0.00000% | 4      |  0.002% | 
 | 350  | Tympole            |  0.00000% | 5      |  0.002% | 
 | 351  | Steenee            |  0.00000% | 9      |  0.004% | 
 | 352  | Barboach           |  0.00000% | 6      |  0.002% | 
 | 353  | Silvally-Rock      |  0.00000% | 2      |  0.001% | 
 | 354  | Budew              |  0.00000% | 3      |  0.001% | 
 | 355  | Grubbin            |  0.00000% | 7      |  0.003% | 
 | 356  | Klink              |  0.00000% | 1      |  0.000% | 
 | 357  | Boldore            |  0.00000% | 14     |  0.006% | 
 | 358  | Ferroseed          |  0.00000% | 19     |  0.008% | 
 | 359  | Seedot             |  0.00000% | 7      |  0.003% | 
 | 360  | Electrike          |  0.00000% | 3      |  0.001% | 
 | 361  | Darumaka-Galar     |  0.00000% | 68     |  0.028% | 
 | 362  | Trapinch           |  0.00000% | 5      |  0.002% | 
 | 363  | Duskull            |  0.00000% | 7      |  0.003% | 
 | 364  | Silvally-Psychic   |  0.00000% | 5      |  0.002% | 
 | 365  | Elgyem             |  0.00000% | 1      |  0.000% | 
 | 366  | Seaking            |  0.00000% | 20     |  0.008% | 
 | 367  | Silvally-Grass     |  0.00000% | 7      |  0.003% | 
 | 368  | Silvally-Bug       |  0.00000% | 3      |  0.001% | 
 | 369  | Frillish           |  0.00000% | 22     |  0.009% | 
 | 370  | Lampent            |  0.00000% | 5      |  0.002% | 
 | 371  | Swinub             |  0.00000% | 3      |  0.001% | 
 | 372  | Gossifleur         |  0.00000% | 1      |  0.000% | 
 | 373  | Shellder           |  0.00000% | 7      |  0.003% | 
 | 374  | Skorupi            |  0.00000% | 9      |  0.004% | 
 | 375  | Shellos            |  0.00000% | 4      |  0.002% | 
 | 376  | Hippopotas         |  0.00000% | 1      |  0.000% | 
 | 377  | Inkay              |  0.00000% | 4      |  0.002% | 
 | 378  | Croagunk           |  0.00000% | 15     |  0.006% | 
 | 379  | Hoothoot           |  0.00000% | 2      |  0.001% | 
 | 380  | Tyrogue            |  0.00000% | 12     |  0.005% | 
 | 381  | Stufful            |  0.00000% | 3      |  0.001% | 
 | 382  | Tranquill          |  0.00000% | 4      |  0.002% | 
 | 383  | Drifloon           |  0.00000% | 2      |  0.001% | 
 | 384  | Solosis            |  0.00000% | 4      |  0.002% | 
 | 385  | Fraxure            |  0.00000% | 7      |  0.003% | 
 | 386  | Morelull           |  0.00000% | 11     |  0.004% | 
 | 387  | Helioptile         |  0.00000% | 1      |  0.000% | 
 | 388  | Blipbug            |  0.00000% | 8      |  0.003% | 
 | 389  | Arrokuda           |  0.00000% | 1      |  0.000% | 
 | 390  | Lotad              |  0.00000% | 5      |  0.002% | 
 | 391  | Snover             |  0.00000% | 5      |  0.002% | 
 | 392  | Yamask-Galar       |  0.00000% | 21     |  0.009% | 
 | 393  | Dewpider           |  0.00000% | 2      |  0.001% | 
 | 394  | Goomy              |  0.00000% | 1      |  0.000% | 
 | 395  | Munna              |  0.00000% | 5      |  0.002% | 
 | 396  | Charmeleon         |  0.00000% | 5      |  0.002% | 
 | 397  | Palpitoad          |  0.00000% | 13     |  0.005% | 
 | 398  | Sizzlipede         |  0.00000% | 16     |  0.007% | 
 | 399  | Vibrava            |  0.00000% | 12     |  0.005% | 
 | 400  | Yamask             |  0.00000% | 25     |  0.010% | 
 | 401  | Silvally-Ice       |  0.00000% | 7      |  0.003% | 
 | 402  | Zweilous           |  0.00000% | 5      |  0.002% | 
 | 403  | Meowth             |  0.00000% | 11     |  0.004% | 
 | 404  | Cubchoo            |  0.00000% | 1      |  0.000% | 
 | 405  | Zigzagoon-Galar    |  0.00000% | 12     |  0.005% | 
 | 406  | Sinistea           |  0.00000% | 14     |  0.006% | 
 | 407  | Vanillite          |  0.00000% | 15     |  0.006% | 
 | 408  | Natu               |  0.00000% | 3      |  0.001% | 
 | 409  | Drilbur            |  0.00000% | 2      |  0.001% | 
 | 410  | Pupitar            |  0.00000% | 8      |  0.003% | 
 | 411  | Nuzleaf            |  0.00000% | 10     |  0.004% | 
 | 412  | Machop             |  0.00000% | 3      |  0.001% | 
 | 413  | Nickit             |  0.00000% | 4      |  0.002% | 
 | 414  | Pumpkaboo-Super    |  0.00000% | 6      |  0.002% | 
 | 415  | Rhyhorn            |  0.00000% | 5      |  0.002% | 
 | 416  | Timburr            |  0.00000% | 7      |  0.003% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
