 Total leads: 15944
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Corviknight        |  7.16884% | 1143   |  7.169% | 
 | 2    | Dragapult          |  5.82037% | 928    |  5.820% | 
 | 3    | Cinderace          |  4.02032% | 641    |  4.020% | 
 | 4    | Shuckle            |  4.02032% | 641    |  4.020% | 
 | 5    | Toxtricity         |  2.52760% | 403    |  2.528% | 
 | 6    | Noivern            |  2.33317% | 372    |  2.333% | 
 | 7    | Eternatus          |  2.10738% | 336    |  2.107% | 
 | 8    | Excadrill          |  1.96312% | 313    |  1.963% | 
 | 9    | Dracovish          |  1.85650% | 296    |  1.856% | 
 | 10   | Darmanitan-Galar   |  1.77496% | 283    |  1.775% | 
 | 11   | Toxapex            |  1.74360% | 278    |  1.744% | 
 | 12   | Orbeetle           |  1.71224% | 273    |  1.712% | 
 | 13   | Weavile            |  1.67461% | 267    |  1.675% | 
 | 14   | Inteleon           |  1.66207% | 265    |  1.662% | 
 | 15   | Ferrothorn         |  1.54917% | 247    |  1.549% | 
 | 16   | Kommo-o            |  1.53663% | 245    |  1.537% | 
 | 17   | Mew                |  1.50527% | 240    |  1.505% | 
 | 18   | Arcanine           |  1.48645% | 237    |  1.486% | 
 | 19   | Hippowdon          |  1.46136% | 233    |  1.461% | 
 | 20   | Grimmsnarl         |  1.43628% | 229    |  1.436% | 
 | 21   | Barraskewda        |  1.43001% | 228    |  1.430% | 
 | 22   | Mandibuzz          |  1.19794% | 191    |  1.198% | 
 | 23   | Cursola            |  1.12268% | 179    |  1.123% | 
 | 24   | Pelipper           |  1.08505% | 173    |  1.085% | 
 | 25   | Ditto              |  1.03487% | 165    |  1.035% | 
 | 26   | Gastrodon          |  1.02860% | 164    |  1.029% | 
 | 27   | Hydreigon          |  1.01606% | 162    |  1.016% | 
 | 28   | Duraludon          |  0.97215% | 155    |  0.972% | 
 | 29   | Tyranitar          |  0.95961% | 153    |  0.960% | 
 | 30   | Mamoswine          |  0.94706% | 151    |  0.947% | 
 | 31   | Mimikyu            |  0.92825% | 148    |  0.928% | 
 | 32   | Gengar             |  0.88435% | 141    |  0.884% | 
 | 33   | Dracozolt          |  0.82163% | 131    |  0.822% | 
 | 34   | Morpeko            |  0.82163% | 131    |  0.822% | 
 | 35   | Charizard          |  0.82163% | 131    |  0.822% | 
 | 36   | Clefable           |  0.77772% | 124    |  0.778% | 
 | 37   | Runerigus          |  0.77772% | 124    |  0.778% | 
 | 38   | Rhyperior          |  0.77145% | 123    |  0.771% | 
 | 39   | Corsola-Galar      |  0.74636% | 119    |  0.746% | 
 | 40   | Hatterene          |  0.72755% | 116    |  0.728% | 
 | 41   | Jolteon            |  0.71500% | 114    |  0.715% | 
 | 42   | Coalossal          |  0.71500% | 114    |  0.715% | 
 | 43   | Galvantula         |  0.70873% | 113    |  0.709% | 
 | 44   | Aegislash          |  0.61465% | 98     |  0.615% | 
 | 45   | Ribombee           |  0.60838% | 97     |  0.608% | 
 | 46   | Vikavolt           |  0.58956% | 94     |  0.590% | 
 | 47   | Haxorus            |  0.56448% | 90     |  0.564% | 
 | 48   | Turtonator         |  0.56448% | 90     |  0.564% | 
 | 49   | Cloyster           |  0.55820% | 89     |  0.558% | 
 | 50   | Gyarados           |  0.55193% | 88     |  0.552% | 
 | 51   | Dugtrio            |  0.55193% | 88     |  0.552% | 
 | 52   | Vaporeon           |  0.52684% | 84     |  0.527% | 
 | 53   | Zamazenta-Crowned  |  0.52057% | 83     |  0.521% | 
 | 54   | Chandelure         |  0.51430% | 82     |  0.514% | 
 | 55   | Rotom-Heat         |  0.51430% | 82     |  0.514% | 
 | 56   | Golisopod          |  0.51430% | 82     |  0.514% | 
 | 57   | Rillaboom          |  0.50803% | 81     |  0.508% | 
 | 58   | Reuniclus          |  0.50176% | 80     |  0.502% | 
 | 59   | Boltund            |  0.45785% | 73     |  0.458% | 
 | 60   | Rotom-Wash         |  0.42649% | 68     |  0.426% | 
 | 61   | Centiskorch        |  0.41395% | 66     |  0.414% | 
 | 62   | Lucario            |  0.40140% | 64     |  0.401% | 
 | 63   | Sirfetch'd         |  0.38259% | 61     |  0.383% | 
 | 64   | Indeedee           |  0.36377% | 58     |  0.364% | 
 | 65   | Snorlax            |  0.33869% | 54     |  0.339% | 
 | 66   | Bronzong           |  0.33241% | 53     |  0.332% | 
 | 67   | Charizard-Mega-X   |  0.33241% | 53     |  0.332% | 
 | 68   | Salazzle           |  0.32614% | 52     |  0.326% | 
 | 69   | Jellicent          |  0.32614% | 52     |  0.326% | 
 | 70   | Bisharp            |  0.32614% | 52     |  0.326% | 
 | 71   | Steelix            |  0.30733% | 49     |  0.307% | 
 | 72   | Falinks            |  0.30733% | 49     |  0.307% | 
 | 73   | Dusknoir           |  0.30733% | 49     |  0.307% | 
 | 74   | Conkeldurr         |  0.30733% | 49     |  0.307% | 
 | 75   | Pikachu            |  0.30105% | 48     |  0.301% | 
 | 76   | Lapras             |  0.29478% | 47     |  0.295% | 
 | 77   | Copperajah         |  0.28851% | 46     |  0.289% | 
 | 78   | Obstagoon          |  0.28224% | 45     |  0.282% | 
 | 79   | Milotic            |  0.26969% | 43     |  0.270% | 
 | 80   | Torkoal            |  0.26342% | 42     |  0.263% | 
 | 81   | Zacian             |  0.25088% | 40     |  0.251% | 
 | 82   | Polteageist        |  0.24461% | 39     |  0.245% | 
 | 83   | Goodra             |  0.24461% | 39     |  0.245% | 
 | 84   | Alcremie           |  0.23833% | 38     |  0.238% | 
 | 85   | Araquanid          |  0.23206% | 37     |  0.232% | 
 | 86   | Gyarados-Mega      |  0.23206% | 37     |  0.232% | 
 | 87   | Cramorant          |  0.23206% | 37     |  0.232% | 
 | 88   | Cinccino           |  0.22579% | 36     |  0.226% | 
 | 89   | Frosmoth           |  0.22579% | 36     |  0.226% | 
 | 90   | Musharna           |  0.22579% | 36     |  0.226% | 
 | 91   | Tyranitar-Mega     |  0.21952% | 35     |  0.220% | 
 | 92   | Rapidash-Galar     |  0.21952% | 35     |  0.220% | 
 | 93   | Pincurchin         |  0.21952% | 35     |  0.220% | 
 | 94   | Drednaw            |  0.21952% | 35     |  0.220% | 
 | 95   | Sylveon            |  0.21325% | 34     |  0.213% | 
 | 96   | Togekiss           |  0.20697% | 33     |  0.207% | 
 | 97   | Seismitoad         |  0.20697% | 33     |  0.207% | 
 | 98   | Accelgor           |  0.20070% | 32     |  0.201% | 
 | 99   | Flygon             |  0.20070% | 32     |  0.201% | 
 | 100  | Ninetales          |  0.19443% | 31     |  0.194% | 
 | 101  | Appletun           |  0.18816% | 30     |  0.188% | 
 | 102  | Hawlucha           |  0.18816% | 30     |  0.188% | 
 | 103  | Glalie             |  0.18816% | 30     |  0.188% | 
 | 104  | Xatu               |  0.18816% | 30     |  0.188% | 
 | 105  | Weezing-Galar      |  0.18189% | 29     |  0.182% | 
 | 106  | Roserade           |  0.18189% | 29     |  0.182% | 
 | 107  | Crustle            |  0.18189% | 29     |  0.182% | 
 | 108  | Dubwool            |  0.17561% | 28     |  0.176% | 
 | 109  | Braviary           |  0.16307% | 26     |  0.163% | 
 | 110  | Arctovish          |  0.16307% | 26     |  0.163% | 
 | 111  | Lucario-Mega       |  0.16307% | 26     |  0.163% | 
 | 112  | Sandaconda         |  0.16307% | 26     |  0.163% | 
 | 113  | Charizard-Mega-Y   |  0.15680% | 25     |  0.157% | 
 | 114  | Grapploct          |  0.15680% | 25     |  0.157% | 
 | 115  | Shiinotic          |  0.15680% | 25     |  0.157% | 
 | 116  | Hitmonchan         |  0.15680% | 25     |  0.157% | 
 | 117  | Gardevoir          |  0.15053% | 24     |  0.151% | 
 | 118  | Claydol            |  0.15053% | 24     |  0.151% | 
 | 119  | Eiscue             |  0.14425% | 23     |  0.144% | 
 | 120  | Avalugg            |  0.14425% | 23     |  0.144% | 
 | 121  | Espeon             |  0.14425% | 23     |  0.144% | 
 | 122  | Durant             |  0.13798% | 22     |  0.138% | 
 | 123  | Tsareena           |  0.13798% | 22     |  0.138% | 
 | 124  | Escavalier         |  0.13798% | 22     |  0.138% | 
 | 125  | Butterfree         |  0.13171% | 21     |  0.132% | 
 | 126  | Drapion            |  0.12544% | 20     |  0.125% | 
 | 127  | Ninjask            |  0.12544% | 20     |  0.125% | 
 | 128  | Garbodor           |  0.11917% | 19     |  0.119% | 
 | 129  | Pangoro            |  0.11917% | 19     |  0.119% | 
 | 130  | Slurpuff           |  0.11290% | 18     |  0.113% | 
 | 131  | Umbreon            |  0.11290% | 18     |  0.113% | 
 | 132  | Zamazenta          |  0.11290% | 18     |  0.113% | 
 | 133  | Machamp            |  0.10035% | 16     |  0.100% | 
 | 134  | Gardevoir-Mega     |  0.10035% | 16     |  0.100% | 
 | 135  | Heliolisk          |  0.10035% | 16     |  0.100% | 
 | 136  | Shedinja           |  0.10035% | 16     |  0.100% | 
 | 137  | Hitmonlee          |  0.09408% | 15     |  0.094% | 
 | 138  | Raichu             |  0.09408% | 15     |  0.094% | 
 | 139  | Gallade-Mega       |  0.09408% | 15     |  0.094% | 
 | 140  | Gigalith           |  0.09408% | 15     |  0.094% | 
 | 141  | Dhelmise           |  0.08781% | 14     |  0.088% | 
 | 142  | Klinklang          |  0.08781% | 14     |  0.088% | 
 | 143  | Eevee              |  0.08154% | 13     |  0.082% | 
 | 144  | Cofagrigus         |  0.08154% | 13     |  0.082% | 
 | 145  | Stonjourner        |  0.08154% | 13     |  0.082% | 
 | 146  | Barbaracle         |  0.08154% | 13     |  0.082% | 
 | 147  | Gallade            |  0.08154% | 13     |  0.082% | 
 | 148  | Hitmontop          |  0.08154% | 13     |  0.082% | 
 | 149  | Mr. Rime           |  0.07526% | 12     |  0.075% | 
 | 150  | Greedent           |  0.07526% | 12     |  0.075% | 
 | 151  | Abomasnow-Mega     |  0.07526% | 12     |  0.075% | 
 | 152  | Pyukumuku          |  0.06899% | 11     |  0.069% | 
 | 153  | Flapple            |  0.06899% | 11     |  0.069% | 
 | 154  | Rotom-Fan          |  0.06899% | 11     |  0.069% | 
 | 155  | Flareon            |  0.06899% | 11     |  0.069% | 
 | 156  | Aromatisse         |  0.06272% | 10     |  0.063% | 
 | 157  | Thievul            |  0.06272% | 10     |  0.063% | 
 | 158  | Wobbuffet          |  0.05645% | 9      |  0.056% | 
 | 159  | Gourgeist-Super    |  0.05645% | 9      |  0.056% | 
 | 160  | Glaceon            |  0.05645% | 9      |  0.056% | 
 | 161  | Vanilluxe          |  0.05645% | 9      |  0.056% | 
 | 162  | Stunfisk-Galar     |  0.05645% | 9      |  0.056% | 
 | 163  | Steelix-Mega       |  0.05645% | 9      |  0.056% | 
 | 164  | Blastoise-Mega     |  0.05018% | 8      |  0.050% | 
 | 165  | Meowstic           |  0.05018% | 8      |  0.050% | 
 | 166  | Arctozolt          |  0.05018% | 8      |  0.050% | 
 | 167  | Vileplume          |  0.05018% | 8      |  0.050% | 
 | 168  | Silvally           |  0.05018% | 8      |  0.050% | 
 | 169  | Abomasnow          |  0.05018% | 8      |  0.050% | 
 | 170  | Rotom              |  0.04390% | 7      |  0.044% | 
 | 171  | Bewear             |  0.04390% | 7      |  0.044% | 
 | 172  | Toxicroak          |  0.04390% | 7      |  0.044% | 
 | 173  | Drifblim           |  0.04390% | 7      |  0.044% | 
 | 174  | Drampa             |  0.04390% | 7      |  0.044% | 
 | 175  | Whimsicott         |  0.04390% | 7      |  0.044% | 
 | 176  | Kingler            |  0.04390% | 7      |  0.044% | 
 | 177  | Solrock            |  0.04390% | 7      |  0.044% | 
 | 178  | Noctowl            |  0.04390% | 7      |  0.044% | 
 | 179  | Dewpider           |  0.04390% | 7      |  0.044% | 
 | 180  | Scrafty            |  0.03763% | 6      |  0.038% | 
 | 181  | Silvally-Ghost     |  0.03763% | 6      |  0.038% | 
 | 182  | Cherrim            |  0.03763% | 6      |  0.038% | 
 | 183  | Mudsdale           |  0.03763% | 6      |  0.038% | 
 | 184  | Vespiquen          |  0.03763% | 6      |  0.038% | 
 | 185  | Octillery          |  0.03763% | 6      |  0.038% | 
 | 186  | Dusclops           |  0.03763% | 6      |  0.038% | 
 | 187  | Sableye-Mega       |  0.03763% | 6      |  0.038% | 
 | 188  | Persian            |  0.03136% | 5      |  0.031% | 
 | 189  | Perrserker         |  0.03136% | 5      |  0.031% | 
 | 190  | Rotom-Mow          |  0.03136% | 5      |  0.031% | 
 | 191  | Manectric          |  0.03136% | 5      |  0.031% | 
 | 192  | Sudowoodo          |  0.03136% | 5      |  0.031% | 
 | 193  | Malamar            |  0.03136% | 5      |  0.031% | 
 | 194  | Mr. Mime-Galar     |  0.02509% | 4      |  0.025% | 
 | 195  | Manectric-Mega     |  0.02509% | 4      |  0.025% | 
 | 196  | Silvally-Water     |  0.02509% | 4      |  0.025% | 
 | 197  | Crawdaunt          |  0.02509% | 4      |  0.025% | 
 | 198  | Magikarp           |  0.02509% | 4      |  0.025% | 
 | 199  | Bellossom          |  0.02509% | 4      |  0.025% | 
 | 200  | Silvally-Fire      |  0.02509% | 4      |  0.025% | 
 | 201  | Quagsire           |  0.02509% | 4      |  0.025% | 
 | 202  | Wailord            |  0.02509% | 4      |  0.025% | 
 | 203  | Ludicolo           |  0.02509% | 4      |  0.025% | 
 | 204  | Golurk             |  0.02509% | 4      |  0.025% | 
 | 205  | Unfezant           |  0.02509% | 4      |  0.025% | 
 | 206  | Trevenant          |  0.02509% | 4      |  0.025% | 
 | 207  | Shiftry            |  0.02509% | 4      |  0.025% | 
 | 208  | Qwilfish           |  0.02509% | 4      |  0.025% | 
 | 209  | Rhyhorn            |  0.02509% | 4      |  0.025% | 
 | 210  | Mawile             |  0.02509% | 4      |  0.025% | 
 | 211  | Doublade           |  0.01882% | 3      |  0.019% | 
 | 212  | Glalie-Mega        |  0.01882% | 3      |  0.019% | 
 | 213  | Togedemaru         |  0.01882% | 3      |  0.019% | 
 | 214  | Type: Null         |  0.01882% | 3      |  0.019% | 
 | 215  | Froslass           |  0.01882% | 3      |  0.019% | 
 | 216  | Whiscash           |  0.01882% | 3      |  0.019% | 
 | 217  | Lunatone           |  0.01882% | 3      |  0.019% | 
 | 218  | Grookey            |  0.01882% | 3      |  0.019% | 
 | 219  | Passimian          |  0.01882% | 3      |  0.019% | 
 | 220  | Blipbug            |  0.01882% | 3      |  0.019% | 
 | 221  | Wooloo             |  0.01882% | 3      |  0.019% | 
 | 222  | Swoobat            |  0.01254% | 2      |  0.013% | 
 | 223  | Baltoy             |  0.01254% | 2      |  0.013% | 
 | 224  | Leafeon            |  0.01254% | 2      |  0.013% | 
 | 225  | Carkol             |  0.01254% | 2      |  0.013% | 
 | 226  | Pawniard           |  0.01254% | 2      |  0.013% | 
 | 227  | Lanturn            |  0.01254% | 2      |  0.013% | 
 | 228  | Ponyta-Galar       |  0.01254% | 2      |  0.013% | 
 | 229  | Linoone-Galar      |  0.01254% | 2      |  0.013% | 
 | 230  | Sobble             |  0.01254% | 2      |  0.013% | 
 | 231  | Haunter            |  0.01254% | 2      |  0.013% | 
 | 232  | Eldegoss           |  0.01254% | 2      |  0.013% | 
 | 233  | Silvally-Dragon    |  0.01254% | 2      |  0.013% | 
 | 234  | Sigilyph           |  0.01254% | 2      |  0.013% | 
 | 235  | Yamper             |  0.01254% | 2      |  0.013% | 
 | 236  | Onix               |  0.01254% | 2      |  0.013% | 
 | 237  | Rotom-Frost        |  0.01254% | 2      |  0.013% | 
 | 238  | Gourgeist-Small    |  0.01254% | 2      |  0.013% | 
 | 239  | Skuntank           |  0.01254% | 2      |  0.013% | 
 | 240  | Liepard            |  0.00627% | 1      |  0.006% | 
 | 241  | Diglett            |  0.00627% | 1      |  0.006% | 
 | 242  | Diggersby          |  0.00627% | 1      |  0.006% | 
 | 243  | Heatmor            |  0.00627% | 1      |  0.006% | 
 | 244  | Dottler            |  0.00627% | 1      |  0.006% | 
 | 245  | Drakloak           |  0.00627% | 1      |  0.006% | 
 | 246  | Morgrem            |  0.00627% | 1      |  0.006% | 
 | 247  | Silvally-Steel     |  0.00627% | 1      |  0.006% | 
 | 248  | Piloswine          |  0.00627% | 1      |  0.006% | 
 | 249  | Silvally-Ice       |  0.00627% | 1      |  0.006% | 
 | 250  | Basculin           |  0.00627% | 1      |  0.006% | 
 | 251  | Noibat             |  0.00627% | 1      |  0.006% | 
 | 252  | Croagunk           |  0.00627% | 1      |  0.006% | 
 | 253  | Thwackey           |  0.00627% | 1      |  0.006% | 
 | 254  | Meowth             |  0.00627% | 1      |  0.006% | 
 | 255  | Roselia            |  0.00627% | 1      |  0.006% | 
 | 256  | Axew               |  0.00627% | 1      |  0.006% | 
 | 257  | Metapod            |  0.00627% | 1      |  0.006% | 
 | 258  | Swirlix            |  0.00627% | 1      |  0.006% | 
 | 259  | Silvally-Fighting  |  0.00627% | 1      |  0.006% | 
 | 260  | Zigzagoon-Galar    |  0.00627% | 1      |  0.006% | 
 | 261  | Milcery            |  0.00627% | 1      |  0.006% | 
 | 262  | Oranguru           |  0.00627% | 1      |  0.006% | 
 | 263  | Sawk               |  0.00627% | 1      |  0.006% | 
 | 264  | Silvally-Psychic   |  0.00627% | 1      |  0.006% | 
 | 265  | Munchlax           |  0.00627% | 1      |  0.006% | 
 | 266  | Toxel              |  0.00627% | 1      |  0.006% | 
 | 267  | Caterpie           |  0.00627% | 1      |  0.006% | 
 | 268  | Togepi             |  0.00627% | 1      |  0.006% | 
 | 269  | Seaking            |  0.00627% | 1      |  0.006% | 
 | 270  | Rhydon             |  0.00627% | 1      |  0.006% | 
 | 271  | Scorbunny          |  0.00627% | 1      |  0.006% | 
 | 272  | Beartic            |  0.00627% | 1      |  0.006% | 
 | 273  | Chewtle            |  0.00627% | 1      |  0.006% | 
 | 274  | Indeedee-F         |  0.00627% | 1      |  0.006% | 
 | 275  | Beheeyem           |  0.00627% | 1      |  0.006% | 
 | 276  | Darumaka-Galar     |  0.00627% | 1      |  0.006% | 
 | 277  | Riolu              |  0.00627% | 1      |  0.006% | 
 | 278  | Mareanie           |  0.00627% | 1      |  0.006% | 
 | 279  | Sneasel            |  0.00627% | 1      |  0.006% | 
 | 280  | Silvally-Grass     |  0.00627% | 1      |  0.006% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
