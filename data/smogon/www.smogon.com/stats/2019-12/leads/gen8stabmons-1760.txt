 Total leads: 4914
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Rotom-Wash         | 11.74844% | 304    |  6.186% | 
 | 2    | Obstagoon          | 10.15734% | 151    |  3.073% | 
 | 3    | Araquanid          |  7.43042% | 88     |  1.791% | 
 | 4    | Dragapult          |  5.51297% | 211    |  4.294% | 
 | 5    | Braviary           |  5.49373% | 31     |  0.631% | 
 | 6    | Excadrill          |  5.43670% | 174    |  3.541% | 
 | 7    | Tyranitar          |  4.41060% | 109    |  2.218% | 
 | 8    | Rotom-Heat         |  4.19545% | 193    |  3.928% | 
 | 9    | Indeedee           |  3.67890% | 44     |  0.895% | 
 | 10   | Shuckle            |  3.33812% | 93     |  1.893% | 
 | 11   | Chandelure         |  3.20206% | 44     |  0.895% | 
 | 12   | Sylveon            |  3.05422% | 72     |  1.465% | 
 | 13   | Diggersby          |  2.54057% | 94     |  1.913% | 
 | 14   | Cinderace          |  2.08454% | 109    |  2.218% | 
 | 15   | Ferrothorn         |  1.99771% | 156    |  3.175% | 
 | 16   | Darmanitan-Galar   |  1.67534% | 100    |  2.035% | 
 | 17   | Mew                |  1.63881% | 16     |  0.326% | 
 | 18   | Heliolisk          |  1.59221% | 44     |  0.895% | 
 | 19   | Seismitoad         |  1.43318% | 48     |  0.977% | 
 | 20   | Grimmsnarl         |  1.39753% | 201    |  4.090% | 
 | 21   | Ribombee           |  1.25565% | 58     |  1.180% | 
 | 22   | Dracovish          |  1.18070% | 113    |  2.300% | 
 | 23   | Hippowdon          |  1.02061% | 17     |  0.346% | 
 | 24   | Dugtrio            |  0.96296% | 46     |  0.936% | 
 | 25   | Toxtricity         |  0.91901% | 82     |  1.669% | 
 | 26   | Rotom-Mow          |  0.88103% | 20     |  0.407% | 
 | 27   | Accelgor           |  0.80878% | 105    |  2.137% | 
 | 28   | Hawlucha           |  0.74942% | 15     |  0.305% | 
 | 29   | Snorlax            |  0.71015% | 39     |  0.794% | 
 | 30   | Kommo-o            |  0.66962% | 33     |  0.672% | 
 | 31   | Escavalier         |  0.63211% | 42     |  0.855% | 
 | 32   | Jellicent          |  0.62396% | 8      |  0.163% | 
 | 33   | Gyarados           |  0.54217% | 25     |  0.509% | 
 | 34   | Hydreigon          |  0.53865% | 53     |  1.079% | 
 | 35   | Runerigus          |  0.47095% | 49     |  0.997% | 
 | 36   | Togekiss           |  0.42570% | 32     |  0.651% | 
 | 37   | Corsola-Galar      |  0.40554% | 44     |  0.895% | 
 | 38   | Ninjask            |  0.40253% | 81     |  1.648% | 
 | 39   | Pelipper           |  0.40003% | 82     |  1.669% | 
 | 40   | Crustle            |  0.36678% | 74     |  1.506% | 
 | 41   | Bisharp            |  0.31281% | 32     |  0.651% | 
 | 42   | Corviknight        |  0.28039% | 42     |  0.855% | 
 | 43   | Orbeetle           |  0.26365% | 42     |  0.855% | 
 | 44   | Linoone-Galar      |  0.23429% | 7      |  0.142% | 
 | 45   | Boltund            |  0.21128% | 88     |  1.791% | 
 | 46   | Hatterene          |  0.20991% | 63     |  1.282% | 
 | 47   | Aegislash          |  0.20709% | 51     |  1.038% | 
 | 48   | Drakloak           |  0.19508% | 1      |  0.020% | 
 | 49   | Roserade           |  0.15643% | 6      |  0.122% | 
 | 50   | Conkeldurr         |  0.14907% | 20     |  0.407% | 
 | 51   | Pikachu            |  0.14127% | 6      |  0.122% | 
 | 52   | Dracozolt          |  0.14076% | 43     |  0.875% | 
 | 53   | Duraludon          |  0.11456% | 16     |  0.326% | 
 | 54   | Gengar             |  0.11302% | 17     |  0.346% | 
 | 55   | Bewear             |  0.09552% | 36     |  0.733% | 
 | 56   | Quagsire           |  0.09487% | 6      |  0.122% | 
 | 57   | Torkoal            |  0.09382% | 30     |  0.611% | 
 | 58   | Type: Null         |  0.09301% | 24     |  0.488% | 
 | 59   | Rillaboom          |  0.08847% | 12     |  0.244% | 
 | 60   | Clefable           |  0.08510% | 19     |  0.387% | 
 | 61   | Rotom-Frost        |  0.08504% | 18     |  0.366% | 
 | 62   | Drapion            |  0.07128% | 30     |  0.611% | 
 | 63   | Avalugg            |  0.05643% | 1      |  0.020% | 
 | 64   | Mandibuzz          |  0.05608% | 14     |  0.285% | 
 | 65   | Steelix            |  0.05466% | 25     |  0.509% | 
 | 66   | Hakamo-o           |  0.04335% | 4      |  0.081% | 
 | 67   | Charizard          |  0.04335% | 9      |  0.183% | 
 | 68   | Weavile            |  0.03296% | 25     |  0.509% | 
 | 69   | Sirfetch'd         |  0.03188% | 11     |  0.224% | 
 | 70   | Perrserker         |  0.03008% | 18     |  0.366% | 
 | 71   | Polteageist        |  0.02891% | 15     |  0.305% | 
 | 72   | Barbaracle         |  0.02228% | 13     |  0.265% | 
 | 73   | Indeedee-F         |  0.02150% | 11     |  0.224% | 
 | 74   | Toxapex            |  0.02088% | 17     |  0.346% | 
 | 75   | Appletun           |  0.01794% | 14     |  0.285% | 
 | 76   | Durant             |  0.01432% | 58     |  1.180% | 
 | 77   | Shedinja           |  0.01240% | 7      |  0.142% | 
 | 78   | Gastrodon          |  0.01201% | 5      |  0.102% | 
 | 79   | Stonjourner        |  0.01091% | 3      |  0.061% | 
 | 80   | Frosmoth           |  0.00816% | 17     |  0.346% | 
 | 81   | Dusknoir           |  0.00746% | 2      |  0.041% | 
 | 82   | Umbreon            |  0.00696% | 4      |  0.081% | 
 | 83   | Toxicroak          |  0.00653% | 7      |  0.142% | 
 | 84   | Haxorus            |  0.00537% | 7      |  0.142% | 
 | 85   | Ninetales          |  0.00421% | 2      |  0.041% | 
 | 86   | Greedent           |  0.00359% | 13     |  0.265% | 
 | 87   | Inteleon           |  0.00326% | 33     |  0.672% | 
 | 88   | Sandaconda         |  0.00225% | 9      |  0.183% | 
 | 89   | Galvantula         |  0.00210% | 30     |  0.611% | 
 | 90   | Centiskorch        |  0.00188% | 41     |  0.834% | 
 | 91   | Pincurchin         |  0.00186% | 21     |  0.427% | 
 | 92   | Butterfree         |  0.00182% | 15     |  0.305% | 
 | 93   | Espeon             |  0.00169% | 2      |  0.041% | 
 | 94   | Coalossal          |  0.00141% | 13     |  0.265% | 
 | 95   | Sigilyph           |  0.00123% | 3      |  0.061% | 
 | 96   | Xatu               |  0.00113% | 1      |  0.020% | 
 | 97   | Grapploct          |  0.00086% | 5      |  0.102% | 
 | 98   | Noivern            |  0.00064% | 3      |  0.061% | 
 | 99   | Whimsicott         |  0.00062% | 19     |  0.387% | 
 | 100  | Scrafty            |  0.00057% | 19     |  0.387% | 
 | 101  | Claydol            |  0.00052% | 6      |  0.122% | 
 | 102  | Salazzle           |  0.00045% | 17     |  0.346% | 
 | 103  | Togedemaru         |  0.00037% | 5      |  0.102% | 
 | 104  | Rhyperior          |  0.00036% | 13     |  0.265% | 
 | 105  | Kingler            |  0.00033% | 1      |  0.020% | 
 | 106  | Vikavolt           |  0.00028% | 12     |  0.244% | 
 | 107  | Copperajah         |  0.00026% | 6      |  0.122% | 
 | 108  | Reuniclus          |  0.00024% | 8      |  0.163% | 
 | 109  | Sableye            |  0.00023% | 7      |  0.142% | 
 | 110  | Vanilluxe          |  0.00022% | 4      |  0.081% | 
 | 111  | Arcanine           |  0.00020% | 14     |  0.285% | 
 | 112  | Gallade            |  0.00019% | 16     |  0.326% | 
 | 113  | Crawdaunt          |  0.00019% | 11     |  0.224% | 
 | 114  | Ditto              |  0.00015% | 23     |  0.468% | 
 | 115  | Lucario            |  0.00015% | 17     |  0.346% | 
 | 116  | Bronzong           |  0.00011% | 17     |  0.346% | 
 | 117  | Vileplume          |  0.00010% | 6      |  0.122% | 
 | 118  | Raichu             |  0.00009% | 9      |  0.183% | 
 | 119  | Tsareena           |  0.00007% | 4      |  0.081% | 
 | 120  | Sneasel            |  0.00005% | 8      |  0.163% | 
 | 121  | Mamoswine          |  0.00003% | 8      |  0.163% | 
 | 122  | Morpeko            |  0.00000% | 17     |  0.346% | 
 | 123  | Lanturn            |  0.00000% | 4      |  0.081% | 
 | 124  | Cloyster           |  0.00000% | 19     |  0.387% | 
 | 125  | Applin             |  0.00000% | 1      |  0.020% | 
 | 126  | Abomasnow          |  0.00000% | 7      |  0.142% | 
 | 127  | Liepard            |  0.00000% | 1      |  0.020% | 
 | 128  | Mr. Mime-Galar     |  0.00000% | 1      |  0.020% | 
 | 129  | Eevee              |  0.00000% | 5      |  0.102% | 
 | 130  | Barraskewda        |  0.00000% | 26     |  0.529% | 
 | 131  | Mimikyu            |  0.00000% | 4      |  0.081% | 
 | 132  | Machamp            |  0.00000% | 1      |  0.020% | 
 | 133  | Wobbuffet          |  0.00000% | 1      |  0.020% | 
 | 134  | Weezing-Galar      |  0.00000% | 13     |  0.265% | 
 | 135  | Cursola            |  0.00000% | 5      |  0.102% | 
 | 136  | Dottler            |  0.00000% | 1      |  0.020% | 
 | 137  | Dhelmise           |  0.00000% | 2      |  0.041% | 
 | 138  | Lapras             |  0.00000% | 9      |  0.183% | 
 | 139  | Eiscue             |  0.00000% | 2      |  0.041% | 
 | 140  | Cinccino           |  0.00000% | 3      |  0.061% | 
 | 141  | Doublade           |  0.00000% | 2      |  0.041% | 
 | 142  | Leafeon            |  0.00000% | 2      |  0.041% | 
 | 143  | Pyukumuku          |  0.00000% | 3      |  0.061% | 
 | 144  | Pangoro            |  0.00000% | 7      |  0.142% | 
 | 145  | Piloswine          |  0.00000% | 4      |  0.081% | 
 | 146  | Arctozolt          |  0.00000% | 1      |  0.020% | 
 | 147  | Falinks            |  0.00000% | 3      |  0.061% | 
 | 148  | Manectric          |  0.00000% | 1      |  0.020% | 
 | 149  | Garbodor           |  0.00000% | 2      |  0.041% | 
 | 150  | Throh              |  0.00000% | 4      |  0.081% | 
 | 151  | Magikarp           |  0.00000% | 1      |  0.020% | 
 | 152  | Basculin           |  0.00000% | 1      |  0.020% | 
 | 153  | Cramorant          |  0.00000% | 6      |  0.122% | 
 | 154  | Jolteon            |  0.00000% | 10     |  0.204% | 
 | 155  | Flapple            |  0.00000% | 14     |  0.285% | 
 | 156  | Drifblim           |  0.00000% | 2      |  0.041% | 
 | 157  | Rapidash-Galar     |  0.00000% | 2      |  0.041% | 
 | 158  | Rotom-Fan          |  0.00000% | 1      |  0.020% | 
 | 159  | Vaporeon           |  0.00000% | 1      |  0.020% | 
 | 160  | Froslass           |  0.00000% | 1      |  0.020% | 
 | 161  | Snom               |  0.00000% | 1      |  0.020% | 
 | 162  | Dubwool            |  0.00000% | 4      |  0.081% | 
 | 163  | Gardevoir          |  0.00000% | 2      |  0.041% | 
 | 164  | Zweilous           |  0.00000% | 1      |  0.020% | 
 | 165  | Onix               |  0.00000% | 3      |  0.061% | 
 | 166  | Drampa             |  0.00000% | 5      |  0.102% | 
 | 167  | Mudsdale           |  0.00000% | 2      |  0.041% | 
 | 168  | Solosis            |  0.00000% | 1      |  0.020% | 
 | 169  | Zigzagoon-Galar    |  0.00000% | 3      |  0.061% | 
 | 170  | Oranguru           |  0.00000% | 5      |  0.102% | 
 | 171  | Sawk               |  0.00000% | 1      |  0.020% | 
 | 172  | Shiinotic          |  0.00000% | 1      |  0.020% | 
 | 173  | Lunatone           |  0.00000% | 2      |  0.041% | 
 | 174  | Eldegoss           |  0.00000% | 2      |  0.041% | 
 | 175  | Goodra             |  0.00000% | 2      |  0.041% | 
 | 176  | Persian            |  0.00000% | 2      |  0.041% | 
 | 177  | Vespiquen          |  0.00000% | 1      |  0.020% | 
 | 178  | Unfezant           |  0.00000% | 4      |  0.081% | 
 | 179  | Flareon            |  0.00000% | 2      |  0.041% | 
 | 180  | Alcremie           |  0.00000% | 2      |  0.041% | 
 | 181  | Passimian          |  0.00000% | 1      |  0.020% | 
 | 182  | Trevenant          |  0.00000% | 2      |  0.041% | 
 | 183  | Pupitar            |  0.00000% | 2      |  0.041% | 
 | 184  | Bunnelby           |  0.00000% | 1      |  0.020% | 
 | 185  | Shiftry            |  0.00000% | 1      |  0.020% | 
 | 186  | Ludicolo           |  0.00000% | 1      |  0.020% | 
 | 187  | Flygon             |  0.00000% | 6      |  0.122% | 
 | 188  | Malamar            |  0.00000% | 5      |  0.102% | 
 | 189  | Glaceon            |  0.00000% | 1      |  0.020% | 
 | 190  | Beheeyem           |  0.00000% | 1      |  0.020% | 
 | 191  | Dusclops           |  0.00000% | 1      |  0.020% | 
 | 192  | Golisopod          |  0.00000% | 4      |  0.081% | 
 | 193  | Mr. Rime           |  0.00000% | 3      |  0.061% | 
 | 194  | Mawile             |  0.00000% | 2      |  0.041% | 
 | 195  | Drednaw            |  0.00000% | 5      |  0.102% | 
 | 196  | Thievul            |  0.00000% | 5      |  0.102% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
