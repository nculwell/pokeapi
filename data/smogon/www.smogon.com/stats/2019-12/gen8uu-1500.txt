 Total battles: 371449
 Avg. weight/team: 0.545
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Gengar             | 22.09009% | 150773 | 20.295% | 109557 | 19.110% | 
 | 2    | Obstagoon          | 19.13411% | 131268 | 17.670% | 101327 | 17.675% | 
 | 3    | Kommo-o            | 18.29291% | 120907 | 16.275% | 88234  | 15.391% | 
 | 4    | Weezing-Galar      | 15.38564% | 107843 | 14.517% | 87797  | 15.315% | 
 | 5    | Mamoswine          | 13.57374% | 87790  | 11.817% | 72292  | 12.610% | 
 | 6    | Mew                | 10.93057% | 74280  |  9.999% | 60047  | 10.474% | 
 | 7    | Polteageist        | 10.54188% | 74588  | 10.040% | 52227  |  9.110% | 
 | 8    | Ribombee           |  9.88807% | 66718  |  8.981% | 58554  | 10.214% | 
 | 9    | Arcanine           |  9.46690% | 75869  | 10.213% | 59360  | 10.354% | 
 | 10   | Noivern            |  9.07248% | 61255  |  8.245% | 47314  |  8.253% | 
 | 11   | Chandelure         |  8.95141% | 66932  |  9.010% | 50562  |  8.820% | 
 | 12   | Crawdaunt          |  8.74427% | 61371  |  8.261% | 46879  |  8.177% | 
 | 13   | Copperajah         |  8.48263% | 62576  |  8.423% | 50570  |  8.821% | 
 | 14   | Inteleon           |  8.46911% | 63186  |  8.505% | 49216  |  8.585% | 
 | 15   | Haxorus            |  8.04068% | 55732  |  7.502% | 38112  |  6.648% | 
 | 16   | Araquanid          |  7.96271% | 58007  |  7.808% | 49877  |  8.700% | 
 | 17   | Duraludon          |  7.83574% | 59629  |  8.027% | 46789  |  8.162% | 
 | 18   | Coalossal          |  7.74560% | 56187  |  7.563% | 47127  |  8.220% | 
 | 19   | Braviary           |  7.52799% | 52951  |  7.128% | 40082  |  6.992% | 
 | 20   | Cloyster           |  7.48383% | 55624  |  7.487% | 41306  |  7.205% | 
 | 21   | Frosmoth           |  7.21576% | 52684  |  7.092% | 37441  |  6.531% | 
 | 22   | Espeon             |  6.81185% | 50721  |  6.827% | 39051  |  6.812% | 
 | 23   | Sirfetch'd         |  6.74523% | 48397  |  6.515% | 36882  |  6.433% | 
 | 24   | Centiskorch        |  6.69593% | 52266  |  7.035% | 40489  |  7.063% | 
 | 25   | Charizard          |  6.66493% | 50013  |  6.732% | 38758  |  6.761% | 
 | 26   | Milotic            |  6.61145% | 47598  |  6.407% | 38060  |  6.639% | 
 | 27   | Boltund            |  6.46291% | 52392  |  7.052% | 38689  |  6.749% | 
 | 28   | Hippowdon          |  6.38518% | 42709  |  5.749% | 36117  |  6.300% | 
 | 29   | Claydol            |  6.11174% | 45686  |  6.150% | 36782  |  6.416% | 
 | 30   | Morpeko            |  6.07059% | 46214  |  6.221% | 34789  |  6.068% | 
 | 31   | Doublade           |  6.06886% | 38377  |  5.166% | 29871  |  5.210% | 
 | 32   | Galvantula         |  5.95405% | 46691  |  6.285% | 41204  |  7.187% | 
 | 33   | Sylveon            |  5.81031% | 40858  |  5.500% | 30645  |  5.345% | 
 | 34   | Weavile            |  5.69274% | 40690  |  5.477% | 30135  |  5.257% | 
 | 35   | Durant             |  5.64663% | 39476  |  5.314% | 30962  |  5.401% | 
 | 36   | Avalugg            |  5.59281% | 39248  |  5.283% | 30266  |  5.279% | 
 | 37   | Appletun           |  5.35513% | 46599  |  6.273% | 33088  |  5.772% | 
 | 38   | Gardevoir          |  5.25359% | 38429  |  5.173% | 28015  |  4.887% | 
 | 39   | Rotom-Mow          |  5.23006% | 36635  |  4.931% | 29461  |  5.139% | 
 | 40   | Cramorant          |  5.14994% | 42500  |  5.721% | 34370  |  5.995% | 
 | 41   | Diggersby          |  5.12047% | 34370  |  4.626% | 25916  |  4.521% | 
 | 42   | Eldegoss           |  4.98991% | 39223  |  5.280% | 28844  |  5.031% | 
 | 43   | Bronzong           |  4.98684% | 37197  |  5.007% | 31690  |  5.528% | 
 | 44   | Runerigus          |  4.96155% | 39257  |  5.284% | 33341  |  5.816% | 
 | 45   | Tsareena           |  4.92362% | 35359  |  4.760% | 26220  |  4.574% | 
 | 46   | Jellicent          |  4.86502% | 35679  |  4.803% | 27970  |  4.879% | 
 | 47   | Lucario            |  4.80577% | 35877  |  4.829% | 26335  |  4.594% | 
 | 48   | Goodra             |  4.77440% | 38197  |  5.142% | 28357  |  4.946% | 
 | 49   | Flygon             |  4.68546% | 36164  |  4.868% | 27353  |  4.771% | 
 | 50   | Falinks            |  4.62138% | 37955  |  5.109% | 25232  |  4.401% | 
 | 51   | Indeedee           |  4.61270% | 31624  |  4.257% | 23803  |  4.152% | 
 | 52   | Golisopod          |  4.48396% | 31870  |  4.290% | 25510  |  4.450% | 
 | 53   | Heliolisk          |  4.42058% | 31423  |  4.230% | 23612  |  4.119% | 
 | 54   | Drapion            |  4.41355% | 31153  |  4.193% | 25769  |  4.495% | 
 | 55   | Salazzle           |  4.21307% | 31961  |  4.302% | 24187  |  4.219% | 
 | 56   | Torkoal            |  4.14410% | 30410  |  4.093% | 28044  |  4.892% | 
 | 57   | Hitmontop          |  4.10257% | 31734  |  4.272% | 25296  |  4.412% | 
 | 58   | Umbreon            |  4.07327% | 27414  |  3.690% | 21472  |  3.745% | 
 | 59   | Dhelmise           |  3.98745% | 28441  |  3.828% | 22074  |  3.850% | 
 | 60   | Steelix            |  3.98700% | 28664  |  3.858% | 24103  |  4.204% | 
 | 61   | Reuniclus          |  3.86772% | 27317  |  3.677% | 19944  |  3.479% | 
 | 62   | Snorlax            |  3.79167% | 29226  |  3.934% | 22462  |  3.918% | 
 | 63   | Alcremie           |  3.57152% | 29874  |  4.021% | 21161  |  3.691% | 
 | 64   | Rillaboom          |  3.48492% | 26494  |  3.566% | 19590  |  3.417% | 
 | 65   | Eiscue             |  3.38760% | 28208  |  3.797% | 19713  |  3.439% | 
 | 66   | Flapple            |  3.38243% | 27534  |  3.706% | 18603  |  3.245% | 
 | 67   | Cursola            |  3.37036% | 26489  |  3.566% | 20254  |  3.533% | 
 | 68   | Roserade           |  3.28392% | 23629  |  3.181% | 18342  |  3.199% | 
 | 69   | Whimsicott         |  3.27909% | 24723  |  3.328% | 19975  |  3.484% | 
 | 70   | Orbeetle           |  3.15830% | 26715  |  3.596% | 22232  |  3.878% | 
 | 71   | Rhyperior          |  3.13651% | 21517  |  2.896% | 17228  |  3.005% | 
 | 72   | Gastrodon          |  3.12033% | 24131  |  3.248% | 18871  |  3.292% | 
 | 73   | Sigilyph           |  3.10475% | 21122  |  2.843% | 15420  |  2.690% | 
 | 74   | Cinccino           |  3.07329% | 26010  |  3.501% | 20138  |  3.513% | 
 | 75   | Arctozolt          |  3.04462% | 24122  |  3.247% | 17338  |  3.024% | 
 | 76   | Hitmonlee          |  2.93240% | 22630  |  3.046% | 16146  |  2.816% | 
 | 77   | Quagsire           |  2.84888% | 22164  |  2.983% | 17318  |  3.021% | 
 | 78   | Xatu               |  2.69528% | 20033  |  2.697% | 16020  |  2.794% | 
 | 79   | Accelgor           |  2.69528% | 22275  |  2.998% | 17825  |  3.109% | 
 | 80   | Rapidash-Galar     |  2.65765% | 22149  |  2.981% | 15087  |  2.632% | 
 | 81   | Shuckle            |  2.60878% | 21274  |  2.864% | 19405  |  3.385% | 
 | 82   | Mantine            |  2.53876% | 17715  |  2.385% | 14373  |  2.507% | 
 | 83   | Mr. Rime           |  2.51049% | 20344  |  2.738% | 14762  |  2.575% | 
 | 84   | Shiftry            |  2.42970% | 17077  |  2.299% | 12535  |  2.187% | 
 | 85   | Jolteon            |  2.39079% | 20222  |  2.722% | 15874  |  2.769% | 
 | 86   | Sandaconda         |  2.32756% | 17781  |  2.393% | 14107  |  2.461% | 
 | 87   | Passimian          |  2.29424% | 15030  |  2.023% | 11589  |  2.021% | 
 | 88   | Barbaracle         |  2.23542% | 17095  |  2.301% | 11846  |  2.066% | 
 | 89   | Vaporeon           |  2.16917% | 16149  |  2.174% | 12887  |  2.248% | 
 | 90   | Bewear             |  2.14833% | 15798  |  2.127% | 11839  |  2.065% | 
 | 91   | Dubwool            |  2.09129% | 17385  |  2.340% | 12615  |  2.200% | 
 | 92   | Machamp            |  2.03875% | 16250  |  2.187% | 12971  |  2.263% | 
 | 93   | Vanilluxe          |  1.99492% | 14028  |  1.888% | 12109  |  2.112% | 
 | 94   | Froslass           |  1.79620% | 13238  |  1.782% | 11576  |  2.019% | 
 | 95   | Ninetales          |  1.78216% | 15186  |  2.044% | 12380  |  2.159% | 
 | 96   | Vikavolt           |  1.77151% | 13230  |  1.781% | 10940  |  1.908% | 
 | 97   | Escavalier         |  1.75643% | 13438  |  1.809% | 10873  |  1.897% | 
 | 98   | Drednaw            |  1.72922% | 15298  |  2.059% | 11592  |  2.022% | 
 | 99   | Lapras             |  1.72261% | 14673  |  1.975% | 11268  |  1.966% | 
 | 100  | Arctovish          |  1.67793% | 13437  |  1.809% | 10148  |  1.770% | 
 | 101  | Pyukumuku          |  1.61766% | 12025  |  1.619% | 9549   |  1.666% | 
 | 102  | Grapploct          |  1.57569% | 13773  |  1.854% | 10586  |  1.847% | 
 | 103  | Perrserker         |  1.55094% | 13637  |  1.836% | 10750  |  1.875% | 
 | 104  | Butterfree         |  1.47923% | 13105  |  1.764% | 9609   |  1.676% | 
 | 105  | Sableye            |  1.39322% | 11275  |  1.518% | 9382   |  1.637% | 
 | 106  | Cofagrigus         |  1.34706% | 11245  |  1.514% | 9299   |  1.622% | 
 | 107  | Vileplume          |  1.31249% | 9977   |  1.343% | 7652   |  1.335% | 
 | 108  | Rotom-Fan          |  1.30406% | 8680   |  1.168% | 6852   |  1.195% | 
 | 109  | Turtonator         |  1.29956% | 9720   |  1.308% | 7567   |  1.320% | 
 | 110  | Gigalith           |  1.29208% | 9535   |  1.283% | 8183   |  1.427% | 
 | 111  | Hitmonchan         |  1.28508% | 10304  |  1.387% | 7827   |  1.365% | 
 | 112  | Abomasnow          |  1.18719% | 10764  |  1.449% | 8986   |  1.567% | 
 | 113  | Leafeon            |  1.18068% | 9810   |  1.321% | 7132   |  1.244% | 
 | 114  | Gallade            |  1.15469% | 9962   |  1.341% | 7169   |  1.251% | 
 | 115  | Mudsdale           |  1.08250% | 8896   |  1.197% | 7234   |  1.262% | 
 | 116  | Scrafty            |  1.06741% | 9152   |  1.232% | 6992   |  1.220% | 
 | 117  | Kingler            |  1.05847% | 8715   |  1.173% | 6324   |  1.103% | 
 | 118  | Malamar            |  1.04330% | 9050   |  1.218% | 6750   |  1.177% | 
 | 119  | Pincurchin         |  1.04029% | 8659   |  1.166% | 7385   |  1.288% | 
 | 120  | Aromatisse         |  0.99989% | 7447   |  1.002% | 5628   |  0.982% | 
 | 121  | Mr. Mime-Galar     |  0.99652% | 7775   |  1.047% | 5612   |  0.979% | 
 | 122  | Greedent           |  0.99043% | 8302   |  1.118% | 6212   |  1.084% | 
 | 123  | Toxicroak          |  0.97482% | 7692   |  1.035% | 5809   |  1.013% | 
 | 124  | Silvally-Steel     |  0.97036% | 6611   |  0.890% | 5350   |  0.933% | 
 | 125  | Drifblim           |  0.94667% | 8049   |  1.083% | 6070   |  1.059% | 
 | 126  | Lanturn            |  0.86496% | 6232   |  0.839% | 4999   |  0.872% | 
 | 127  | Togedemaru         |  0.85913% | 6481   |  0.872% | 5260   |  0.918% | 
 | 128  | Ninjask            |  0.81527% | 7157   |  0.963% | 5482   |  0.956% | 
 | 129  | Crustle            |  0.77630% | 6443   |  0.867% | 5326   |  0.929% | 
 | 130  | Dusclops           |  0.75691% | 6739   |  0.907% | 5419   |  0.945% | 
 | 131  | Qwilfish           |  0.67718% | 5615   |  0.756% | 4860   |  0.848% | 
 | 132  | Thievul            |  0.67469% | 6032   |  0.812% | 4405   |  0.768% | 
 | 133  | Indeedee-F         |  0.65779% | 5576   |  0.751% | 4264   |  0.744% | 
 | 134  | Silvally-Fairy     |  0.64290% | 4589   |  0.618% | 3506   |  0.612% | 
 | 135  | Unfezant           |  0.63317% | 4864   |  0.655% | 3800   |  0.663% | 
 | 136  | Dusknoir           |  0.62717% | 6186   |  0.833% | 4851   |  0.846% | 
 | 137  | Trevenant          |  0.61022% | 5566   |  0.749% | 4124   |  0.719% | 
 | 138  | Stunfisk-Galar     |  0.60151% | 4678   |  0.630% | 3904   |  0.681% | 
 | 139  | Shedinja           |  0.59998% | 4702   |  0.633% | 3357   |  0.586% | 
 | 140  | Pangoro            |  0.59140% | 4550   |  0.612% | 3473   |  0.606% | 
 | 141  | Ludicolo           |  0.57858% | 5461   |  0.735% | 4135   |  0.721% | 
 | 142  | Klinklang          |  0.57764% | 4656   |  0.627% | 3472   |  0.606% | 
 | 143  | Stonjourner        |  0.57597% | 4905   |  0.660% | 3705   |  0.646% | 
 | 144  | Beartic            |  0.57569% | 5256   |  0.707% | 3908   |  0.682% | 
 | 145  | Glalie             |  0.57227% | 4199   |  0.565% | 3179   |  0.555% | 
 | 146  | Rotom-Frost        |  0.57112% | 4516   |  0.608% | 3513   |  0.613% | 
 | 147  | Garbodor           |  0.56531% | 4883   |  0.657% | 3993   |  0.697% | 
 | 148  | Drampa             |  0.55897% | 5169   |  0.696% | 3765   |  0.657% | 
 | 149  | Mawile             |  0.55565% | 4397   |  0.592% | 3636   |  0.634% | 
 | 150  | Slurpuff           |  0.51063% | 4058   |  0.546% | 3261   |  0.569% | 
 | 151  | Ferroseed          |  0.48725% | 3277   |  0.441% | 2792   |  0.487% | 
 | 152  | Golurk             |  0.45093% | 4057   |  0.546% | 3197   |  0.558% | 
 | 153  | Flareon            |  0.42350% | 4279   |  0.576% | 3283   |  0.573% | 
 | 154  | Gourgeist-Super    |  0.41942% | 3429   |  0.462% | 2718   |  0.474% | 
 | 155  | Bellossom          |  0.40562% | 4308   |  0.580% | 3102   |  0.541% | 
 | 156  | Shiinotic          |  0.40167% | 3850   |  0.518% | 2902   |  0.506% | 
 | 157  | Silvally-Ghost     |  0.39811% | 2604   |  0.351% | 2021   |  0.353% | 
 | 158  | Glaceon            |  0.36233% | 3518   |  0.474% | 2581   |  0.450% | 
 | 159  | Gurdurr            |  0.35711% | 2547   |  0.343% | 1932   |  0.337% | 
 | 160  | Sawk               |  0.34499% | 2645   |  0.356% | 1994   |  0.348% | 
 | 161  | Rotom              |  0.34303% | 2588   |  0.348% | 2123   |  0.370% | 
 | 162  | Octillery          |  0.34267% | 2927   |  0.394% | 2255   |  0.393% | 
 | 163  | Basculin           |  0.33709% | 3489   |  0.470% | 2418   |  0.422% | 
 | 164  | Skuntank           |  0.33626% | 2674   |  0.360% | 2141   |  0.373% | 
 | 165  | Manectric          |  0.32330% | 2814   |  0.379% | 2258   |  0.394% | 
 | 166  | Morgrem            |  0.31696% | 2435   |  0.328% | 2022   |  0.353% | 
 | 167  | Musharna           |  0.28837% | 2494   |  0.336% | 1957   |  0.341% | 
 | 168  | Raichu             |  0.26730% | 2738   |  0.369% | 2100   |  0.366% | 
 | 169  | Silvally           |  0.26556% | 2128   |  0.286% | 1601   |  0.279% | 
 | 170  | Maractus           |  0.25878% | 2198   |  0.296% | 1596   |  0.278% | 
 | 171  | Pikachu            |  0.25350% | 2400   |  0.323% | 1761   |  0.307% | 
 | 172  | Rhydon             |  0.25246% | 2257   |  0.304% | 1793   |  0.313% | 
 | 173  | Trapinch           |  0.24671% | 1460   |  0.197% | 1099   |  0.192% | 
 | 174  | Noctowl            |  0.22946% | 1787   |  0.241% | 1340   |  0.234% | 
 | 175  | Type: Null         |  0.21870% | 1737   |  0.234% | 1340   |  0.234% | 
 | 176  | Silvally-Fire      |  0.19133% | 1697   |  0.228% | 1341   |  0.234% | 
 | 177  | Wishiwashi         |  0.18766% | 1552   |  0.209% | 1210   |  0.211% | 
 | 178  | Silvally-Dark      |  0.18511% | 1266   |  0.170% | 954    |  0.166% | 
 | 179  | Piloswine          |  0.18481% | 1327   |  0.179% | 1124   |  0.196% | 
 | 180  | Beheeyem           |  0.18239% | 1938   |  0.261% | 1403   |  0.245% | 
 | 181  | Vullaby            |  0.18039% | 1289   |  0.174% | 1025   |  0.179% | 
 | 182  | Silvally-Water     |  0.17817% | 1278   |  0.172% | 1018   |  0.178% | 
 | 183  | Mr. Mime           |  0.16006% | 1403   |  0.189% | 1066   |  0.186% | 
 | 184  | Hattrem            |  0.15534% | 1391   |  0.187% | 1097   |  0.191% | 
 | 185  | Lunatone           |  0.15208% | 1446   |  0.195% | 1079   |  0.188% | 
 | 186  | Onix               |  0.14715% | 1064   |  0.143% | 923    |  0.161% | 
 | 187  | Delibird           |  0.14192% | 1400   |  0.188% | 1111   |  0.194% | 
 | 188  | Silvally-Ground    |  0.14174% | 1027   |  0.138% | 763    |  0.133% | 
 | 189  | Silvally-Flying    |  0.14081% | 1216   |  0.164% | 916    |  0.160% | 
 | 190  | Togetic            |  0.13966% | 1308   |  0.176% | 1007   |  0.176% | 
 | 191  | Gothitelle         |  0.13628% | 1494   |  0.201% | 1100   |  0.192% | 
 | 192  | Oranguru           |  0.12819% | 1175   |  0.158% | 923    |  0.161% | 
 | 193  | Whiscash           |  0.12644% | 1242   |  0.167% | 911    |  0.159% | 
 | 194  | Liepard            |  0.12352% | 1258   |  0.169% | 954    |  0.166% | 
 | 195  | Solrock            |  0.12145% | 1183   |  0.159% | 909    |  0.159% | 
 | 196  | Persian            |  0.11677% | 1178   |  0.159% | 937    |  0.163% | 
 | 197  | Cherrim            |  0.11662% | 1238   |  0.167% | 829    |  0.145% | 
 | 198  | Meowstic           |  0.11132% | 1067   |  0.144% | 801    |  0.140% | 
 | 199  | Sudowoodo          |  0.11115% | 1214   |  0.163% | 1009   |  0.176% | 
 | 200  | Wailord            |  0.10854% | 1145   |  0.154% | 875    |  0.153% | 
 | 201  | Drakloak           |  0.10725% | 982    |  0.132% | 743    |  0.130% | 
 | 202  | Clefairy           |  0.09953% | 972    |  0.131% | 799    |  0.139% | 
 | 203  | Diglett            |  0.09894% | 858    |  0.115% | 655    |  0.114% | 
 | 204  | Carkol             |  0.09595% | 789    |  0.106% | 679    |  0.118% | 
 | 205  | Heatmor            |  0.09020% | 940    |  0.127% | 738    |  0.129% | 
 | 206  | Wobbuffet          |  0.09013% | 855    |  0.115% | 593    |  0.103% | 
 | 207  | Swoobat            |  0.08561% | 799    |  0.108% | 615    |  0.107% | 
 | 208  | Vespiquen          |  0.08485% | 897    |  0.121% | 712    |  0.124% | 
 | 209  | Silvally-Electric  |  0.08334% | 850    |  0.114% | 632    |  0.110% | 
 | 210  | Swirlix            |  0.08184% | 531    |  0.071% | 483    |  0.084% | 
 | 211  | Mareanie           |  0.08144% | 573    |  0.077% | 469    |  0.082% | 
 | 212  | Throh              |  0.07996% | 969    |  0.130% | 705    |  0.123% | 
 | 213  | Silvally-Fighting  |  0.07086% | 555    |  0.075% | 408    |  0.071% | 
 | 214  | Silvally-Grass     |  0.06776% | 570    |  0.077% | 429    |  0.075% | 
 | 215  | Dottler            |  0.06612% | 727    |  0.098% | 601    |  0.105% | 
 | 216  | Silvally-Poison    |  0.06605% | 628    |  0.085% | 440    |  0.077% | 
 | 217  | Linoone-Galar      |  0.05645% | 611    |  0.082% | 518    |  0.090% | 
 | 218  | Drilbur            |  0.05240% | 468    |  0.063% | 392    |  0.068% | 
 | 219  | Silvally-Rock      |  0.04933% | 396    |  0.053% | 296    |  0.052% | 
 | 220  | Gourgeist          |  0.04926% | 526    |  0.071% | 404    |  0.070% | 
 | 221  | Gloom              |  0.04692% | 343    |  0.046% | 275    |  0.048% | 
 | 222  | Eevee              |  0.04515% | 502    |  0.068% | 390    |  0.068% | 
 | 223  | Palpitoad          |  0.04477% | 364    |  0.049% | 312    |  0.054% | 
 | 224  | Silvally-Dragon    |  0.04393% | 397    |  0.053% | 301    |  0.053% | 
 | 225  | Roselia            |  0.04386% | 401    |  0.054% | 326    |  0.057% | 
 | 226  | Gourgeist-Small    |  0.04353% | 417    |  0.056% | 327    |  0.057% | 
 | 227  | Seaking            |  0.04062% | 472    |  0.064% | 373    |  0.065% | 
 | 228  | Haunter            |  0.03876% | 457    |  0.062% | 371    |  0.065% | 
 | 229  | Corvisquire        |  0.03147% | 236    |  0.032% | 185    |  0.032% | 
 | 230  | Sneasel            |  0.02607% | 205    |  0.028% | 166    |  0.029% | 
 | 231  | Duosion            |  0.02260% | 192    |  0.026% | 139    |  0.024% | 
 | 232  | Pawniard           |  0.02200% | 205    |  0.028% | 150    |  0.026% | 
 | 233  | Silvally-Psychic   |  0.02185% | 172    |  0.023% | 119    |  0.021% | 
 | 234  | Gourgeist-Large    |  0.02158% | 181    |  0.024% | 154    |  0.027% | 
 | 235  | Zweilous           |  0.02014% | 341    |  0.046% | 230    |  0.040% | 
 | 236  | Raboot             |  0.01831% | 214    |  0.029% | 155    |  0.027% | 
 | 237  | Farfetch'd-Galar   |  0.01759% | 191    |  0.026% | 145    |  0.025% | 
 | 238  | Charjabug          |  0.01582% | 151    |  0.020% | 129    |  0.023% | 
 | 239  | Silvally-Bug       |  0.01472% | 140    |  0.019% | 94     |  0.016% | 
 | 240  | Munchlax           |  0.01431% | 136    |  0.018% | 103    |  0.018% | 
 | 241  | Snom               |  0.01416% | 169    |  0.023% | 136    |  0.024% | 
 | 242  | Hakamo-o           |  0.01302% | 101    |  0.014% | 86     |  0.015% | 
 | 243  | Darumaka-Galar     |  0.01268% | 106    |  0.014% | 85     |  0.015% | 
 | 244  | Meowth             |  0.01253% | 181    |  0.024% | 158    |  0.028% | 
 | 245  | Machoke            |  0.01144% | 134    |  0.018% | 109    |  0.019% | 
 | 246  | Riolu              |  0.01079% | 155    |  0.021% | 123    |  0.021% | 
 | 247  | Cutiefly           |  0.01067% | 163    |  0.022% | 144    |  0.025% | 
 | 248  | Thwackey           |  0.01007% | 229    |  0.031% | 178    |  0.031% | 
 | 249  | Pupitar            |  0.00981% | 86     |  0.012% | 55     |  0.010% | 
 | 250  | Skorupi            |  0.00956% | 213    |  0.029% | 170    |  0.030% | 
 | 251  | Fraxure            |  0.00872% | 83     |  0.011% | 66     |  0.012% | 
 | 252  | Natu               |  0.00855% | 75     |  0.010% | 60     |  0.010% | 
 | 253  | Cottonee           |  0.00853% | 88     |  0.012% | 70     |  0.012% | 
 | 254  | Sliggoo            |  0.00848% | 88     |  0.012% | 62     |  0.011% | 
 | 255  | Silvally-Ice       |  0.00796% | 83     |  0.011% | 62     |  0.011% | 
 | 256  | Shelmet            |  0.00793% | 49     |  0.007% | 42     |  0.007% | 
 | 257  | Blipbug            |  0.00771% | 97     |  0.013% | 95     |  0.017% | 
 | 258  | Wailmer            |  0.00765% | 63     |  0.008% | 42     |  0.007% | 
 | 259  | Drizzile           |  0.00757% | 89     |  0.012% | 70     |  0.012% | 
 | 260  | Sinistea           |  0.00743% | 93     |  0.013% | 68     |  0.012% | 
 | 261  | Rufflet            |  0.00693% | 45     |  0.006% | 30     |  0.005% | 
 | 262  | Vibrava            |  0.00669% | 103    |  0.014% | 63     |  0.011% | 
 | 263  | Klang              |  0.00632% | 90     |  0.012% | 68     |  0.012% | 
 | 264  | Litwick            |  0.00588% | 63     |  0.008% | 54     |  0.009% | 
 | 265  | Bronzor            |  0.00581% | 57     |  0.008% | 45     |  0.008% | 
 | 266  | Wooloo             |  0.00552% | 84     |  0.011% | 67     |  0.012% | 
 | 267  | Gastly             |  0.00521% | 72     |  0.010% | 61     |  0.011% | 
 | 268  | Pumpkaboo-Large    |  0.00493% | 90     |  0.012% | 75     |  0.013% | 
 | 269  | Axew               |  0.00461% | 59     |  0.008% | 38     |  0.007% | 
 | 270  | Solosis            |  0.00441% | 32     |  0.004% | 24     |  0.004% | 
 | 271  | Krabby             |  0.00437% | 57     |  0.008% | 48     |  0.008% | 
 | 272  | Gossifleur         |  0.00433% | 41     |  0.006% | 30     |  0.005% | 
 | 273  | Boldore            |  0.00358% | 45     |  0.006% | 40     |  0.007% | 
 | 274  | Wimpod             |  0.00353% | 36     |  0.005% | 31     |  0.005% | 
 | 275  | Snorunt            |  0.00350% | 43     |  0.006% | 23     |  0.004% | 
 | 276  | Mantyke            |  0.00346% | 53     |  0.007% | 48     |  0.008% | 
 | 277  | Ponyta-Galar       |  0.00318% | 34     |  0.005% | 23     |  0.004% | 
 | 278  | Scorbunny          |  0.00314% | 33     |  0.004% | 25     |  0.004% | 
 | 279  | Remoraid           |  0.00314% | 41     |  0.006% | 25     |  0.004% | 
 | 280  | Magikarp           |  0.00311% | 41     |  0.006% | 36     |  0.006% | 
 | 281  | Wooper             |  0.00303% | 27     |  0.004% | 25     |  0.004% | 
 | 282  | Applin             |  0.00292% | 26     |  0.003% | 21     |  0.004% | 
 | 283  | Shellder           |  0.00283% | 29     |  0.004% | 22     |  0.004% | 
 | 284  | Koffing            |  0.00277% | 37     |  0.005% | 29     |  0.005% | 
 | 285  | Cherubi            |  0.00274% | 16     |  0.002% | 11     |  0.002% | 
 | 286  | Clobbopus          |  0.00264% | 33     |  0.004% | 29     |  0.005% | 
 | 287  | Charmeleon         |  0.00261% | 29     |  0.004% | 19     |  0.003% | 
 | 288  | Drifloon           |  0.00254% | 59     |  0.008% | 48     |  0.008% | 
 | 289  | Corphish           |  0.00247% | 25     |  0.003% | 16     |  0.003% | 
 | 290  | Salandit           |  0.00228% | 40     |  0.005% | 31     |  0.005% | 
 | 291  | Mime Jr.           |  0.00214% | 20     |  0.003% | 18     |  0.003% | 
 | 292  | Honedge            |  0.00213% | 24     |  0.003% | 17     |  0.003% | 
 | 293  | Frillish           |  0.00213% | 30     |  0.004% | 26     |  0.005% | 
 | 294  | Ralts              |  0.00207% | 24     |  0.003% | 24     |  0.004% | 
 | 295  | Yamper             |  0.00206% | 29     |  0.004% | 25     |  0.004% | 
 | 296  | Stufful            |  0.00200% | 24     |  0.003% | 22     |  0.004% | 
 | 297  | Pichu              |  0.00198% | 34     |  0.005% | 34     |  0.006% | 
 | 298  | Dwebble            |  0.00196% | 44     |  0.006% | 44     |  0.008% | 
 | 299  | Croagunk           |  0.00184% | 25     |  0.003% | 20     |  0.003% | 
 | 300  | Grookey            |  0.00176% | 18     |  0.002% | 14     |  0.002% | 
 | 301  | Lampent            |  0.00174% | 20     |  0.003% | 16     |  0.003% | 
 | 302  | Cleffa             |  0.00170% | 36     |  0.005% | 34     |  0.006% | 
 | 303  | Zigzagoon-Galar    |  0.00167% | 15     |  0.002% | 11     |  0.002% | 
 | 304  | Trubbish           |  0.00162% | 27     |  0.004% | 26     |  0.005% | 
 | 305  | Yamask-Galar       |  0.00162% | 23     |  0.003% | 22     |  0.004% | 
 | 306  | Phantump           |  0.00157% | 57     |  0.008% | 41     |  0.007% | 
 | 307  | Rolycoly           |  0.00155% | 36     |  0.005% | 26     |  0.005% | 
 | 308  | Arrokuda           |  0.00150% | 20     |  0.003% | 16     |  0.003% | 
 | 309  | Lotad              |  0.00146% | 19     |  0.003% | 15     |  0.003% | 
 | 310  | Growlithe          |  0.00146% | 17     |  0.002% | 15     |  0.003% | 
 | 311  | Hippopotas         |  0.00144% | 17     |  0.002% | 15     |  0.003% | 
 | 312  | Caterpie           |  0.00141% | 19     |  0.003% | 17     |  0.003% | 
 | 313  | Roggenrola         |  0.00128% | 20     |  0.003% | 19     |  0.003% | 
 | 314  | Cufant             |  0.00126% | 11     |  0.001% | 9      |  0.002% | 
 | 315  | Gothorita          |  0.00126% | 21     |  0.003% | 10     |  0.002% | 
 | 316  | Swinub             |  0.00125% | 17     |  0.002% | 13     |  0.002% | 
 | 317  | Elgyem             |  0.00123% | 19     |  0.003% | 15     |  0.003% | 
 | 318  | Hatenna            |  0.00115% | 10     |  0.001% | 8      |  0.001% | 
 | 319  | Impidimp           |  0.00112% | 20     |  0.003% | 18     |  0.003% | 
 | 320  | Vanillish          |  0.00110% | 17     |  0.002% | 14     |  0.002% | 
 | 321  | Feebas             |  0.00108% | 13     |  0.002% | 9      |  0.002% | 
 | 322  | Sobble             |  0.00108% | 11     |  0.001% | 9      |  0.002% | 
 | 323  | Steenee            |  0.00104% | 14     |  0.002% | 14     |  0.002% | 
 | 324  | Milcery            |  0.00100% | 15     |  0.002% | 13     |  0.002% | 
 | 325  | Scraggy            |  0.00098% | 13     |  0.002% | 12     |  0.002% | 
 | 326  | Togepi             |  0.00097% | 15     |  0.002% | 14     |  0.002% | 
 | 327  | Grubbin            |  0.00097% | 10     |  0.001% | 10     |  0.002% | 
 | 328  | Duskull            |  0.00097% | 14     |  0.002% | 13     |  0.002% | 
 | 329  | Charmander         |  0.00096% | 26     |  0.003% | 24     |  0.004% | 
 | 330  | Pumpkaboo-Super    |  0.00096% | 10     |  0.001% | 6      |  0.001% | 
 | 331  | Silicobra          |  0.00093% | 9      |  0.001% | 6      |  0.001% | 
 | 332  | Mudbray            |  0.00091% | 33     |  0.004% | 31     |  0.005% | 
 | 333  | Vulpix             |  0.00088% | 11     |  0.001% | 10     |  0.002% | 
 | 334  | Munna              |  0.00080% | 8      |  0.001% | 7      |  0.001% | 
 | 335  | Helioptile         |  0.00080% | 7      |  0.001% | 3      |  0.001% | 
 | 336  | Joltik             |  0.00078% | 12     |  0.002% | 8      |  0.001% | 
 | 337  | Nincada            |  0.00075% | 7      |  0.001% | 7      |  0.001% | 
 | 338  | Baltoy             |  0.00074% | 13     |  0.002% | 13     |  0.002% | 
 | 339  | Yamask             |  0.00073% | 13     |  0.002% | 11     |  0.002% | 
 | 340  | Larvitar           |  0.00069% | 10     |  0.001% | 9      |  0.002% | 
 | 341  | Stunky             |  0.00069% | 10     |  0.001% | 7      |  0.001% | 
 | 342  | Dreepy             |  0.00068% | 9      |  0.001% | 7      |  0.001% | 
 | 343  | Pancham            |  0.00063% | 7      |  0.001% | 7      |  0.001% | 
 | 344  | Hoothoot           |  0.00062% | 8      |  0.001% | 7      |  0.001% | 
 | 345  | Machop             |  0.00055% | 5      |  0.001% | 3      |  0.001% | 
 | 346  | Oddish             |  0.00053% | 6      |  0.001% | 4      |  0.001% | 
 | 347  | Shellos            |  0.00052% | 5      |  0.001% | 4      |  0.001% | 
 | 348  | Bonsly             |  0.00050% | 11     |  0.001% | 9      |  0.002% | 
 | 349  | Noibat             |  0.00046% | 6      |  0.001% | 5      |  0.001% | 
 | 350  | Cubchoo            |  0.00044% | 5      |  0.001% | 5      |  0.001% | 
 | 351  | Pumpkaboo-Small    |  0.00043% | 73     |  0.010% | 60     |  0.010% | 
 | 352  | Chinchou           |  0.00042% | 5      |  0.001% | 4      |  0.001% | 
 | 353  | Lombre             |  0.00040% | 5      |  0.001% | 5      |  0.001% | 
 | 354  | Sizzlipede         |  0.00040% | 5      |  0.001% | 4      |  0.001% | 
 | 355  | Goldeen            |  0.00038% | 5      |  0.001% | 4      |  0.001% | 
 | 356  | Nuzleaf            |  0.00037% | 4      |  0.001% | 4      |  0.001% | 
 | 357  | Toxel              |  0.00037% | 4      |  0.001% | 4      |  0.001% | 
 | 358  | Bergmite           |  0.00037% | 4      |  0.001% | 2      |  0.000% | 
 | 359  | Meowth-Galar       |  0.00035% | 4      |  0.001% | 4      |  0.001% | 
 | 360  | Wingull            |  0.00034% | 8      |  0.001% | 8      |  0.001% | 
 | 361  | Goomy              |  0.00033% | 6      |  0.001% | 4      |  0.001% | 
 | 362  | Seedot             |  0.00033% | 4      |  0.001% | 2      |  0.000% | 
 | 363  | Spritzee           |  0.00033% | 7      |  0.001% | 7      |  0.001% | 
 | 364  | Wynaut             |  0.00033% | 7      |  0.001% | 5      |  0.001% | 
 | 365  | Snover             |  0.00032% | 7      |  0.001% | 6      |  0.001% | 
 | 366  | Bunnelby           |  0.00031% | 4      |  0.001% | 3      |  0.001% | 
 | 367  | Woobat             |  0.00030% | 4      |  0.001% | 3      |  0.001% | 
 | 368  | Combee             |  0.00029% | 4      |  0.001% | 4      |  0.001% | 
 | 369  | Binacle            |  0.00028% | 3      |  0.000% | 3      |  0.001% | 
 | 370  | Pumpkaboo          |  0.00027% | 3      |  0.000% | 3      |  0.001% | 
 | 371  | Budew              |  0.00026% | 3      |  0.000% | 3      |  0.001% | 
 | 372  | Rhyhorn            |  0.00025% | 4      |  0.001% | 4      |  0.001% | 
 | 373  | Klink              |  0.00025% | 5      |  0.001% | 2      |  0.000% | 
 | 374  | Tranquill          |  0.00024% | 3      |  0.000% | 1      |  0.000% | 
 | 375  | Chewtle            |  0.00024% | 3      |  0.000% | 2      |  0.000% | 
 | 376  | Golett             |  0.00024% | 3      |  0.000% | 3      |  0.001% | 
 | 377  | Inkay              |  0.00023% | 4      |  0.001% | 4      |  0.001% | 
 | 378  | Metapod            |  0.00022% | 3      |  0.000% | 3      |  0.001% | 
 | 379  | Kirlia             |  0.00021% | 3      |  0.000% | 1      |  0.000% | 
 | 380  | Minccino           |  0.00020% | 3      |  0.000% | 2      |  0.000% | 
 | 381  | Barboach           |  0.00019% | 4      |  0.001% | 4      |  0.001% | 
 | 382  | Morelull           |  0.00018% | 2      |  0.000% | 2      |  0.000% | 
 | 383  | Electrike          |  0.00018% | 2      |  0.000% | 1      |  0.000% | 
 | 384  | Vanillite          |  0.00018% | 7      |  0.001% | 7      |  0.001% | 
 | 385  | Nickit             |  0.00017% | 2      |  0.000% | 2      |  0.000% | 
 | 386  | Dewpider           |  0.00017% | 2      |  0.000% | 2      |  0.000% | 
 | 387  | Espurr             |  0.00016% | 5      |  0.001% | 4      |  0.001% | 
 | 388  | Jangmo-o           |  0.00014% | 2      |  0.000% | 1      |  0.000% | 
 | 389  | Bounsweet          |  0.00012% | 2      |  0.000% | 1      |  0.000% | 
 | 390  | Timburr            |  0.00009% | 1      |  0.000% | 1      |  0.000% | 
 | 391  | Skwovet            |  0.00009% | 1      |  0.000% | 1      |  0.000% | 
 | 392  | Rookidee           |  0.00009% | 1      |  0.000% | 1      |  0.000% | 
 | 393  | Pidove             |  0.00008% | 2      |  0.000% | 2      |  0.000% | 
 | 394  | Purrloin           |  0.00006% | 2      |  0.000% | 2      |  0.000% | 
 | 395  | Karrablast         |  0.00005% | 3      |  0.000% | 2      |  0.000% | 
 | 396  | Deino              |  0.00003% | 1      |  0.000% | 1      |  0.000% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
