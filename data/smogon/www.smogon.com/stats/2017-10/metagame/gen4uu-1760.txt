 sun...........................63.83978%
 hyperoffense..................63.83978%
 sunoffense....................63.83978%
 weatherless...................13.60159%
 semistall.....................11.31516%
 offense....................... 2.28643%
 rain.......................... 0.00000%
 choice........................ 0.00000%
 stall......................... 0.00000%
 balance....................... 0.00000%

 Stalliness (mean: -0.852)
 -1.0|##############################
     |
 -0.5|
     |#
  0.0|
     |
 +0.5|
     |
 +1.0|
     |#####
 +1.5|
     |
 more negative = more offensive, more positive = more stall
 one # =  2.75%
