 +----------------------------------------+ 
 | Haunter                                | 
 +----------------------------------------+ 
 | Raw count: 3                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 83                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Lum Berry 66.667%                      | 
 | Choice Scarf 33.333%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Shadow Ball 100.000%                   | 
 | Thunderbolt 66.667%                    | 
 | Hypnosis 66.667%                       | 
 | Energy Ball 66.667%                    | 
 | Sludge Bomb 33.333%                    | 
 | Hidden Power Ground 33.333%            | 
 | Trick 33.333%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Tauros +25.000%                        | 
 | Gardevoir +16.667%                     | 
 | Regirock +16.667%                      | 
 | Floatzel +16.667%                      | 
 | Pinsir +8.333%                         | 
 | Jynx +8.333%                           | 
 | Rhydon +8.333%                         | 
 | Politoed +8.333%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Charizard                              | 
 +----------------------------------------+ 
 | Raw count: 3                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 58                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Blaze 100.000%                         | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 100.000%                      | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/160 66.667%          | 
 | Timid:0/0/4/252/0/252 33.333%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Hidden Power Grass 100.000%            | 
 | Roost 100.000%                         | 
 | Air Slash 100.000%                     | 
 | Flamethrower 66.667%                   | 
 | Fire Blast 33.333%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Gardevoir +16.667%                     | 
 | Floatzel +16.667%                      | 
 | Regirock +16.667%                      | 
 | Quagsire +8.333%                       | 
 | Lapras +8.333%                         | 
 | Sandslash +8.333%                      | 
 | Ninetales +8.333%                      | 
 | Skuntank +8.333%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Tauros                                 | 
 +----------------------------------------+ 
 | Raw count: 3                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 83                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Intimidate 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 66.667%                       | 
 | Choice Band 33.333%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/4/0/0/252 66.667%          | 
 | Jolly:0/252/0/0/4/252 33.333%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Earthquake 100.000%                    | 
 | Double-Edge 100.000%                   | 
 | Return 66.667%                         | 
 | Stone Edge 66.667%                     | 
 | Pursuit 33.333%                        | 
 | Iron Head 33.333%                      | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Haunter +25.000%                       | 
 | Gardevoir +16.667%                     | 
 | Regirock +16.667%                      | 
 | Floatzel +16.667%                      | 
 | Pinsir +8.333%                         | 
 | Jynx +8.333%                           | 
 | Rhydon +8.333%                         | 
 | Politoed +8.333%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Gardevoir                              | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 58                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Trace 100.000%                         | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Trick 100.000%                         | 
 | Energy Ball 100.000%                   | 
 | Psychic 100.000%                       | 
 | Focus Blast 100.000%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Regirock +50.000%                      | 
 | Floatzel +50.000%                      | 
 | Haunter +25.000%                       | 
 | Tauros +25.000%                        | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Regirock                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 58                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Clear Body 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Mental Herb 50.000%                    | 
 | Chesto Berry 50.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Careful:252/4/0/0/252/0 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Stealth Rock 100.000%                  | 
 | Explosion 100.000%                     | 
 | Earthquake 100.000%                    | 
 | Rock Slide 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Floatzel +50.000%                      | 
 | Gardevoir +50.000%                     | 
 | Haunter +25.000%                       | 
 | Tauros +25.000%                        | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Floatzel                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 58                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Swift Swim 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/252/0/0/4/252 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Crunch 100.000%                        | 
 | Ice Punch 100.000%                     | 
 | Waterfall 100.000%                     | 
 | Brick Break 100.000%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Regirock +50.000%                      | 
 | Gardevoir +50.000%                     | 
 | Tauros +25.000%                        | 
 | Haunter +25.000%                       | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Quagsire                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 40                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Water Absorb 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Careful:252/4/0/0/252/0 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Curse 100.000%                         | 
 | Recover 100.000%                       | 
 | Earthquake 100.000%                    | 
 | Waterfall 100.000%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Lapras +75.000%                        | 
 | Skuntank +75.000%                      | 
 | Sandslash +75.000%                     | 
 | Ninetales +75.000%                     | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Pinsir                                 | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 83                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Hyper Cutter 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Lum Berry 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Stealth Rock 100.000%                  | 
 | Quick Attack 100.000%                  | 
 | Stone Edge 100.000%                    | 
 | X-Scissor 100.000%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Rhydon +75.000%                        | 
 | Jynx +75.000%                          | 
 | Politoed +75.000%                      | 
 | Haunter +25.000%                       | 
 | Tauros +25.000%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Jynx                                   | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 83                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Forewarn 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:200/0/0/56/0/252 100.000%        | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Calm Mind 100.000%                     | 
 | Lovely Kiss 100.000%                   | 
 | Substitute 100.000%                    | 
 | Ice Beam 100.000%                      | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Rhydon +75.000%                        | 
 | Pinsir +75.000%                        | 
 | Politoed +75.000%                      | 
 | Haunter +25.000%                       | 
 | Tauros +25.000%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Rhydon                                 | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 83                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Rock Head 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/252/4/0/0/252 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Megahorn 100.000%                      | 
 | Earthquake 100.000%                    | 
 | Rock Blast 100.000%                    | 
 | Stone Edge 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Pinsir +75.000%                        | 
 | Jynx +75.000%                          | 
 | Politoed +75.000%                      | 
 | Haunter +25.000%                       | 
 | Tauros +25.000%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Lapras                                 | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 40                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Water Absorb 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Modest:252/0/0/200/0/56 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Surf 100.000%                          | 
 | Perish Song 100.000%                   | 
 | Thunderbolt 100.000%                   | 
 | Ice Beam 100.000%                      | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Quagsire +75.000%                      | 
 | Skuntank +75.000%                      | 
 | Sandslash +75.000%                     | 
 | Ninetales +75.000%                     | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Sandslash                              | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 40                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Sand Veil 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Impish:252/0/252/0/4/0 100.000%        | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Stealth Rock 100.000%                  | 
 | Night Slash 100.000%                   | 
 | Earthquake 100.000%                    | 
 | Rapid Spin 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Quagsire +75.000%                      | 
 | Skuntank +75.000%                      | 
 | Ninetales +75.000%                     | 
 | Lapras +75.000%                        | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Skuntank                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 40                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Aftermath 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Black Glasses 100.000%                 | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/252/0/0/4/252 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Crunch 100.000%                        | 
 | Sucker Punch 100.000%                  | 
 | Explosion 100.000%                     | 
 | Pursuit 100.000%                       | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Quagsire +75.000%                      | 
 | Sandslash +75.000%                     | 
 | Ninetales +75.000%                     | 
 | Lapras +75.000%                        | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Ninetales                              | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 40                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Flash Fire 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Wide Lens 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:184/0/0/0/104/220 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Hypnosis 100.000%                      | 
 | Fire Blast 100.000%                    | 
 | Energy Ball 100.000%                   | 
 | Will-O-Wisp 100.000%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Quagsire +75.000%                      | 
 | Skuntank +75.000%                      | 
 | Sandslash +75.000%                     | 
 | Lapras +75.000%                        | 
 | Charizard +25.000%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Politoed                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 1.0                       | 
 | Viability Ceiling: 83                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Water Absorb 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Specs 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Modest:152/0/0/252/0/104 100.000%      | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Surf 100.000%                          | 
 | Hydro Pump 100.000%                    | 
 | Hidden Power Grass 100.000%            | 
 | Ice Beam 100.000%                      | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Rhydon +75.000%                        | 
 | Pinsir +75.000%                        | 
 | Jynx +75.000%                          | 
 | Haunter +25.000%                       | 
 | Tauros +25.000%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
