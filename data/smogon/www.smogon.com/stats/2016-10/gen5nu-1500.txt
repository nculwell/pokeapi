 Total battles: 2
 Avg. weight/team: 0.5
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Ampharos           | 68.59386% | 3      | 75.000% | 2      | 70.588% | 
 | 2    | Charizard          | 50.00000% | 2      | 50.000% | 1      | 35.294% | 
 | 3    | Exeggutor          | 50.00000% | 2      | 50.000% | 1      | 35.294% | 
 | 4    | Altaria            | 31.40614% | 1      | 25.000% | 1      | 35.294% | 
 | 5    | Glaceon            | 31.40614% | 1      | 25.000% | 0      |  0.000% | 
 | 6    | Pikachu            | 31.40614% | 1      | 25.000% | 0      |  0.000% | 
 | 7    | Slowpoke           | 31.40614% | 1      | 25.000% | 1      | 35.294% | 
 | 8    | Misdreavus         | 31.40614% | 1      | 25.000% | 1      | 35.294% | 
 | 9    | Gigalith           | 31.40614% | 1      | 25.000% | 1      | 35.294% | 
 | 10   | Alomomola          | 31.40614% | 1      | 25.000% | 0      |  0.000% | 
 | 11   | Eevee              | 31.40614% | 1      | 25.000% | 1      | 35.294% | 
 | 12   | Mantine            | 31.40614% | 1      | 25.000% | 1      | 35.294% | 
 | 13   | Electabuzz         | 18.59386% | 1      | 25.000% | 1      | 35.294% | 
 | 14   | Lapras             | 18.59386% | 1      | 25.000% | 1      | 35.294% | 
 | 15   | Ariados            | 18.59386% | 1      | 25.000% | 0      |  0.000% | 
 | 16   | Shedinja           | 18.59386% | 1      | 25.000% | 1      | 35.294% | 
 | 17   | Articuno           | 18.59386% | 1      | 25.000% | 1      | 35.294% | 
 | 18   | Shuckle            | 18.59386% | 1      | 25.000% | 1      | 35.294% | 
 | 19   | Dragonair          | 18.59386% | 1      | 25.000% | 1      | 35.294% | 
 | 20   | Grovyle            | 18.59386% | 1      | 25.000% | 1      | 35.294% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
