 Total battles: 94764
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Abra               | 27.05384% | 51275  | 27.054% | 35358  | 24.822% | 
 | 2    | Mienfoo            | 22.54684% | 42732  | 22.547% | 34496  | 24.217% | 
 | 3    | Pawniard           | 17.12422% | 32454  | 17.124% | 24564  | 17.245% | 
 | 4    | Fletchling         | 14.71856% | 27895  | 14.718% | 20663  | 14.506% | 
 | 5    | Chinchou           | 13.93249% | 26405  | 13.932% | 20752  | 14.568% | 
 | 6    | Aipom              | 12.68858% | 24049  | 12.689% | 19795  | 13.897% | 
 | 7    | Gastly             | 12.54239% | 23772  | 12.543% | 17033  | 11.958% | 
 | 8    | Archen             | 11.29650% | 21411  | 11.297% | 15582  | 10.939% | 
 | 9    | Ponyta             | 10.28025% | 19484  | 10.280% | 14520  | 10.193% | 
 | 10   | Diglett            |  9.97831% | 18911  |  9.978% | 14442  | 10.139% | 
 | 11   | Ferroseed          |  9.68441% | 18354  |  9.684% | 15819  | 11.105% | 
 | 12   | Carvanha           |  9.50014% | 18005  |  9.500% | 13381  |  9.394% | 
 | 13   | Magnemite          |  9.31295% | 17652  |  9.314% | 13313  |  9.346% | 
 | 14   | Onix               |  8.18801% | 15518  |  8.188% | 12897  |  9.054% | 
 | 15   | Porygon            |  7.82499% | 14831  |  7.825% | 10729  |  7.532% | 
 | 16   | Scraggy            |  7.33850% | 13908  |  7.338% | 10589  |  7.434% | 
 | 17   | Vullaby            |  7.30197% | 13839  |  7.302% | 10865  |  7.627% | 
 | 18   | Cottonee           |  7.26199% | 13763  |  7.262% | 10918  |  7.665% | 
 | 19   | Drilbur            |  7.22189% | 13687  |  7.222% | 10373  |  7.282% | 
 | 20   | Staryu             |  7.17335% | 13595  |  7.173% | 10158  |  7.131% | 
 | 21   | Timburr            |  7.00819% | 13282  |  7.008% | 10314  |  7.241% | 
 | 22   | Snivy              |  6.54452% | 12405  |  6.545% | 9029   |  6.339% | 
 | 23   | Foongus            |  6.46934% | 12261  |  6.469% | 9488   |  6.661% | 
 | 24   | Croagunk           |  6.43886% | 12204  |  6.439% | 9652   |  6.776% | 
 | 25   | Munchlax           |  6.24230% | 11832  |  6.243% | 8353   |  5.864% | 
 | 26   | Elekid             |  6.03310% | 11434  |  6.033% | 8328   |  5.846% | 
 | 27   | Shellder           |  5.90659% | 11195  |  5.907% | 8285   |  5.816% | 
 | 28   | Spritzee           |  5.77257% | 10941  |  5.773% | 7357   |  5.165% | 
 | 29   | Torchic            |  5.17292% | 9804   |  5.173% | 7916   |  5.557% | 
 | 30   | Dwebble            |  5.08915% | 9645   |  5.089% | 7942   |  5.575% | 
 | 31   | Cranidos           |  5.07012% | 9612   |  5.072% | 6630   |  4.654% | 
 | 32   | Meowth             |  4.85804% | 9207   |  4.858% | 7719   |  5.419% | 
 | 33   | Tirtouga           |  4.73562% | 8975   |  4.735% | 6573   |  4.614% | 
 | 34   | Honedge            |  4.70396% | 8915   |  4.704% | 6906   |  4.848% | 
 | 35   | Froakie            |  4.63537% | 8785   |  4.635% | 6607   |  4.638% | 
 | 36   | Vulpix             |  4.59105% | 8701   |  4.591% | 7232   |  5.077% | 
 | 37   | Aron               |  4.46271% | 8458   |  4.463% | 6560   |  4.605% | 
 | 38   | Axew               |  4.42642% | 8389   |  4.426% | 5892   |  4.136% | 
 | 39   | Amaura             |  4.25388% | 8062   |  4.254% | 5726   |  4.020% | 
 | 40   | Cubone             |  4.18951% | 7940   |  4.189% | 5888   |  4.134% | 
 | 41   | Bulbasaur          |  4.16313% | 7890   |  4.163% | 5932   |  4.164% | 
 | 42   | Houndour           |  4.04599% | 7668   |  4.046% | 5653   |  3.969% | 
 | 43   | Riolu              |  4.02277% | 7624   |  4.023% | 5677   |  3.985% | 
 | 44   | Larvesta           |  3.93348% | 7455   |  3.933% | 5515   |  3.872% | 
 | 45   | Zigzagoon          |  3.92252% | 7434   |  3.922% | 5262   |  3.694% | 
 | 46   | Eevee              |  3.87081% | 7336   |  3.871% | 5138   |  3.607% | 
 | 47   | Bunnelby           |  3.80392% | 7210   |  3.804% | 5358   |  3.761% | 
 | 48   | Snubbull           |  3.35214% | 6353   |  3.352% | 4932   |  3.462% | 
 | 49   | Growlithe          |  3.27258% | 6203   |  3.273% | 4613   |  3.238% | 
 | 50   | Omanyte            |  3.25347% | 6166   |  3.253% | 4653   |  3.267% | 
 | 51   | Chimchar           |  3.18276% | 6032   |  3.183% | 4868   |  3.417% | 
 | 52   | Tyrunt             |  3.15942% | 5988   |  3.159% | 4248   |  2.982% | 
 | 53   | Anorith            |  3.14688% | 5964   |  3.147% | 4820   |  3.384% | 
 | 54   | Corphish           |  3.14465% | 5960   |  3.145% | 4462   |  3.132% | 
 | 55   | Charmander         |  3.13409% | 5940   |  3.134% | 4291   |  3.012% | 
 | 56   | Dratini            |  3.12419% | 5921   |  3.124% | 4252   |  2.985% | 
 | 57   | Zorua              |  2.98014% | 5648   |  2.980% | 2965   |  2.082% | 
 | 58   | Gothita            |  2.95798% | 5606   |  2.958% | 4029   |  2.828% | 
 | 59   | Duskull            |  2.94004% | 5573   |  2.940% | 4201   |  2.949% | 
 | 60   | Inkay              |  2.88833% | 5474   |  2.888% | 4108   |  2.884% | 
 | 61   | Tentacool          |  2.83610% | 5375   |  2.836% | 4157   |  2.918% | 
 | 62   | Clamperl           |  2.82238% | 5349   |  2.822% | 3899   |  2.737% | 
 | 63   | Bellsprout         |  2.80338% | 5313   |  2.803% | 3979   |  2.793% | 
 | 64   | Machop             |  2.78281% | 5274   |  2.783% | 4133   |  2.901% | 
 | 65   | Bronzor            |  2.76763% | 5246   |  2.768% | 4155   |  2.917% | 
 | 66   | Lileep             |  2.71685% | 5149   |  2.717% | 4092   |  2.873% | 
 | 67   | Buizel             |  2.67569% | 5071   |  2.676% | 3937   |  2.764% | 
 | 68   | Natu               |  2.63454% | 4993   |  2.634% | 3796   |  2.665% | 
 | 69   | Stunky             |  2.62979% | 4984   |  2.630% | 3792   |  2.662% | 
 | 70   | Frillish           |  2.60129% | 4930   |  2.601% | 3802   |  2.669% | 
 | 71   | Taillow            |  2.51939% | 4775   |  2.519% | 3460   |  2.429% | 
 | 72   | Slowpoke           |  2.48046% | 4701   |  2.480% | 3639   |  2.555% | 
 | 73   | Koffing            |  2.46768% | 4677   |  2.468% | 3735   |  2.622% | 
 | 74   | Mantyke            |  2.42162% | 4591   |  2.422% | 3485   |  2.447% | 
 | 75   | Surskit            |  2.41926% | 4585   |  2.419% | 4207   |  2.953% | 
 | 76   | Lickitung          |  2.35212% | 4458   |  2.352% | 3302   |  2.318% | 
 | 77   | Hippopotas         |  2.30317% | 4365   |  2.303% | 3742   |  2.627% | 
 | 78   | Purrloin           |  2.24566% | 4256   |  2.246% | 3507   |  2.462% | 
 | 79   | Magby              |  2.24026% | 4246   |  2.240% | 3136   |  2.202% | 
 | 80   | Voltorb            |  2.19883% | 4168   |  2.199% | 3291   |  2.310% | 
 | 81   | Blitzle            |  2.19659% | 4163   |  2.197% | 2978   |  2.091% | 
 | 82   | Buneary            |  2.18657% | 4145   |  2.187% | 3326   |  2.335% | 
 | 83   | Squirtle           |  2.17853% | 4129   |  2.179% | 3244   |  2.277% | 
 | 84   | Darumaka           |  2.15280% | 4080   |  2.153% | 3026   |  2.124% | 
 | 85   | Wailmer            |  2.15174% | 4078   |  2.152% | 3062   |  2.150% | 
 | 86   | Pancham            |  2.11428% | 4007   |  2.114% | 3079   |  2.162% | 
 | 87   | Skrelp             |  2.10754% | 3995   |  2.108% | 3015   |  2.117% | 
 | 88   | Pumpkaboo-Super    |  2.09435% | 3970   |  2.095% | 3130   |  2.197% | 
 | 89   | Treecko            |  1.99702% | 3785   |  1.997% | 2817   |  1.978% | 
 | 90   | Bagon              |  1.94701% | 3690   |  1.947% | 2536   |  1.780% | 
 | 91   | Geodude            |  1.94068% | 3678   |  1.941% | 3131   |  2.198% | 
 | 92   | Doduo              |  1.83860% | 3485   |  1.839% | 2493   |  1.750% | 
 | 93   | Phantump           |  1.83661% | 3481   |  1.837% | 2598   |  1.824% | 
 | 94   | Pineco             |  1.83304% | 3474   |  1.833% | 3132   |  2.199% | 
 | 95   | Cyndaquil          |  1.81932% | 3448   |  1.819% | 2531   |  1.777% | 
 | 96   | Gible              |  1.81287% | 3436   |  1.813% | 2547   |  1.788% | 
 | 97   | Totodile           |  1.69520% | 3213   |  1.695% | 2317   |  1.627% | 
 | 98   | Cacnea             |  1.68952% | 3202   |  1.689% | 2475   |  1.738% | 
 | 99   | Baltoy             |  1.67211% | 3169   |  1.672% | 2448   |  1.719% | 
 | 100  | Mankey             |  1.61354% | 3058   |  1.613% | 2322   |  1.630% | 
 | 101  | Mime Jr.           |  1.60721% | 3046   |  1.607% | 2297   |  1.613% | 
 | 102  | Minccino           |  1.57819% | 2991   |  1.578% | 2171   |  1.524% | 
 | 103  | Clauncher          |  1.57251% | 2981   |  1.573% | 2199   |  1.544% | 
 | 104  | Sandshrew          |  1.55286% | 2943   |  1.553% | 2449   |  1.719% | 
 | 105  | Krabby             |  1.48057% | 2806   |  1.481% | 2024   |  1.421% | 
 | 106  | Chespin            |  1.46844% | 2783   |  1.468% | 2106   |  1.478% | 
 | 107  | Bidoof             |  1.46475% | 2776   |  1.465% | 2102   |  1.476% | 
 | 108  | Litwick            |  1.40881% | 2670   |  1.409% | 1982   |  1.391% | 
 | 109  | Noibat             |  1.40829% | 2669   |  1.408% | 1984   |  1.393% | 
 | 110  | Grimer             |  1.39562% | 2645   |  1.396% | 2030   |  1.425% | 
 | 111  | Spinarak           |  1.38994% | 2635   |  1.390% | 2373   |  1.666% | 
 | 112  | Nosepass           |  1.38718% | 2629   |  1.387% | 2171   |  1.524% | 
 | 113  | Numel              |  1.37821% | 2612   |  1.378% | 1982   |  1.391% | 
 | 114  | Sandile            |  1.37768% | 2611   |  1.378% | 1927   |  1.353% | 
 | 115  | Togepi             |  1.36766% | 2592   |  1.368% | 1828   |  1.283% | 
 | 116  | Trubbish           |  1.35658% | 2571   |  1.357% | 1962   |  1.377% | 
 | 117  | Rattata            |  1.35316% | 2565   |  1.353% | 1969   |  1.382% | 
 | 118  | Fennekin           |  1.34984% | 2559   |  1.350% | 1850   |  1.299% | 
 | 119  | Joltik             |  1.29801% | 2460   |  1.298% | 1875   |  1.316% | 
 | 120  | Litleo             |  1.23047% | 2332   |  1.230% | 1613   |  1.132% | 
 | 121  | Deino              |  1.21306% | 2299   |  1.213% | 1607   |  1.128% | 
 | 122  | Snover             |  1.20567% | 2285   |  1.206% | 1745   |  1.225% | 
 | 123  | Exeggcute          |  1.20527% | 2285   |  1.206% | 1694   |  1.189% | 
 | 124  | Wooper             |  1.19301% | 2261   |  1.193% | 1833   |  1.287% | 
 | 125  | Yamask             |  1.18562% | 2247   |  1.186% | 1857   |  1.304% | 
 | 126  | Golett             |  1.18391% | 2244   |  1.184% | 1723   |  1.210% | 
 | 127  | Helioptile         |  1.17032% | 2218   |  1.170% | 1587   |  1.114% | 
 | 128  | Azurill            |  1.16768% | 2213   |  1.168% | 1655   |  1.162% | 
 | 129  | Deerling           |  1.15502% | 2189   |  1.155% | 1658   |  1.164% | 
 | 130  | Wynaut             |  1.15343% | 2186   |  1.153% | 1688   |  1.185% | 
 | 131  | Solosis            |  1.12705% | 2136   |  1.127% | 1485   |  1.043% | 
 | 132  | Bergmite           |  1.12441% | 2131   |  1.124% | 1469   |  1.031% | 
 | 133  | Shieldon           |  1.12125% | 2125   |  1.121% | 1727   |  1.212% | 
 | 134  | Chikorita          |  1.09170% | 2069   |  1.092% | 1515   |  1.064% | 
 | 135  | Bonsly             |  1.08748% | 2061   |  1.087% | 1658   |  1.164% | 
 | 136  | Drowzee            |  1.08167% | 2050   |  1.082% | 1548   |  1.087% | 
 | 137  | Goomy              |  1.07745% | 2042   |  1.077% | 1434   |  1.007% | 
 | 138  | Beldum             |  1.04527% | 1981   |  1.045% | 1474   |  1.035% | 
 | 139  | Phanpy             |  1.03723% | 1966   |  1.037% | 1703   |  1.196% | 
 | 140  | Shinx              |  1.03682% | 1965   |  1.037% | 1468   |  1.031% | 
 | 141  | Rufflet            |  1.02891% | 1950   |  1.029% | 1417   |  0.995% | 
 | 142  | Barboach           |  0.97615% | 1850   |  0.976% | 1353   |  0.950% | 
 | 143  | Woobat             |  0.96718% | 1833   |  0.967% | 1377   |  0.967% | 
 | 144  | Ekans              |  0.96466% | 1829   |  0.965% | 1378   |  0.967% | 
 | 145  | Piplup             |  0.95504% | 1810   |  0.955% | 1443   |  1.013% | 
 | 146  | Trapinch           |  0.93868% | 1779   |  0.939% | 1325   |  0.930% | 
 | 147  | Pichu              |  0.93235% | 1767   |  0.932% | 1355   |  0.951% | 
 | 148  | Rhyhorn            |  0.91904% | 1742   |  0.919% | 1266   |  0.889% | 
 | 149  | Kabuto             |  0.88064% | 1669   |  0.881% | 1336   |  0.938% | 
 | 150  | Larvitar           |  0.87378% | 1656   |  0.874% | 1177   |  0.826% | 
 | 151  | Skiddo             |  0.86534% | 1640   |  0.865% | 1188   |  0.834% | 
 | 152  | Teddiursa          |  0.85848% | 1627   |  0.858% | 1219   |  0.856% | 
 | 153  | Ralts              |  0.83948% | 1591   |  0.839% | 1146   |  0.805% | 
 | 154  | Binacle            |  0.81785% | 1550   |  0.818% | 1112   |  0.781% | 
 | 155  | Skorupi            |  0.81363% | 1542   |  0.814% | 1232   |  0.865% | 
 | 156  | Shroomish          |  0.81257% | 1540   |  0.813% | 1156   |  0.812% | 
 | 157  | Sewaddle           |  0.80519% | 1526   |  0.805% | 1324   |  0.929% | 
 | 158  | Cleffa             |  0.80360% | 1523   |  0.804% | 1151   |  0.808% | 
 | 159  | Goldeen            |  0.78778% | 1493   |  0.788% | 1109   |  0.779% | 
 | 160  | Smoochum           |  0.77881% | 1476   |  0.779% | 1059   |  0.743% | 
 | 161  | Zubat              |  0.77881% | 1476   |  0.779% | 1154   |  0.810% | 
 | 162  | Elgyem             |  0.73408% | 1392   |  0.734% | 994    |  0.698% | 
 | 163  | Turtwig            |  0.70388% | 1334   |  0.704% | 1049   |  0.736% | 
 | 164  | Electrike          |  0.70282% | 1332   |  0.703% | 946    |  0.664% | 
 | 165  | Poliwag            |  0.69385% | 1315   |  0.694% | 966    |  0.678% | 
 | 166  | Espurr             |  0.69385% | 1315   |  0.694% | 944    |  0.663% | 
 | 167  | Horsea             |  0.69215% | 1312   |  0.692% | 939    |  0.659% | 
 | 168  | Glameow            |  0.68752% | 1303   |  0.687% | 1086   |  0.762% | 
 | 169  | Ducklett           |  0.67486% | 1279   |  0.675% | 967    |  0.679% | 
 | 170  | Starly             |  0.67157% | 1273   |  0.672% | 952    |  0.668% | 
 | 171  | Flabebe            |  0.67104% | 1272   |  0.671% | 885    |  0.621% | 
 | 172  | Mudkip             |  0.66483% | 1260   |  0.665% | 928    |  0.651% | 
 | 173  | NidoranM           |  0.62684% | 1188   |  0.627% | 842    |  0.591% | 
 | 174  | Happiny            |  0.62209% | 1179   |  0.622% | 850    |  0.597% | 
 | 175  | Oddish             |  0.60152% | 1140   |  0.601% | 851    |  0.597% | 
 | 176  | Munna              |  0.58885% | 1116   |  0.589% | 714    |  0.501% | 
 | 177  | Venipede           |  0.57936% | 1098   |  0.579% | 903    |  0.634% | 
 | 178  | Wingull            |  0.56722% | 1075   |  0.567% | 848    |  0.595% | 
 | 179  | Oshawott           |  0.55086% | 1044   |  0.551% | 794    |  0.557% | 
 | 180  | Venonat            |  0.53345% | 1011   |  0.533% | 779    |  0.547% | 
 | 181  | Mareep             |  0.51551% | 977    |  0.515% | 735    |  0.516% | 
 | 182  | Shellos            |  0.51287% | 972    |  0.513% | 762    |  0.535% | 
 | 183  | Remoraid           |  0.49229% | 933    |  0.492% | 704    |  0.494% | 
 | 184  | Tyrogue            |  0.49177% | 932    |  0.492% | 733    |  0.515% | 
 | 185  | Paras              |  0.48702% | 923    |  0.487% | 698    |  0.490% | 
 | 186  | Lotad              |  0.48649% | 922    |  0.486% | 723    |  0.508% | 
 | 187  | Psyduck            |  0.44533% | 844    |  0.445% | 626    |  0.439% | 
 | 188  | Budew              |  0.42106% | 798    |  0.421% | 581    |  0.408% | 
 | 189  | Tepig              |  0.40629% | 770    |  0.406% | 595    |  0.418% | 
 | 190  | Makuhita           |  0.39626% | 751    |  0.396% | 584    |  0.410% | 
 | 191  | Chingling          |  0.39257% | 744    |  0.393% | 531    |  0.373% | 
 | 192  | Shuppet            |  0.37199% | 705    |  0.372% | 532    |  0.373% | 
 | 193  | Swinub             |  0.36408% | 690    |  0.364% | 567    |  0.398% | 
 | 194  | Skitty             |  0.36038% | 683    |  0.360% | 527    |  0.370% | 
 | 195  | Seel               |  0.35933% | 681    |  0.359% | 530    |  0.372% | 
 | 196  | Hoppip             |  0.34983% | 663    |  0.350% | 516    |  0.362% | 
 | 197  | Shelmet            |  0.34666% | 657    |  0.347% | 506    |  0.355% | 
 | 198  | Finneon            |  0.32925% | 624    |  0.329% | 502    |  0.352% | 
 | 199  | Cubchoo            |  0.32028% | 607    |  0.320% | 467    |  0.328% | 
 | 200  | Poochyena          |  0.31395% | 595    |  0.314% | 396    |  0.278% | 
 | 201  | Roggenrola         |  0.30234% | 573    |  0.302% | 505    |  0.355% | 
 | 202  | Pidgey             |  0.29759% | 564    |  0.298% | 429    |  0.301% | 
 | 203  | Magikarp           |  0.28915% | 548    |  0.289% | 441    |  0.310% | 
 | 204  | Karrablast         |  0.27438% | 520    |  0.274% | 361    |  0.253% | 
 | 205  | Cherubi            |  0.27438% | 520    |  0.274% | 360    |  0.253% | 
 | 206  | Swablu             |  0.27279% | 517    |  0.273% | 353    |  0.248% | 
 | 207  | Spoink             |  0.27003% | 512    |  0.270% | 365    |  0.256% | 
 | 208  | Spheal             |  0.26382% | 500    |  0.264% | 385    |  0.270% | 
 | 209  | Slugma             |  0.26330% | 499    |  0.263% | 379    |  0.266% | 
 | 210  | Caterpie           |  0.25327% | 480    |  0.253% | 359    |  0.252% | 
 | 211  | Pumpkaboo-Small    |  0.24061% | 456    |  0.241% | 359    |  0.252% | 
 | 212  | Vanillite          |  0.23533% | 446    |  0.235% | 317    |  0.223% | 
 | 213  | Pumpkaboo          |  0.23058% | 437    |  0.231% | 355    |  0.249% | 
 | 214  | Igglybuff          |  0.23005% | 436    |  0.230% | 322    |  0.226% | 
 | 215  | Spearow            |  0.21317% | 404    |  0.213% | 301    |  0.211% | 
 | 216  | Panpour            |  0.20578% | 390    |  0.206% | 297    |  0.209% | 
 | 217  | Hoothoot           |  0.20314% | 385    |  0.203% | 286    |  0.201% | 
 | 218  | Feebas             |  0.19206% | 364    |  0.192% | 287    |  0.201% | 
 | 219  | Snorunt            |  0.18890% | 358    |  0.189% | 272    |  0.191% | 
 | 220  | Lillipup           |  0.18045% | 342    |  0.180% | 232    |  0.163% | 
 | 221  | Ledyba             |  0.17834% | 338    |  0.178% | 253    |  0.178% | 
 | 222  | Petilil            |  0.17623% | 334    |  0.176% | 229    |  0.161% | 
 | 223  | Pansage            |  0.17201% | 326    |  0.172% | 237    |  0.166% | 
 | 224  | Pidove             |  0.17096% | 324    |  0.171% | 252    |  0.177% | 
 | 225  | Weedle             |  0.15460% | 293    |  0.155% | 216    |  0.152% | 
 | 226  | Pansear            |  0.15038% | 285    |  0.150% | 204    |  0.143% | 
 | 227  | Gulpin             |  0.14985% | 284    |  0.150% | 205    |  0.144% | 
 | 228  | NidoranF           |  0.14669% | 278    |  0.147% | 199    |  0.140% | 
 | 229  | Patrat             |  0.13561% | 257    |  0.136% | 163    |  0.114% | 
 | 230  | Klink              |  0.12716% | 241    |  0.127% | 183    |  0.128% | 
 | 231  | Pumpkaboo-Large    |  0.12030% | 228    |  0.120% | 175    |  0.123% | 
 | 232  | Seedot             |  0.11608% | 220    |  0.116% | 164    |  0.115% | 
 | 233  | Sentret            |  0.11608% | 220    |  0.116% | 160    |  0.112% | 
 | 234  | Tynamo             |  0.11344% | 215    |  0.113% | 168    |  0.118% | 
 | 235  | Whismur            |  0.09339% | 177    |  0.093% | 133    |  0.093% | 
 | 236  | Nincada            |  0.08601% | 163    |  0.086% | 126    |  0.088% | 
 | 237  | Wurmple            |  0.08548% | 162    |  0.085% | 118    |  0.083% | 
 | 238  | Tympole            |  0.08390% | 159    |  0.084% | 122    |  0.086% | 
 | 239  | Slakoth            |  0.08231% | 156    |  0.082% | 121    |  0.085% | 
 | 240  | Sunkern            |  0.07862% | 149    |  0.079% | 127    |  0.089% | 
 | 241  | Scatterbug         |  0.07809% | 148    |  0.078% | 96     |  0.067% | 
 | 242  | Combee             |  0.07756% | 147    |  0.078% | 127    |  0.089% | 
 | 243  | Kricketot          |  0.03799% | 72     |  0.038% | 57     |  0.040% | 
 | 244  | Burmy              |  0.03324% | 63     |  0.033% | 46     |  0.032% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
