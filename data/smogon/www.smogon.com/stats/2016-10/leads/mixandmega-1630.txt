 Total leads: 13632
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Weavile            |  5.24456% | 270    |  1.981% | 
 | 2    | Gengar             |  4.95339% | 243    |  1.783% | 
 | 3    | Blissey            |  4.78801% | 181    |  1.328% | 
 | 4    | Deoxys-Speed       |  4.68029% | 203    |  1.489% | 
 | 5    | Salamence          |  4.17661% | 93     |  0.682% | 
 | 6    | Entei              |  4.11084% | 90     |  0.660% | 
 | 7    | Groudon-Primal     |  3.58924% | 317    |  2.325% | 
 | 8    | Mew                |  3.54837% | 258    |  1.893% | 
 | 9    | Thundurus          |  2.54166% | 66     |  0.484% | 
 | 10   | Ninetales          |  2.17143% | 92     |  0.675% | 
 | 11   | Volcarona          |  1.95719% | 72     |  0.528% | 
 | 12   | Skarmory           |  1.91856% | 149    |  1.093% | 
 | 13   | Greninja           |  1.87776% | 297    |  2.179% | 
 | 14   | Zapdos             |  1.87166% | 90     |  0.660% | 
 | 15   | Noivern            |  1.65690% | 167    |  1.225% | 
 | 16   | Landorus-Therian   |  1.50306% | 150    |  1.100% | 
 | 17   | Heatran            |  1.44819% | 111    |  0.814% | 
 | 18   | Mienshao           |  1.42455% | 68     |  0.499% | 
 | 19   | Ferrothorn         |  1.35999% | 102    |  0.748% | 
 | 20   | Kyogre             |  1.23222% | 39     |  0.286% | 
 | 21   | Hoopa-Unbound      |  1.20781% | 74     |  0.543% | 
 | 22   | Suicune            |  1.15405% | 41     |  0.301% | 
 | 23   | Jirachi            |  1.12418% | 54     |  0.396% | 
 | 24   | Garchomp           |  1.11706% | 93     |  0.682% | 
 | 25   | Raikou             |  1.03777% | 86     |  0.631% | 
 | 26   | Terrakion          |  1.03395% | 65     |  0.477% | 
 | 27   | Arcanine           |  1.00256% | 101    |  0.741% | 
 | 28   | Cobalion           |  0.97830% | 28     |  0.205% | 
 | 29   | Kyogre-Primal      |  0.94011% | 162    |  1.188% | 
 | 30   | Keldeo             |  0.89862% | 67     |  0.491% | 
 | 31   | Victini            |  0.74325% | 89     |  0.653% | 
 | 32   | Gyarados           |  0.74136% | 40     |  0.293% | 
 | 33   | Breloom            |  0.73701% | 80     |  0.587% | 
 | 34   | Raichu             |  0.71970% | 53     |  0.389% | 
 | 35   | Chandelure         |  0.70502% | 45     |  0.330% | 
 | 36   | Tornadus           |  0.69875% | 40     |  0.293% | 
 | 37   | Scizor             |  0.65816% | 35     |  0.257% | 
 | 38   | Tornadus-Therian   |  0.64294% | 46     |  0.337% | 
 | 39   | Excadrill          |  0.63796% | 45     |  0.330% | 
 | 40   | Archeops           |  0.63210% | 53     |  0.389% | 
 | 41   | Starmie            |  0.59460% | 71     |  0.521% | 
 | 42   | Azelf              |  0.55028% | 28     |  0.205% | 
 | 43   | Shuckle            |  0.54667% | 99     |  0.726% | 
 | 44   | Thundurus-Therian  |  0.54533% | 40     |  0.293% | 
 | 45   | Latios             |  0.52199% | 36     |  0.264% | 
 | 46   | Metagross          |  0.49912% | 33     |  0.242% | 
 | 47   | Zygarde            |  0.49058% | 68     |  0.499% | 
 | 48   | Volcanion          |  0.48353% | 64     |  0.469% | 
 | 49   | Rhyperior          |  0.46241% | 40     |  0.293% | 
 | 50   | Klefki             |  0.45688% | 53     |  0.389% | 
 | 51   | Empoleon           |  0.44190% | 43     |  0.315% | 
 | 52   | Jolteon            |  0.43785% | 79     |  0.580% | 
 | 53   | Charizard          |  0.41489% | 36     |  0.264% | 
 | 54   | Gengar-Mega        |  0.40843% | 278    |  2.039% | 
 | 55   | Rayquaza           |  0.39208% | 34     |  0.249% | 
 | 56   | Infernape          |  0.38999% | 81     |  0.594% | 
 | 57   | Mewtwo-Mega-Y      |  0.38415% | 188    |  1.379% | 
 | 58   | Mewtwo-Mega-X      |  0.38001% | 168    |  1.232% | 
 | 59   | Dialga             |  0.37316% | 49     |  0.359% | 
 | 60   | Blaziken           |  0.37239% | 32     |  0.235% | 
 | 61   | Snorlax            |  0.35095% | 53     |  0.389% | 
 | 62   | Nidoking           |  0.34568% | 34     |  0.249% | 
 | 63   | Cloyster           |  0.33414% | 64     |  0.469% | 
 | 64   | Kyurem             |  0.33243% | 27     |  0.198% | 
 | 65   | Porygon-Z          |  0.32210% | 56     |  0.411% | 
 | 66   | Charizard-Mega-X   |  0.31745% | 443    |  3.250% | 
 | 67   | Smeargle           |  0.30813% | 63     |  0.462% | 
 | 68   | Clefable           |  0.29847% | 49     |  0.359% | 
 | 69   | Rayquaza-Mega      |  0.29311% | 167    |  1.225% | 
 | 70   | Sableye-Mega       |  0.29067% | 48     |  0.352% | 
 | 71   | Galvantula         |  0.28375% | 13     |  0.095% | 
 | 72   | Ambipom            |  0.28243% | 37     |  0.271% | 
 | 73   | Registeel          |  0.28107% | 19     |  0.139% | 
 | 74   | Vaporeon           |  0.26985% | 47     |  0.345% | 
 | 75   | Metagross-Mega     |  0.26725% | 189    |  1.386% | 
 | 76   | Flygon             |  0.26523% | 24     |  0.176% | 
 | 77   | Alakazam           |  0.26287% | 43     |  0.315% | 
 | 78   | Venusaur           |  0.25804% | 20     |  0.147% | 
 | 79   | Crobat             |  0.25398% | 69     |  0.506% | 
 | 80   | Staraptor          |  0.25025% | 25     |  0.183% | 
 | 81   | Froslass           |  0.23483% | 18     |  0.132% | 
 | 82   | Chansey            |  0.22925% | 21     |  0.154% | 
 | 83   | Groudon            |  0.22629% | 19     |  0.139% | 
 | 84   | Sceptile-Mega      |  0.22042% | 100    |  0.734% | 
 | 85   | Rampardos          |  0.20751% | 19     |  0.139% | 
 | 86   | Dragonite          |  0.20034% | 93     |  0.682% | 
 | 87   | Klinklang          |  0.20008% | 9      |  0.066% | 
 | 88   | Bastiodon          |  0.19937% | 7      |  0.051% | 
 | 89   | Shiftry            |  0.19530% | 6      |  0.044% | 
 | 90   | Luxray             |  0.18690% | 30     |  0.220% | 
 | 91   | Gliscor            |  0.18044% | 95     |  0.697% | 
 | 92   | Conkeldurr         |  0.17525% | 17     |  0.125% | 
 | 93   | Diancie            |  0.17076% | 5      |  0.037% | 
 | 94   | Sceptile           |  0.17042% | 10     |  0.073% | 
 | 95   | Glalie-Mega        |  0.17021% | 9      |  0.066% | 
 | 96   | Gardevoir          |  0.16957% | 34     |  0.249% | 
 | 97   | Tyranitar          |  0.16795% | 65     |  0.477% | 
 | 98   | Zoroark            |  0.16116% | 64     |  0.469% | 
 | 99   | Rotom-Mow          |  0.16100% | 8      |  0.059% | 
 | 100  | Lucario-Mega       |  0.16071% | 153    |  1.122% | 
 | 101  | Gyarados-Mega      |  0.15993% | 78     |  0.572% | 
 | 102  | Lopunny            |  0.15889% | 7      |  0.051% | 
 | 103  | Bellossom          |  0.15815% | 15     |  0.110% | 
 | 104  | Espeon             |  0.15801% | 26     |  0.191% | 
 | 105  | Bisharp            |  0.15530% | 73     |  0.536% | 
 | 106  | Mismagius          |  0.14374% | 9      |  0.066% | 
 | 107  | Rotom-Wash         |  0.13561% | 52     |  0.381% | 
 | 108  | Forretress         |  0.13296% | 40     |  0.293% | 
 | 109  | Charizard-Mega-Y   |  0.12494% | 126    |  0.924% | 
 | 110  | Scolipede          |  0.12476% | 40     |  0.293% | 
 | 111  | Serperior          |  0.11639% | 40     |  0.293% | 
 | 112  | Pachirisu          |  0.11619% | 11     |  0.081% | 
 | 113  | Hydreigon          |  0.11574% | 26     |  0.191% | 
 | 114  | Kyurem-Black       |  0.10979% | 61     |  0.447% | 
 | 115  | Arceus-Rock        |  0.10897% | 1      |  0.007% | 
 | 116  | Regigigas          |  0.10671% | 9      |  0.066% | 
 | 117  | Tangrowth          |  0.10603% | 9      |  0.066% | 
 | 118  | Hawlucha           |  0.10549% | 43     |  0.315% | 
 | 119  | Electivire         |  0.10423% | 52     |  0.381% | 
 | 120  | Blastoise-Mega     |  0.10222% | 113    |  0.829% | 
 | 121  | Torkoal            |  0.10187% | 6      |  0.044% | 
 | 122  | Ditto              |  0.10164% | 60     |  0.440% | 
 | 123  | Alakazam-Mega      |  0.10108% | 48     |  0.352% | 
 | 124  | Blaziken-Mega      |  0.10053% | 111    |  0.814% | 
 | 125  | Gastrodon          |  0.09641% | 10     |  0.073% | 
 | 126  | Rotom-Heat         |  0.08994% | 7      |  0.051% | 
 | 127  | Cofagrigus         |  0.08932% | 32     |  0.235% | 
 | 128  | Drifblim           |  0.08522% | 21     |  0.154% | 
 | 129  | Darkrai            |  0.08391% | 137    |  1.005% | 
 | 130  | Salamence-Mega     |  0.08366% | 80     |  0.587% | 
 | 131  | Kangaskhan-Mega    |  0.08015% | 90     |  0.660% | 
 | 132  | Pidgeot-Mega       |  0.07980% | 58     |  0.425% | 
 | 133  | Lickilicky         |  0.07707% | 5      |  0.037% | 
 | 134  | Mawile             |  0.07634% | 3      |  0.022% | 
 | 135  | Jynx               |  0.07444% | 7      |  0.051% | 
 | 136  | Floatzel           |  0.07412% | 19     |  0.139% | 
 | 137  | Gallade-Mega       |  0.07340% | 61     |  0.447% | 
 | 138  | Gallade            |  0.07218% | 15     |  0.110% | 
 | 139  | Tyranitar-Mega     |  0.06867% | 132    |  0.968% | 
 | 140  | Arceus-Dark        |  0.06685% | 4      |  0.029% | 
 | 141  | Umbreon            |  0.06447% | 41     |  0.301% | 
 | 142  | Dusclops           |  0.06387% | 12     |  0.088% | 
 | 143  | Glaceon            |  0.06368% | 7      |  0.051% | 
 | 144  | Rattata            |  0.06346% | 95     |  0.697% | 
 | 145  | Mamoswine          |  0.06155% | 8      |  0.059% | 
 | 146  | Haxorus            |  0.06119% | 53     |  0.389% | 
 | 147  | Blastoise          |  0.06074% | 23     |  0.169% | 
 | 148  | Tepig              |  0.06048% | 11     |  0.081% | 
 | 149  | Shaymin            |  0.06032% | 23     |  0.169% | 
 | 150  | Dusknoir           |  0.05849% | 53     |  0.389% | 
 | 151  | Heliolisk          |  0.05661% | 14     |  0.103% | 
 | 152  | Politoed           |  0.05650% | 6      |  0.044% | 
 | 153  | NidoranM           |  0.05571% | 6      |  0.044% | 
 | 154  | Lopunny-Mega       |  0.05561% | 46     |  0.337% | 
 | 155  | Abomasnow          |  0.05438% | 13     |  0.095% | 
 | 156  | Milotic            |  0.05419% | 32     |  0.235% | 
 | 157  | Landorus           |  0.05319% | 7      |  0.051% | 
 | 158  | Ninjask            |  0.05314% | 22     |  0.161% | 
 | 159  | Aegislash          |  0.05224% | 41     |  0.301% | 
 | 160  | Deoxys-Defense     |  0.05203% | 27     |  0.198% | 
 | 161  | Yveltal            |  0.05147% | 44     |  0.323% | 
 | 162  | Dugtrio            |  0.04879% | 2      |  0.015% | 
 | 163  | Aggron-Mega        |  0.04808% | 33     |  0.242% | 
 | 164  | Flareon            |  0.04730% | 9      |  0.066% | 
 | 165  | Xerneas            |  0.04637% | 69     |  0.506% | 
 | 166  | Garchomp-Mega      |  0.04505% | 109    |  0.800% | 
 | 167  | Gardevoir-Mega     |  0.04411% | 117    |  0.858% | 
 | 168  | Giratina-Origin    |  0.04274% | 39     |  0.286% | 
 | 169  | Feraligatr         |  0.04240% | 30     |  0.220% | 
 | 170  | Latias-Mega        |  0.04225% | 21     |  0.154% | 
 | 171  | Honchkrow          |  0.04173% | 11     |  0.081% | 
 | 172  | Simipour           |  0.04114% | 5      |  0.037% | 
 | 173  | Altaria-Mega       |  0.03918% | 45     |  0.330% | 
 | 174  | Mawile-Mega        |  0.03798% | 15     |  0.110% | 
 | 175  | Venomoth           |  0.03771% | 5      |  0.037% | 
 | 176  | Diancie-Mega       |  0.03760% | 143    |  1.049% | 
 | 177  | Latios-Mega        |  0.03749% | 47     |  0.345% | 
 | 178  | Yanmega            |  0.03652% | 18     |  0.132% | 
 | 179  | Clawitzer          |  0.03547% | 5      |  0.037% | 
 | 180  | Medicham-Mega      |  0.03432% | 35     |  0.257% | 
 | 181  | Togekiss           |  0.03400% | 18     |  0.132% | 
 | 182  | Aurorus            |  0.03379% | 5      |  0.037% | 
 | 183  | Scrafty            |  0.03321% | 6      |  0.044% | 
 | 184  | Eelektross         |  0.03267% | 13     |  0.095% | 
 | 185  | Volbeat            |  0.03109% | 6      |  0.044% | 
 | 186  | Parasect           |  0.03084% | 7      |  0.051% | 
 | 187  | Arceus-Ghost       |  0.03007% | 11     |  0.081% | 
 | 188  | Nidoqueen          |  0.02980% | 13     |  0.095% | 
 | 189  | Articuno           |  0.02946% | 11     |  0.081% | 
 | 190  | Jumpluff           |  0.02906% | 2      |  0.015% | 
 | 191  | Gigalith           |  0.02903% | 8      |  0.059% | 
 | 192  | Swampert-Mega      |  0.02879% | 82     |  0.602% | 
 | 193  | Shaymin-Sky        |  0.02865% | 7      |  0.051% | 
 | 194  | Sableye            |  0.02848% | 3      |  0.022% | 
 | 195  | Arceus             |  0.02831% | 38     |  0.279% | 
 | 196  | Zekrom             |  0.02662% | 30     |  0.220% | 
 | 197  | Gothitelle         |  0.02593% | 2      |  0.015% | 
 | 198  | Hippowdon          |  0.02466% | 47     |  0.345% | 
 | 199  | Claydol            |  0.02398% | 1      |  0.007% | 
 | 200  | Sylveon            |  0.02394% | 23     |  0.169% | 
 | 201  | Roserade           |  0.02316% | 4      |  0.029% | 
 | 202  | Samurott           |  0.02294% | 13     |  0.095% | 
 | 203  | Abomasnow-Mega     |  0.02280% | 18     |  0.132% | 
 | 204  | Magnezone          |  0.02198% | 14     |  0.103% | 
 | 205  | Hitmonchan         |  0.02186% | 5      |  0.037% | 
 | 206  | Machamp            |  0.02160% | 18     |  0.132% | 
 | 207  | Tentacruel         |  0.02155% | 6      |  0.044% | 
 | 208  | Pikachu            |  0.02141% | 97     |  0.712% | 
 | 209  | Lilligant          |  0.02140% | 1      |  0.007% | 
 | 210  | Arceus-Fire        |  0.02064% | 11     |  0.081% | 
 | 211  | Sigilyph           |  0.01973% | 16     |  0.117% | 
 | 212  | Bronzong           |  0.01885% | 5      |  0.037% | 
 | 213  | Absol-Mega         |  0.01885% | 20     |  0.147% | 
 | 214  | Heracross-Mega     |  0.01884% | 43     |  0.315% | 
 | 215  | Vileplume          |  0.01881% | 1      |  0.007% | 
 | 216  | Venusaur-Mega      |  0.01865% | 66     |  0.484% | 
 | 217  | Persian            |  0.01739% | 2      |  0.015% | 
 | 218  | Talonflame         |  0.01675% | 47     |  0.345% | 
 | 219  | Carnivine          |  0.01669% | 1      |  0.007% | 
 | 220  | Whimsicott         |  0.01669% | 9      |  0.066% | 
 | 221  | Camerupt-Mega      |  0.01599% | 5      |  0.037% | 
 | 222  | Shedinja           |  0.01523% | 9      |  0.066% | 
 | 223  | Durant             |  0.01511% | 12     |  0.088% | 
 | 224  | Zangoose           |  0.01510% | 2      |  0.015% | 
 | 225  | Ampharos-Mega      |  0.01417% | 26     |  0.191% | 
 | 226  | Regice             |  0.01359% | 7      |  0.051% | 
 | 227  | Electrode          |  0.01356% | 38     |  0.279% | 
 | 228  | Qwilfish           |  0.01305% | 10     |  0.073% | 
 | 229  | Simisear           |  0.01287% | 2      |  0.015% | 
 | 230  | Torterra           |  0.01283% | 23     |  0.169% | 
 | 231  | Trevenant          |  0.01274% | 28     |  0.205% | 
 | 232  | Omastar            |  0.01259% | 10     |  0.073% | 
 | 233  | Hoopa              |  0.01194% | 5      |  0.037% | 
 | 234  | Emboar             |  0.01166% | 8      |  0.059% | 
 | 235  | Granbull           |  0.01162% | 8      |  0.059% | 
 | 236  | Togetic            |  0.01140% | 2      |  0.015% | 
 | 237  | Alomomola          |  0.01137% | 5      |  0.037% | 
 | 238  | Gourgeist-Super    |  0.01067% | 22     |  0.161% | 
 | 239  | Simisage           |  0.01061% | 1      |  0.007% | 
 | 240  | Leafeon            |  0.01001% | 7      |  0.051% | 
 | 241  | Dodrio             |  0.00991% | 6      |  0.044% | 
 | 242  | Seaking            |  0.00936% | 14     |  0.103% | 
 | 243  | Unown              |  0.00919% | 1      |  0.007% | 
 | 244  | Steelix-Mega       |  0.00890% | 21     |  0.154% | 
 | 245  | Slowbro            |  0.00878% | 8      |  0.059% | 
 | 246  | Spinda             |  0.00857% | 6      |  0.044% | 
 | 247  | Drapion            |  0.00825% | 7      |  0.051% | 
 | 248  | Weezing            |  0.00824% | 8      |  0.059% | 
 | 249  | Druddigon          |  0.00771% | 6      |  0.044% | 
 | 250  | Miltank            |  0.00761% | 9      |  0.066% | 
 | 251  | Kyurem-White       |  0.00758% | 19     |  0.139% | 
 | 252  | Reuniclus          |  0.00740% | 4      |  0.029% | 
 | 253  | Lanturn            |  0.00700% | 8      |  0.059% | 
 | 254  | Swampert           |  0.00689% | 4      |  0.029% | 
 | 255  | Azumarill          |  0.00682% | 19     |  0.139% | 
 | 256  | Aerodactyl         |  0.00674% | 11     |  0.081% | 
 | 257  | Pinsir-Mega        |  0.00665% | 18     |  0.132% | 
 | 258  | Meloetta           |  0.00654% | 3      |  0.022% | 
 | 259  | Aron               |  0.00607% | 2      |  0.015% | 
 | 260  | Rotom              |  0.00594% | 1      |  0.007% | 
 | 261  | Escavalier         |  0.00575% | 9      |  0.066% | 
 | 262  | Chesnaught         |  0.00569% | 10     |  0.073% | 
 | 263  | Darmanitan         |  0.00563% | 13     |  0.095% | 
 | 264  | Golem              |  0.00547% | 14     |  0.103% | 
 | 265  | Stunfisk           |  0.00518% | 1      |  0.007% | 
 | 266  | Tyrantrum          |  0.00507% | 13     |  0.095% | 
 | 267  | Scizor-Mega        |  0.00500% | 55     |  0.403% | 
 | 268  | Sudowoodo          |  0.00488% | 2      |  0.015% | 
 | 269  | Manectric-Mega     |  0.00486% | 49     |  0.359% | 
 | 270  | Deoxys-Attack      |  0.00411% | 20     |  0.147% | 
 | 271  | Meganium           |  0.00397% | 5      |  0.037% | 
 | 272  | Ho-Oh              |  0.00392% | 32     |  0.235% | 
 | 273  | Muk                |  0.00354% | 14     |  0.103% | 
 | 274  | Lugia              |  0.00349% | 35     |  0.257% | 
 | 275  | Spiritomb          |  0.00308% | 7      |  0.051% | 
 | 276  | Linoone            |  0.00303% | 3      |  0.022% | 
 | 277  | Walrein            |  0.00292% | 1      |  0.007% | 
 | 278  | Krokorok           |  0.00291% | 1      |  0.007% | 
 | 279  | Steelix            |  0.00277% | 10     |  0.073% | 
 | 280  | Pangoro            |  0.00266% | 8      |  0.059% | 
 | 281  | Banette-Mega       |  0.00257% | 12     |  0.088% | 
 | 282  | Marowak            |  0.00248% | 15     |  0.110% | 
 | 283  | Moltres            |  0.00243% | 30     |  0.220% | 
 | 284  | Arceus-Fairy       |  0.00238% | 8      |  0.059% | 
 | 285  | Florges            |  0.00235% | 6      |  0.044% | 
 | 286  | Cinccino           |  0.00229% | 7      |  0.051% | 
 | 287  | Slaking            |  0.00204% | 8      |  0.059% | 
 | 288  | Typhlosion         |  0.00174% | 11     |  0.081% | 
 | 289  | Beedrill-Mega      |  0.00173% | 13     |  0.095% | 
 | 290  | Celebi             |  0.00147% | 8      |  0.059% | 
 | 291  | Absol              |  0.00145% | 7      |  0.051% | 
 | 292  | Starly             |  0.00140% | 14     |  0.103% | 
 | 293  | Goodra             |  0.00135% | 43     |  0.315% | 
 | 294  | Pyroar             |  0.00118% | 4      |  0.029% | 
 | 295  | Accelgor           |  0.00095% | 5      |  0.037% | 
 | 296  | Amoonguss          |  0.00093% | 13     |  0.095% | 
 | 297  | Regirock           |  0.00091% | 10     |  0.073% | 
 | 298  | Arceus-Steel       |  0.00083% | 5      |  0.037% | 
 | 299  | Raticate           |  0.00083% | 14     |  0.103% | 
 | 300  | Chikorita          |  0.00064% | 1      |  0.007% | 
 | 301  | Delphox            |  0.00063% | 23     |  0.169% | 
 | 302  | Arceus-Ice         |  0.00057% | 1      |  0.007% | 
 | 303  | Avalugg            |  0.00053% | 8      |  0.059% | 
 | 304  | Charmander         |  0.00032% | 2      |  0.015% | 
 | 305  | Crawdaunt          |  0.00029% | 9      |  0.066% | 
 | 306  | Reshiram           |  0.00028% | 10     |  0.073% | 
 | 307  | Arceus-Water       |  0.00020% | 4      |  0.029% | 
 | 308  | Gorebyss           |  0.00019% | 2      |  0.015% | 
 | 309  | Houndoom-Mega      |  0.00013% | 11     |  0.081% | 
 | 310  | Lapras             |  0.00012% | 24     |  0.176% | 
 | 311  | Audino-Mega        |  0.00007% | 12     |  0.088% | 
 | 312  | Seismitoad         |  0.00007% | 7      |  0.051% | 
 | 313  | Palkia             |  0.00006% | 12     |  0.088% | 
 | 314  | Chatot             |  0.00005% | 9      |  0.066% | 
 | 315  | Zebstrika          |  0.00005% | 3      |  0.022% | 
 | 316  | Doublade           |  0.00004% | 3      |  0.022% | 
 | 317  | Cryogonal          |  0.00003% | 1      |  0.007% | 
 | 318  | Slurpuff           |  0.00003% | 4      |  0.029% | 
 | 319  | Arceus-Dragon      |  0.00002% | 6      |  0.044% | 
 | 320  | Exploud            |  0.00001% | 4      |  0.029% | 
 | 321  | Mewtwo             |  0.00001% | 46     |  0.337% | 
 | 322  | Malamar            |  0.00001% | 3      |  0.022% | 
 | 323  | Toxicroak          |  0.00001% | 13     |  0.095% | 
 | 324  | Banette            |  0.00000% | 1      |  0.007% | 
 | 325  | Primeape           |  0.00000% | 17     |  0.125% | 
 | 326  | Castform           |  0.00000% | 1      |  0.007% | 
 | 327  | Armaldo            |  0.00000% | 5      |  0.037% | 
 | 328  | Braviary           |  0.00000% | 13     |  0.095% | 
 | 329  | Seviper            |  0.00000% | 3      |  0.022% | 
 | 330  | Aggron             |  0.00000% | 1      |  0.007% | 
 | 331  | Rotom-Fan          |  0.00000% | 1      |  0.007% | 
 | 332  | Wobbuffet          |  0.00000% | 31     |  0.227% | 
 | 333  | Porygon2           |  0.00000% | 3      |  0.022% | 
 | 334  | Slowking           |  0.00000% | 6      |  0.044% | 
 | 335  | Genesect           |  0.00000% | 7      |  0.051% | 
 | 336  | Kabutops           |  0.00000% | 8      |  0.059% | 
 | 337  | Huntail            |  0.00000% | 2      |  0.015% | 
 | 338  | Tauros             |  0.00000% | 11     |  0.081% | 
 | 339  | Braixen            |  0.00000% | 3      |  0.022% | 
 | 340  | Golduck            |  0.00000% | 1      |  0.007% | 
 | 341  | Ampharos           |  0.00000% | 3      |  0.022% | 
 | 342  | Krookodile         |  0.00000% | 12     |  0.088% | 
 | 343  | Squirtle           |  0.00000% | 1      |  0.007% | 
 | 344  | Grumpig            |  0.00000% | 2      |  0.015% | 
 | 345  | Ludicolo           |  0.00000% | 4      |  0.029% | 
 | 346  | Torchic            |  0.00000% | 2      |  0.015% | 
 | 347  | Emolga             |  0.00000% | 3      |  0.022% | 
 | 348  | Bulbasaur          |  0.00000% | 1      |  0.007% | 
 | 349  | Barbaracle         |  0.00000% | 3      |  0.022% | 
 | 350  | Fletchinder        |  0.00000% | 2      |  0.015% | 
 | 351  | Eevee              |  0.00000% | 3      |  0.022% | 
 | 352  | Poliwrath          |  0.00000% | 5      |  0.037% | 
 | 353  | Butterfree         |  0.00000% | 1      |  0.007% | 
 | 354  | Cresselia          |  0.00000% | 2      |  0.015% | 
 | 355  | Frogadier          |  0.00000% | 1      |  0.007% | 
 | 356  | Magnemite          |  0.00000% | 1      |  0.007% | 
 | 357  | Snubbull           |  0.00000% | 1      |  0.007% | 
 | 358  | Sharpedo           |  0.00000% | 4      |  0.029% | 
 | 359  | Psyduck            |  0.00000% | 1      |  0.007% | 
 | 360  | Zorua              |  0.00000% | 1      |  0.007% | 
 | 361  | Ledian             |  0.00000% | 1      |  0.007% | 
 | 362  | Doduo              |  0.00000% | 1      |  0.007% | 
 | 363  | Arceus-Electric    |  0.00000% | 2      |  0.015% | 
 | 364  | Arceus-Ground      |  0.00000% | 1      |  0.007% | 
 | 365  | Victreebel         |  0.00000% | 2      |  0.015% | 
 | 366  | Unfezant           |  0.00000% | 6      |  0.044% | 
 | 367  | Togepi             |  0.00000% | 2      |  0.015% | 
 | 368  | Leavanny           |  0.00000% | 3      |  0.022% | 
 | 369  | Octillery          |  0.00000% | 2      |  0.015% | 
 | 370  | Treecko            |  0.00000% | 1      |  0.007% | 
 | 371  | Bunnelby           |  0.00000% | 1      |  0.007% | 
 | 372  | Camerupt           |  0.00000% | 1      |  0.007% | 
 | 373  | Carracosta         |  0.00000% | 2      |  0.015% | 
 | 374  | Staryu             |  0.00000% | 1      |  0.007% | 
 | 375  | Swoobat            |  0.00000% | 1      |  0.007% | 
 | 376  | Kangaskhan         |  0.00000% | 2      |  0.015% | 
 | 377  | Magmortar          |  0.00000% | 5      |  0.037% | 
 | 378  | Gligar             |  0.00000% | 1      |  0.007% | 
 | 379  | Exeggutor          |  0.00000% | 13     |  0.095% | 
 | 380  | Altaria            |  0.00000% | 1      |  0.007% | 
 | 381  | Jigglypuff         |  0.00000% | 3      |  0.022% | 
 | 382  | Wartortle          |  0.00000% | 2      |  0.015% | 
 | 383  | Magikarp           |  0.00000% | 1      |  0.007% | 
 | 384  | Dedenne            |  0.00000% | 5      |  0.037% | 
 | 385  | Nosepass           |  0.00000% | 1      |  0.007% | 
 | 386  | Scyther            |  0.00000% | 4      |  0.029% | 
 | 387  | Quagsire           |  0.00000% | 2      |  0.015% | 
 | 388  | Axew               |  0.00000% | 1      |  0.007% | 
 | 389  | Bayleef            |  0.00000% | 1      |  0.007% | 
 | 390  | Hariyama           |  0.00000% | 4      |  0.029% | 
 | 391  | Magcargo           |  0.00000% | 1      |  0.007% | 
 | 392  | Onix               |  0.00000% | 1      |  0.007% | 
 | 393  | Rotom-Frost        |  0.00000% | 1      |  0.007% | 
 | 394  | Blitzle            |  0.00000% | 1      |  0.007% | 
 | 395  | Lumineon           |  0.00000% | 2      |  0.015% | 
 | 396  | Crustle            |  0.00000% | 6      |  0.044% | 
 | 397  | Arceus-Psychic     |  0.00000% | 13     |  0.095% | 
 | 398  | Manectric          |  0.00000% | 1      |  0.007% | 
 | 399  | Arceus-Fighting    |  0.00000% | 2      |  0.015% | 
 | 400  | Dunsparce          |  0.00000% | 2      |  0.015% | 
 | 401  | Ariados            |  0.00000% | 3      |  0.022% | 
 | 402  | Sunflora           |  0.00000% | 2      |  0.015% | 
 | 403  | Mandibuzz          |  0.00000% | 3      |  0.022% | 
 | 404  | Stoutland          |  0.00000% | 2      |  0.015% | 
 | 405  | Uxie               |  0.00000% | 12     |  0.088% | 
 | 406  | Hitmonlee          |  0.00000% | 2      |  0.015% | 
 | 407  | Musharna           |  0.00000% | 2      |  0.015% | 
 | 408  | Rhydon             |  0.00000% | 2      |  0.015% | 
 | 409  | Heatmor            |  0.00000% | 1      |  0.007% | 
 | 410  | Mantine            |  0.00000% | 2      |  0.015% | 
 | 411  | Corphish           |  0.00000% | 1      |  0.007% | 
 | 412  | Meowstic           |  0.00000% | 1      |  0.007% | 
 | 413  | Sharpedo-Mega      |  0.00000% | 10     |  0.073% | 
 | 414  | Slowbro-Mega       |  0.00000% | 16     |  0.117% | 
 | 415  | Arbok              |  0.00000% | 3      |  0.022% | 
 | 416  | Heracross          |  0.00000% | 9      |  0.066% | 
 | 417  | Virizion           |  0.00000% | 2      |  0.015% | 
 | 418  | Totodile           |  0.00000% | 1      |  0.007% | 
 | 419  | Hypno              |  0.00000% | 6      |  0.044% | 
 | 420  | Porygon            |  0.00000% | 1      |  0.007% | 
 | 421  | Chespin            |  0.00000% | 1      |  0.007% | 
 | 422  | Golurk             |  0.00000% | 9      |  0.066% | 
 | 423  | Tropius            |  0.00000% | 3      |  0.022% | 
 | 424  | Jellicent          |  0.00000% | 6      |  0.044% | 
 | 425  | Honedge            |  0.00000% | 1      |  0.007% | 
 | 426  | Gogoat             |  0.00000% | 2      |  0.015% | 
 | 427  | Houndoom           |  0.00000% | 1      |  0.007% | 
 | 428  | Grimer             |  0.00000% | 2      |  0.015% | 
 | 429  | Manaphy            |  0.00000% | 3      |  0.022% | 
 | 430  | Goomy              |  0.00000% | 1      |  0.007% | 
 | 431  | Mightyena          |  0.00000% | 2      |  0.015% | 
 | 432  | Dragonair          |  0.00000% | 1      |  0.007% | 
 | 433  | Diggersby          |  0.00000% | 13     |  0.095% | 
 | 434  | Latias             |  0.00000% | 5      |  0.037% | 
 | 435  | Lucario            |  0.00000% | 8      |  0.059% | 
 | 436  | Charmeleon         |  0.00000% | 1      |  0.007% | 
 | 437  | Vullaby            |  0.00000% | 1      |  0.007% | 
 | 438  | Deoxys             |  0.00000% | 1      |  0.007% | 
 | 439  | Oshawott           |  0.00000% | 1      |  0.007% | 
 | 440  | Giratina           |  0.00000% | 4      |  0.029% | 
 | 441  | Beartic            |  0.00000% | 1      |  0.007% | 
 | 442  | Arceus-Flying      |  0.00000% | 3      |  0.022% | 
 | 443  | Garbodor           |  0.00000% | 13     |  0.095% | 
 | 444  | Graveler           |  0.00000% | 2      |  0.015% | 
 | 445  | Quilladin          |  0.00000% | 1      |  0.007% | 
 | 446  | Beautifly          |  0.00000% | 3      |  0.022% | 
 | 447  | Pelipper           |  0.00000% | 1      |  0.007% | 
 | 448  | Mr. Mime           |  0.00000% | 4      |  0.029% | 
 | 449  | Wailord            |  0.00000% | 6      |  0.044% | 
 | 450  | Pidgey             |  0.00000% | 1      |  0.007% | 
 | 451  | Ursaring           |  0.00000% | 5      |  0.037% | 
 | 452  | Kingler            |  0.00000% | 7      |  0.051% | 
 | 453  | Aerodactyl-Mega    |  0.00000% | 24     |  0.176% | 
 | 454  | Rapidash           |  0.00000% | 2      |  0.015% | 
 | 455  | Krabby             |  0.00000% | 1      |  0.007% | 
 | 456  | Donphan            |  0.00000% | 1      |  0.007% | 
 | 457  | Weedle             |  0.00000% | 3      |  0.022% | 
 | 458  | Kricketune         |  0.00000% | 1      |  0.007% | 
 | 459  | Dewgong            |  0.00000% | 6      |  0.044% | 
 | 460  | Arceus-Grass       |  0.00000% | 4      |  0.029% | 
 | 461  | Hitmontop          |  0.00000% | 7      |  0.051% | 
 | 462  | Kingdra            |  0.00000% | 5      |  0.037% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
