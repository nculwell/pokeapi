 Total leads: 1260
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Slaking            | 28.60234% | 72     |  5.714% | 
 | 2    | Deoxys-Attack      | 22.69896% | 167    | 13.254% | 
 | 3    | Deoxys-Defense     | 18.66802% | 110    |  8.730% | 
 | 4    | Mewtwo             |  8.55727% | 93     |  7.381% | 
 | 5    | Groudon            |  6.59930% | 80     |  6.349% | 
 | 6    | Metagross          |  5.24785% | 16     |  1.270% | 
 | 7    | Latios             |  5.02068% | 47     |  3.730% | 
 | 8    | Rayquaza           |  1.57651% | 64     |  5.079% | 
 | 9    | Latias             |  1.30779% | 29     |  2.302% | 
 | 10   | Blissey            |  1.26024% | 15     |  1.190% | 
 | 11   | Forretress         |  0.24941% | 13     |  1.032% | 
 | 12   | Ho-Oh              |  0.12717% | 44     |  3.492% | 
 | 13   | Deoxys-Speed       |  0.06869% | 24     |  1.905% | 
 | 14   | Gengar             |  0.00588% | 20     |  1.587% | 
 | 15   | Ninjask            |  0.00244% | 35     |  2.778% | 
 | 16   | Venusaur           |  0.00203% | 7      |  0.556% | 
 | 17   | Pidgeot            |  0.00174% | 8      |  0.635% | 
 | 18   | Kyogre             |  0.00121% | 43     |  3.413% | 
 | 19   | Crobat             |  0.00120% | 16     |  1.270% | 
 | 20   | Salamence          |  0.00070% | 18     |  1.429% | 
 | 21   | Swampert           |  0.00058% | 12     |  0.952% | 
 | 22   | Chinchou           |  0.00001% | 3      |  0.238% | 
 | 23   | Primeape           |  0.00000% | 2      |  0.159% | 
 | 24   | Pikachu            |  0.00000% | 5      |  0.397% | 
 | 25   | Mudkip             |  0.00000% | 2      |  0.159% | 
 | 26   | Sandslash          |  0.00000% | 2      |  0.159% | 
 | 27   | Kangaskhan         |  0.00000% | 1      |  0.079% | 
 | 28   | Wynaut             |  0.00000% | 4      |  0.317% | 
 | 29   | Wobbuffet          |  0.00000% | 27     |  2.143% | 
 | 30   | Blaziken           |  0.00000% | 18     |  1.429% | 
 | 31   | Rhydon             |  0.00000% | 1      |  0.079% | 
 | 32   | Exeggutor          |  0.00000% | 1      |  0.079% | 
 | 33   | Absol              |  0.00000% | 1      |  0.079% | 
 | 34   | Milotic            |  0.00000% | 5      |  0.397% | 
 | 35   | Sceptile           |  0.00000% | 25     |  1.984% | 
 | 36   | Aerodactyl         |  0.00000% | 6      |  0.476% | 
 | 37   | Lapras             |  0.00000% | 4      |  0.317% | 
 | 38   | Deoxys             |  0.00000% | 14     |  1.111% | 
 | 39   | Snorlax            |  0.00000% | 5      |  0.397% | 
 | 40   | Celebi             |  0.00000% | 3      |  0.238% | 
 | 41   | Ampharos           |  0.00000% | 3      |  0.238% | 
 | 42   | Omastar            |  0.00000% | 2      |  0.159% | 
 | 43   | Articuno           |  0.00000% | 3      |  0.238% | 
 | 44   | Blastoise          |  0.00000% | 4      |  0.317% | 
 | 45   | Cloyster           |  0.00000% | 1      |  0.079% | 
 | 46   | Electrike          |  0.00000% | 3      |  0.238% | 
 | 47   | Hitmonlee          |  0.00000% | 3      |  0.238% | 
 | 48   | Wigglytuff         |  0.00000% | 1      |  0.079% | 
 | 49   | Magikarp           |  0.00000% | 1      |  0.079% | 
 | 50   | Mew                |  0.00000% | 13     |  1.032% | 
 | 51   | Gardevoir          |  0.00000% | 12     |  0.952% | 
 | 52   | Lanturn            |  0.00000% | 1      |  0.079% | 
 | 53   | Jolteon            |  0.00000% | 1      |  0.079% | 
 | 54   | Umbreon            |  0.00000% | 13     |  1.032% | 
 | 55   | Alakazam           |  0.00000% | 5      |  0.397% | 
 | 56   | Feraligatr         |  0.00000% | 1      |  0.079% | 
 | 57   | Dragonite          |  0.00000% | 6      |  0.476% | 
 | 58   | Gyarados           |  0.00000% | 2      |  0.159% | 
 | 59   | Vaporeon           |  0.00000% | 1      |  0.079% | 
 | 60   | Dustox             |  0.00000% | 1      |  0.079% | 
 | 61   | Lickitung          |  0.00000% | 1      |  0.079% | 
 | 62   | Hariyama           |  0.00000% | 1      |  0.079% | 
 | 63   | Slowking           |  0.00000% | 2      |  0.159% | 
 | 64   | Yanma              |  0.00000% | 1      |  0.079% | 
 | 65   | Smeargle           |  0.00000% | 3      |  0.238% | 
 | 66   | Jirachi            |  0.00000% | 2      |  0.159% | 
 | 67   | Regice             |  0.00000% | 1      |  0.079% | 
 | 68   | Chimecho           |  0.00000% | 1      |  0.079% | 
 | 69   | Typhlosion         |  0.00000% | 2      |  0.159% | 
 | 70   | Suicune            |  0.00000% | 3      |  0.238% | 
 | 71   | Scizor             |  0.00000% | 3      |  0.238% | 
 | 72   | Donphan            |  0.00000% | 3      |  0.238% | 
 | 73   | Zapdos             |  0.00000% | 1      |  0.079% | 
 | 74   | Flareon            |  0.00000% | 2      |  0.159% | 
 | 75   | Tyranitar          |  0.00000% | 15     |  1.190% | 
 | 76   | Xatu               |  0.00000% | 1      |  0.079% | 
 | 77   | Charizard          |  0.00000% | 22     |  1.746% | 
 | 78   | Skarmory           |  0.00000% | 3      |  0.238% | 
 | 79   | Shiftry            |  0.00000% | 2      |  0.159% | 
 | 80   | Flygon             |  0.00000% | 8      |  0.635% | 
 | 81   | Entei              |  0.00000% | 1      |  0.079% | 
 | 82   | Steelix            |  0.00000% | 1      |  0.079% | 
 | 83   | Tentacruel         |  0.00000% | 1      |  0.079% | 
 | 84   | Houndoom           |  0.00000% | 1      |  0.079% | 
 | 85   | Cubone             |  0.00000% | 3      |  0.238% | 
 | 86   | Lugia              |  0.00000% | 31     |  2.460% | 
 | 87   | Onix               |  0.00000% | 2      |  0.159% | 
 | 88   | Camerupt           |  0.00000% | 1      |  0.079% | 
 | 89   | Magneton           |  0.00000% | 1      |  0.079% | 
 | 90   | Sneasel            |  0.00000% | 2      |  0.159% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
