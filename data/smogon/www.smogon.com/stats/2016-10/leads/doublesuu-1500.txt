 Total leads: 43752
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Hitmontop          |  1.84012% | 608    |  1.390% | 
 | 2    | Ambipom            |  1.79220% | 781    |  1.785% | 
 | 3    | Arcanine           |  1.76734% | 734    |  1.678% | 
 | 4    | Raichu             |  1.67357% | 611    |  1.397% | 
 | 5    | Sableye            |  1.55887% | 595    |  1.360% | 
 | 6    | Cofagrigus         |  1.48627% | 606    |  1.385% | 
 | 7    | Ninetales          |  1.45736% | 607    |  1.387% | 
 | 8    | Sceptile-Mega      |  1.40196% | 575    |  1.314% | 
 | 9    | Crobat             |  1.35812% | 524    |  1.198% | 
 | 10   | Krookodile         |  1.27683% | 502    |  1.147% | 
 | 11   | Blastoise-Mega     |  1.24661% | 464    |  1.061% | 
 | 12   | Bronzong           |  1.23151% | 476    |  1.088% | 
 | 13   | Volcarona          |  1.17609% | 379    |  0.866% | 
 | 14   | Infernape          |  1.06601% | 480    |  1.097% | 
 | 15   | Aerodactyl-Mega    |  1.01196% | 400    |  0.914% | 
 | 16   | Audino-Mega        |  0.98499% | 538    |  1.230% | 
 | 17   | Hariyama           |  0.98203% | 338    |  0.773% | 
 | 18   | Shuckle            |  0.95787% | 460    |  1.051% | 
 | 19   | Spinda             |  0.89307% | 405    |  0.926% | 
 | 20   | Camerupt-Mega      |  0.83202% | 309    |  0.706% | 
 | 21   | Manectric          |  0.80937% | 356    |  0.814% | 
 | 22   | Snorlax            |  0.77730% | 360    |  0.823% | 
 | 23   | Lopunny-Mega       |  0.74101% | 327    |  0.747% | 
 | 24   | Jellicent          |  0.73780% | 247    |  0.565% | 
 | 25   | Chandelure         |  0.73457% | 293    |  0.670% | 
 | 26   | Aromatisse         |  0.73047% | 264    |  0.603% | 
 | 27   | Tauros             |  0.72264% | 354    |  0.809% | 
 | 28   | Genesect           |  0.72175% | 251    |  0.574% | 
 | 29   | Heliolisk          |  0.72014% | 367    |  0.839% | 
 | 30   | Slaking            |  0.71852% | 385    |  0.880% | 
 | 31   | Froslass           |  0.71170% | 351    |  0.802% | 
 | 32   | Galvantula         |  0.70525% | 309    |  0.706% | 
 | 33   | Regigigas          |  0.68491% | 357    |  0.816% | 
 | 34   | Meowstic           |  0.68270% | 293    |  0.670% | 
 | 35   | Jolteon            |  0.66220% | 283    |  0.647% | 
 | 36   | Abomasnow-Mega     |  0.65502% | 262    |  0.599% | 
 | 37   | Porygon2           |  0.63345% | 234    |  0.535% | 
 | 38   | Vaporeon           |  0.62660% | 307    |  0.702% | 
 | 39   | Liepard            |  0.62318% | 288    |  0.658% | 
 | 40   | Togetic            |  0.62097% | 244    |  0.558% | 
 | 41   | Sableye-Mega       |  0.60897% | 218    |  0.498% | 
 | 42   | Espeon             |  0.60783% | 296    |  0.677% | 
 | 43   | Tornadus           |  0.60576% | 229    |  0.523% | 
 | 44   | Machamp            |  0.60473% | 238    |  0.544% | 
 | 45   | Aerodactyl         |  0.59064% | 233    |  0.533% | 
 | 46   | Mew                |  0.57307% | 260    |  0.594% | 
 | 47   | Smeargle           |  0.54547% | 257    |  0.587% | 
 | 48   | Kecleon            |  0.52740% | 201    |  0.459% | 
 | 49   | Diancie            |  0.52652% | 194    |  0.443% | 
 | 50   | Hippowdon          |  0.52052% | 198    |  0.453% | 
 | 51   | Klefki             |  0.51885% | 222    |  0.507% | 
 | 52   | Electivire         |  0.49602% | 245    |  0.560% | 
 | 53   | Lanturn            |  0.49217% | 236    |  0.539% | 
 | 54   | Raikou             |  0.48951% | 161    |  0.368% | 
 | 55   | Clefable           |  0.48911% | 223    |  0.510% | 
 | 56   | Reuniclus          |  0.47509% | 194    |  0.443% | 
 | 57   | Swoobat            |  0.47196% | 244    |  0.558% | 
 | 58   | Victini            |  0.47047% | 190    |  0.434% | 
 | 59   | Dusclops           |  0.45976% | 160    |  0.366% | 
 | 60   | Mienshao           |  0.45973% | 195    |  0.446% | 
 | 61   | Swampert-Mega      |  0.44367% | 208    |  0.475% | 
 | 62   | Roserade           |  0.44362% | 168    |  0.384% | 
 | 63   | Blastoise          |  0.43922% | 192    |  0.439% | 
 | 64   | Goodra             |  0.43901% | 170    |  0.389% | 
 | 65   | Slowking           |  0.43259% | 162    |  0.370% | 
 | 66   | Shedinja           |  0.42070% | 253    |  0.578% | 
 | 67   | Murkrow            |  0.41926% | 188    |  0.430% | 
 | 68   | Serperior          |  0.41227% | 205    |  0.469% | 
 | 69   | Staraptor          |  0.41108% | 170    |  0.389% | 
 | 70   | Drapion            |  0.40656% | 204    |  0.466% | 
 | 71   | Pidgeot-Mega       |  0.40514% | 159    |  0.363% | 
 | 72   | Cloyster           |  0.40102% | 193    |  0.441% | 
 | 73   | Gothitelle         |  0.39706% | 174    |  0.398% | 
 | 74   | Mr. Mime           |  0.39672% | 165    |  0.377% | 
 | 75   | Lucario            |  0.39008% | 195    |  0.446% | 
 | 76   | Cobalion           |  0.37760% | 148    |  0.338% | 
 | 77   | Dusknoir           |  0.37711% | 168    |  0.384% | 
 | 78   | Salamence          |  0.37698% | 141    |  0.322% | 
 | 79   | Cradily            |  0.37171% | 149    |  0.341% | 
 | 80   | Shiftry            |  0.37143% | 140    |  0.320% | 
 | 81   | Volbeat            |  0.36936% | 140    |  0.320% | 
 | 82   | Accelgor           |  0.36698% | 162    |  0.370% | 
 | 83   | Aggron-Mega        |  0.36588% | 179    |  0.409% | 
 | 84   | Tentacruel         |  0.36061% | 156    |  0.357% | 
 | 85   | Altaria-Mega       |  0.36001% | 148    |  0.338% | 
 | 86   | Gardevoir          |  0.35666% | 157    |  0.359% | 
 | 87   | Nidoking           |  0.35003% | 162    |  0.370% | 
 | 88   | Xatu               |  0.34937% | 152    |  0.347% | 
 | 89   | Gastrodon          |  0.34605% | 153    |  0.350% | 
 | 90   | Crawdaunt          |  0.34429% | 133    |  0.304% | 
 | 91   | Florges            |  0.34391% | 166    |  0.379% | 
 | 92   | Porygon-Z          |  0.34026% | 156    |  0.357% | 
 | 93   | Excadrill          |  0.33962% | 115    |  0.263% | 
 | 94   | Noivern            |  0.33687% | 163    |  0.373% | 
 | 95   | Kingdra            |  0.33594% | 138    |  0.315% | 
 | 96   | Dugtrio            |  0.33487% | 155    |  0.354% | 
 | 97   | Seaking            |  0.33104% | 180    |  0.411% | 
 | 98   | Trevenant          |  0.32916% | 147    |  0.336% | 
 | 99   | Typhlosion         |  0.32893% | 123    |  0.281% | 
 | 100  | Rotom-Heat         |  0.32417% | 128    |  0.293% | 
 | 101  | Aurorus            |  0.32402% | 137    |  0.313% | 
 | 102  | Banette-Mega       |  0.32361% | 148    |  0.338% | 
 | 103  | Marowak            |  0.31960% | 137    |  0.313% | 
 | 104  | Pachirisu          |  0.31945% | 143    |  0.327% | 
 | 105  | Malamar            |  0.31921% | 151    |  0.345% | 
 | 106  | Forretress         |  0.30939% | 162    |  0.370% | 
 | 107  | Hitmonlee          |  0.30858% | 115    |  0.263% | 
 | 108  | Escavalier         |  0.30706% | 110    |  0.251% | 
 | 109  | Plusle             |  0.30544% | 170    |  0.389% | 
 | 110  | Gallade-Mega       |  0.30275% | 118    |  0.270% | 
 | 111  | Empoleon           |  0.29884% | 138    |  0.315% | 
 | 112  | Eelektross         |  0.29862% | 133    |  0.304% | 
 | 113  | Feraligatr         |  0.29490% | 126    |  0.288% | 
 | 114  | Absol-Mega         |  0.29391% | 149    |  0.341% | 
 | 115  | Manectric-Mega     |  0.28945% | 126    |  0.288% | 
 | 116  | Metagross-Mega     |  0.28808% | 113    |  0.258% | 
 | 117  | Blissey            |  0.28380% | 159    |  0.363% | 
 | 118  | Ampharos-Mega      |  0.28186% | 139    |  0.318% | 
 | 119  | Metagross          |  0.27807% | 125    |  0.286% | 
 | 120  | Toxicroak          |  0.27587% | 145    |  0.331% | 
 | 121  | Jynx               |  0.27134% | 107    |  0.245% | 
 | 122  | Beedrill-Mega      |  0.27113% | 134    |  0.306% | 
 | 123  | Sigilyph           |  0.26797% | 129    |  0.295% | 
 | 124  | Archeops           |  0.26696% | 130    |  0.297% | 
 | 125  | Hawlucha           |  0.26219% | 127    |  0.290% | 
 | 126  | Mismagius          |  0.25858% | 104    |  0.238% | 
 | 127  | Mamoswine          |  0.25823% | 112    |  0.256% | 
 | 128  | Alakazam           |  0.25792% | 117    |  0.267% | 
 | 129  | Alakazam-Mega      |  0.25281% | 102    |  0.233% | 
 | 130  | Cinccino           |  0.25077% | 113    |  0.258% | 
 | 131  | Latias             |  0.24891% | 104    |  0.238% | 
 | 132  | Jumpluff           |  0.24884% | 99     |  0.226% | 
 | 133  | Lucario-Mega       |  0.24880% | 123    |  0.281% | 
 | 134  | Stoutland          |  0.24837% | 92     |  0.210% | 
 | 135  | Blaziken           |  0.24229% | 94     |  0.215% | 
 | 136  | Exeggutor          |  0.24221% | 112    |  0.256% | 
 | 137  | Lapras             |  0.24077% | 113    |  0.258% | 
 | 138  | Lilligant          |  0.24048% | 99     |  0.226% | 
 | 139  | Musharna           |  0.24045% | 97     |  0.222% | 
 | 140  | Rhyperior          |  0.23692% | 97     |  0.222% | 
 | 141  | Blaziken-Mega      |  0.23577% | 129    |  0.295% | 
 | 142  | Exploud            |  0.23131% | 101    |  0.231% | 
 | 143  | Manaphy            |  0.22594% | 81     |  0.185% | 
 | 144  | Electrode          |  0.22255% | 87     |  0.199% | 
 | 145  | Charizard-Mega-X   |  0.21868% | 115    |  0.263% | 
 | 146  | Entei              |  0.21769% | 81     |  0.185% | 
 | 147  | Arbok              |  0.21751% | 101    |  0.231% | 
 | 148  | Gourgeist-Super    |  0.21682% | 92     |  0.210% | 
 | 149  | Houndoom-Mega      |  0.21397% | 86     |  0.197% | 
 | 150  | Carbink            |  0.21388% | 136    |  0.311% | 
 | 151  | Luxray             |  0.21312% | 92     |  0.210% | 
 | 152  | Gliscor            |  0.21294% | 111    |  0.254% | 
 | 153  | Deoxys-Attack      |  0.21103% | 82     |  0.187% | 
 | 154  | Kyurem             |  0.21019% | 76     |  0.174% | 
 | 155  | Victreebel         |  0.21011% | 75     |  0.171% | 
 | 156  | Medicham-Mega      |  0.21006% | 78     |  0.178% | 
 | 157  | Chesnaught         |  0.20709% | 92     |  0.210% | 
 | 158  | Clefairy           |  0.20563% | 97     |  0.222% | 
 | 159  | Audino             |  0.20223% | 129    |  0.295% | 
 | 160  | Darkrai            |  0.20076% | 78     |  0.178% | 
 | 161  | Golem              |  0.19609% | 85     |  0.194% | 
 | 162  | Articuno           |  0.19316% | 95     |  0.217% | 
 | 163  | Drifblim           |  0.19211% | 110    |  0.251% | 
 | 164  | Leafeon            |  0.18727% | 90     |  0.206% | 
 | 165  | Smoochum           |  0.18568% | 91     |  0.208% | 
 | 166  | Braviary           |  0.18549% | 88     |  0.201% | 
 | 167  | Darmanitan         |  0.18454% | 90     |  0.206% | 
 | 168  | Wobbuffet          |  0.18010% | 81     |  0.185% | 
 | 169  | Swampert           |  0.17712% | 70     |  0.160% | 
 | 170  | Flygon             |  0.17688% | 86     |  0.197% | 
 | 171  | Chatot             |  0.17672% | 74     |  0.169% | 
 | 172  | Sceptile           |  0.17464% | 97     |  0.222% | 
 | 173  | Primeape           |  0.17443% | 74     |  0.169% | 
 | 174  | Magnezone          |  0.17425% | 71     |  0.162% | 
 | 175  | Linoone            |  0.17410% | 105    |  0.240% | 
 | 176  | Golduck            |  0.16457% | 89     |  0.203% | 
 | 177  | Vileplume          |  0.16371% | 58     |  0.133% | 
 | 178  | Gallade            |  0.16282% | 57     |  0.130% | 
 | 179  | Abomasnow          |  0.16265% | 74     |  0.169% | 
 | 180  | Rhydon             |  0.15891% | 67     |  0.153% | 
 | 181  | Durant             |  0.15540% | 74     |  0.169% | 
 | 182  | Claydol            |  0.15519% | 66     |  0.151% | 
 | 183  | Ninjask            |  0.15168% | 75     |  0.171% | 
 | 184  | Donphan            |  0.15111% | 69     |  0.158% | 
 | 185  | Azelf              |  0.14968% | 69     |  0.158% | 
 | 186  | Nidoqueen          |  0.14949% | 63     |  0.144% | 
 | 187  | Hoopa              |  0.14762% | 52     |  0.119% | 
 | 188  | Tangrowth          |  0.14497% | 54     |  0.123% | 
 | 189  | Lickilicky         |  0.14385% | 66     |  0.151% | 
 | 190  | Minun              |  0.14358% | 90     |  0.206% | 
 | 191  | Illumise           |  0.14290% | 67     |  0.153% | 
 | 192  | Sharpedo           |  0.14193% | 60     |  0.137% | 
 | 193  | Umbreon            |  0.14183% | 97     |  0.222% | 
 | 194  | Masquerain         |  0.14034% | 59     |  0.135% | 
 | 195  | Slurpuff           |  0.13975% | 75     |  0.171% | 
 | 196  | Slowbro-Mega       |  0.13358% | 61     |  0.139% | 
 | 197  | Zebstrika          |  0.13252% | 83     |  0.190% | 
 | 198  | Chansey            |  0.13251% | 70     |  0.160% | 
 | 199  | Ampharos           |  0.13118% | 64     |  0.146% | 
 | 200  | Honchkrow          |  0.13114% | 76     |  0.174% | 
 | 201  | Tangela            |  0.13050% | 58     |  0.133% | 
 | 202  | Dedenne            |  0.13044% | 77     |  0.176% | 
 | 203  | Haxorus            |  0.12975% | 55     |  0.126% | 
 | 204  | Sudowoodo          |  0.12812% | 80     |  0.183% | 
 | 205  | Deoxys-Defense     |  0.12714% | 62     |  0.142% | 
 | 206  | Parasect           |  0.12658% | 63     |  0.144% | 
 | 207  | Slowbro            |  0.12604% | 62     |  0.142% | 
 | 208  | Alomomola          |  0.12529% | 64     |  0.146% | 
 | 209  | Vivillon           |  0.12506% | 53     |  0.121% | 
 | 210  | Pyroar             |  0.12446% | 62     |  0.142% | 
 | 211  | Medicham           |  0.12361% | 60     |  0.137% | 
 | 212  | Mantine            |  0.12064% | 57     |  0.130% | 
 | 213  | Skarmory           |  0.11911% | 56     |  0.128% | 
 | 214  | Cherrim            |  0.11795% | 58     |  0.133% | 
 | 215  | Aggron             |  0.11754% | 61     |  0.139% | 
 | 216  | Emolga             |  0.11642% | 53     |  0.121% | 
 | 217  | Delcatty           |  0.11492% | 50     |  0.114% | 
 | 218  | Starmie            |  0.11489% | 59     |  0.135% | 
 | 219  | Tornadus-Therian   |  0.11480% | 44     |  0.101% | 
 | 220  | Tyrantrum          |  0.11393% | 53     |  0.121% | 
 | 221  | Ariados            |  0.11280% | 58     |  0.133% | 
 | 222  | Glaceon            |  0.11261% | 63     |  0.144% | 
 | 223  | Delphox            |  0.11244% | 50     |  0.114% | 
 | 224  | Bastiodon          |  0.11231% | 58     |  0.133% | 
 | 225  | Thundurus-Therian  |  0.10987% | 41     |  0.094% | 
 | 226  | Virizion           |  0.10765% | 42     |  0.096% | 
 | 227  | Meganium           |  0.10609% | 40     |  0.091% | 
 | 228  | Qwilfish           |  0.10569% | 40     |  0.091% | 
 | 229  | Kabutops           |  0.10547% | 44     |  0.101% | 
 | 230  | Granbull           |  0.10513% | 48     |  0.110% | 
 | 231  | Cryogonal          |  0.10333% | 46     |  0.105% | 
 | 232  | Seismitoad         |  0.10257% | 51     |  0.117% | 
 | 233  | Celebi             |  0.10159% | 49     |  0.112% | 
 | 234  | Dragalge           |  0.10065% | 62     |  0.142% | 
 | 235  | Charizard          |  0.10047% | 56     |  0.128% | 
 | 236  | Barbaracle         |  0.09987% | 34     |  0.078% | 
 | 237  | Weezing            |  0.09853% | 59     |  0.135% | 
 | 238  | Floatzel           |  0.09841% | 71     |  0.162% | 
 | 239  | Seviper            |  0.09814% | 53     |  0.121% | 
 | 240  | Swellow            |  0.09793% | 49     |  0.112% | 
 | 241  | Magcargo           |  0.09790% | 38     |  0.087% | 
 | 242  | Bouffalant         |  0.09683% | 54     |  0.123% | 
 | 243  | Hitmonchan         |  0.09608% | 31     |  0.071% | 
 | 244  | Meloetta           |  0.09449% | 42     |  0.096% | 
 | 245  | Garbodor           |  0.09307% | 32     |  0.073% | 
 | 246  | Uxie               |  0.09190% | 46     |  0.105% | 
 | 247  | Purugly            |  0.09044% | 51     |  0.117% | 
 | 248  | Deoxys-Speed       |  0.08990% | 37     |  0.085% | 
 | 249  | Magneton           |  0.08858% | 38     |  0.087% | 
 | 250  | Cacturne           |  0.08687% | 44     |  0.101% | 
 | 251  | Scolipede          |  0.08645% | 53     |  0.121% | 
 | 252  | Delibird           |  0.08487% | 47     |  0.107% | 
 | 253  | Magmortar          |  0.08378% | 46     |  0.105% | 
 | 254  | Pinsir-Mega        |  0.08348% | 33     |  0.075% | 
 | 255  | Camerupt           |  0.08340% | 50     |  0.114% | 
 | 256  | Avalugg            |  0.08258% | 38     |  0.087% | 
 | 257  | Heracross          |  0.08173% | 38     |  0.087% | 
 | 258  | Pikachu            |  0.08060% | 51     |  0.117% | 
 | 259  | Hippopotas         |  0.07978% | 25     |  0.057% | 
 | 260  | Wigglytuff         |  0.07923% | 39     |  0.089% | 
 | 261  | Ditto              |  0.07878% | 37     |  0.085% | 
 | 262  | Heracross-Mega     |  0.07836% | 46     |  0.105% | 
 | 263  | Eevee              |  0.07824% | 40     |  0.091% | 
 | 264  | Persian            |  0.07782% | 36     |  0.082% | 
 | 265  | Steelix-Mega       |  0.07678% | 40     |  0.091% | 
 | 266  | Yanmega            |  0.07676% | 33     |  0.075% | 
 | 267  | Sharpedo-Mega      |  0.07641% | 40     |  0.091% | 
 | 268  | Spiritomb          |  0.07617% | 34     |  0.078% | 
 | 269  | Clawitzer          |  0.07560% | 39     |  0.089% | 
 | 270  | Aipom              |  0.07516% | 22     |  0.050% | 
 | 271  | Poliwrath          |  0.07477% | 50     |  0.114% | 
 | 272  | Throh              |  0.07289% | 31     |  0.071% | 
 | 273  | Miltank            |  0.07271% | 38     |  0.087% | 
 | 274  | Butterfree         |  0.07258% | 26     |  0.059% | 
 | 275  | Sneasel            |  0.07256% | 29     |  0.066% | 
 | 276  | Mandibuzz          |  0.07203% | 33     |  0.075% | 
 | 277  | Aron               |  0.07194% | 31     |  0.071% | 
 | 278  | Druddigon          |  0.07184% | 28     |  0.064% | 
 | 279  | Grumpig            |  0.07108% | 30     |  0.069% | 
 | 280  | Torterra           |  0.06996% | 34     |  0.078% | 
 | 281  | Absol              |  0.06987% | 39     |  0.089% | 
 | 282  | Farfetch'd         |  0.06892% | 32     |  0.073% | 
 | 283  | Cottonee           |  0.06866% | 42     |  0.096% | 
 | 284  | Crustle            |  0.06762% | 35     |  0.080% | 
 | 285  | Hypno              |  0.06726% | 27     |  0.062% | 
 | 286  | Muk                |  0.06722% | 37     |  0.085% | 
 | 287  | Klinklang          |  0.06699% | 43     |  0.098% | 
 | 288  | Doublade           |  0.06628% | 33     |  0.075% | 
 | 289  | Golurk             |  0.06608% | 34     |  0.078% | 
 | 290  | Girafarig          |  0.06588% | 45     |  0.103% | 
 | 291  | Ursaring           |  0.06558% | 35     |  0.080% | 
 | 292  | Furret             |  0.06482% | 33     |  0.075% | 
 | 293  | Beheeyem           |  0.06462% | 35     |  0.080% | 
 | 294  | Swanna             |  0.06391% | 25     |  0.057% | 
 | 295  | Pangoro            |  0.06379% | 32     |  0.073% | 
 | 296  | Lopunny            |  0.06356% | 33     |  0.075% | 
 | 297  | Flareon            |  0.06342% | 28     |  0.064% | 
 | 298  | Quagsire           |  0.06265% | 41     |  0.094% | 
 | 299  | Golbat             |  0.06246% | 27     |  0.062% | 
 | 300  | Armaldo            |  0.06192% | 31     |  0.071% | 
 | 301  | Gourgeist          |  0.06048% | 21     |  0.048% | 
 | 302  | Dodrio             |  0.05856% | 30     |  0.069% | 
 | 303  | Rotom              |  0.05830% | 20     |  0.046% | 
 | 304  | Glalie-Mega        |  0.05780% | 28     |  0.064% | 
 | 305  | Omastar            |  0.05740% | 29     |  0.066% | 
 | 306  | Unfezant           |  0.05600% | 32     |  0.073% | 
 | 307  | Zangoose           |  0.05567% | 47     |  0.107% | 
 | 308  | Rotom-Mow          |  0.05478% | 19     |  0.043% | 
 | 309  | Wailord            |  0.05408% | 25     |  0.057% | 
 | 310  | Gorebyss           |  0.05318% | 28     |  0.064% | 
 | 311  | Latias-Mega        |  0.05310% | 19     |  0.043% | 
 | 312  | Mesprit            |  0.05298% | 25     |  0.057% | 
 | 313  | Solrock            |  0.05236% | 21     |  0.048% | 
 | 314  | Scyther            |  0.05188% | 30     |  0.069% | 
 | 315  | Diggersby          |  0.05159% | 22     |  0.050% | 
 | 316  | Chinchou           |  0.05047% | 21     |  0.048% | 
 | 317  | Gogoat             |  0.05005% | 25     |  0.057% | 
 | 318  | Leavanny           |  0.04995% | 32     |  0.073% | 
 | 319  | Beartic            |  0.04948% | 32     |  0.073% | 
 | 320  | Rampardos          |  0.04881% | 26     |  0.059% | 
 | 321  | Sandslash          |  0.04849% | 33     |  0.075% | 
 | 322  | Walrein            |  0.04782% | 23     |  0.053% | 
 | 323  | Glalie             |  0.04776% | 20     |  0.046% | 
 | 324  | Maractus           |  0.04755% | 23     |  0.053% | 
 | 325  | Venomoth           |  0.04722% | 28     |  0.064% | 
 | 326  | Probopass          |  0.04694% | 26     |  0.059% | 
 | 327  | Chimecho           |  0.04664% | 23     |  0.053% | 
 | 328  | Purrloin           |  0.04456% | 22     |  0.050% | 
 | 329  | Zygarde            |  0.04454% | 18     |  0.041% | 
 | 330  | Emboar             |  0.04319% | 22     |  0.050% | 
 | 331  | Houndoom           |  0.04271% | 16     |  0.037% | 
 | 332  | Solosis            |  0.03937% | 12     |  0.027% | 
 | 333  | Swalot             |  0.03854% | 31     |  0.071% | 
 | 334  | Fletchinder        |  0.03834% | 17     |  0.039% | 
 | 335  | Meowth             |  0.03821% | 18     |  0.041% | 
 | 336  | Torkoal            |  0.03820% | 20     |  0.046% | 
 | 337  | Shaymin            |  0.03768% | 15     |  0.034% | 
 | 338  | Gigalith           |  0.03736% | 44     |  0.101% | 
 | 339  | Mightyena          |  0.03716% | 17     |  0.039% | 
 | 340  | Dewgong            |  0.03647% | 21     |  0.048% | 
 | 341  | Altaria            |  0.03621% | 19     |  0.043% | 
 | 342  | Samurott           |  0.03544% | 17     |  0.039% | 
 | 343  | Rotom-Frost        |  0.03462% | 15     |  0.034% | 
 | 344  | Moltres            |  0.03458% | 20     |  0.046% | 
 | 345  | Rotom-Fan          |  0.03450% | 14     |  0.032% | 
 | 346  | Gothorita          |  0.03294% | 15     |  0.034% | 
 | 347  | Bibarel            |  0.03215% | 15     |  0.034% | 
 | 348  | Steelix            |  0.03195% | 23     |  0.053% | 
 | 349  | Regice             |  0.03156% | 14     |  0.032% | 
 | 350  | Wartortle          |  0.03104% | 22     |  0.050% | 
 | 351  | Haunter            |  0.03097% | 18     |  0.041% | 
 | 352  | Sawk               |  0.02976% | 11     |  0.025% | 
 | 353  | Gligar             |  0.02921% | 15     |  0.034% | 
 | 354  | Tropius            |  0.02824% | 17     |  0.039% | 
 | 355  | Bellossom          |  0.02631% | 18     |  0.041% | 
 | 356  | Gourgeist-Small    |  0.02627% | 17     |  0.039% | 
 | 357  | Octillery          |  0.02526% | 13     |  0.030% | 
 | 358  | Dunsparce          |  0.02486% | 17     |  0.039% | 
 | 359  | Basculin           |  0.02448% | 16     |  0.037% | 
 | 360  | Magmar             |  0.02436% | 10     |  0.023% | 
 | 361  | Munchlax           |  0.02416% | 13     |  0.030% | 
 | 362  | Skuntank           |  0.02410% | 13     |  0.030% | 
 | 363  | Kangaskhan         |  0.02398% | 10     |  0.023% | 
 | 364  | Registeel          |  0.02374% | 9      |  0.021% | 
 | 365  | Rapidash           |  0.02291% | 14     |  0.032% | 
 | 366  | Wormadam-Sandy     |  0.02291% | 7      |  0.016% | 
 | 367  | Pelipper           |  0.02081% | 15     |  0.034% | 
 | 368  | Regirock           |  0.02000% | 10     |  0.023% | 
 | 369  | Lumineon           |  0.01877% | 15     |  0.034% | 
 | 370  | Clamperl           |  0.01870% | 21     |  0.048% | 
 | 371  | Stantler           |  0.01846% | 16     |  0.037% | 
 | 372  | Ledian             |  0.01819% | 10     |  0.023% | 
 | 373  | Castform           |  0.01765% | 11     |  0.025% | 
 | 374  | Beedrill           |  0.01752% | 15     |  0.034% | 
 | 375  | Furfrou            |  0.01733% | 8      |  0.018% | 
 | 376  | Gastly             |  0.01715% | 10     |  0.023% | 
 | 377  | Raticate           |  0.01684% | 5      |  0.011% | 
 | 378  | Carracosta         |  0.01672% | 11     |  0.025% | 
 | 379  | Simisear           |  0.01568% | 7      |  0.016% | 
 | 380  | Shieldon           |  0.01555% | 17     |  0.039% | 
 | 381  | Onix               |  0.01544% | 9      |  0.021% | 
 | 382  | Sawsbuck           |  0.01521% | 10     |  0.023% | 
 | 383  | Pidgeot            |  0.01504% | 6      |  0.014% | 
 | 384  | Kingler            |  0.01375% | 9      |  0.021% | 
 | 385  | Electabuzz         |  0.01375% | 7      |  0.016% | 
 | 386  | Gurdurr            |  0.01316% | 4      |  0.009% | 
 | 387  | Relicanth          |  0.01251% | 6      |  0.014% | 
 | 388  | Servine            |  0.01239% | 7      |  0.016% | 
 | 389  | Lunatone           |  0.01174% | 9      |  0.021% | 
 | 390  | Numel              |  0.01166% | 9      |  0.021% | 
 | 391  | Vespiquen          |  0.01153% | 7      |  0.016% | 
 | 392  | Whiscash           |  0.01093% | 9      |  0.021% | 
 | 393  | Noctowl            |  0.01087% | 9      |  0.021% | 
 | 394  | Frogadier          |  0.01066% | 4      |  0.009% | 
 | 395  | Mothim             |  0.01057% | 5      |  0.011% | 
 | 396  | Stunfisk           |  0.01021% | 6      |  0.014% | 
 | 397  | Corsola            |  0.01018% | 6      |  0.014% | 
 | 398  | Diglett            |  0.00964% | 21     |  0.048% | 
 | 399  | Ivysaur            |  0.00957% | 7      |  0.016% | 
 | 400  | Sandile            |  0.00902% | 5      |  0.011% | 
 | 401  | Quilava            |  0.00874% | 5      |  0.011% | 
 | 402  | Mantyke            |  0.00857% | 6      |  0.014% | 
 | 403  | Kadabra            |  0.00820% | 7      |  0.016% | 
 | 404  | Bulbasaur          |  0.00797% | 6      |  0.014% | 
 | 405  | Vigoroth           |  0.00783% | 2      |  0.005% | 
 | 406  | Snubbull           |  0.00783% | 3      |  0.007% | 
 | 407  | Simipour           |  0.00778% | 4      |  0.009% | 
 | 408  | Fearow             |  0.00760% | 7      |  0.016% | 
 | 409  | Bronzor            |  0.00732% | 3      |  0.007% | 
 | 410  | Combusken          |  0.00728% | 5      |  0.011% | 
 | 411  | Jigglypuff         |  0.00723% | 5      |  0.011% | 
 | 412  | Snivy              |  0.00652% | 4      |  0.009% | 
 | 413  | Nosepass           |  0.00637% | 3      |  0.007% | 
 | 414  | Seadra             |  0.00599% | 2      |  0.005% | 
 | 415  | Huntail            |  0.00590% | 4      |  0.009% | 
 | 416  | Floette            |  0.00584% | 4      |  0.009% | 
 | 417  | Togepi             |  0.00552% | 3      |  0.007% | 
 | 418  | Mime Jr.           |  0.00549% | 2      |  0.005% | 
 | 419  | Seedot             |  0.00542% | 6      |  0.014% | 
 | 420  | Carnivine          |  0.00537% | 3      |  0.007% | 
 | 421  | Magnemite          |  0.00535% | 4      |  0.009% | 
 | 422  | Deoxys             |  0.00534% | 2      |  0.005% | 
 | 423  | Squirtle           |  0.00529% | 4      |  0.009% | 
 | 424  | Ducklett           |  0.00528% | 3      |  0.007% | 
 | 425  | Porygon            |  0.00487% | 3      |  0.007% | 
 | 426  | Trapinch           |  0.00460% | 3      |  0.007% | 
 | 427  | Klang              |  0.00459% | 3      |  0.007% | 
 | 428  | Banette            |  0.00456% | 4      |  0.009% | 
 | 429  | Mawile             |  0.00446% | 2      |  0.005% | 
 | 430  | Mankey             |  0.00443% | 3      |  0.007% | 
 | 431  | Prinplup           |  0.00437% | 3      |  0.007% | 
 | 432  | Roselia            |  0.00437% | 3      |  0.007% | 
 | 433  | Honedge            |  0.00437% | 3      |  0.007% | 
 | 434  | Metang             |  0.00415% | 4      |  0.009% | 
 | 435  | Heatmor            |  0.00395% | 2      |  0.005% | 
 | 436  | Foongus            |  0.00391% | 2      |  0.005% | 
 | 437  | Simisage           |  0.00391% | 2      |  0.005% | 
 | 438  | Unown              |  0.00391% | 2      |  0.005% | 
 | 439  | Misdreavus         |  0.00391% | 2      |  0.005% | 
 | 440  | Caterpie           |  0.00391% | 2      |  0.005% | 
 | 441  | Scraggy            |  0.00388% | 1      |  0.002% | 
 | 442  | Piloswine          |  0.00388% | 1      |  0.002% | 
 | 443  | Vulpix             |  0.00377% | 1      |  0.002% | 
 | 444  | Duosion            |  0.00368% | 3      |  0.007% | 
 | 445  | Palpitoad          |  0.00337% | 2      |  0.005% | 
 | 446  | Pichu              |  0.00329% | 3      |  0.007% | 
 | 447  | Shroomish          |  0.00329% | 3      |  0.007% | 
 | 448  | Rattata            |  0.00318% | 2      |  0.005% | 
 | 449  | Machoke            |  0.00303% | 2      |  0.005% | 
 | 450  | Paras              |  0.00294% | 3      |  0.007% | 
 | 451  | Swirlix            |  0.00291% | 2      |  0.005% | 
 | 452  | Luvdisc            |  0.00291% | 2      |  0.005% | 
 | 453  | Spewpa             |  0.00291% | 2      |  0.005% | 
 | 454  | Exeggcute          |  0.00291% | 2      |  0.005% | 
 | 455  | Wormadam-Trash     |  0.00291% | 2      |  0.005% | 
 | 456  | Lileep             |  0.00281% | 2      |  0.005% | 
 | 457  | Poliwhirl          |  0.00278% | 1      |  0.002% | 
 | 458  | Joltik             |  0.00258% | 1      |  0.002% | 
 | 459  | Eelektrik          |  0.00246% | 1      |  0.002% | 
 | 460  | Pignite            |  0.00246% | 1      |  0.002% | 
 | 461  | Dustox             |  0.00246% | 1      |  0.002% | 
 | 462  | Riolu              |  0.00246% | 1      |  0.002% | 
 | 463  | Vanilluxe          |  0.00246% | 1      |  0.002% | 
 | 464  | Wormadam           |  0.00246% | 1      |  0.002% | 
 | 465  | Sealeo             |  0.00234% | 2      |  0.005% | 
 | 466  | Graveler           |  0.00227% | 1      |  0.002% | 
 | 467  | Seel               |  0.00220% | 1      |  0.002% | 
 | 468  | Froakie            |  0.00217% | 2      |  0.005% | 
 | 469  | Makuhita           |  0.00208% | 1      |  0.002% | 
 | 470  | Swablu             |  0.00196% | 1      |  0.002% | 
 | 471  | Krabby             |  0.00185% | 1      |  0.002% | 
 | 472  | Phanpy             |  0.00169% | 1      |  0.002% | 
 | 473  | Snorunt            |  0.00151% | 2      |  0.005% | 
 | 474  | Boldore            |  0.00147% | 1      |  0.002% | 
 | 475  | Bonsly             |  0.00146% | 3      |  0.007% | 
 | 476  | Cyndaquil          |  0.00146% | 2      |  0.005% | 
 | 477  | Braixen            |  0.00146% | 1      |  0.002% | 
 | 478  | Sliggoo            |  0.00146% | 1      |  0.002% | 
 | 479  | Azurill            |  0.00146% | 1      |  0.002% | 
 | 480  | Totodile           |  0.00146% | 1      |  0.002% | 
 | 481  | Shuppet            |  0.00146% | 1      |  0.002% | 
 | 482  | Piplup             |  0.00146% | 1      |  0.002% | 
 | 483  | Ralts              |  0.00146% | 1      |  0.002% | 
 | 484  | Surskit            |  0.00146% | 1      |  0.002% | 
 | 485  | Poochyena          |  0.00146% | 1      |  0.002% | 
 | 486  | Woobat             |  0.00146% | 1      |  0.002% | 
 | 487  | Marshtomp          |  0.00146% | 1      |  0.002% | 
 | 488  | Pancham            |  0.00146% | 1      |  0.002% | 
 | 489  | Dwebble            |  0.00146% | 1      |  0.002% | 
 | 490  | Kabuto             |  0.00146% | 1      |  0.002% | 
 | 491  | Dragonair          |  0.00146% | 1      |  0.002% | 
 | 492  | Feebas             |  0.00146% | 1      |  0.002% | 
 | 493  | Beautifly          |  0.00146% | 1      |  0.002% | 
 | 494  | Herdier            |  0.00146% | 1      |  0.002% | 
 | 495  | Pidgey             |  0.00146% | 1      |  0.002% | 
 | 496  | Anorith            |  0.00146% | 1      |  0.002% | 
 | 497  | Golett             |  0.00146% | 1      |  0.002% | 
 | 498  | Bidoof             |  0.00146% | 1      |  0.002% | 
 | 499  | Pupitar            |  0.00146% | 1      |  0.002% | 
 | 500  | Growlithe          |  0.00143% | 1      |  0.002% | 
 | 501  | Whirlipede         |  0.00130% | 1      |  0.002% | 
 | 502  | Monferno           |  0.00126% | 2      |  0.005% | 
 | 503  | Nidorino           |  0.00120% | 1      |  0.002% | 
 | 504  | Electrike          |  0.00119% | 1      |  0.002% | 
 | 505  | Krokorok           |  0.00116% | 1      |  0.002% | 
 | 506  | Tyrunt             |  0.00113% | 1      |  0.002% | 
 | 507  | Nidorina           |  0.00108% | 2      |  0.005% | 
 | 508  | Flabebe            |  0.00103% | 1      |  0.002% | 
 | 509  | Ledyba             |  0.00079% | 1      |  0.002% | 
 | 510  | Kricketune         |  0.00077% | 1      |  0.002% | 
 | 511  | Lairon             |  0.00052% | 1      |  0.002% | 
 | 512  | Charmeleon         |  0.00037% | 1      |  0.002% | 
 | 513  | Frillish           |  0.00036% | 1      |  0.002% | 
 | 514  | Tranquill          |  0.00025% | 1      |  0.002% | 
 | 515  | Marill             |  0.00005% | 2      |  0.005% | 
 | 516  | Meditite           |  0.00000% | 1      |  0.002% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
