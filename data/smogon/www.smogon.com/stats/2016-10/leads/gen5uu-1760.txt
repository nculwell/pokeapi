 Total leads: 32
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Crobat             | 20.63169% | 2      |  6.250% | 
 | 2    | Swampert           | 16.39315% | 1      |  3.125% | 
 | 3    | Raikou             |  0.00000% | 1      |  3.125% | 
 | 4    | Kangaskhan         |  0.00000% | 1      |  3.125% | 
 | 5    | Registeel          |  0.00000% | 1      |  3.125% | 
 | 6    | Hitmonlee          |  0.00000% | 1      |  3.125% | 
 | 7    | Umbreon            |  0.00000% | 1      |  3.125% | 
 | 8    | Magikarp           |  0.00000% | 1      |  3.125% | 
 | 9    | Abomasnow          |  0.00000% | 1      |  3.125% | 
 | 10   | Galvantula         |  0.00000% | 1      |  3.125% | 
 | 11   | Mew                |  0.00000% | 2      |  6.250% | 
 | 12   | Mienshao           |  0.00000% | 2      |  6.250% | 
 | 13   | Milotic            |  0.00000% | 1      |  3.125% | 
 | 14   | Sceptile           |  0.00000% | 1      |  3.125% | 
 | 15   | Eelektross         |  0.00000% | 1      |  3.125% | 
 | 16   | Rotom-Heat         |  0.00000% | 1      |  3.125% | 
 | 17   | Regigigas          |  0.00000% | 1      |  3.125% | 
 | 18   | Krookodile         |  0.00000% | 1      |  3.125% | 
 | 19   | Roserade           |  0.00000% | 1      |  3.125% | 
 | 20   | Azumarill          |  0.00000% | 1      |  3.125% | 
 | 21   | Cinccino           |  0.00000% | 1      |  3.125% | 
 | 22   | Sableye            |  0.00000% | 1      |  3.125% | 
 | 23   | Victini            |  0.00000% | 1      |  3.125% | 
 | 24   | Empoleon           |  0.00000% | 1      |  3.125% | 
 | 25   | Arcanine           |  0.00000% | 1      |  3.125% | 
 | 26   | Nidoking           |  0.00000% | 1      |  3.125% | 
 | 27   | Zapdos             |  0.00000% | 2      |  6.250% | 
 | 28   | Blastoise          |  0.00000% | 1      |  3.125% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
