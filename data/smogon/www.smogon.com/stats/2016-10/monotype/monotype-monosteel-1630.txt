 Total battles: 315331
 Avg. weight/team: 0.007
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Heatran            | 95.71304% | 26443  | 72.182% | 20351  | 76.744% | 
 | 2    | Scizor-Mega        | 75.53328% | 21020  | 57.378% | 14547  | 54.857% | 
 | 3    | Skarmory           | 74.86339% | 21468  | 58.601% | 17580  | 66.294% | 
 | 4    | Magnezone          | 48.22573% | 14900  | 40.673% | 10351  | 39.034% | 
 | 5    | Bisharp            | 45.74061% | 16107  | 43.967% | 10345  | 39.011% | 
 | 6    | Excadrill          | 44.89298% | 13667  | 37.307% | 9324   | 35.161% | 
 | 7    | Doublade           | 43.42867% | 8734   | 23.841% | 6086   | 22.950% | 
 | 8    | Jirachi            | 42.78627% | 11251  | 30.712% | 7878   | 29.708% | 
 | 9    | Ferrothorn         | 32.34564% | 13384  | 36.534% | 10591  | 39.939% | 
 | 10   | Empoleon           | 24.06743% | 14186  | 38.724% | 10086  | 38.034% | 
 | 11   | Klefki             | 13.70099% | 7521   | 20.530% | 6088   | 22.958% | 
 | 12   | Lucario            | 10.95731% | 8057   | 21.993% | 5321   | 20.065% | 
 | 13   | Metagross          |  7.53848% | 8321   | 22.714% | 5521   | 20.820% | 
 | 14   | Scizor             |  7.53082% | 3826   | 10.444% | 2672   | 10.076% | 
 | 15   | Bronzong           |  6.64696% | 4162   | 11.361% | 3350   | 12.633% | 
 | 16   | Aggron-Mega        |  6.11159% | 5466   | 14.921% | 3814   | 14.383% | 
 | 17   | Registeel          |  3.92849% | 1735   |  4.736% | 1275   |  4.808% | 
 | 18   | Cobalion           |  3.31685% | 2051   |  5.599% | 1398   |  5.272% | 
 | 19   | Aggron             |  2.49757% | 2169   |  5.921% | 1512   |  5.702% | 
 | 20   | Forretress         |  2.41523% | 2167   |  5.915% | 1862   |  7.022% | 
 | 21   | Durant             |  1.93622% | 2053   |  5.604% | 1380   |  5.204% | 
 | 22   | Magneton           |  1.78986% | 1021   |  2.787% | 712    |  2.685% | 
 | 23   | Steelix-Mega       |  1.63203% | 2746   |  7.496% | 1953   |  7.365% | 
 | 24   | Steelix            |  0.44927% | 1387   |  3.786% | 1032   |  3.892% | 
 | 25   | Klinklang          |  0.39782% | 1389   |  3.792% | 907    |  3.420% | 
 | 26   | Escavalier         |  0.33563% | 922    |  2.517% | 630    |  2.376% | 
 | 27   | Probopass          |  0.32662% | 798    |  2.178% | 598    |  2.255% | 
 | 28   | Aron               |  0.30657% | 186    |  0.508% | 140    |  0.528% | 
 | 29   | Bastiodon          |  0.25249% | 952    |  2.599% | 698    |  2.632% | 
 | 30   | Mawile             |  0.23492% | 1131   |  3.087% | 771    |  2.907% | 
 | 31   | Wormadam-Trash     |  0.03448% | 42     |  0.115% | 30     |  0.113% | 
 | 32   | Magnemite          |  0.01645% | 98     |  0.268% | 82     |  0.309% | 
 | 33   | Klang              |  0.01620% | 37     |  0.101% | 28     |  0.106% | 
 | 34   | Bronzor            |  0.01346% | 40     |  0.109% | 36     |  0.136% | 
 | 35   | Metang             |  0.01052% | 43     |  0.117% | 28     |  0.106% | 
 | 36   | Lairon             |  0.00247% | 77     |  0.210% | 62     |  0.234% | 
 | 37   | Shieldon           |  0.00227% | 27     |  0.074% | 19     |  0.072% | 
 | 38   | Honedge            |  0.00001% | 29     |  0.079% | 25     |  0.094% | 
 | 39   | Ferroseed          |  0.00000% | 16     |  0.044% | 16     |  0.060% | 
 | 40   | Pawniard           |  0.00000% | 7      |  0.019% | 5      |  0.019% | 
 | 41   | Beldum             |  0.00000% | 4      |  0.011% | 4      |  0.015% | 
 | 42   | Klink              |  0.00000% | 1      |  0.003% | 1      |  0.004% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
