 Total battles: 315331
 Avg. weight/team: 0.006
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Staraptor          | 79.61224% | 22395  | 49.011% | 16099  | 48.065% | 
 | 2    | Chansey            | 68.16761% | 19369  | 42.388% | 14737  | 43.998% | 
 | 3    | Porygon2           | 65.35035% | 18044  | 39.489% | 13823  | 41.269% | 
 | 4    | Lopunny-Mega       | 60.60584% | 28175  | 61.660% | 21778  | 65.020% | 
 | 5    | Diggersby          | 50.33360% | 13859  | 30.330% | 9628   | 28.745% | 
 | 6    | Ditto              | 46.94835% | 15144  | 33.142% | 11078  | 33.074% | 
 | 7    | Meloetta           | 42.62548% | 12453  | 27.253% | 8504   | 25.389% | 
 | 8    | Pidgeot-Mega       | 30.93088% | 10693  | 23.401% | 7863   | 23.475% | 
 | 9    | Heliolisk          | 20.16216% | 10259  | 22.452% | 7079   | 21.135% | 
 | 10   | Porygon-Z          | 17.68859% | 12682  | 27.754% | 8666   | 25.873% | 
 | 11   | Smeargle           | 16.66801% | 6858   | 15.009% | 6232   | 18.606% | 
 | 12   | Snorlax            | 11.51262% | 12082  | 26.441% | 8519   | 25.434% | 
 | 13   | Pyroar             | 10.36227% | 6644   | 14.540% | 4469   | 13.342% | 
 | 14   | Miltank            |  9.73323% | 5171   | 11.317% | 4028   | 12.026% | 
 | 15   | Exploud            |  7.06271% | 5730   | 12.540% | 4098   | 12.235% | 
 | 16   | Cinccino           |  6.65693% | 7540   | 16.501% | 5293   | 15.803% | 
 | 17   | Audino-Mega        |  5.74249% | 2283   |  4.996% | 1631   |  4.869% | 
 | 18   | Blissey            |  5.62967% | 4407   |  9.645% | 3103   |  9.264% | 
 | 19   | Ambipom            |  5.51150% | 8297   | 18.158% | 6677   | 19.935% | 
 | 20   | Furfrou            |  5.04089% | 2262   |  4.950% | 1635   |  4.881% | 
 | 21   | Kecleon            |  4.77285% | 3797   |  8.310% | 3022   |  9.022% | 
 | 22   | Staravia           |  3.13414% | 496    |  1.085% | 399    |  1.191% | 
 | 23   | Slaking            |  2.81194% | 3761   |  8.231% | 2786   |  8.318% | 
 | 24   | Zangoose           |  2.25702% | 2431   |  5.320% | 1660   |  4.956% | 
 | 25   | Tauros             |  2.12000% | 3034   |  6.640% | 2182   |  6.515% | 
 | 26   | Chatot             |  2.04944% | 2447   |  5.355% | 1874   |  5.595% | 
 | 27   | Swellow            |  2.02021% | 2821   |  6.174% | 1964   |  5.864% | 
 | 28   | Regigigas          |  1.83596% | 1725   |  3.775% | 1352   |  4.036% | 
 | 29   | Ursaring           |  1.42032% | 2412   |  5.279% | 1704   |  5.087% | 
 | 30   | Braviary           |  0.91981% | 1926   |  4.215% | 1312   |  3.917% | 
 | 31   | Linoone            |  0.85770% | 1613   |  3.530% | 1173   |  3.502% | 
 | 32   | Sawsbuck           |  0.70470% | 1860   |  4.071% | 1234   |  3.684% | 
 | 33   | Persian            |  0.67235% | 1345   |  2.943% | 1050   |  3.135% | 
 | 34   | Lickilicky         |  0.66559% | 1503   |  3.289% | 1136   |  3.392% | 
 | 35   | Kangaskhan         |  0.66007% | 1663   |  3.639% | 1256   |  3.750% | 
 | 36   | Girafarig          |  0.63885% | 960    |  2.101% | 649    |  1.938% | 
 | 37   | Wigglytuff         |  0.61689% | 1688   |  3.694% | 1223   |  3.651% | 
 | 38   | Vigoroth           |  0.53282% | 444    |  0.972% | 284    |  0.848% | 
 | 39   | Stoutland          |  0.49945% | 1372   |  3.003% | 970    |  2.896% | 
 | 40   | Munchlax           |  0.47841% | 240    |  0.525% | 175    |  0.522% | 
 | 41   | Raticate           |  0.44095% | 1006   |  2.202% | 674    |  2.012% | 
 | 42   | Dunsparce          |  0.37373% | 907    |  1.985% | 709    |  2.117% | 
 | 43   | Lopunny            |  0.36263% | 463    |  1.013% | 396    |  1.182% | 
 | 44   | Bouffalant         |  0.35721% | 1197   |  2.620% | 837    |  2.499% | 
 | 45   | Bibarel            |  0.32711% | 1184   |  2.591% | 894    |  2.669% | 
 | 46   | Purugly            |  0.29116% | 509    |  1.114% | 368    |  1.099% | 
 | 47   | Fletchling         |  0.28950% | 30     |  0.066% | 24     |  0.072% | 
 | 48   | Noctowl            |  0.24560% | 577    |  1.263% | 400    |  1.194% | 
 | 49   | Dodrio             |  0.24350% | 669    |  1.464% | 488    |  1.457% | 
 | 50   | Eevee              |  0.19942% | 274    |  0.600% | 193    |  0.576% | 
 | 51   | Spinda             |  0.13561% | 592    |  1.296% | 470    |  1.403% | 
 | 52   | Rattata            |  0.09720% | 498    |  1.090% | 400    |  1.194% | 
 | 53   | Unfezant           |  0.08392% | 295    |  0.646% | 207    |  0.618% | 
 | 54   | Stantler           |  0.06747% | 277    |  0.606% | 202    |  0.603% | 
 | 55   | Delcatty           |  0.06135% | 295    |  0.646% | 215    |  0.642% | 
 | 56   | Pidgeot            |  0.05011% | 283    |  0.619% | 206    |  0.615% | 
 | 57   | Castform           |  0.05001% | 471    |  1.031% | 366    |  1.093% | 
 | 58   | Audino             |  0.04348% | 227    |  0.497% | 165    |  0.493% | 
 | 59   | Farfetch'd         |  0.04024% | 144    |  0.315% | 110    |  0.328% | 
 | 60   | Furret             |  0.03818% | 439    |  0.961% | 341    |  1.018% | 
 | 61   | Watchog            |  0.03774% | 167    |  0.365% | 111    |  0.331% | 
 | 62   | Fearow             |  0.03422% | 212    |  0.464% | 189    |  0.564% | 
 | 63   | Starly             |  0.02798% | 62     |  0.136% | 32     |  0.096% | 
 | 64   | Lickitung          |  0.02136% | 287    |  0.628% | 219    |  0.654% | 
 | 65   | Herdier            |  0.01200% | 9      |  0.020% | 7      |  0.021% | 
 | 66   | Bidoof             |  0.01066% | 31     |  0.068% | 21     |  0.063% | 
 | 67   | Buneary            |  0.00466% | 38     |  0.083% | 18     |  0.054% | 
 | 68   | Porygon            |  0.00433% | 60     |  0.131% | 42     |  0.125% | 
 | 69   | Rufflet            |  0.00409% | 22     |  0.048% | 15     |  0.045% | 
 | 70   | Aipom              |  0.00277% | 39     |  0.085% | 36     |  0.107% | 
 | 71   | Helioptile         |  0.00252% | 5      |  0.011% | 5      |  0.015% | 
 | 72   | Taillow            |  0.00187% | 56     |  0.123% | 34     |  0.102% | 
 | 73   | Patrat             |  0.00117% | 13     |  0.028% | 11     |  0.033% | 
 | 74   | Swablu             |  0.00113% | 30     |  0.066% | 20     |  0.060% | 
 | 75   | Doduo              |  0.00088% | 48     |  0.105% | 32     |  0.096% | 
 | 76   | Bunnelby           |  0.00078% | 29     |  0.063% | 16     |  0.048% | 
 | 77   | Sentret            |  0.00075% | 12     |  0.026% | 9      |  0.027% | 
 | 78   | Zigzagoon          |  0.00075% | 9      |  0.020% | 8      |  0.024% | 
 | 79   | Slakoth            |  0.00063% | 6      |  0.013% | 6      |  0.018% | 
 | 80   | Igglybuff          |  0.00057% | 5      |  0.011% | 5      |  0.015% | 
 | 81   | Jigglypuff         |  0.00029% | 20     |  0.044% | 13     |  0.039% | 
 | 82   | Azurill            |  0.00017% | 10     |  0.022% | 9      |  0.027% | 
 | 83   | Lillipup           |  0.00003% | 1      |  0.002% | 1      |  0.003% | 
 | 84   | Deerling           |  0.00000% | 2      |  0.004% | 2      |  0.006% | 
 | 85   | Meowth             |  0.00000% | 26     |  0.057% | 21     |  0.063% | 
 | 86   | Pidgeotto          |  0.00000% | 56     |  0.123% | 42     |  0.125% | 
 | 87   | Whismur            |  0.00000% | 3      |  0.007% | 3      |  0.009% | 
 | 88   | Glameow            |  0.00000% | 7      |  0.015% | 6      |  0.018% | 
 | 89   | Minccino           |  0.00000% | 15     |  0.033% | 12     |  0.036% | 
 | 90   | Skitty             |  0.00000% | 3      |  0.007% | 2      |  0.006% | 
 | 91   | Loudred            |  0.00000% | 1      |  0.002% | 0      |  0.000% | 
 | 92   | Happiny            |  0.00000% | 10     |  0.022% | 5      |  0.015% | 
 | 93   | Teddiursa          |  0.00000% | 4      |  0.009% | 3      |  0.009% | 
 | 94   | Tranquill          |  0.00000% | 3      |  0.007% | 2      |  0.006% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
