 monotype......................100.00000%
 monorock......................100.00000%
 sand..........................95.21217%
 balance.......................81.47469%
 offense.......................14.60980%
 weatherless................... 4.69318%
 semistall..................... 2.30215%
 trickroom..................... 2.16585%
 hyperoffense.................. 1.60940%
 sandoffense................... 1.55230%
 choice........................ 0.44526%
 hail.......................... 0.20175%
 multiweather.................. 0.10710%
 tricksand..................... 0.04627%
 gravity....................... 0.03997%
 monoground.................... 0.01015%
 stall......................... 0.00395%
 sandstall..................... 0.00395%
 monowater..................... 0.00022%
 tailwind...................... 0.00003%
 hailoffense................... 0.00000%
 monobug....................... 0.00000%
 rain.......................... 0.00000%
 sun........................... 0.00000%
 monosteel..................... 0.00000%
 monofighting.................. 0.00000%
 monoice....................... 0.00000%
 monodark...................... 0.00000%
 hailstall..................... 0.00000%
 monodragon.................... 0.00000%

 Stalliness (mean:  0.319)
     |
 -1.5|
     |
 -1.0|#
     |#####
 -0.5|#
     |###
  0.0|######
     |##############################
 +0.5|######
     |#
 +1.0|#############
     |#
 +1.5|
     |
 +2.0|
     |
 +2.5|
     |
 more negative = more offensive, more positive = more stall
 one # =  1.47%
