 Total battles: 315331
 Avg. weight/team: 0.04
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Keldeo             | 67.45475% | 26609  | 57.893% | 18183  | 55.813% | 
 | 2    | Infernape          | 54.09179% | 24086  | 52.404% | 19000  | 58.321% | 
 | 3    | Breloom            | 53.38732% | 23843  | 51.875% | 16924  | 51.948% | 
 | 4    | Terrakion          | 51.30522% | 19809  | 43.099% | 13287  | 40.785% | 
 | 5    | Hawlucha           | 47.65244% | 21257  | 46.249% | 15025  | 46.119% | 
 | 6    | Medicham-Mega      | 43.64831% | 18652  | 40.581% | 13638  | 41.862% | 
 | 7    | Gallade-Mega       | 32.88075% | 14763  | 32.120% | 9842   | 30.210% | 
 | 8    | Lucario            | 32.12634% | 16311  | 35.488% | 10922  | 33.525% | 
 | 9    | Heracross          | 26.46314% | 10741  | 23.369% | 7126   | 21.873% | 
 | 10   | Cobalion           | 26.30271% | 10318  | 22.449% | 8607   | 26.419% | 
 | 11   | Conkeldurr         | 25.38980% | 11651  | 25.349% | 8553   | 26.254% | 
 | 12   | Scrafty            | 19.53973% | 10281  | 22.368% | 7275   | 22.331% | 
 | 13   | Machamp            | 16.67424% | 9249   | 20.123% | 6767   | 20.771% | 
 | 14   | Heracross-Mega     | 14.80331% | 7606   | 16.548% | 5145   | 15.793% | 
 | 15   | Toxicroak          | 13.00004% | 7402   | 16.105% | 4977   | 15.277% | 
 | 16   | Hitmonlee          | 10.52810% | 5613   | 12.212% | 4193   | 12.870% | 
 | 17   | Chesnaught         |  8.35001% | 4625   | 10.063% | 3138   |  9.632% | 
 | 18   | Pangoro            |  7.86414% | 4243   |  9.232% | 2840   |  8.717% | 
 | 19   | Hitmontop          |  7.53645% | 3989   |  8.679% | 2926   |  8.981% | 
 | 20   | Mienshao           |  6.22897% | 3502   |  7.619% | 2539   |  7.793% | 
 | 21   | Poliwrath          |  4.97135% | 3405   |  7.408% | 2414   |  7.410% | 
 | 22   | Hitmonchan         |  4.53176% | 3059   |  6.655% | 2216   |  6.802% | 
 | 23   | Medicham           |  4.22543% | 2016   |  4.386% | 1379   |  4.233% | 
 | 24   | Gallade            |  3.36176% | 2058   |  4.478% | 1364   |  4.187% | 
 | 25   | Emboar             |  2.81524% | 1682   |  3.660% | 1199   |  3.680% | 
 | 26   | Hariyama           |  2.67494% | 1856   |  4.038% | 1325   |  4.067% | 
 | 27   | Sawk               |  2.48690% | 1480   |  3.220% | 948    |  2.910% | 
 | 28   | Virizion           |  2.41076% | 1315   |  2.861% | 805    |  2.471% | 
 | 29   | Combusken          |  2.23711% | 923    |  2.008% | 742    |  2.278% | 
 | 30   | Primeape           |  2.18565% | 1504   |  3.272% | 1036   |  3.180% | 
 | 31   | Throh              |  1.30122% | 764    |  1.662% | 534    |  1.639% | 
 | 32   | Machoke            |  0.34252% | 266    |  0.579% | 190    |  0.583% | 
 | 33   | Riolu              |  0.31794% | 203    |  0.442% | 154    |  0.473% | 
 | 34   | Gurdurr            |  0.18352% | 80     |  0.174% | 62     |  0.190% | 
 | 35   | Meditite           |  0.07525% | 49     |  0.107% | 39     |  0.120% | 
 | 36   | Pignite            |  0.05854% | 38     |  0.083% | 21     |  0.064% | 
 | 37   | Monferno           |  0.05140% | 31     |  0.067% | 24     |  0.074% | 
 | 38   | Scraggy            |  0.03379% | 28     |  0.061% | 20     |  0.061% | 
 | 39   | Tyrogue            |  0.03201% | 46     |  0.100% | 37     |  0.114% | 
 | 40   | Pancham            |  0.01246% | 15     |  0.033% | 15     |  0.046% | 
 | 41   | Mankey             |  0.01070% | 10     |  0.022% | 9      |  0.028% | 
 | 42   | Mienfoo            |  0.00626% | 4      |  0.009% | 3      |  0.009% | 
 | 43   | Croagunk           |  0.00458% | 8      |  0.017% | 7      |  0.021% | 
 | 44   | Machop             |  0.00433% | 4      |  0.009% | 4      |  0.012% | 
 | 45   | Timburr            |  0.00401% | 6      |  0.013% | 6      |  0.018% | 
 | 46   | Makuhita           |  0.00332% | 16     |  0.035% | 11     |  0.034% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
