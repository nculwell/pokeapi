 Total battles: 315331
 Avg. weight/team: 0.05
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Ferrothorn         | 70.39327% | 22191  | 70.394% | 18502  | 82.553% | 
 | 2    | Breloom            | 67.52479% | 21286  | 67.523% | 15448  | 68.926% | 
 | 3    | Venusaur-Mega      | 65.23834% | 20566  | 65.239% | 14415  | 64.317% | 
 | 4    | Serperior          | 49.59908% | 15636  | 49.600% | 10562  | 47.126% | 
 | 5    | Cradily            | 42.30288% | 13336  | 42.304% | 10112  | 45.118% | 
 | 6    | Whimsicott         | 37.23603% | 11738  | 37.235% | 8380   | 37.390% | 
 | 7    | Ludicolo           | 26.91983% | 8486   | 26.919% | 5945   | 26.526% | 
 | 8    | Sceptile-Mega      | 24.17582% | 7621   | 24.175% | 5036   | 22.470% | 
 | 9    | Rotom-Mow          | 19.03992% | 6002   | 19.039% | 4410   | 19.677% | 
 | 10   | Celebi             | 15.16025% | 4779   | 15.160% | 3066   | 13.680% | 
 | 11   | Roserade           | 14.11023% | 4448   | 14.110% | 2942   | 13.127% | 
 | 12   | Trevenant          | 13.23151% | 4171   | 13.231% | 2686   | 11.984% | 
 | 13   | Tangrowth          | 12.92063% | 4073   | 12.920% | 2682   | 11.967% | 
 | 14   | Shiftry            | 12.16246% | 3834   | 12.162% | 2637   | 11.766% | 
 | 15   | Amoonguss          | 11.09415% | 3498   | 11.096% | 2305   | 10.285% | 
 | 16   | Torterra           | 10.58342% | 3337   | 10.586% | 2410   | 10.753% | 
 | 17   | Chesnaught         |  9.22811% | 2909   |  9.228% | 1915   |  8.544% | 
 | 18   | Exeggutor          |  9.03461% | 2848   |  9.034% | 1786   |  7.969% | 
 | 19   | Gourgeist-Super    |  7.65150% | 2412   |  7.651% | 1718   |  7.665% | 
 | 20   | Virizion           |  6.55072% | 2065   |  6.551% | 1336   |  5.961% | 
 | 21   | Lilligant          |  5.77035% | 1819   |  5.770% | 1207   |  5.385% | 
 | 22   | Leafeon            |  5.34209% | 1684   |  5.342% | 1085   |  4.841% | 
 | 23   | Victreebel         |  5.11686% | 1613   |  5.117% | 1077   |  4.805% | 
 | 24   | Sceptile           |  5.04390% | 1590   |  5.044% | 1072   |  4.783% | 
 | 25   | Leavanny           |  4.92652% | 1553   |  4.926% | 1226   |  5.470% | 
 | 26   | Venusaur           |  4.55537% | 1436   |  4.555% | 1001   |  4.466% | 
 | 27   | Meganium           |  4.46972% | 1409   |  4.470% | 965    |  4.306% | 
 | 28   | Abomasnow-Mega     |  4.30476% | 1357   |  4.305% | 902    |  4.025% | 
 | 29   | Abomasnow          |  3.85113% | 1214   |  3.851% | 766    |  3.418% | 
 | 30   | Jumpluff           |  3.52438% | 1111   |  3.524% | 826    |  3.685% | 
 | 31   | Tropius            |  3.47997% | 1097   |  3.480% | 760    |  3.391% | 
 | 32   | Vileplume          |  3.33405% | 1051   |  3.334% | 698    |  3.114% | 
 | 33   | Cacturne           |  3.30233% | 1041   |  3.302% | 740    |  3.302% | 
 | 34   | Shaymin            |  2.33479% | 736    |  2.335% | 482    |  2.151% | 
 | 35   | Sawsbuck           |  2.02073% | 637    |  2.021% | 431    |  1.923% | 
 | 36   | Gogoat             |  2.00170% | 631    |  2.002% | 370    |  1.651% | 
 | 37   | Tangela            |  1.67178% | 527    |  1.672% | 358    |  1.597% | 
 | 38   | Bellossom          |  1.34504% | 424    |  1.345% | 304    |  1.356% | 
 | 39   | Parasect           |  1.21180% | 382    |  1.212% | 265    |  1.182% | 
 | 40   | Simisage           |  0.93582% | 295    |  0.936% | 182    |  0.812% | 
 | 41   | Cherrim            |  0.82162% | 259    |  0.822% | 182    |  0.812% | 
 | 42   | Gourgeist          |  0.59004% | 186    |  0.590% | 140    |  0.625% | 
 | 43   | Sunflora           |  0.55832% | 176    |  0.558% | 117    |  0.522% | 
 | 44   | Maractus           |  0.51073% | 161    |  0.511% | 120    |  0.535% | 
 | 45   | Gourgeist-Small    |  0.48536% | 153    |  0.485% | 120    |  0.535% | 
 | 46   | Carnivine          |  0.39653% | 125    |  0.397% | 87     |  0.388% | 
 | 47   | Cottonee           |  0.39019% | 123    |  0.390% | 108    |  0.482% | 
 | 48   | Roselia            |  0.30454% | 96     |  0.305% | 68     |  0.303% | 
 | 49   | Servine            |  0.23475% | 74     |  0.235% | 49     |  0.219% | 
 | 50   | Gourgeist-Large    |  0.18082% | 57     |  0.181% | 42     |  0.187% | 
 | 51   | Wormadam           |  0.16179% | 51     |  0.162% | 35     |  0.156% | 
 | 52   | Ivysaur            |  0.15227% | 48     |  0.152% | 37     |  0.165% | 
 | 53   | Lileep             |  0.14592% | 46     |  0.146% | 37     |  0.165% | 
 | 54   | Bayleef            |  0.14275% | 45     |  0.143% | 29     |  0.129% | 
 | 55   | Swadloon           |  0.13324% | 42     |  0.133% | 29     |  0.129% | 
 | 56   | Cacnea             |  0.11737% | 37     |  0.117% | 25     |  0.112% | 
 | 57   | Gloom              |  0.11420% | 36     |  0.114% | 26     |  0.116% | 
 | 58   | Ferroseed          |  0.09834% | 31     |  0.098% | 24     |  0.107% | 
 | 59   | Grovyle            |  0.09200% | 29     |  0.092% | 27     |  0.120% | 
 | 60   | Bulbasaur          |  0.07613% | 24     |  0.076% | 11     |  0.049% | 
 | 61   | Treecko            |  0.06979% | 22     |  0.070% | 13     |  0.058% | 
 | 62   | Nuzleaf            |  0.06662% | 21     |  0.067% | 11     |  0.049% | 
 | 63   | Turtwig            |  0.05710% | 18     |  0.057% | 15     |  0.067% | 
 | 64   | Chikorita          |  0.05076% | 16     |  0.051% | 12     |  0.054% | 
 | 65   | Weepinbell         |  0.05076% | 16     |  0.051% | 10     |  0.045% | 
 | 66   | Quilladin          |  0.04441% | 14     |  0.044% | 11     |  0.049% | 
 | 67   | Grotle             |  0.03807% | 12     |  0.038% | 10     |  0.045% | 
 | 68   | Foongus            |  0.03489% | 11     |  0.035% | 7      |  0.031% | 
 | 69   | Paras              |  0.02538% | 8      |  0.025% | 6      |  0.027% | 
 | 70   | Sewaddle           |  0.02538% | 8      |  0.025% | 7      |  0.031% | 
 | 71   | Shroomish          |  0.02538% | 8      |  0.025% | 8      |  0.036% | 
 | 72   | Budew              |  0.02221% | 7      |  0.022% | 4      |  0.018% | 
 | 73   | Lombre             |  0.01903% | 6      |  0.019% | 4      |  0.018% | 
 | 74   | Bellsprout         |  0.01903% | 6      |  0.019% | 6      |  0.027% | 
 | 75   | Snivy              |  0.01586% | 5      |  0.016% | 4      |  0.018% | 
 | 76   | Snover             |  0.01586% | 5      |  0.016% | 4      |  0.018% | 
 | 77   | Deerling           |  0.01269% | 4      |  0.013% | 2      |  0.009% | 
 | 78   | Exeggcute          |  0.01269% | 4      |  0.013% | 3      |  0.013% | 
 | 79   | Chespin            |  0.01269% | 4      |  0.013% | 3      |  0.013% | 
 | 80   | Seedot             |  0.00952% | 3      |  0.010% | 3      |  0.013% | 
 | 81   | Oddish             |  0.00634% | 2      |  0.006% | 1      |  0.004% | 
 | 82   | Pumpkaboo-Super    |  0.00634% | 2      |  0.006% | 2      |  0.009% | 
 | 83   | Skiploom           |  0.00317% | 1      |  0.003% | 1      |  0.004% | 
 | 84   | Phantump           |  0.00317% | 1      |  0.003% | 1      |  0.004% | 
 | 85   | Skiddo             |  0.00317% | 1      |  0.003% | 1      |  0.004% | 
 | 86   | Hoppip             |  0.00317% | 1      |  0.003% | 1      |  0.004% | 
 | 87   | Lotad              |  0.00317% | 1      |  0.003% | 1      |  0.004% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
