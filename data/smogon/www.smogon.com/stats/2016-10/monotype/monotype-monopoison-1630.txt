 Total battles: 315331
 Avg. weight/team: 0.003
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Gengar             | 86.13358% | 25062  | 71.770% | 18000  | 69.509% | 
 | 2    | Nidoking           | 67.53965% | 19124  | 54.765% | 14181  | 54.761% | 
 | 3    | Venusaur-Mega      | 61.41367% | 18317  | 52.454% | 13001  | 50.205% | 
 | 4    | Tentacruel         | 55.15148% | 18013  | 51.584% | 14202  | 54.842% | 
 | 5    | Drapion            | 51.85012% | 18394  | 52.675% | 13718  | 52.973% | 
 | 6    | Scolipede          | 46.61707% | 13105  | 37.529% | 9847   | 38.025% | 
 | 7    | Crobat             | 36.63794% | 17324  | 49.611% | 12910  | 49.853% | 
 | 8    | Nidoqueen          | 35.88762% | 6254   | 17.910% | 5346   | 20.644% | 
 | 9    | Skuntank           | 31.66590% | 5890   | 16.867% | 4284   | 16.543% | 
 | 10   | Beedrill-Mega      | 25.84553% | 10033  | 28.731% | 7114   | 27.471% | 
 | 11   | Weezing            | 25.21935% | 10936  | 31.317% | 8902   | 34.376% | 
 | 12   | Dragalge           | 21.96881% | 9670   | 27.692% | 6864   | 26.506% | 
 | 13   | Amoonguss          | 17.87963% | 5624   | 16.105% | 4025   | 15.543% | 
 | 14   | Toxicroak          | 13.49741% | 10710  | 30.670% | 7636   | 29.487% | 
 | 15   | Roserade           |  5.08522% | 4330   | 12.400% | 3003   | 11.596% | 
 | 16   | Muk                |  3.58057% | 3911   | 11.200% | 2865   | 11.063% | 
 | 17   | Golbat             |  3.35492% | 1500   |  4.296% | 1156   |  4.464% | 
 | 18   | Venomoth           |  2.66789% | 1920   |  5.498% | 1315   |  5.078% | 
 | 19   | Ariados            |  1.73563% | 1522   |  4.359% | 1362   |  5.259% | 
 | 20   | Qwilfish           |  1.18683% | 828    |  2.371% | 693    |  2.676% | 
 | 21   | Arbok              |  1.08713% | 1568   |  4.490% | 1067   |  4.120% | 
 | 22   | Venusaur           |  0.76935% | 766    |  2.194% | 518    |  2.000% | 
 | 23   | Ekans              |  0.70332% | 122    |  0.349% | 98     |  0.378% | 
 | 24   | Garbodor           |  0.67689% | 713    |  2.042% | 565    |  2.182% | 
 | 25   | Victreebel         |  0.35486% | 503    |  1.440% | 356    |  1.375% | 
 | 26   | Vileplume          |  0.25654% | 577    |  1.652% | 390    |  1.506% | 
 | 27   | Seviper            |  0.24273% | 613    |  1.755% | 428    |  1.653% | 
 | 28   | Whirlipede         |  0.23488% | 265    |  0.759% | 202    |  0.780% | 
 | 29   | Swalot             |  0.23352% | 452    |  1.294% | 319    |  1.232% | 
 | 30   | Haunter            |  0.18748% | 159    |  0.455% | 114    |  0.440% | 
 | 31   | Dustox             |  0.18534% | 140    |  0.401% | 89     |  0.344% | 
 | 32   | Koffing            |  0.03510% | 259    |  0.742% | 219    |  0.846% | 
 | 33   | Beedrill           |  0.02804% | 143    |  0.410% | 105    |  0.405% | 
 | 34   | Gulpin             |  0.01395% | 8      |  0.023% | 5      |  0.019% | 
 | 35   | Ivysaur            |  0.01211% | 67     |  0.192% | 43     |  0.166% | 
 | 36   | Nidorino           |  0.00979% | 75     |  0.215% | 48     |  0.185% | 
 | 37   | Nidorina           |  0.00939% | 50     |  0.143% | 42     |  0.162% | 
 | 38   | Weedle             |  0.00763% | 31     |  0.089% | 22     |  0.085% | 
 | 39   | Weepinbell         |  0.00580% | 26     |  0.074% | 18     |  0.070% | 
 | 40   | Roselia            |  0.00529% | 146    |  0.418% | 109    |  0.421% | 
 | 41   | Skorupi            |  0.00275% | 21     |  0.060% | 15     |  0.058% | 
 | 42   | Trubbish           |  0.00090% | 12     |  0.034% | 11     |  0.042% | 
 | 43   | Grimer             |  0.00034% | 15     |  0.043% | 15     |  0.058% | 
 | 44   | NidoranF           |  0.00015% | 13     |  0.037% | 11     |  0.042% | 
 | 45   | NidoranM           |  0.00015% | 10     |  0.029% | 7      |  0.027% | 
 | 46   | Foongus            |  0.00003% | 19     |  0.054% | 16     |  0.062% | 
 | 47   | Budew              |  0.00000% | 3      |  0.009% | 1      |  0.004% | 
 | 48   | Gloom              |  0.00000% | 17     |  0.049% | 11     |  0.042% | 
 | 49   | Croagunk           |  0.00000% | 31     |  0.089% | 25     |  0.097% | 
 | 50   | Spinarak           |  0.00000% | 4      |  0.011% | 4      |  0.015% | 
 | 51   | Skrelp             |  0.00000% | 10     |  0.029% | 8      |  0.031% | 
 | 52   | Oddish             |  0.00000% | 9      |  0.026% | 9      |  0.035% | 
 | 53   | Bulbasaur          |  0.00000% | 1      |  0.003% | 0      |  0.000% | 
 | 54   | Zubat              |  0.00000% | 9      |  0.026% | 9      |  0.035% | 
 | 55   | Gastly             |  0.00000% | 20     |  0.057% | 19     |  0.073% | 
 | 56   | Kakuna             |  0.00000% | 6      |  0.017% | 6      |  0.023% | 
 | 57   | Tentacool          |  0.00000% | 14     |  0.040% | 13     |  0.050% | 
 | 58   | Venipede           |  0.00000% | 10     |  0.029% | 10     |  0.039% | 
 | 59   | Bellsprout         |  0.00000% | 3      |  0.009% | 3      |  0.012% | 
 | 60   | Venonat            |  0.00000% | 3      |  0.009% | 2      |  0.008% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
