 Total battles: 315331
 Avg. weight/team: 0.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Kyurem-Black       | 92.08996% | 12288  | 52.619% | 8660   | 49.757% | 
 | 2    | Mamoswine          | 91.10329% | 15255  | 65.324% | 11831  | 67.977% | 
 | 3    | Avalugg            | 72.59959% | 11930  | 51.086% | 9091   | 52.234% | 
 | 4    | Weavile            | 65.96622% | 14617  | 62.592% | 10698  | 61.467% | 
 | 5    | Froslass           | 57.73127% | 12961  | 55.500% | 10437  | 59.967% | 
 | 6    | Aurorus            | 47.67030% | 6937   | 29.705% | 5782   | 33.221% | 
 | 7    | Cloyster           | 38.69447% | 12093  | 51.783% | 8807   | 50.602% | 
 | 8    | Lapras             | 30.98186% | 5882   | 25.187% | 4290   | 24.649% | 
 | 9    | Abomasnow-Mega     | 20.10715% | 10461  | 44.795% | 8001   | 45.971% | 
 | 10   | Rotom-Frost        | 19.11698% | 3831   | 16.405% | 2876   | 16.524% | 
 | 11   | Glalie-Mega        | 16.29260% | 5436   | 23.278% | 3534   | 20.305% | 
 | 12   | Walrein            | 12.82911% | 3212   | 13.754% | 2297   | 13.198% | 
 | 13   | Articuno           |  8.06193% | 4170   | 17.856% | 2982   | 17.133% | 
 | 14   | Piloswine          |  5.90565% | 1924   |  8.239% | 1688   |  9.699% | 
 | 15   | Jynx               |  5.12639% | 2775   | 11.883% | 1939   | 11.141% | 
 | 16   | Regice             |  4.62426% | 3352   | 14.354% | 2464   | 14.157% | 
 | 17   | Cryogonal          |  3.22595% | 2151   |  9.211% | 1493   |  8.578% | 
 | 18   | Glaceon            |  3.08238% | 4161   | 17.818% | 2709   | 15.565% | 
 | 19   | Kyurem             |  2.47722% | 982    |  4.205% | 692    |  3.976% | 
 | 20   | Abomasnow          |  1.17210% | 2497   | 10.692% | 1966   | 11.296% | 
 | 21   | Sneasel            |  0.46297% | 230    |  0.985% | 160    |  0.919% | 
 | 22   | Dewgong            |  0.32737% | 477    |  2.043% | 352    |  2.022% | 
 | 23   | Beartic            |  0.16902% | 662    |  2.835% | 455    |  2.614% | 
 | 24   | Vanilluxe          |  0.07949% | 916    |  3.922% | 625    |  3.591% | 
 | 25   | Smoochum           |  0.05045% | 20     |  0.086% | 15     |  0.086% | 
 | 26   | Glalie             |  0.02888% | 379    |  1.623% | 290    |  1.666% | 
 | 27   | Sealeo             |  0.00850% | 31     |  0.133% | 25     |  0.144% | 
 | 28   | Delibird           |  0.00821% | 209    |  0.895% | 166    |  0.954% | 
 | 29   | Bergmite           |  0.00029% | 31     |  0.133% | 28     |  0.161% | 
 | 30   | Amaura             |  0.00013% | 35     |  0.150% | 29     |  0.167% | 
 | 31   | Snorunt            |  0.00005% | 1      |  0.004% | 1      |  0.006% | 
 | 32   | Snover             |  0.00000% | 16     |  0.069% | 14     |  0.080% | 
 | 33   | Vanillish          |  0.00000% | 5      |  0.021% | 3      |  0.017% | 
 | 34   | Swinub             |  0.00000% | 18     |  0.077% | 16     |  0.092% | 
 | 35   | Cubchoo            |  0.00000% | 9      |  0.039% | 6      |  0.034% | 
 | 36   | Spheal             |  0.00000% | 2      |  0.009% | 1      |  0.006% | 
 | 37   | Vanillite          |  0.00000% | 8      |  0.034% | 4      |  0.023% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
