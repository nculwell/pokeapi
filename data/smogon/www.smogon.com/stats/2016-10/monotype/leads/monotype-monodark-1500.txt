 Total leads: 630662
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Sableye            | 13.56814% | 5407   | 12.688% | 
 | 2    | Tyranitar          | 11.59901% | 4044   |  9.490% | 
 | 3    | Bisharp            |  8.13477% | 3186   |  7.476% | 
 | 4    | Weavile            |  7.52771% | 3434   |  8.058% | 
 | 5    | Hydreigon          |  6.89732% | 2703   |  6.343% | 
 | 6    | Krookodile         |  5.67486% | 2296   |  5.388% | 
 | 7    | Absol-Mega         |  5.39862% | 2407   |  5.648% | 
 | 8    | Drapion            |  4.03556% | 1929   |  4.527% | 
 | 9    | Hoopa-Unbound      |  3.59898% | 1192   |  2.797% | 
 | 10   | Mandibuzz          |  3.57367% | 1318   |  3.093% | 
 | 11   | Zoroark            |  3.51009% | 1797   |  4.217% | 
 | 12   | Tyranitar-Mega     |  3.41632% | 1509   |  3.541% | 
 | 13   | Liepard            |  2.77394% | 1276   |  2.994% | 
 | 14   | Houndoom-Mega      |  2.50641% | 1120   |  2.628% | 
 | 15   | Crawdaunt          |  2.40665% | 1006   |  2.361% | 
 | 16   | Umbreon            |  2.16857% | 1093   |  2.565% | 
 | 17   | Sharpedo-Mega      |  2.06136% | 871    |  2.044% | 
 | 18   | Spiritomb          |  1.84919% | 855    |  2.006% | 
 | 19   | Scrafty            |  1.66047% | 902    |  2.117% | 
 | 20   | Malamar            |  1.59575% | 888    |  2.084% | 
 | 21   | Honchkrow          |  1.43873% | 796    |  1.868% | 
 | 22   | Houndoom           |  0.90397% | 472    |  1.108% | 
 | 23   | Sharpedo           |  0.90177% | 476    |  1.117% | 
 | 24   | Pangoro            |  0.51481% | 277    |  0.650% | 
 | 25   | Murkrow            |  0.45336% | 259    |  0.608% | 
 | 26   | Cacturne           |  0.42257% | 248    |  0.582% | 
 | 27   | Shiftry            |  0.41447% | 245    |  0.575% | 
 | 28   | Skuntank           |  0.35760% | 182    |  0.427% | 
 | 29   | Absol              |  0.31174% | 229    |  0.537% | 
 | 30   | Mightyena          |  0.13247% | 89     |  0.209% | 
 | 31   | Inkay              |  0.06698% | 25     |  0.059% | 
 | 32   | Purrloin           |  0.02688% | 14     |  0.033% | 
 | 33   | Sneasel            |  0.02318% | 21     |  0.049% | 
 | 34   | Zorua              |  0.02043% | 10     |  0.023% | 
 | 35   | Scraggy            |  0.01645% | 11     |  0.026% | 
 | 36   | Pawniard           |  0.01512% | 10     |  0.023% | 
 | 37   | Vullaby            |  0.00764% | 6      |  0.014% | 
 | 38   | Poochyena          |  0.00340% | 3      |  0.007% | 
 | 39   | Carvanha           |  0.00335% | 2      |  0.005% | 
 | 40   | Krokorok           |  0.00283% | 1      |  0.002% | 
 | 41   | Zweilous           |  0.00212% | 2      |  0.005% | 
 | 42   | Sandile            |  0.00171% | 2      |  0.005% | 
 | 43   | Deino              |  0.00089% | 1      |  0.002% | 
 | 44   | Nuzleaf            |  0.00017% | 1      |  0.002% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
