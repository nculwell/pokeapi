 Total leads: 630662
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Lopunny-Mega       | 13.12207% | 5996   | 13.122% | 
 | 2    | Smeargle           | 10.44995% | 4775   | 10.450% | 
 | 3    | Chansey            |  6.84991% | 3130   |  6.850% | 
 | 4    | Ambipom            |  6.58730% | 3010   |  6.587% | 
 | 5    | Staraptor          |  5.06631% | 2315   |  5.066% | 
 | 6    | Porygon2           |  4.88248% | 2231   |  4.882% | 
 | 7    | Diggersby          |  3.79700% | 1735   |  3.797% | 
 | 8    | Porygon-Z          |  3.72915% | 1704   |  3.729% | 
 | 9    | Pidgeot-Mega       |  3.32210% | 1518   |  3.322% | 
 | 10   | Heliolisk          |  3.20392% | 1464   |  3.204% | 
 | 11   | Ditto              |  3.16234% | 1445   |  3.162% | 
 | 12   | Cinccino           |  2.86033% | 1307   |  2.860% | 
 | 13   | Snorlax            |  2.59334% | 1185   |  2.593% | 
 | 14   | Meloetta           |  2.34604% | 1072   |  2.346% | 
 | 15   | Miltank            |  2.34385% | 1071   |  2.344% | 
 | 16   | Kecleon            |  2.11844% | 968    |  2.118% | 
 | 17   | Exploud            |  1.96306% | 897    |  1.963% | 
 | 18   | Pyroar             |  1.85801% | 849    |  1.858% | 
 | 19   | Chatot             |  1.54944% | 708    |  1.549% | 
 | 20   | Blissey            |  1.12706% | 515    |  1.127% | 
 | 21   | Linoone            |  1.00889% | 461    |  1.009% | 
 | 22   | Tauros             |  1.00670% | 460    |  1.007% | 
 | 23   | Wigglytuff         |  0.91259% | 417    |  0.913% | 
 | 24   | Persian            |  0.89290% | 408    |  0.893% | 
 | 25   | Regigigas          |  0.85132% | 389    |  0.851% | 
 | 26   | Slaking            |  0.81849% | 374    |  0.818% | 
 | 27   | Dunsparce          |  0.77691% | 355    |  0.777% | 
 | 28   | Ursaring           |  0.75502% | 345    |  0.755% | 
 | 29   | Bibarel            |  0.73533% | 336    |  0.735% | 
 | 30   | Zangoose           |  0.66092% | 302    |  0.661% | 
 | 31   | Swellow            |  0.65435% | 299    |  0.654% | 
 | 32   | Kangaskhan         |  0.58432% | 267    |  0.584% | 
 | 33   | Lickilicky         |  0.57776% | 264    |  0.578% | 
 | 34   | Audino-Mega        |  0.52523% | 240    |  0.525% | 
 | 35   | Lopunny            |  0.49022% | 224    |  0.490% | 
 | 36   | Spinda             |  0.46396% | 212    |  0.464% | 
 | 37   | Furfrou            |  0.45301% | 207    |  0.453% | 
 | 38   | Sawsbuck           |  0.44864% | 205    |  0.449% | 
 | 39   | Rattata            |  0.43769% | 200    |  0.438% | 
 | 40   | Braviary           |  0.39174% | 179    |  0.392% | 
 | 41   | Stoutland          |  0.37204% | 170    |  0.372% | 
 | 42   | Raticate           |  0.35453% | 162    |  0.355% | 
 | 43   | Furret             |  0.34578% | 158    |  0.346% | 
 | 44   | Bouffalant         |  0.31733% | 145    |  0.317% | 
 | 45   | Girafarig          |  0.22104% | 101    |  0.221% | 
 | 46   | Dodrio             |  0.22104% | 101    |  0.221% | 
 | 47   | Purugly            |  0.19696% | 90     |  0.197% | 
 | 48   | Fearow             |  0.17070% | 78     |  0.171% | 
 | 49   | Castform           |  0.15319% | 70     |  0.153% | 
 | 50   | Eevee              |  0.11380% | 52     |  0.114% | 
 | 51   | Noctowl            |  0.11161% | 51     |  0.112% | 
 | 52   | Vigoroth           |  0.10942% | 50     |  0.109% | 
 | 53   | Delcatty           |  0.10724% | 49     |  0.107% | 
 | 54   | Unfezant           |  0.09629% | 44     |  0.096% | 
 | 55   | Stantler           |  0.09192% | 42     |  0.092% | 
 | 56   | Pidgeot            |  0.07003% | 32     |  0.070% | 
 | 57   | Lickitung          |  0.06347% | 29     |  0.063% | 
 | 58   | Farfetch'd         |  0.05690% | 26     |  0.057% | 
 | 59   | Audino             |  0.04596% | 21     |  0.046% | 
 | 60   | Aipom              |  0.04158% | 19     |  0.042% | 
 | 61   | Watchog            |  0.04158% | 19     |  0.042% | 
 | 62   | Munchlax           |  0.03283% | 15     |  0.033% | 
 | 63   | Meowth             |  0.03283% | 15     |  0.033% | 
 | 64   | Porygon            |  0.03064% | 14     |  0.031% | 
 | 65   | Staravia           |  0.03064% | 14     |  0.031% | 
 | 66   | Taillow            |  0.02407% | 11     |  0.024% | 
 | 67   | Starly             |  0.02188% | 10     |  0.022% | 
 | 68   | Bidoof             |  0.02188% | 10     |  0.022% | 
 | 69   | Doduo              |  0.01313% | 6      |  0.013% | 
 | 70   | Fletchling         |  0.01313% | 6      |  0.013% | 
 | 71   | Minccino           |  0.01313% | 6      |  0.013% | 
 | 72   | Pidgeotto          |  0.01094% | 5      |  0.011% | 
 | 73   | Buneary            |  0.01094% | 5      |  0.011% | 
 | 74   | Azurill            |  0.01094% | 5      |  0.011% | 
 | 75   | Rufflet            |  0.00657% | 3      |  0.007% | 
 | 76   | Glameow            |  0.00657% | 3      |  0.007% | 
 | 77   | Helioptile         |  0.00657% | 3      |  0.007% | 
 | 78   | Bunnelby           |  0.00657% | 3      |  0.007% | 
 | 79   | Igglybuff          |  0.00657% | 3      |  0.007% | 
 | 80   | Teddiursa          |  0.00438% | 2      |  0.004% | 
 | 81   | Sentret            |  0.00438% | 2      |  0.004% | 
 | 82   | Jigglypuff         |  0.00438% | 2      |  0.004% | 
 | 83   | Skitty             |  0.00219% | 1      |  0.002% | 
 | 84   | Swablu             |  0.00219% | 1      |  0.002% | 
 | 85   | Herdier            |  0.00219% | 1      |  0.002% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
