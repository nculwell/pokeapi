 Total leads: 630662
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Victini            | 18.56431% | 3617   |  8.798% | 
 | 2    | Mew                | 14.93530% | 3091   |  7.519% | 
 | 3    | Medicham-Mega      | 13.97820% | 2655   |  6.458% | 
 | 4    | Jirachi            |  9.82956% | 2546   |  6.193% | 
 | 5    | Hoopa-Unbound      |  7.22608% | 1094   |  2.661% | 
 | 6    | Deoxys-Speed       |  5.44126% | 1758   |  4.276% | 
 | 7    | Gardevoir-Mega     |  4.48693% | 1031   |  2.508% | 
 | 8    | Gardevoir          |  4.11366% | 737    |  1.793% | 
 | 9    | Latios             |  3.11161% | 710    |  1.727% | 
 | 10   | Slowbro            |  3.10323% | 1323   |  3.218% | 
 | 11   | Gallade-Mega       |  2.45742% | 762    |  1.854% | 
 | 12   | Azelf              |  2.05484% | 3045   |  7.407% | 
 | 13   | Bronzong           |  1.68470% | 1186   |  2.885% | 
 | 14   | Latias-Mega        |  1.04304% | 102    |  0.248% | 
 | 15   | Metagross          |  0.93381% | 1574   |  3.829% | 
 | 16   | Meloetta           |  0.89745% | 492    |  1.197% | 
 | 17   | Latias             |  0.70156% | 229    |  0.557% | 
 | 18   | Wobbuffet          |  0.68534% | 558    |  1.357% | 
 | 19   | Deoxys-Defense     |  0.63073% | 1232   |  2.997% | 
 | 20   | Latios-Mega        |  0.55366% | 98     |  0.238% | 
 | 21   | Medicham           |  0.52874% | 326    |  0.793% | 
 | 22   | Alakazam           |  0.48424% | 716    |  1.742% | 
 | 23   | Starmie            |  0.42516% | 906    |  2.204% | 
 | 24   | Cresselia          |  0.26953% | 227    |  0.552% | 
 | 25   | Gallade            |  0.26562% | 379    |  0.922% | 
 | 26   | Espeon             |  0.25252% | 1980   |  4.816% | 
 | 27   | Malamar            |  0.22322% | 1183   |  2.878% | 
 | 28   | Celebi             |  0.20576% | 466    |  1.134% | 
 | 29   | Uxie               |  0.15859% | 434    |  1.056% | 
 | 30   | Gothorita          |  0.14721% | 33     |  0.080% | 
 | 31   | Hoopa              |  0.13516% | 120    |  0.292% | 
 | 32   | Meowstic           |  0.08887% | 1565   |  3.807% | 
 | 33   | Alakazam-Mega      |  0.08260% | 494    |  1.202% | 
 | 34   | Gothitelle         |  0.06640% | 151    |  0.367% | 
 | 35   | Claydol            |  0.04754% | 460    |  1.119% | 
 | 36   | Sigilyph           |  0.03783% | 580    |  1.411% | 
 | 37   | Wynaut             |  0.02835% | 156    |  0.379% | 
 | 38   | Slowking           |  0.02564% | 270    |  0.657% | 
 | 39   | Delphox            |  0.02457% | 629    |  1.530% | 
 | 40   | Swoobat            |  0.01454% | 235    |  0.572% | 
 | 41   | Reuniclus          |  0.01226% | 289    |  0.703% | 
 | 42   | Exeggutor          |  0.01033% | 268    |  0.652% | 
 | 43   | Mr. Mime           |  0.00900% | 218    |  0.530% | 
 | 44   | Jynx               |  0.00579% | 145    |  0.353% | 
 | 45   | Xatu               |  0.00494% | 374    |  0.910% | 
 | 46   | Gothita            |  0.00444% | 4      |  0.010% | 
 | 47   | Solrock            |  0.00283% | 36     |  0.088% | 
 | 48   | Musharna           |  0.00206% | 89     |  0.216% | 
 | 49   | Grumpig            |  0.00135% | 62     |  0.151% | 
 | 50   | Lunatone           |  0.00102% | 38     |  0.092% | 
 | 51   | Hypno              |  0.00057% | 93     |  0.226% | 
 | 52   | Duosion            |  0.00033% | 3      |  0.007% | 
 | 53   | Metang             |  0.00020% | 6      |  0.015% | 
 | 54   | Unown              |  0.00005% | 12     |  0.029% | 
 | 55   | Chimecho           |  0.00001% | 13     |  0.032% | 
 | 56   | Beheeyem           |  0.00001% | 66     |  0.161% | 
 | 57   | Mesprit            |  0.00000% | 74     |  0.180% | 
 | 58   | Kadabra            |  0.00000% | 15     |  0.036% | 
 | 59   | Ralts              |  0.00000% | 13     |  0.032% | 
 | 60   | Chingling          |  0.00000% | 1      |  0.002% | 
 | 61   | Girafarig          |  0.00000% | 56     |  0.136% | 
 | 62   | Bronzor            |  0.00000% | 66     |  0.161% | 
 | 63   | Natu               |  0.00000% | 4      |  0.010% | 
 | 64   | Inkay              |  0.00000% | 1      |  0.002% | 
 | 65   | Meditite           |  0.00000% | 5      |  0.012% | 
 | 66   | Slowpoke           |  0.00000% | 2      |  0.005% | 
 | 67   | Spoink             |  0.00000% | 3      |  0.007% | 
 | 68   | Espurr             |  0.00000% | 1      |  0.002% | 
 | 69   | Solosis            |  0.00000% | 2      |  0.005% | 
 | 70   | Exeggcute          |  0.00000% | 1      |  0.002% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
