 Total leads: 630662
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Skarmory           | 15.28693% | 7649   | 18.703% | 
 | 2    | Landorus-Therian   | 14.58466% | 3248   |  7.942% | 
 | 3    | Zapdos             | 10.71562% | 1895   |  4.633% | 
 | 4    | Thundurus          |  7.29121% | 1823   |  4.457% | 
 | 5    | Charizard-Mega-Y   |  7.15402% | 2302   |  5.629% | 
 | 6    | Togekiss           |  6.94713% | 2212   |  5.409% | 
 | 7    | Aerodactyl         |  4.12803% | 936    |  2.289% | 
 | 8    | Thundurus-Therian  |  3.93759% | 1061   |  2.594% | 
 | 9    | Gliscor            |  3.89655% | 2741   |  6.702% | 
 | 10   | Landorus           |  3.84008% | 787    |  1.924% | 
 | 11   | Tornadus-Therian   |  3.67060% | 884    |  2.161% | 
 | 12   | Dragonite          |  2.65993% | 1411   |  3.450% | 
 | 13   | Gyarados-Mega      |  2.16201% | 658    |  1.609% | 
 | 14   | Pidgeot-Mega       |  1.91886% | 1235   |  3.020% | 
 | 15   | Staraptor          |  1.50533% | 928    |  2.269% | 
 | 16   | Hawlucha           |  1.40313% | 1429   |  3.494% | 
 | 17   | Gyarados           |  1.10137% | 944    |  2.308% | 
 | 18   | Aerodactyl-Mega    |  0.85645% | 518    |  1.267% | 
 | 19   | Archeops           |  0.78156% | 297    |  0.726% | 
 | 20   | Crobat             |  0.60179% | 706    |  1.726% | 
 | 21   | Salamence          |  0.51040% | 349    |  0.853% | 
 | 22   | Xatu               |  0.49220% | 400    |  0.978% | 
 | 23   | Vivillon           |  0.48312% | 361    |  0.883% | 
 | 24   | Mandibuzz          |  0.46701% | 268    |  0.655% | 
 | 25   | Yanmega            |  0.37724% | 248    |  0.606% | 
 | 26   | Honchkrow          |  0.37484% | 331    |  0.809% | 
 | 27   | Sigilyph           |  0.34050% | 488    |  1.193% | 
 | 28   | Articuno           |  0.31403% | 268    |  0.655% | 
 | 29   | Chatot             |  0.30907% | 623    |  1.523% | 
 | 30   | Emolga             |  0.25558% | 211    |  0.516% | 
 | 31   | Noivern            |  0.24705% | 395    |  0.966% | 
 | 32   | Masquerain         |  0.17602% | 153    |  0.374% | 
 | 33   | Moltres            |  0.13937% | 206    |  0.504% | 
 | 34   | Scyther            |  0.11515% | 214    |  0.523% | 
 | 35   | Swellow            |  0.11262% | 213    |  0.521% | 
 | 36   | Gligar             |  0.09155% | 139    |  0.340% | 
 | 37   | Tornadus           |  0.07961% | 99     |  0.242% | 
 | 38   | Murkrow            |  0.07759% | 145    |  0.355% | 
 | 39   | Togetic            |  0.06206% | 75     |  0.183% | 
 | 40   | Ninjask            |  0.05325% | 190    |  0.465% | 
 | 41   | Mantine            |  0.05127% | 97     |  0.237% | 
 | 42   | Fletchinder        |  0.04570% | 37     |  0.090% | 
 | 43   | Mantyke            |  0.04464% | 10     |  0.024% | 
 | 44   | Drifblim           |  0.04107% | 235    |  0.575% | 
 | 45   | Fearow             |  0.03239% | 64     |  0.156% | 
 | 46   | Charizard          |  0.02946% | 176    |  0.430% | 
 | 47   | Jumpluff           |  0.02876% | 72     |  0.176% | 
 | 48   | Braviary           |  0.02395% | 78     |  0.191% | 
 | 49   | Rotom-Fan          |  0.02261% | 100    |  0.245% | 
 | 50   | Farfetch'd         |  0.02170% | 62     |  0.152% | 
 | 51   | Golbat             |  0.01803% | 43     |  0.105% | 
 | 52   | Swoobat            |  0.01798% | 141    |  0.345% | 
 | 53   | Unfezant           |  0.01637% | 76     |  0.186% | 
 | 54   | Vespiquen          |  0.01505% | 63     |  0.154% | 
 | 55   | Noctowl            |  0.01150% | 98     |  0.240% | 
 | 56   | Delibird           |  0.01124% | 48     |  0.117% | 
 | 57   | Beautifly          |  0.00934% | 26     |  0.064% | 
 | 58   | Swanna             |  0.00826% | 82     |  0.200% | 
 | 59   | Tropius            |  0.00792% | 62     |  0.152% | 
 | 60   | Altaria            |  0.00766% | 48     |  0.117% | 
 | 61   | Butterfree         |  0.00602% | 81     |  0.198% | 
 | 62   | Ledian             |  0.00425% | 18     |  0.044% | 
 | 63   | Dodrio             |  0.00149% | 60     |  0.147% | 
 | 64   | Fletchling         |  0.00124% | 2      |  0.005% | 
 | 65   | Natu               |  0.00055% | 1      |  0.002% | 
 | 66   | Noibat             |  0.00028% | 3      |  0.007% | 
 | 67   | Pidgeot            |  0.00007% | 14     |  0.034% | 
 | 68   | Doduo              |  0.00007% | 9      |  0.022% | 
 | 69   | Pelipper           |  0.00004% | 20     |  0.049% | 
 | 70   | Taillow            |  0.00000% | 6      |  0.015% | 
 | 71   | Ducklett           |  0.00000% | 2      |  0.005% | 
 | 72   | Woobat             |  0.00000% | 4      |  0.010% | 
 | 73   | Mothim             |  0.00000% | 2      |  0.005% | 
 | 74   | Yanma              |  0.00000% | 1      |  0.002% | 
 | 75   | Drifloon           |  0.00000% | 1      |  0.002% | 
 | 76   | Staravia           |  0.00000% | 3      |  0.007% | 
 | 77   | Archen             |  0.00000% | 1      |  0.002% | 
 | 78   | Starly             |  0.00000% | 5      |  0.012% | 
 | 79   | Swablu             |  0.00000% | 3      |  0.007% | 
 | 80   | Hoothoot           |  0.00000% | 1      |  0.002% | 
 | 81   | Wingull            |  0.00000% | 1      |  0.002% | 
 | 82   | Zubat              |  0.00000% | 2      |  0.005% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
