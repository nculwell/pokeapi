 Total leads: 630662
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Landorus           | 19.21624% | 1334   |  5.195% | 
 | 2    | Hippowdon          | 18.74113% | 8883   | 34.595% | 
 | 3    | Mamoswine          | 12.45020% | 1548   |  6.029% | 
 | 4    | Garchomp-Mega      | 11.00073% | 942    |  3.669% | 
 | 5    | Nidoking           |  5.15433% | 1150   |  4.479% | 
 | 6    | Seismitoad         |  4.92875% | 604    |  2.352% | 
 | 7    | Gliscor            |  4.34421% | 1442   |  5.616% | 
 | 8    | Garchomp           |  3.79234% | 934    |  3.637% | 
 | 9    | Landorus-Therian   |  3.54594% | 872    |  3.396% | 
 | 10   | Diggersby          |  2.64576% | 408    |  1.589% | 
 | 11   | Gastrodon          |  2.58337% | 555    |  2.161% | 
 | 12   | Camerupt-Mega      |  2.42417% | 524    |  2.041% | 
 | 13   | Krookodile         |  2.04933% | 570    |  2.220% | 
 | 14   | Excadrill          |  1.79304% | 776    |  3.022% | 
 | 15   | Nidoqueen          |  1.32929% | 420    |  1.636% | 
 | 16   | Dugtrio            |  1.01010% | 282    |  1.098% | 
 | 17   | Swampert-Mega      |  0.75004% | 314    |  1.223% | 
 | 18   | Zygarde            |  0.46437% | 92     |  0.358% | 
 | 19   | Steelix-Mega       |  0.34198% | 407    |  1.585% | 
 | 20   | Torterra           |  0.32811% | 267    |  1.040% | 
 | 21   | Donphan            |  0.25106% | 357    |  1.390% | 
 | 22   | Piloswine          |  0.20505% | 61     |  0.238% | 
 | 23   | Golem              |  0.19388% | 441    |  1.717% | 
 | 24   | Golurk             |  0.18479% | 367    |  1.429% | 
 | 25   | Claydol            |  0.09389% | 309    |  1.203% | 
 | 26   | Quagsire           |  0.04910% | 247    |  0.962% | 
 | 27   | Flygon             |  0.03231% | 186    |  0.724% | 
 | 28   | Swampert           |  0.01461% | 207    |  0.806% | 
 | 29   | Rhyperior          |  0.01442% | 230    |  0.896% | 
 | 30   | Steelix            |  0.01254% | 128    |  0.499% | 
 | 31   | Stunfisk           |  0.01040% | 174    |  0.678% | 
 | 32   | Gligar             |  0.00946% | 47     |  0.183% | 
 | 33   | Graveler           |  0.00912% | 9      |  0.035% | 
 | 34   | Camerupt           |  0.00720% | 111    |  0.432% | 
 | 35   | Sandslash          |  0.00711% | 123    |  0.479% | 
 | 36   | Marowak            |  0.00424% | 117    |  0.456% | 
 | 37   | Onix               |  0.00350% | 24     |  0.093% | 
 | 38   | Rhydon             |  0.00260% | 60     |  0.234% | 
 | 39   | Wormadam-Sandy     |  0.00091% | 35     |  0.136% | 
 | 40   | Marshtomp          |  0.00040% | 7      |  0.027% | 
 | 41   | Swinub             |  0.00001% | 2      |  0.008% | 
 | 42   | Hippopotas         |  0.00000% | 38     |  0.148% | 
 | 43   | Numel              |  0.00000% | 14     |  0.055% | 
 | 44   | Whiscash           |  0.00000% | 29     |  0.113% | 
 | 45   | Palpitoad          |  0.00000% | 8      |  0.031% | 
 | 46   | Larvitar           |  0.00000% | 4      |  0.016% | 
 | 47   | Cubone             |  0.00000% | 5      |  0.019% | 
 | 48   | Trapinch           |  0.00000% | 2      |  0.008% | 
 | 49   | Pupitar            |  0.00000% | 2      |  0.008% | 
 | 50   | Diglett            |  0.00000% | 4      |  0.016% | 
 | 51   | Sandshrew          |  0.00000% | 1      |  0.004% | 
 | 52   | Rhyhorn            |  0.00000% | 1      |  0.004% | 
 | 53   | Wooper             |  0.00000% | 1      |  0.004% | 
 | 54   | Vibrava            |  0.00000% | 2      |  0.008% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
