 weatherless...................74.58439%
 offense.......................47.46094%
 balance.......................33.83493%
 sand..........................12.26328%
 sun...........................10.90392%
 hyperoffense..................10.21431%
 semistall..................... 7.15810%
 voltturn...................... 5.80035%
 rain.......................... 3.39517%
 sunoffense.................... 2.25963%
 sandoffense................... 1.45409%
 stall......................... 1.33172%
 multiweather.................. 1.14744%
 trickroom..................... 1.07519%
 rainoffense................... 1.00987%
 sandstall..................... 0.75414%
 monotype...................... 0.18278%
 monofairy..................... 0.10546%
 mononormal.................... 0.05304%
 dragmag....................... 0.03453%
 monobug....................... 0.01160%
 monoelectric.................. 0.01015%
 monowater..................... 0.00216%
 hail.......................... 0.00069%
 monoflying.................... 0.00041%
 choice........................ 0.00002%
 rainstall..................... 0.00001%
 sunstall...................... 0.00000%
 monodark...................... 0.00000%
 monograss..................... 0.00000%
 monopoison.................... 0.00000%
 tailwind...................... 0.00000%
 monopsychic................... 0.00000%
 monosteel..................... 0.00000%
 monofighting.................. 0.00000%
 monorock...................... 0.00000%
 monoghost..................... 0.00000%
 monofire...................... 0.00000%
 trapper....................... 0.00000%
 monoground.................... 0.00000%
 hailoffense................... 0.00000%
 monodragon.................... 0.00000%

 Stalliness (mean: -0.117)
     |##
 -1.5|#
     |##########
 -1.0|#########
     |###############
 -0.5|##############################
     |#######################
  0.0|#####################
     |##################
 +0.5|###############
     |###########
 +1.0|#########
     |#
 +1.5|####
     |##
 +2.0|
 more negative = more offensive, more positive = more stall
 one # =  0.58%
