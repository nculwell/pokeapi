 weatherless...................91.89385%
 hyperoffense..................54.17341%
 voltturn......................35.59846%
 offense.......................35.46758%
 batonpass.....................30.75699%
 balance.......................10.35901%
 sand.......................... 8.10615%

 Stalliness (mean: -0.963)
 -2.0|###
     |#
 -1.5|##
     |##
 -1.0|##############################
     |#
 -0.5|#
     |##
  0.0|###
     |
 +0.5|
 more negative = more offensive, more positive = more stall
 one # =  2.11%
