 weatherless...................100.00000%
 balance.......................62.81228%
 offense.......................37.18772%

 Stalliness (mean: -0.054)
     |##############################
 -0.2|
     |
 -0.1|
     |
  0.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  1.24%
