 weatherless...................88.55699%
 offense.......................44.18646%
 balance.......................35.94742%
 hyperoffense..................10.65099%
 sand.......................... 6.47591%
 voltturn...................... 5.47806%
 semistall..................... 5.45441%
 stall......................... 3.76073%
 sun........................... 2.64105%
 monotype...................... 2.38577%
 rain.......................... 1.63597%
 hail.......................... 0.85236%
 sandoffense................... 0.82120%
 trickroom..................... 0.81850%
 sunoffense.................... 0.39729%
 monodark...................... 0.39152%
 rainoffense................... 0.38783%
 choice........................ 0.37943%
 monoground.................... 0.34248%
 mononormal.................... 0.33229%
 monowater..................... 0.25444%
 monofairy..................... 0.20376%
 monograss..................... 0.18522%
 monorock...................... 0.16877%
 multiweather.................. 0.16227%
 monofighting.................. 0.14684%
 monoice....................... 0.14334%
 monofire...................... 0.09523%
 dragmag....................... 0.07728%
 monoflying.................... 0.07230%
 monosteel..................... 0.07083%
 monoelectric.................. 0.05982%
 tailwind...................... 0.03996%
 monodragon.................... 0.02662%
 monopsychic................... 0.02662%

 Stalliness (mean: -0.009)
 -2.0|##
     |#
 -1.5|#######
     |########
 -1.0|##############
     |#######################
 -0.5|########################
     |##############################
  0.0|###########################
     |########################
 +0.5|#####################
     |##############
 +1.0|#############
     |#####
 +1.5|######
     |##
 +2.0|
     |
 +2.5|#
     |#
 +3.0|##
 more negative = more offensive, more positive = more stall
 one # =  0.44%
