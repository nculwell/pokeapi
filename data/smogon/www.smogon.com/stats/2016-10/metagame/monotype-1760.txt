 monotype......................100.00000%
 weatherless...................76.57959%
 offense.......................39.86555%
 balance.......................34.91120%
 hyperoffense..................19.67179%
 monosteel.....................12.60170%
 monopsychic...................11.64729%
 sand..........................10.60273%
 monoflying.................... 8.98146%
 monowater..................... 8.53055%
 monofighting.................. 7.96791%
 sun........................... 7.96555%
 monodragon.................... 7.94461%
 monobug....................... 6.15576%
 choice........................ 6.10187%
 monoground.................... 6.01617%
 mononormal.................... 5.78118%
 voltturn...................... 4.89083%
 monodark...................... 4.67622%
 rain.......................... 3.85637%
 semistall..................... 3.67805%
 monofairy..................... 3.63385%
 monoelectric.................. 3.31322%
 trickroom..................... 3.20630%
 monofire...................... 3.13436%
 monoghost..................... 2.31891%
 monopoison.................... 2.21253%
 stall......................... 1.87342%
 monorock...................... 1.86704%
 monograss..................... 1.75931%
 monoice....................... 1.45871%
 hail.......................... 0.99887%
 sunoffense.................... 0.75186%
 rainoffense................... 0.51905%
 sandoffense................... 0.39517%
 tailwind...................... 0.36563%
 trapper....................... 0.12439%
 sunstall...................... 0.05732%
 hailoffense................... 0.04341%
 dragmag....................... 0.02894%
 sandstall..................... 0.01774%
 multiweather.................. 0.00311%
 rainstall..................... 0.00182%
 gravity....................... 0.00115%
 hailstall..................... 0.00091%
 tricksand..................... 0.00088%
 tricksun...................... 0.00038%
 trickhail..................... 0.00000%

 Stalliness (mean: -0.213)
 -2.0|##
     |####
 -1.5|#########
     |#################
 -1.0|###################
     |###########
 -0.5|###################
     |#########################
  0.0|##############################
     |#######################
 +0.5|################
     |############
 +1.0|######
     |####
 +1.5|####
     |#
 +2.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  0.49%
