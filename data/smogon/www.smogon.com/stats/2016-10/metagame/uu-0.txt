 weatherless...................95.08427%
 offense.......................43.90458%
 balance.......................38.19425%
 hyperoffense..................10.42836%
 voltturn...................... 5.99543%
 semistall..................... 5.18482%
 stall......................... 2.28799%
 hail.......................... 2.12644%
 rain.......................... 2.01881%
 monotype...................... 1.36032%
 sun........................... 0.61542%
 trickroom..................... 0.60469%
 choice........................ 0.59986%
 rainoffense................... 0.22664%
 sand.......................... 0.18160%
 monowater..................... 0.17791%
 monobug....................... 0.17478%
 monopoison.................... 0.16249%
 monofighting.................. 0.13997%
 monodark...................... 0.13124%
 monorock...................... 0.10875%
 mononormal.................... 0.10389%
 monoice....................... 0.09481%
 hailoffense................... 0.08714%
 dragmag....................... 0.08195%
 sunoffense.................... 0.08125%
 monoghost..................... 0.07169%
 trickhail..................... 0.06828%
 tailwind...................... 0.05412%
 monofire...................... 0.05401%
 monopsychic................... 0.04717%
 monograss..................... 0.04174%
 monosteel..................... 0.03915%
 monoelectric.................. 0.03526%
 monoflying.................... 0.03361%
 hailstall..................... 0.03302%
 monoground.................... 0.02995%
 monofairy..................... 0.02700%
 multiweather.................. 0.02606%
 monodragon.................... 0.01615%
 rainstall..................... 0.01215%
 sandstall..................... 0.00814%
 gravity....................... 0.00389%
 sandoffense................... 0.00342%
 tricksand..................... 0.00224%
 trapper....................... 0.00094%
 trickrain..................... 0.00071%
 tricksun...................... 0.00071%
 sunstall...................... 0.00012%

 Stalliness (mean: -0.061)
 -2.0|#
     |##
 -1.5|####
     |#########
 -1.0|##############
     |###################
 -0.5|#########################
     |#############################
  0.0|##############################
     |###########################
 +0.5|######################
     |##############
 +1.0|#########
     |#####
 +1.5|###
     |##
 +2.0|#
     |#
 +2.5|
     |#
 +3.0|
 more negative = more offensive, more positive = more stall
 one # =  0.46%
