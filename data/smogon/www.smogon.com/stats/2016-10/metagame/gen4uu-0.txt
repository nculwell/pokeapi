 weatherless...................94.69697%
 offense.......................50.00000%
 balance.......................32.57576%
 hyperoffense.................. 9.84848%
 semistall..................... 4.54545%
 stall......................... 3.03030%
 sun........................... 2.27273%
 hail.......................... 1.51515%
 sunoffense.................... 1.51515%
 rain.......................... 1.51515%

 Stalliness (mean: -0.110)
 -1.5|####
     |###
 -1.0|###########
     |###############
 -0.5|###################
     |##############################
  0.0|########
     |################
 +0.5|############
     |###########
 +1.0|######
     |#
 +1.5|#
 more negative = more offensive, more positive = more stall
 one # =  0.71%
