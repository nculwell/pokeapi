 Total battles: 94764
 Avg. weight/team: 0.565
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Abra               | 28.60395% | 51275  | 27.054% | 35358  | 24.822% | 
 | 2    | Mienfoo            | 26.96690% | 42732  | 22.547% | 34496  | 24.217% | 
 | 3    | Pawniard           | 20.41175% | 32454  | 17.124% | 24564  | 17.245% | 
 | 4    | Fletchling         | 17.45817% | 27895  | 14.718% | 20663  | 14.506% | 
 | 5    | Chinchou           | 16.29444% | 26405  | 13.932% | 20752  | 14.568% | 
 | 6    | Gastly             | 12.79791% | 23772  | 12.543% | 17033  | 11.958% | 
 | 7    | Archen             | 11.99343% | 21411  | 11.297% | 15582  | 10.939% | 
 | 8    | Aipom              | 11.92466% | 24049  | 12.689% | 19795  | 13.897% | 
 | 9    | Diglett            | 11.65425% | 18911  |  9.978% | 14442  | 10.139% | 
 | 10   | Ponyta             | 11.14878% | 19484  | 10.280% | 14520  | 10.193% | 
 | 11   | Magnemite          | 10.53325% | 17652  |  9.314% | 13313  |  9.346% | 
 | 12   | Ferroseed          | 10.34374% | 18354  |  9.684% | 15819  | 11.105% | 
 | 13   | Carvanha           |  9.86407% | 18005  |  9.500% | 13381  |  9.394% | 
 | 14   | Vullaby            |  8.95058% | 13839  |  7.302% | 10865  |  7.627% | 
 | 15   | Porygon            |  8.71924% | 14831  |  7.825% | 10729  |  7.532% | 
 | 16   | Drilbur            |  8.63995% | 13687  |  7.222% | 10373  |  7.282% | 
 | 17   | Timburr            |  8.60883% | 13282  |  7.008% | 10314  |  7.241% | 
 | 18   | Onix               |  8.38558% | 15518  |  8.188% | 12897  |  9.054% | 
 | 19   | Staryu             |  8.30283% | 13595  |  7.173% | 10158  |  7.131% | 
 | 20   | Cottonee           |  7.96470% | 13763  |  7.262% | 10918  |  7.665% | 
 | 21   | Foongus            |  7.88782% | 12261  |  6.469% | 9488   |  6.661% | 
 | 22   | Scraggy            |  7.81675% | 13908  |  7.338% | 10589  |  7.434% | 
 | 23   | Croagunk           |  6.95060% | 12204  |  6.439% | 9652   |  6.776% | 
 | 24   | Spritzee           |  6.75393% | 10941  |  5.773% | 7357   |  5.165% | 
 | 25   | Snivy              |  6.23111% | 12405  |  6.545% | 9029   |  6.339% | 
 | 26   | Shellder           |  6.06037% | 11195  |  5.907% | 8285   |  5.816% | 
 | 27   | Dwebble            |  5.74787% | 9645   |  5.089% | 7942   |  5.575% | 
 | 28   | Munchlax           |  5.65227% | 11832  |  6.243% | 8353   |  5.864% | 
 | 29   | Elekid             |  5.52687% | 11434  |  6.033% | 8328   |  5.846% | 
 | 30   | Tirtouga           |  5.50688% | 8975   |  4.735% | 6573   |  4.614% | 
 | 31   | Meowth             |  5.35478% | 9207   |  4.858% | 7719   |  5.419% | 
 | 32   | Honedge            |  4.69973% | 8915   |  4.704% | 6906   |  4.848% | 
 | 33   | Vulpix             |  4.69944% | 8701   |  4.591% | 7232   |  5.077% | 
 | 34   | Cranidos           |  4.63167% | 9612   |  5.072% | 6630   |  4.654% | 
 | 35   | Torchic            |  4.55737% | 9804   |  5.173% | 7916   |  5.557% | 
 | 36   | Larvesta           |  4.38925% | 7455   |  3.933% | 5515   |  3.872% | 
 | 37   | Zigzagoon          |  4.37095% | 7434   |  3.922% | 5262   |  3.694% | 
 | 38   | Houndour           |  4.19531% | 7668   |  4.046% | 5653   |  3.969% | 
 | 39   | Froakie            |  3.92699% | 8785   |  4.635% | 6607   |  4.638% | 
 | 40   | Bunnelby           |  3.90182% | 7210   |  3.804% | 5358   |  3.761% | 
 | 41   | Snubbull           |  3.90138% | 6353   |  3.352% | 4932   |  3.462% | 
 | 42   | Amaura             |  3.77388% | 8062   |  4.254% | 5726   |  4.020% | 
 | 43   | Gothita            |  3.72477% | 5606   |  2.958% | 4029   |  2.828% | 
 | 44   | Aron               |  3.71452% | 8458   |  4.463% | 6560   |  4.605% | 
 | 45   | Riolu              |  3.63806% | 7624   |  4.023% | 5677   |  3.985% | 
 | 46   | Cubone             |  3.62706% | 7940   |  4.189% | 5888   |  4.134% | 
 | 47   | Axew               |  3.59184% | 8389   |  4.426% | 5892   |  4.136% | 
 | 48   | Omanyte            |  3.42630% | 6166   |  3.253% | 4653   |  3.267% | 
 | 49   | Bulbasaur          |  3.32605% | 7890   |  4.163% | 5932   |  4.164% | 
 | 50   | Anorith            |  3.31618% | 5964   |  3.147% | 4820   |  3.384% | 
 | 51   | Eevee              |  3.29227% | 7336   |  3.871% | 5138   |  3.607% | 
 | 52   | Corphish           |  3.21530% | 5960   |  3.145% | 4462   |  3.132% | 
 | 53   | Stunky             |  3.13529% | 4984   |  2.630% | 3792   |  2.662% | 
 | 54   | Growlithe          |  2.96025% | 6203   |  3.273% | 4613   |  3.238% | 
 | 55   | Tentacool          |  2.95739% | 5375   |  2.836% | 4157   |  2.918% | 
 | 56   | Bellsprout         |  2.88945% | 5313   |  2.803% | 3979   |  2.793% | 
 | 57   | Lileep             |  2.87070% | 5149   |  2.717% | 4092   |  2.873% | 
 | 58   | Tyrunt             |  2.84332% | 5988   |  3.159% | 4248   |  2.982% | 
 | 59   | Machop             |  2.82859% | 5274   |  2.783% | 4133   |  2.901% | 
 | 60   | Clamperl           |  2.74536% | 5349   |  2.822% | 3899   |  2.737% | 
 | 61   | Dratini            |  2.69020% | 5921   |  3.124% | 4252   |  2.985% | 
 | 62   | Slowpoke           |  2.64138% | 4701   |  2.480% | 3639   |  2.555% | 
 | 63   | Natu               |  2.63521% | 4993   |  2.634% | 3796   |  2.665% | 
 | 64   | Surskit            |  2.62949% | 4585   |  2.419% | 4207   |  2.953% | 
 | 65   | Chimchar           |  2.55670% | 6032   |  3.183% | 4868   |  3.417% | 
 | 66   | Frillish           |  2.55606% | 4930   |  2.601% | 3802   |  2.669% | 
 | 67   | Inkay              |  2.54077% | 5474   |  2.888% | 4108   |  2.884% | 
 | 68   | Taillow            |  2.46426% | 4775   |  2.519% | 3460   |  2.429% | 
 | 69   | Skrelp             |  2.45157% | 3995   |  2.108% | 3015   |  2.117% | 
 | 70   | Bronzor            |  2.45128% | 5246   |  2.768% | 4155   |  2.917% | 
 | 71   | Koffing            |  2.43548% | 4677   |  2.468% | 3735   |  2.622% | 
 | 72   | Duskull            |  2.43218% | 5573   |  2.940% | 4201   |  2.949% | 
 | 73   | Zorua              |  2.42451% | 5648   |  2.980% | 2965   |  2.082% | 
 | 74   | Hippopotas         |  2.39785% | 4365   |  2.303% | 3742   |  2.627% | 
 | 75   | Pumpkaboo-Super    |  2.33960% | 3970   |  2.095% | 3130   |  2.197% | 
 | 76   | Charmander         |  2.28953% | 5940   |  3.134% | 4291   |  3.012% | 
 | 77   | Mantyke            |  2.26445% | 4591   |  2.422% | 3485   |  2.447% | 
 | 78   | Pancham            |  2.19786% | 4007   |  2.114% | 3079   |  2.162% | 
 | 79   | Purrloin           |  2.18931% | 4256   |  2.246% | 3507   |  2.462% | 
 | 80   | Buizel             |  2.13383% | 5071   |  2.676% | 3937   |  2.764% | 
 | 81   | Lickitung          |  2.13163% | 4458   |  2.352% | 3302   |  2.318% | 
 | 82   | Magby              |  2.09573% | 4246   |  2.240% | 3136   |  2.202% | 
 | 83   | Voltorb            |  1.91492% | 4168   |  2.199% | 3291   |  2.310% | 
 | 84   | Pineco             |  1.91155% | 3474   |  1.833% | 3132   |  2.199% | 
 | 85   | Darumaka           |  1.88866% | 4080   |  2.153% | 3026   |  2.124% | 
 | 86   | Wailmer            |  1.87683% | 4078   |  2.152% | 3062   |  2.150% | 
 | 87   | Doduo              |  1.86601% | 3485   |  1.839% | 2493   |  1.750% | 
 | 88   | Geodude            |  1.84979% | 3678   |  1.941% | 3131   |  2.198% | 
 | 89   | Buneary            |  1.82758% | 4145   |  2.187% | 3326   |  2.335% | 
 | 90   | Squirtle           |  1.75341% | 4129   |  2.179% | 3244   |  2.277% | 
 | 91   | Blitzle            |  1.69977% | 4163   |  2.197% | 2978   |  2.091% | 
 | 92   | Sandshrew          |  1.65437% | 2943   |  1.553% | 2449   |  1.719% | 
 | 93   | Treecko            |  1.57743% | 3785   |  1.997% | 2817   |  1.978% | 
 | 94   | Mankey             |  1.52109% | 3058   |  1.613% | 2322   |  1.630% | 
 | 95   | Bagon              |  1.50546% | 3690   |  1.947% | 2536   |  1.780% | 
 | 96   | Baltoy             |  1.43905% | 3169   |  1.672% | 2448   |  1.719% | 
 | 97   | Cacnea             |  1.42907% | 3202   |  1.689% | 2475   |  1.738% | 
 | 98   | Mime Jr.           |  1.41172% | 3046   |  1.607% | 2297   |  1.613% | 
 | 99   | Cyndaquil          |  1.41078% | 3448   |  1.819% | 2531   |  1.777% | 
 | 100  | Totodile           |  1.40528% | 3213   |  1.695% | 2317   |  1.627% | 
 | 101  | Phantump           |  1.40338% | 3481   |  1.837% | 2598   |  1.824% | 
 | 102  | Minccino           |  1.39328% | 2991   |  1.578% | 2171   |  1.524% | 
 | 103  | Trubbish           |  1.35494% | 2571   |  1.357% | 1962   |  1.377% | 
 | 104  | Gible              |  1.33551% | 3436   |  1.813% | 2547   |  1.788% | 
 | 105  | Numel              |  1.33444% | 2612   |  1.378% | 1982   |  1.391% | 
 | 106  | Krabby             |  1.32800% | 2806   |  1.481% | 2024   |  1.421% | 
 | 107  | Grimer             |  1.31009% | 2645   |  1.396% | 2030   |  1.425% | 
 | 108  | Sandile            |  1.29036% | 2611   |  1.378% | 1927   |  1.353% | 
 | 109  | Spinarak           |  1.25657% | 2635   |  1.390% | 2373   |  1.666% | 
 | 110  | Nosepass           |  1.19796% | 2629   |  1.387% | 2171   |  1.524% | 
 | 111  | Clauncher          |  1.19521% | 2981   |  1.573% | 2199   |  1.544% | 
 | 112  | Bidoof             |  1.18009% | 2776   |  1.465% | 2102   |  1.476% | 
 | 113  | Chespin            |  1.15139% | 2783   |  1.468% | 2106   |  1.478% | 
 | 114  | Snover             |  1.15124% | 2285   |  1.206% | 1745   |  1.225% | 
 | 115  | Noibat             |  1.11879% | 2669   |  1.408% | 1984   |  1.393% | 
 | 116  | Yamask             |  1.11166% | 2247   |  1.186% | 1857   |  1.304% | 
 | 117  | Joltik             |  1.10468% | 2460   |  1.298% | 1875   |  1.316% | 
 | 118  | Litwick            |  1.10000% | 2670   |  1.409% | 1982   |  1.391% | 
 | 119  | Togepi             |  1.08774% | 2592   |  1.368% | 1828   |  1.283% | 
 | 120  | Wooper             |  1.07940% | 2261   |  1.193% | 1833   |  1.287% | 
 | 121  | Phanpy             |  1.07654% | 1966   |  1.037% | 1703   |  1.196% | 
 | 122  | Helioptile         |  1.05121% | 2218   |  1.170% | 1587   |  1.114% | 
 | 123  | Wynaut             |  1.04605% | 2186   |  1.153% | 1688   |  1.185% | 
 | 124  | Golett             |  1.04462% | 2244   |  1.184% | 1723   |  1.210% | 
 | 125  | Fennekin           |  1.03992% | 2559   |  1.350% | 1850   |  1.299% | 
 | 126  | Rufflet            |  1.02887% | 1950   |  1.029% | 1417   |  0.995% | 
 | 127  | Rattata            |  1.01983% | 2565   |  1.353% | 1969   |  1.382% | 
 | 128  | Litleo             |  0.97447% | 2332   |  1.230% | 1613   |  1.132% | 
 | 129  | Deerling           |  0.96354% | 2189   |  1.155% | 1658   |  1.164% | 
 | 130  | Deino              |  0.94877% | 2299   |  1.213% | 1607   |  1.128% | 
 | 131  | Solosis            |  0.94313% | 2136   |  1.127% | 1485   |  1.043% | 
 | 132  | Exeggcute          |  0.92759% | 2285   |  1.206% | 1694   |  1.189% | 
 | 133  | Drowzee            |  0.92564% | 2050   |  1.082% | 1548   |  1.087% | 
 | 134  | Shieldon           |  0.90792% | 2125   |  1.121% | 1727   |  1.212% | 
 | 135  | Azurill            |  0.86233% | 2213   |  1.168% | 1655   |  1.162% | 
 | 136  | Bergmite           |  0.85255% | 2131   |  1.124% | 1469   |  1.031% | 
 | 137  | Trapinch           |  0.85153% | 1779   |  0.939% | 1325   |  0.930% | 
 | 138  | Woobat             |  0.84100% | 1833   |  0.967% | 1377   |  0.967% | 
 | 139  | Kabuto             |  0.83853% | 1669   |  0.881% | 1336   |  0.938% | 
 | 140  | Ekans              |  0.83793% | 1829   |  0.965% | 1378   |  0.967% | 
 | 141  | Bonsly             |  0.83076% | 2061   |  1.087% | 1658   |  1.164% | 
 | 142  | Goomy              |  0.82337% | 2042   |  1.077% | 1434   |  1.007% | 
 | 143  | Shinx              |  0.79534% | 1965   |  1.037% | 1468   |  1.031% | 
 | 144  | Teddiursa          |  0.78867% | 1627   |  0.858% | 1219   |  0.856% | 
 | 145  | Skorupi            |  0.77872% | 1542   |  0.814% | 1232   |  0.865% | 
 | 146  | Barboach           |  0.77672% | 1850   |  0.976% | 1353   |  0.950% | 
 | 147  | Larvitar           |  0.76746% | 1656   |  0.874% | 1177   |  0.826% | 
 | 148  | Chikorita          |  0.74540% | 2069   |  1.092% | 1515   |  1.064% | 
 | 149  | Skiddo             |  0.74310% | 1640   |  0.865% | 1188   |  0.834% | 
 | 150  | Goldeen            |  0.73891% | 1493   |  0.788% | 1109   |  0.779% | 
 | 151  | Piplup             |  0.72629% | 1810   |  0.955% | 1443   |  1.013% | 
 | 152  | Beldum             |  0.72445% | 1981   |  1.045% | 1474   |  1.035% | 
 | 153  | Sewaddle           |  0.72337% | 1526   |  0.805% | 1324   |  0.929% | 
 | 154  | Rhyhorn            |  0.71179% | 1742   |  0.919% | 1266   |  0.889% | 
 | 155  | Smoochum           |  0.69240% | 1476   |  0.779% | 1059   |  0.743% | 
 | 156  | Binacle            |  0.68147% | 1550   |  0.818% | 1112   |  0.781% | 
 | 157  | Shroomish          |  0.67553% | 1540   |  0.813% | 1156   |  0.812% | 
 | 158  | Zubat              |  0.62892% | 1476   |  0.779% | 1154   |  0.810% | 
 | 159  | Pichu              |  0.60555% | 1767   |  0.932% | 1355   |  0.951% | 
 | 160  | Poliwag            |  0.59129% | 1315   |  0.694% | 966    |  0.678% | 
 | 161  | Cleffa             |  0.58916% | 1523   |  0.804% | 1151   |  0.808% | 
 | 162  | Electrike          |  0.58370% | 1332   |  0.703% | 946    |  0.664% | 
 | 163  | Ralts              |  0.57872% | 1591   |  0.839% | 1146   |  0.805% | 
 | 164  | Flabebe            |  0.57371% | 1272   |  0.671% | 885    |  0.621% | 
 | 165  | Elgyem             |  0.55931% | 1392   |  0.734% | 994    |  0.698% | 
 | 166  | Wingull            |  0.55773% | 1075   |  0.567% | 848    |  0.595% | 
 | 167  | Oddish             |  0.55547% | 1140   |  0.601% | 851    |  0.597% | 
 | 168  | Munna              |  0.53148% | 1116   |  0.589% | 714    |  0.501% | 
 | 169  | NidoranM           |  0.52996% | 1188   |  0.627% | 842    |  0.591% | 
 | 170  | Glameow            |  0.52306% | 1303   |  0.687% | 1086   |  0.762% | 
 | 171  | Turtwig            |  0.51717% | 1334   |  0.704% | 1049   |  0.736% | 
 | 172  | Espurr             |  0.51567% | 1315   |  0.694% | 944    |  0.663% | 
 | 173  | Horsea             |  0.51330% | 1312   |  0.692% | 939    |  0.659% | 
 | 174  | Starly             |  0.50893% | 1273   |  0.672% | 952    |  0.668% | 
 | 175  | Remoraid           |  0.50291% | 933    |  0.492% | 704    |  0.494% | 
 | 176  | Shellos            |  0.49196% | 972    |  0.513% | 762    |  0.535% | 
 | 177  | Venipede           |  0.49166% | 1098   |  0.579% | 903    |  0.634% | 
 | 178  | Ducklett           |  0.48838% | 1279   |  0.675% | 967    |  0.679% | 
 | 179  | Venonat            |  0.46322% | 1011   |  0.533% | 779    |  0.547% | 
 | 180  | Mudkip             |  0.44037% | 1260   |  0.665% | 928    |  0.651% | 
 | 181  | Happiny            |  0.41375% | 1179   |  0.622% | 850    |  0.597% | 
 | 182  | Paras              |  0.38895% | 923    |  0.487% | 698    |  0.490% | 
 | 183  | Mareep             |  0.38719% | 977    |  0.515% | 735    |  0.516% | 
 | 184  | Oshawott           |  0.38646% | 1044   |  0.551% | 794    |  0.557% | 
 | 185  | Budew              |  0.35823% | 798    |  0.421% | 581    |  0.408% | 
 | 186  | Tyrogue            |  0.34839% | 932    |  0.492% | 733    |  0.515% | 
 | 187  | Lotad              |  0.33196% | 922    |  0.486% | 723    |  0.508% | 
 | 188  | Psyduck            |  0.32961% | 844    |  0.445% | 626    |  0.439% | 
 | 189  | Shelmet            |  0.32909% | 657    |  0.347% | 506    |  0.355% | 
 | 190  | Makuhita           |  0.32571% | 751    |  0.396% | 584    |  0.410% | 
 | 191  | Seel               |  0.30377% | 681    |  0.359% | 530    |  0.372% | 
 | 192  | Chingling          |  0.29912% | 744    |  0.393% | 531    |  0.373% | 
 | 193  | Hoppip             |  0.28007% | 663    |  0.350% | 516    |  0.362% | 
 | 194  | Finneon            |  0.27866% | 624    |  0.329% | 502    |  0.352% | 
 | 195  | Poochyena          |  0.27777% | 595    |  0.314% | 396    |  0.278% | 
 | 196  | Shuppet            |  0.27604% | 705    |  0.372% | 532    |  0.373% | 
 | 197  | Swinub             |  0.27012% | 690    |  0.364% | 567    |  0.398% | 
 | 198  | Tepig              |  0.25830% | 770    |  0.406% | 595    |  0.418% | 
 | 199  | Roggenrola         |  0.25630% | 573    |  0.302% | 505    |  0.355% | 
 | 200  | Pumpkaboo-Small    |  0.25189% | 456    |  0.241% | 359    |  0.252% | 
 | 201  | Skitty             |  0.25117% | 683    |  0.360% | 527    |  0.370% | 
 | 202  | Karrablast         |  0.24194% | 520    |  0.274% | 361    |  0.253% | 
 | 203  | Spoink             |  0.22850% | 512    |  0.270% | 365    |  0.256% | 
 | 204  | Pumpkaboo          |  0.22093% | 437    |  0.231% | 355    |  0.249% | 
 | 205  | Cubchoo            |  0.20679% | 607    |  0.320% | 467    |  0.328% | 
 | 206  | Pidgey             |  0.20156% | 564    |  0.298% | 429    |  0.301% | 
 | 207  | Cherubi            |  0.19207% | 520    |  0.274% | 360    |  0.253% | 
 | 208  | Slugma             |  0.19173% | 499    |  0.263% | 379    |  0.266% | 
 | 209  | Swablu             |  0.18973% | 517    |  0.273% | 353    |  0.248% | 
 | 210  | Spearow            |  0.18277% | 404    |  0.213% | 301    |  0.211% | 
 | 211  | Spheal             |  0.17058% | 500    |  0.264% | 385    |  0.270% | 
 | 212  | Vanillite          |  0.16858% | 446    |  0.235% | 317    |  0.223% | 
 | 213  | Magikarp           |  0.16774% | 548    |  0.289% | 441    |  0.310% | 
 | 214  | Igglybuff          |  0.15887% | 436    |  0.230% | 322    |  0.226% | 
 | 215  | Caterpie           |  0.15395% | 480    |  0.253% | 359    |  0.252% | 
 | 216  | Hoothoot           |  0.13922% | 385    |  0.203% | 286    |  0.201% | 
 | 217  | Ledyba             |  0.13590% | 338    |  0.178% | 253    |  0.178% | 
 | 218  | Petilil            |  0.13489% | 334    |  0.176% | 229    |  0.161% | 
 | 219  | Panpour            |  0.13480% | 390    |  0.206% | 297    |  0.209% | 
 | 220  | Feebas             |  0.12959% | 364    |  0.192% | 287    |  0.201% | 
 | 221  | Lillipup           |  0.12490% | 342    |  0.180% | 232    |  0.163% | 
 | 222  | Snorunt            |  0.12060% | 358    |  0.189% | 272    |  0.191% | 
 | 223  | Gulpin             |  0.11816% | 284    |  0.150% | 205    |  0.144% | 
 | 224  | Pansage            |  0.11209% | 326    |  0.172% | 237    |  0.166% | 
 | 225  | Pidove             |  0.11145% | 324    |  0.171% | 252    |  0.177% | 
 | 226  | Pansear            |  0.10096% | 285    |  0.150% | 204    |  0.143% | 
 | 227  | Pumpkaboo-Large    |  0.10035% | 228    |  0.120% | 175    |  0.123% | 
 | 228  | NidoranF           |  0.09780% | 278    |  0.147% | 199    |  0.140% | 
 | 229  | Weedle             |  0.09421% | 293    |  0.155% | 216    |  0.152% | 
 | 230  | Seedot             |  0.08892% | 220    |  0.116% | 164    |  0.115% | 
 | 231  | Klink              |  0.08410% | 241    |  0.127% | 183    |  0.128% | 
 | 232  | Sentret            |  0.08047% | 220    |  0.116% | 160    |  0.112% | 
 | 233  | Patrat             |  0.07732% | 257    |  0.136% | 163    |  0.114% | 
 | 234  | Whismur            |  0.06924% | 177    |  0.093% | 133    |  0.093% | 
 | 235  | Tynamo             |  0.06863% | 215    |  0.113% | 168    |  0.118% | 
 | 236  | Slakoth            |  0.05691% | 156    |  0.082% | 121    |  0.085% | 
 | 237  | Wurmple            |  0.05684% | 162    |  0.085% | 118    |  0.083% | 
 | 238  | Nincada            |  0.05441% | 163    |  0.086% | 126    |  0.088% | 
 | 239  | Tympole            |  0.05304% | 159    |  0.084% | 122    |  0.086% | 
 | 240  | Scatterbug         |  0.05186% | 148    |  0.078% | 96     |  0.067% | 
 | 241  | Combee             |  0.04981% | 147    |  0.078% | 127    |  0.089% | 
 | 242  | Sunkern            |  0.04853% | 149    |  0.079% | 127    |  0.089% | 
 | 243  | Kricketot          |  0.02476% | 72     |  0.038% | 57     |  0.040% | 
 | 244  | Burmy              |  0.02049% | 63     |  0.033% | 46     |  0.032% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
