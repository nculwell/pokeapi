 +----------------------------------------+ 
 | Mewtwo                                 | 
 +----------------------------------------+ 
 | Raw count: 4                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 50.000%                      | 
 | Life Orb 50.000%                       | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/4/252/0/252 25.000%          | 
 | Timid:4/0/0/252/0/252 25.000%          | 
 | Timid:252/0/224/0/0/32 25.000%         | 
 | Hasty:0/4/0/252/0/252 25.000%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Ice Beam 100.000%                      | 
 | Aura Sphere 75.000%                    | 
 | Calm Mind 50.000%                      | 
 | Self-Destruct 25.000%                  | 
 | Taunt 25.000%                          | 
 | Grass Knot 25.000%                     | 
 | Will-O-Wisp 25.000%                    | 
 | Thunder 25.000%                        | 
 | Recover 25.000%                        | 
 | Substitute 25.000%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Groudon +25.000%                       | 
 | Giratina-Origin +16.667%               | 
 | Rayquaza +16.667%                      | 
 | Mew +16.667%                           | 
 | Dialga +16.667%                        | 
 | Palkia +8.333%                         | 
 | Cloyster +8.333%                       | 
 | Kyogre +8.333%                         | 
 | Azelf +8.333%                          | 
 | Scizor +8.333%                         | 
 | Forretress +8.333%                     | 
 | Jumpluff +8.333%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Groudon                                | 
 +----------------------------------------+ 
 | Raw count: 3                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drought 100.000%                       | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 66.667%                      | 
 | Life Orb 33.333%                       | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/252/0/0/4/252 33.333%        | 
 | Adamant:252/4/252/0/0/0 33.333%        | 
 | Adamant:112/252/0/0/8/136 33.333%      | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Dragon Claw 100.000%                   | 
 | Earthquake 100.000%                    | 
 | Stone Edge 66.667%                     | 
 | Roar 33.333%                           | 
 | Rock Polish 33.333%                    | 
 | Stealth Rock 33.333%                   | 
 | Fire Punch 33.333%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Giratina-Origin +33.333%               | 
 | Mewtwo +33.333%                        | 
 | Palkia +16.667%                        | 
 | Cloyster +16.667%                      | 
 | Azelf +16.667%                         | 
 | Forretress +16.667%                    | 
 | Jumpluff +16.667%                      | 
 | Ninjask +16.667%                       | 
 | Froslass +16.667%                      | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Rayquaza                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Air Lock 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 50.000%                       | 
 | Choice Band 50.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 50.000%          | 
 | Hasty:0/4/0/252/0/252 50.000%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Extreme Speed 100.000%                 | 
 | Outrage 100.000%                       | 
 | Overheat 50.000%                       | 
 | Dragon Claw 50.000%                    | 
 | Earthquake 50.000%                     | 
 | Draco Meteor 50.000%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Mew +66.667%                           | 
 | Mewtwo +33.333%                        | 
 | Azelf +33.333%                         | 
 | Scizor +33.333%                        | 
 | Ninjask +33.333%                       | 
 | Kyogre +33.333%                        | 
 | Dialga +16.667%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Mew                                    | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Synchronize 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | Lum Berry 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Calm:252/0/0/0/188/68 50.000%          | 
 | Jolly:252/36/0/0/0/220 50.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Taunt 100.000%                         | 
 | U-turn 50.000%                         | 
 | Explosion 50.000%                      | 
 | Rock Polish 50.000%                    | 
 | Baton Pass 50.000%                     | 
 | Swords Dance 50.000%                   | 
 | Stealth Rock 50.000%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Rayquaza +66.667%                      | 
 | Mewtwo +33.333%                        | 
 | Azelf +33.333%                         | 
 | Scizor +33.333%                        | 
 | Ninjask +33.333%                       | 
 | Kyogre +33.333%                        | 
 | Dialga +16.667%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Giratina-Origin                        | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Griseous Orb 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Naive:0/248/0/64/0/196 50.000%         | 
 | Lonely:0/248/0/64/0/196 50.000%        | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Outrage 100.000%                       | 
 | Hidden Power Fire 100.000%             | 
 | Draco Meteor 100.000%                  | 
 | Shadow Sneak 100.000%                  | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 | Palkia +33.333%                        | 
 | Cloyster +33.333%                      | 
 | Forretress +33.333%                    | 
 | Froslass +33.333%                      | 
 | Jumpluff +33.333%                      | 
 | Dialga +16.667%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Dialga                                 | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 50.000%                   | 
 | Choice Specs 50.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:4/0/0/252/0/252 50.000%          | 
 | Hasty:0/4/0/252/0/252 50.000%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Thunder 100.000%                       | 
 | Draco Meteor 100.000%                  | 
 | Dragon Pulse 50.000%                   | 
 | Outrage 50.000%                        | 
 | Fire Blast 50.000%                     | 
 | Flamethrower 50.000%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Mewtwo +33.333%                        | 
 | Cloyster +33.333%                      | 
 | Scizor +33.333%                        | 
 | Jumpluff +33.333%                      | 
 | Kyogre +33.333%                        | 
 | Giratina-Origin +16.667%               | 
 | Rayquaza +16.667%                      | 
 | Mew +16.667%                           | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Palkia                                 | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Haban Berry 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/4/0/252/0/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Surf 100.000%                          | 
 | Thunder 100.000%                       | 
 | Aqua Tail 100.000%                     | 
 | Spacial Rend 100.000%                  | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Forretress +83.333%                    | 
 | Froslass +83.333%                      | 
 | Giratina-Origin +66.667%               | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Cloyster                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Shell Armor 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | Focus Sash 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:0/252/0/0/4/252 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Ice Shard 100.000%                     | 
 | Spikes 100.000%                        | 
 | Explosion 100.000%                     | 
 | Rapid Spin 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Jumpluff +83.333%                      | 
 | Dialga +66.667%                        | 
 | Giratina-Origin +66.667%               | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Froslass                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Snow Cloak 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Focus Sash 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Icy Wind 100.000%                      | 
 | Shadow Ball 100.000%                   | 
 | Spikes 100.000%                        | 
 | Taunt 100.000%                         | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Forretress +83.333%                    | 
 | Palkia +83.333%                        | 
 | Giratina-Origin +66.667%               | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Regirock                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Clear Body 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Brave:248/248/0/8/0/0 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Shock Wave 100.000%                    | 
 | Focus Punch 100.000%                   | 
 | Stone Edge 100.000%                    | 
 | Hyper Beam 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Registeel +33.333%                     | 
 | Regice +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Registeel                              | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Clear Body 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Quiet:248/4/0/252/0/0 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Iron Head 100.000%                     | 
 | Flash Cannon 100.000%                  | 
 | Hyper Beam 100.000%                    | 
 | Zap Cannon 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Regirock +33.333%                      | 
 | Regice +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Hariyama                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Thick Fat 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Black Belt 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hardy:0/0/0/0/0/0 100.000%             | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Nothing 200.000%                       | 
 | Force Palm 100.000%                    | 
 | Arm Thrust 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Monferno +33.333%                      | 
 | Lairon +33.333%                        | 
 | Ursaring +33.333%                      | 
 | Electabuzz +33.333%                    | 
 | Nidoking +33.333%                      | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Electabuzz                             | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Static 100.000%                        | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/252/0/8/0/248 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Thunder 100.000%                       | 
 | Brick Break 100.000%                   | 
 | Protect 100.000%                       | 
 | Thunder Punch 100.000%                 | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Monferno +33.333%                      | 
 | Hariyama +33.333%                      | 
 | Ursaring +33.333%                      | 
 | Nidoking +33.333%                      | 
 | Lairon +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Azelf                                  | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Light Clay 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:252/4/0/0/0/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Reflect 100.000%                       | 
 | Taunt 100.000%                         | 
 | Light Screen 100.000%                  | 
 | Explosion 100.000%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Ninjask +83.333%                       | 
 | Mew +66.667%                           | 
 | Rayquaza +66.667%                      | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Kyogre                                 | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drizzle 100.000%                       | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Specs 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Modest:220/0/0/252/0/36 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Surf 100.000%                          | 
 | Thunder 100.000%                       | 
 | Water Spout 100.000%                   | 
 | Ice Beam 100.000%                      | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Scizor +83.333%                        | 
 | Dialga +66.667%                        | 
 | Mew +66.667%                           | 
 | Rayquaza +66.667%                      | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Jumpluff                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Chlorophyll 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:252/0/64/0/0/192 100.000%        | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Leech Seed 100.000%                    | 
 | Protect 100.000%                       | 
 | Sleep Powder 100.000%                  | 
 | Substitute 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Cloyster +83.333%                      | 
 | Dialga +66.667%                        | 
 | Giratina-Origin +66.667%               | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Regice                                 | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Clear Body 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Quiet:248/8/0/248/0/0 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Focus Punch 100.000%                   | 
 | Zap Cannon 100.000%                    | 
 | Hyper Beam 100.000%                    | 
 | Ice Beam 100.000%                      | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Regirock +33.333%                      | 
 | Registeel +33.333%                     | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Lairon                                 | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Rock Head 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 100.000%                      | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hardy:0/0/0/0/0/0 100.000%             | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Nothing 300.000%                       | 
 | Iron Defense 100.000%                  | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Monferno +33.333%                      | 
 | Hariyama +33.333%                      | 
 | Ursaring +33.333%                      | 
 | Electabuzz +33.333%                    | 
 | Nidoking +33.333%                      | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Ursaring                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Guts 100.000%                          | 
 +----------------------------------------+ 
 | Items                                  | 
 | Mental Herb 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Brave:248/252/0/8/0/0 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Hammer Arm 100.000%                    | 
 | Focus Blast 100.000%                   | 
 | Bulk Up 100.000%                       | 
 | Slash 100.000%                         | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Monferno +33.333%                      | 
 | Hariyama +33.333%                      | 
 | Electabuzz +33.333%                    | 
 | Nidoking +33.333%                      | 
 | Lairon +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Forretress                             | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Sturdy 100.000%                        | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Impish:252/0/4/0/252/0 100.000%        | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Payback 100.000%                       | 
 | Stealth Rock 100.000%                  | 
 | Toxic Spikes 100.000%                  | 
 | Rapid Spin 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Froslass +83.333%                      | 
 | Palkia +83.333%                        | 
 | Giratina-Origin +66.667%               | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Scizor                                 | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Technician 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:200/56/0/0/252/0 100.000%      | 
 +----------------------------------------+ 
 | Moves                                  | 
 | U-turn 100.000%                        | 
 | Bullet Punch 100.000%                  | 
 | Pursuit 100.000%                       | 
 | Superpower 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Kyogre +83.333%                        | 
 | Dialga +66.667%                        | 
 | Mew +66.667%                           | 
 | Rayquaza +66.667%                      | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Nidoking                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Rivalry 100.000%                       | 
 +----------------------------------------+ 
 | Items                                  | 
 | Sitrus Berry 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Rash:248/4/0/252/0/0 100.000%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Nothing 100.000%                       | 
 | Thunderbolt 100.000%                   | 
 | Earth Power 100.000%                   | 
 | Double Kick 100.000%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Monferno +33.333%                      | 
 | Hariyama +33.333%                      | 
 | Ursaring +33.333%                      | 
 | Electabuzz +33.333%                    | 
 | Lairon +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Monferno                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Blaze 100.000%                         | 
 +----------------------------------------+ 
 | Items                                  | 
 | Charcoal 100.000%                      | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/248/0/4/0/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Focus Punch 100.000%                   | 
 | Shadow Claw 100.000%                   | 
 | Fire Punch 100.000%                    | 
 | Flamethrower 100.000%                  | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Lairon +83.333%                        | 
 | Hariyama +83.333%                      | 
 | Ursaring +83.333%                      | 
 | Electabuzz +83.333%                    | 
 | Nidoking +83.333%                      | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Ninjask                                | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: ---                       | 
 | Viability Ceiling: 0                   | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Speed Boost 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | Liechi Berry 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/224/108/0/0/176 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Swords Dance 100.000%                  | 
 | Baton Pass 100.000%                    | 
 | X-Scissor 100.000%                     | 
 | Substitute 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Azelf +83.333%                         | 
 | Mew +66.667%                           | 
 | Rayquaza +66.667%                      | 
 | Groudon +50.000%                       | 
 | Mewtwo +33.333%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
