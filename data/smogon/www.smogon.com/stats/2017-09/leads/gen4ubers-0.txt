 Total leads: 884
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Groudon            | 14.02715% | 124    | 14.027% | 
 | 2    | Deoxys-Speed       | 10.18100% | 90     | 10.181% | 
 | 3    | Darkrai            |  7.23982% | 64     |  7.240% | 
 | 4    | Kyogre             |  5.20362% | 46     |  5.204% | 
 | 5    | Infernape          |  4.18552% | 37     |  4.186% | 
 | 6    | Qwilfish           |  4.07240% | 36     |  4.072% | 
 | 7    | Jirachi            |  3.84615% | 34     |  3.846% | 
 | 8    | Deoxys-Attack      |  3.28054% | 29     |  3.281% | 
 | 9    | Mew                |  3.28054% | 29     |  3.281% | 
 | 10   | Rayquaza           |  2.82805% | 25     |  2.828% | 
 | 11   | Garchomp           |  2.48869% | 22     |  2.489% | 
 | 12   | Dialga             |  2.48869% | 22     |  2.489% | 
 | 13   | Giratina-Origin    |  2.03620% | 18     |  2.036% | 
 | 14   | Charizard          |  1.92308% | 17     |  1.923% | 
 | 15   | Scizor             |  1.69683% | 15     |  1.697% | 
 | 16   | Tyranitar          |  1.69683% | 15     |  1.697% | 
 | 17   | Empoleon           |  1.69683% | 15     |  1.697% | 
 | 18   | Froslass           |  1.58371% | 14     |  1.584% | 
 | 19   | Deoxys-Defense     |  1.47059% | 13     |  1.471% | 
 | 20   | Mewtwo             |  1.47059% | 13     |  1.471% | 
 | 21   | Lucario            |  1.13122% | 10     |  1.131% | 
 | 22   | Ninjask            |  1.01810% | 9      |  1.018% | 
 | 23   | Salamence          |  0.90498% | 8      |  0.905% | 
 | 24   | Metagross          |  0.90498% | 8      |  0.905% | 
 | 25   | Blissey            |  0.79186% | 7      |  0.792% | 
 | 26   | Shaymin-Sky        |  0.79186% | 7      |  0.792% | 
 | 27   | Latios             |  0.79186% | 7      |  0.792% | 
 | 28   | Vaporeon           |  0.67873% | 6      |  0.679% | 
 | 29   | Gengar             |  0.67873% | 6      |  0.679% | 
 | 30   | Swampert           |  0.56561% | 5      |  0.566% | 
 | 31   | Spiritomb          |  0.56561% | 5      |  0.566% | 
 | 32   | Wobbuffet          |  0.56561% | 5      |  0.566% | 
 | 33   | Palkia             |  0.56561% | 5      |  0.566% | 
 | 34   | Torterra           |  0.56561% | 5      |  0.566% | 
 | 35   | Gallade            |  0.56561% | 5      |  0.566% | 
 | 36   | Blaziken           |  0.45249% | 4      |  0.452% | 
 | 37   | Azelf              |  0.45249% | 4      |  0.452% | 
 | 38   | Electivire         |  0.45249% | 4      |  0.452% | 
 | 39   | Machamp            |  0.33937% | 3      |  0.339% | 
 | 40   | Aerodactyl         |  0.33937% | 3      |  0.339% | 
 | 41   | Celebi             |  0.33937% | 3      |  0.339% | 
 | 42   | Feraligatr         |  0.33937% | 3      |  0.339% | 
 | 43   | Weavile            |  0.33937% | 3      |  0.339% | 
 | 44   | Ho-Oh              |  0.33937% | 3      |  0.339% | 
 | 45   | Moltres            |  0.33937% | 3      |  0.339% | 
 | 46   | Flareon            |  0.33937% | 3      |  0.339% | 
 | 47   | Sceptile           |  0.33937% | 3      |  0.339% | 
 | 48   | Pikachu            |  0.33937% | 3      |  0.339% | 
 | 49   | Magmortar          |  0.33937% | 3      |  0.339% | 
 | 50   | Ditto              |  0.22624% | 2      |  0.226% | 
 | 51   | Lapras             |  0.22624% | 2      |  0.226% | 
 | 52   | Nidoqueen          |  0.22624% | 2      |  0.226% | 
 | 53   | Ampharos           |  0.22624% | 2      |  0.226% | 
 | 54   | Cloyster           |  0.22624% | 2      |  0.226% | 
 | 55   | Mamoswine          |  0.22624% | 2      |  0.226% | 
 | 56   | Toxicroak          |  0.22624% | 2      |  0.226% | 
 | 57   | Gyarados           |  0.22624% | 2      |  0.226% | 
 | 58   | Breloom            |  0.22624% | 2      |  0.226% | 
 | 59   | Roserade           |  0.22624% | 2      |  0.226% | 
 | 60   | Typhlosion         |  0.22624% | 2      |  0.226% | 
 | 61   | Milotic            |  0.22624% | 2      |  0.226% | 
 | 62   | Shuckle            |  0.22624% | 2      |  0.226% | 
 | 63   | Luxray             |  0.22624% | 2      |  0.226% | 
 | 64   | Mawile             |  0.22624% | 2      |  0.226% | 
 | 65   | Porygon-Z          |  0.11312% | 1      |  0.113% | 
 | 66   | Weezing            |  0.11312% | 1      |  0.113% | 
 | 67   | Kadabra            |  0.11312% | 1      |  0.113% | 
 | 68   | Snorlax            |  0.11312% | 1      |  0.113% | 
 | 69   | Eevee              |  0.11312% | 1      |  0.113% | 
 | 70   | Dusknoir           |  0.11312% | 1      |  0.113% | 
 | 71   | Gliscor            |  0.11312% | 1      |  0.113% | 
 | 72   | Rhydon             |  0.11312% | 1      |  0.113% | 
 | 73   | Forretress         |  0.11312% | 1      |  0.113% | 
 | 74   | Absol              |  0.11312% | 1      |  0.113% | 
 | 75   | Magnemite          |  0.11312% | 1      |  0.113% | 
 | 76   | Latias             |  0.11312% | 1      |  0.113% | 
 | 77   | Deoxys             |  0.11312% | 1      |  0.113% | 
 | 78   | Blastoise          |  0.11312% | 1      |  0.113% | 
 | 79   | Staraptor          |  0.11312% | 1      |  0.113% | 
 | 80   | Giratina           |  0.11312% | 1      |  0.113% | 
 | 81   | Raikou             |  0.11312% | 1      |  0.113% | 
 | 82   | Rattata            |  0.11312% | 1      |  0.113% | 
 | 83   | Rotom-Wash         |  0.11312% | 1      |  0.113% | 
 | 84   | Umbreon            |  0.11312% | 1      |  0.113% | 
 | 85   | Alakazam           |  0.11312% | 1      |  0.113% | 
 | 86   | Heracross          |  0.11312% | 1      |  0.113% | 
 | 87   | Dragonite          |  0.11312% | 1      |  0.113% | 
 | 88   | Floatzel           |  0.11312% | 1      |  0.113% | 
 | 89   | Shaymin            |  0.11312% | 1      |  0.113% | 
 | 90   | Kricketune         |  0.11312% | 1      |  0.113% | 
 | 91   | Abomasnow          |  0.11312% | 1      |  0.113% | 
 | 92   | Flygon             |  0.11312% | 1      |  0.113% | 
 | 93   | Rotom-Heat         |  0.11312% | 1      |  0.113% | 
 | 94   | Onix               |  0.11312% | 1      |  0.113% | 
 | 95   | Manaphy            |  0.11312% | 1      |  0.113% | 
 | 96   | Skarmory           |  0.11312% | 1      |  0.113% | 
 | 97   | Prinplup           |  0.11312% | 1      |  0.113% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
