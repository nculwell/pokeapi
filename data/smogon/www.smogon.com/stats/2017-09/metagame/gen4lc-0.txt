 hyperoffense..................77.50000%
 weatherless...................52.50000%
 rainoffense...................35.00000%
 rain..........................35.00000%
 offense.......................22.50000%
 hail.......................... 7.50000%
 sand.......................... 5.00000%

 Stalliness (mean: -1.318)
     |############################
     |#########
     |##############################
     |##
     |
 -1.2|##
     |
 -1.0|##
     |##
 -0.8|##
     |
 -0.6|##
     |
 -0.4|#####
     |
 -0.2|
     |#######
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  1.08%
