 weatherless...................87.81300%
 offense.......................52.09786%
 balance.......................24.66469%
 hyperoffense..................16.83214%
 sun...........................12.18700%
 sunoffense....................12.18700%
 semistall..................... 6.40531%
 choice........................ 2.32257%

 Stalliness (mean: -0.245)
     |##
 -1.2|###########
     |##
 -0.8|########
     |
 -0.4|###############
     |######
  0.0|##############################
     |#####
 +0.4|
     |#######
 +0.8|
 more negative = more offensive, more positive = more stall
 one # =  1.09%
