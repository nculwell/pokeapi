 Total leads: 14
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Machamp            |  0.00000% | 1      |  7.143% | 
 | 2    | Electabuzz         |  0.00000% | 1      |  7.143% | 
 | 3    | Palkia             |  0.00000% | 1      |  7.143% | 
 | 4    | Dragonite          |  0.00000% | 1      |  7.143% | 
 | 5    | Rayquaza           |  0.00000% | 1      |  7.143% | 
 | 6    | Darkrai            |  0.00000% | 3      | 21.429% | 
 | 7    | Tyranitar          |  0.00000% | 1      |  7.143% | 
 | 8    | Alakazam           |  0.00000% | 1      |  7.143% | 
 | 9    | Salamence          |  0.00000% | 1      |  7.143% | 
 | 10   | Dialga             |  0.00000% | 1      |  7.143% | 
 | 11   | Deoxys-Speed       |  0.00000% | 2      | 14.286% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
