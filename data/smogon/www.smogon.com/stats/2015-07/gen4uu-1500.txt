 Total battles: 5
 Avg. weight/team: 0.472
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Rhyperior          | 48.76214% | 4      | 40.000% | 3      | 38.298% | 
 | 2    | Milotic            | 46.68442% | 5      | 50.000% | 5      | 63.830% | 
 | 3    | Spiritomb          | 40.40471% | 4      | 40.000% | 4      | 51.064% | 
 | 4    | Registeel          | 38.18103% | 4      | 40.000% | 2      | 25.532% | 
 | 5    | Arcanine           | 33.86010% | 3      | 30.000% | 2      | 25.532% | 
 | 6    | Flareon            | 28.20677% | 2      | 20.000% | 2      | 25.532% | 
 | 7    | Swellow            | 28.20677% | 2      | 20.000% | 2      | 25.532% | 
 | 8    | Miltank            | 28.20677% | 2      | 20.000% | 2      | 25.532% | 
 | 9    | Sceptile           | 28.20677% | 2      | 20.000% | 2      | 25.532% | 
 | 10   | Venusaur           | 24.87630% | 3      | 30.000% | 3      | 38.298% | 
 | 11   | Torterra           | 21.18174% | 2      | 20.000% | 2      | 25.532% | 
 | 12   | Slowbro            | 20.55537% | 2      | 20.000% | 0      |  0.000% | 
 | 13   | Altaria            | 20.07495% | 3      | 30.000% | 2      | 25.532% | 
 | 14   | Rotom              | 13.30474% | 1      | 10.000% | 0      |  0.000% | 
 | 15   | Scyther            | 13.30474% | 1      | 10.000% | 1      | 12.766% | 
 | 16   | Donphan            | 13.30474% | 1      | 10.000% | 1      | 12.766% | 
 | 17   | Uxie               | 13.30474% | 1      | 10.000% | 1      | 12.766% | 
 | 18   | Ursaring           | 13.30474% | 1      | 10.000% | 1      | 12.766% | 
 | 19   | Primeape           | 13.30474% | 1      | 10.000% | 0      |  0.000% | 
 | 20   | Azumarill          | 13.30474% | 1      | 10.000% | 1      | 12.766% | 
 | 21   | Toxicroak          | 12.67836% | 1      | 10.000% | 1      | 12.766% | 
 | 22   | Claydol            | 12.19794% | 2      | 20.000% | 2      | 25.532% | 
 | 23   | Ambipom            |  7.87701% | 1      | 10.000% | 1      | 12.766% | 
 | 24   | Dugtrio            |  7.87701% | 1      | 10.000% | 1      | 12.766% | 
 | 25   | Hariyama           |  7.87701% | 1      | 10.000% | 1      | 12.766% | 
 | 26   | Leafeon            |  7.87701% | 1      | 10.000% | 1      | 12.766% | 
 | 27   | Drapion            |  7.87701% | 1      | 10.000% | 0      |  0.000% | 
 | 28   | Steelix            |  7.87701% | 1      | 10.000% | 1      | 12.766% | 
 | 29   | Camerupt           |  4.55344% | 1      | 10.000% | 1      | 12.766% | 
 | 30   | Exeggutor          |  4.55344% | 1      | 10.000% | 0      |  0.000% | 
 | 31   | Shiftry            |  4.55344% | 1      | 10.000% | 0      |  0.000% | 
 | 32   | Torkoal            |  4.55344% | 1      | 10.000% | 1      | 12.766% | 
 | 33   | Solrock            |  4.55344% | 1      | 10.000% | 0      |  0.000% | 
 | 34   | Cherrim            |  4.55344% | 1      | 10.000% | 1      | 12.766% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
