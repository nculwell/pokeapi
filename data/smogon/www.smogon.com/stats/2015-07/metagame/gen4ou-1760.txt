 sand..........................56.69212%
 weatherless...................39.63649%
 offense.......................34.26284%
 hyperoffense..................24.67823%
 balance.......................23.32190%
 semistall.....................15.00200%
 sandoffense...................11.54234%
 hail.......................... 3.21811%
 stall......................... 2.73504%
 sandstall..................... 2.04054%
 rain.......................... 1.32557%
 multiweather.................. 0.88305%
 dragmag....................... 0.71322%
 batonpass..................... 0.68804%
 hailstall..................... 0.56548%
 voltturn...................... 0.45446%
 choice........................ 0.12844%
 trickroom..................... 0.06526%
 tricksand..................... 0.06525%
 sun........................... 0.01077%
 sunoffense.................... 0.00835%
 monotype...................... 0.00287%
 monofire...................... 0.00286%
 monoground.................... 0.00000%
 monodragon.................... 0.00000%
 monoice....................... 0.00000%
 monowater..................... 0.00000%
 rainoffense................... 0.00000%
 monopoison.................... 0.00000%
 monopsychic................... 0.00000%
 monosteel..................... 0.00000%
 monofighting.................. 0.00000%
 monorock...................... 0.00000%
 hailoffense................... 0.00000%
 mononormal.................... 0.00000%

 Stalliness (mean: -0.205)
     |#############
 -1.5|#####
     |##########
 -1.0|##############################
     |######
 -0.5|################
     |##############
  0.0|#########
     |################
 +0.5|############
     |####
 +1.0|#
     |####################
 +1.5|####
     |#
 +2.0|##
     |
 +2.5|#
 more negative = more offensive, more positive = more stall
 one # =  0.60%
