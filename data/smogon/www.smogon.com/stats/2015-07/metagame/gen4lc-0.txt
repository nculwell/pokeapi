 hyperoffense..................85.71429%
 weatherless...................57.14286%
 sandoffense...................14.28571%
 sun...........................14.28571%
 sand..........................14.28571%
 offense.......................14.28571%
 hail.......................... 7.14286%
 hailoffense................... 7.14286%
 rain.......................... 7.14286%
 rainoffense................... 7.14286%

 Stalliness (mean: -1.384)
     |##############################
     |
     |
     |
     |######
     |
     |
 -1.2|##############################
     |
 -1.0|######
     |
 -0.8|
     |
 -0.6|######
     |######
 -0.4|
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  1.19%
