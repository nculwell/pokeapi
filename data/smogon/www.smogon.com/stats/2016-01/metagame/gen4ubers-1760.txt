 offense.......................15.70695%
 weatherless...................15.70695%
 semistall..................... 0.00000%
 dragmag....................... 0.00000%
 monodragon.................... 0.00000%
 sandoffense................... 0.00000%
 sun........................... 0.00000%
 choice........................ 0.00000%
 monowater..................... 0.00000%
 batonpass..................... 0.00000%
 rain.......................... 0.00000%
 monotype...................... 0.00000%
 hyperoffense.................. 0.00000%
 sand.......................... 0.00000%
 rainoffense................... 0.00000%
 hail.......................... 0.00000%
 monodark...................... 0.00000%
 monoelectric.................. 0.00000%
 mononormal.................... 0.00000%
 balance....................... 0.00000%
 multiweather.................. 0.00000%

 Stalliness (mean: -0.117)
     |
 -1.0|
     |
 -0.5|
     |
  0.0|##############################
     |
 +0.5|
     |
 +1.0|
     |
 more negative = more offensive, more positive = more stall
 one # =  3.33%
