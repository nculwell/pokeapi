 Total battles: 8
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Abra               | 31.25000% | 5      | 31.250% | 5      | 46.875% | 
 | 2    | Gastly             | 31.25000% | 5      | 31.250% | 3      | 28.125% | 
 | 3    | Dratini            | 31.25000% | 5      | 31.250% | 2      | 18.750% | 
 | 4    | Bronzor            | 31.25000% | 5      | 31.250% | 3      | 28.125% | 
 | 5    | Cranidos           | 25.00000% | 4      | 25.000% | 3      | 28.125% | 
 | 6    | Chinchou           | 25.00000% | 4      | 25.000% | 2      | 18.750% | 
 | 7    | Mankey             | 25.00000% | 4      | 25.000% | 1      |  9.375% | 
 | 8    | Munchlax           | 18.75000% | 3      | 18.750% | 3      | 28.125% | 
 | 9    | Diglett            | 18.75000% | 3      | 18.750% | 3      | 28.125% | 
 | 10   | Onix               | 18.75000% | 3      | 18.750% | 3      | 28.125% | 
 | 11   | Voltorb            | 12.50000% | 2      | 12.500% | 1      |  9.375% | 
 | 12   | Magby              | 12.50000% | 2      | 12.500% | 1      |  9.375% | 
 | 13   | Piplup             | 12.50000% | 2      | 12.500% | 1      |  9.375% | 
 | 14   | Aipom              | 12.50000% | 2      | 12.500% | 2      | 18.750% | 
 | 15   | Buizel             | 12.50000% | 2      | 12.500% | 2      | 18.750% | 
 | 16   | Carvanha           | 12.50000% | 2      | 12.500% | 2      | 18.750% | 
 | 17   | Snover             | 12.50000% | 2      | 12.500% | 2      | 18.750% | 
 | 18   | Mantyke            | 12.50000% | 2      | 12.500% | 2      | 18.750% | 
 | 19   | Gible              | 12.50000% | 2      | 12.500% | 2      | 18.750% | 
 | 20   | Togepi             |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 21   | Buneary            |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 22   | Baltoy             |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 23   | Duskull            |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 24   | Growlithe          |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 25   | Turtwig            |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 26   | Eevee              |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 27   | Bidoof             |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 28   | Glameow            |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 29   | Drowzee            |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 30   | Gligar             |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 31   | Drifloon           |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 32   | Electrike          |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 33   | Magnemite          |  6.25000% | 1      |  6.250% | 0      |  0.000% | 
 | 34   | Cyndaquil          |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 35   | Rhyhorn            |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 36   | Chimchar           |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 37   | Wailmer            |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 38   | Shinx              |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 39   | Meowth             |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 40   | Makuhita           |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 | 41   | Slugma             |  6.25000% | 1      |  6.250% | 1      |  9.375% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
