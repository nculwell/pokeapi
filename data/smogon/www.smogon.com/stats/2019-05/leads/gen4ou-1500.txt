 Total leads: 51236
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Azelf              |  9.26733% | 4251   |  8.297% | 
 | 2    | Machamp            |  8.56263% | 3621   |  7.067% | 
 | 3    | Metagross          |  7.42507% | 3250   |  6.343% | 
 | 4    | Swampert           |  6.37254% | 2615   |  5.104% | 
 | 5    | Jirachi            |  6.13132% | 2663   |  5.198% | 
 | 6    | Heatran            |  4.51753% | 1973   |  3.851% | 
 | 7    | Aerodactyl         |  3.77465% | 2223   |  4.339% | 
 | 8    | Infernape          |  3.46118% | 2011   |  3.925% | 
 | 9    | Bronzong           |  3.26169% | 1780   |  3.474% | 
 | 10   | Flygon             |  2.78982% | 1271   |  2.481% | 
 | 11   | Gliscor            |  2.69137% | 1243   |  2.426% | 
 | 12   | Hippowdon          |  2.66578% | 1304   |  2.545% | 
 | 13   | Empoleon           |  2.47389% | 1296   |  2.529% | 
 | 14   | Zapdos             |  2.47300% | 1060   |  2.069% | 
 | 15   | Roserade           |  2.37714% | 1313   |  2.563% | 
 | 16   | Skarmory           |  2.15745% | 1166   |  2.276% | 
 | 17   | Tyranitar          |  2.04124% | 1075   |  2.098% | 
 | 18   | Starmie            |  1.88754% | 905    |  1.766% | 
 | 19   | Dragonite          |  1.88500% | 874    |  1.706% | 
 | 20   | Forretress         |  1.72556% | 1108   |  2.163% | 
 | 21   | Gallade            |  1.44321% | 639    |  1.247% | 
 | 22   | Mamoswine          |  1.18156% | 557    |  1.087% | 
 | 23   | Gengar             |  1.09974% | 678    |  1.323% | 
 | 24   | Nidoqueen          |  1.01129% | 408    |  0.796% | 
 | 25   | Gyarados           |  0.98288% | 531    |  1.036% | 
 | 26   | Uxie               |  0.88878% | 382    |  0.746% | 
 | 27   | Smeargle           |  0.79380% | 467    |  0.911% | 
 | 28   | Scizor             |  0.78937% | 532    |  1.038% | 
 | 29   | Breloom            |  0.71346% | 391    |  0.763% | 
 | 30   | Crobat             |  0.59769% | 339    |  0.662% | 
 | 31   | Togekiss           |  0.53623% | 327    |  0.638% | 
 | 32   | Lucario            |  0.50993% | 320    |  0.625% | 
 | 33   | Rotom-Wash         |  0.49320% | 198    |  0.386% | 
 | 34   | Celebi             |  0.48669% | 305    |  0.595% | 
 | 35   | Ninjask            |  0.46172% | 404    |  0.789% | 
 | 36   | Staraptor          |  0.45676% | 237    |  0.463% | 
 | 37   | Latias             |  0.45389% | 276    |  0.539% | 
 | 38   | Abomasnow          |  0.39404% | 235    |  0.459% | 
 | 39   | Yanmega            |  0.38120% | 250    |  0.488% | 
 | 40   | Dusknoir           |  0.35864% | 290    |  0.566% | 
 | 41   | Vaporeon           |  0.32996% | 159    |  0.310% | 
 | 42   | Froslass           |  0.31976% | 217    |  0.424% | 
 | 43   | Alakazam           |  0.31118% | 194    |  0.379% | 
 | 44   | Ambipom            |  0.28298% | 262    |  0.511% | 
 | 45   | Weavile            |  0.28085% | 240    |  0.468% | 
 | 46   | Jolteon            |  0.27741% | 162    |  0.316% | 
 | 47   | Typhlosion         |  0.27455% | 206    |  0.402% | 
 | 48   | Charizard          |  0.27317% | 200    |  0.390% | 
 | 49   | Kingdra            |  0.23018% | 170    |  0.332% | 
 | 50   | Torterra           |  0.22430% | 181    |  0.353% | 
 | 51   | Nidoking           |  0.22199% | 118    |  0.230% | 
 | 52   | Rotom-Mow          |  0.21504% | 104    |  0.203% | 
 | 53   | Blissey            |  0.18902% | 156    |  0.304% | 
 | 54   | Pikachu            |  0.17966% | 194    |  0.379% | 
 | 55   | Jynx               |  0.17761% | 98     |  0.191% | 
 | 56   | Hariyama           |  0.15956% | 69     |  0.135% | 
 | 57   | Umbreon            |  0.14942% | 140    |  0.273% | 
 | 58   | Blastoise          |  0.13743% | 93     |  0.182% | 
 | 59   | Electivire         |  0.13369% | 108    |  0.211% | 
 | 60   | Mesprit            |  0.12868% | 53     |  0.103% | 
 | 61   | Snorlax            |  0.12866% | 89     |  0.174% | 
 | 62   | Claydol            |  0.12128% | 125    |  0.244% | 
 | 63   | Tentacruel         |  0.11421% | 122    |  0.238% | 
 | 64   | Donphan            |  0.11307% | 112    |  0.219% | 
 | 65   | Electrode          |  0.10816% | 109    |  0.213% | 
 | 66   | Floatzel           |  0.10206% | 148    |  0.289% | 
 | 67   | Rotom-Heat         |  0.10151% | 47     |  0.092% | 
 | 68   | Drapion            |  0.09856% | 139    |  0.271% | 
 | 69   | Raikou             |  0.09827% | 69     |  0.135% | 
 | 70   | Heracross          |  0.08983% | 65     |  0.127% | 
 | 71   | Arcanine           |  0.08291% | 75     |  0.146% | 
 | 72   | Porygon-Z          |  0.07943% | 110    |  0.215% | 
 | 73   | Bastiodon          |  0.07752% | 73     |  0.142% | 
 | 74   | Magnezone          |  0.06854% | 52     |  0.101% | 
 | 75   | Spiritomb          |  0.06052% | 50     |  0.098% | 
 | 76   | Rhyperior          |  0.05548% | 48     |  0.094% | 
 | 77   | Suicune            |  0.05289% | 32     |  0.062% | 
 | 78   | Aggron             |  0.05200% | 44     |  0.086% | 
 | 79   | Mismagius          |  0.05161% | 25     |  0.049% | 
 | 80   | Linoone            |  0.05025% | 44     |  0.086% | 
 | 81   | Registeel          |  0.04954% | 28     |  0.055% | 
 | 82   | Blaziken           |  0.04932% | 45     |  0.088% | 
 | 83   | Poliwrath          |  0.04763% | 30     |  0.059% | 
 | 84   | Gardevoir          |  0.04626% | 41     |  0.080% | 
 | 85   | Pinsir             |  0.04553% | 63     |  0.123% | 
 | 86   | Feraligatr         |  0.04538% | 33     |  0.064% | 
 | 87   | Steelix            |  0.04412% | 43     |  0.084% | 
 | 88   | Wailord            |  0.04249% | 39     |  0.076% | 
 | 89   | Jumpluff           |  0.04188% | 30     |  0.059% | 
 | 90   | Sceptile           |  0.04071% | 39     |  0.076% | 
 | 91   | Raichu             |  0.03752% | 34     |  0.066% | 
 | 92   | Omastar            |  0.03443% | 23     |  0.045% | 
 | 93   | Cloyster           |  0.03427% | 41     |  0.080% | 
 | 94   | Shaymin            |  0.03397% | 33     |  0.064% | 
 | 95   | Exeggutor          |  0.03356% | 44     |  0.086% | 
 | 96   | Houndoom           |  0.03241% | 54     |  0.105% | 
 | 97   | Rapidash           |  0.03150% | 31     |  0.061% | 
 | 98   | Quagsire           |  0.03122% | 26     |  0.051% | 
 | 99   | Piloswine          |  0.03106% | 19     |  0.037% | 
 | 100  | Cradily            |  0.02774% | 23     |  0.045% | 
 | 101  | Miltank            |  0.02698% | 18     |  0.035% | 
 | 102  | Shuckle            |  0.02660% | 30     |  0.059% | 
 | 103  | Luxray             |  0.02653% | 20     |  0.039% | 
 | 104  | Lopunny            |  0.02602% | 36     |  0.070% | 
 | 105  | Ludicolo           |  0.02580% | 25     |  0.049% | 
 | 106  | Milotic            |  0.02369% | 26     |  0.051% | 
 | 107  | Magmortar          |  0.02213% | 21     |  0.041% | 
 | 108  | Butterfree         |  0.02178% | 14     |  0.027% | 
 | 109  | Scyther            |  0.02125% | 13     |  0.025% | 
 | 110  | Clefable           |  0.02092% | 23     |  0.045% | 
 | 111  | Altaria            |  0.02077% | 13     |  0.025% | 
 | 112  | Medicham           |  0.01986% | 8      |  0.016% | 
 | 113  | Hitmonlee          |  0.01945% | 14     |  0.027% | 
 | 114  | Dusclops           |  0.01917% | 13     |  0.025% | 
 | 115  | Rampardos          |  0.01838% | 17     |  0.033% | 
 | 116  | Delcatty           |  0.01703% | 12     |  0.023% | 
 | 117  | Ampharos           |  0.01668% | 17     |  0.033% | 
 | 118  | Slowbro            |  0.01651% | 22     |  0.043% | 
 | 119  | Meganium           |  0.01633% | 13     |  0.025% | 
 | 120  | Hitmonchan         |  0.01629% | 10     |  0.020% | 
 | 121  | Rattata            |  0.01611% | 15     |  0.029% | 
 | 122  | Drifblim           |  0.01604% | 24     |  0.047% | 
 | 123  | Ursaring           |  0.01587% | 24     |  0.047% | 
 | 124  | Munchlax           |  0.01555% | 10     |  0.020% | 
 | 125  | Venusaur           |  0.01511% | 21     |  0.041% | 
 | 126  | Espeon             |  0.01457% | 12     |  0.023% | 
 | 127  | Hitmontop          |  0.01429% | 12     |  0.023% | 
 | 128  | Armaldo            |  0.01310% | 33     |  0.064% | 
 | 129  | Chansey            |  0.01298% | 10     |  0.020% | 
 | 130  | Ninetales          |  0.01292% | 16     |  0.031% | 
 | 131  | Regirock           |  0.01226% | 17     |  0.033% | 
 | 132  | Rotom-Frost        |  0.01219% | 4      |  0.008% | 
 | 133  | Mightyena          |  0.01177% | 8      |  0.016% | 
 | 134  | Entei              |  0.01165% | 7      |  0.014% | 
 | 135  | Articuno           |  0.01153% | 9      |  0.018% | 
 | 136  | Kingler            |  0.01096% | 11     |  0.021% | 
 | 137  | Honchkrow          |  0.01077% | 10     |  0.020% | 
 | 138  | Yanma              |  0.01047% | 11     |  0.021% | 
 | 139  | Persian            |  0.01043% | 7      |  0.014% | 
 | 140  | Cresselia          |  0.01013% | 9      |  0.018% | 
 | 141  | Lickilicky         |  0.01011% | 13     |  0.025% | 
 | 142  | Shedinja           |  0.01005% | 13     |  0.025% | 
 | 143  | Dugtrio            |  0.01005% | 8      |  0.016% | 
 | 144  | Toxicroak          |  0.00984% | 7      |  0.014% | 
 | 145  | Swellow            |  0.00979% | 18     |  0.035% | 
 | 146  | Qwilfish           |  0.00945% | 10     |  0.020% | 
 | 147  | Leafeon            |  0.00920% | 5      |  0.010% | 
 | 148  | Charmeleon         |  0.00909% | 7      |  0.014% | 
 | 149  | Regice             |  0.00899% | 7      |  0.014% | 
 | 150  | Lapras             |  0.00874% | 9      |  0.018% | 
 | 151  | Solrock            |  0.00871% | 7      |  0.014% | 
 | 152  | Furret             |  0.00833% | 7      |  0.014% | 
 | 153  | Banette            |  0.00831% | 5      |  0.010% | 
 | 154  | Plusle             |  0.00796% | 11     |  0.021% | 
 | 155  | Beedrill           |  0.00779% | 6      |  0.012% | 
 | 156  | Politoed           |  0.00756% | 8      |  0.016% | 
 | 157  | Skuntank           |  0.00738% | 5      |  0.010% | 
 | 158  | Rotom-Fan          |  0.00705% | 3      |  0.006% | 
 | 159  | Vileplume          |  0.00698% | 4      |  0.008% | 
 | 160  | Girafarig          |  0.00690% | 5      |  0.010% | 
 | 161  | Golem              |  0.00668% | 12     |  0.023% | 
 | 162  | Probopass          |  0.00648% | 3      |  0.006% | 
 | 163  | Absol              |  0.00609% | 6      |  0.012% | 
 | 164  | Slaking            |  0.00609% | 4      |  0.008% | 
 | 165  | Sneasel            |  0.00609% | 4      |  0.008% | 
 | 166  | Pelipper           |  0.00590% | 9      |  0.018% | 
 | 167  | Lumineon           |  0.00527% | 5      |  0.010% | 
 | 168  | Piplup             |  0.00519% | 5      |  0.010% | 
 | 169  | Mr. Mime           |  0.00503% | 13     |  0.025% | 
 | 170  | Regigigas          |  0.00498% | 5      |  0.010% | 
 | 171  | Starly             |  0.00479% | 3      |  0.006% | 
 | 172  | Azumarill          |  0.00449% | 10     |  0.020% | 
 | 173  | Golduck            |  0.00448% | 4      |  0.008% | 
 | 174  | Vespiquen          |  0.00422% | 5      |  0.010% | 
 | 175  | Moltres            |  0.00413% | 3      |  0.006% | 
 | 176  | Tauros             |  0.00412% | 3      |  0.006% | 
 | 177  | Relicanth          |  0.00411% | 8      |  0.016% | 
 | 178  | Mantine            |  0.00394% | 3      |  0.006% | 
 | 179  | Torkoal            |  0.00389% | 4      |  0.008% | 
 | 180  | Ivysaur            |  0.00389% | 18     |  0.035% | 
 | 181  | Seaking            |  0.00389% | 3      |  0.006% | 
 | 182  | Exploud            |  0.00370% | 2      |  0.004% | 
 | 183  | Gastrodon          |  0.00366% | 3      |  0.006% | 
 | 184  | Pachirisu          |  0.00349% | 2      |  0.004% | 
 | 185  | Sableye            |  0.00332% | 3      |  0.006% | 
 | 186  | Castform           |  0.00324% | 2      |  0.004% | 
 | 187  | Venomoth           |  0.00285% | 1      |  0.002% | 
 | 188  | Onix               |  0.00262% | 3      |  0.006% | 
 | 189  | Muk                |  0.00260% | 3      |  0.006% | 
 | 190  | Haunter            |  0.00260% | 2      |  0.004% | 
 | 191  | Dodrio             |  0.00260% | 2      |  0.004% | 
 | 192  | Quilava            |  0.00260% | 2      |  0.004% | 
 | 193  | Surskit            |  0.00260% | 2      |  0.004% | 
 | 194  | Zigzagoon          |  0.00260% | 2      |  0.004% | 
 | 195  | Murkrow            |  0.00260% | 2      |  0.004% | 
 | 196  | Weezing            |  0.00256% | 3      |  0.006% | 
 | 197  | Slowking           |  0.00254% | 2      |  0.004% | 
 | 198  | Ariados            |  0.00254% | 5      |  0.010% | 
 | 199  | Electabuzz         |  0.00244% | 4      |  0.008% | 
 | 200  | Eevee              |  0.00219% | 1      |  0.002% | 
 | 201  | Tangrowth          |  0.00192% | 6      |  0.012% | 
 | 202  | Shiftry            |  0.00181% | 8      |  0.016% | 
 | 203  | Pidgeot            |  0.00172% | 5      |  0.010% | 
 | 204  | Bibarel            |  0.00162% | 3      |  0.006% | 
 | 205  | Manectric          |  0.00137% | 9      |  0.018% | 
 | 206  | Rhydon             |  0.00133% | 3      |  0.006% | 
 | 207  | Noctowl            |  0.00133% | 2      |  0.004% | 
 | 208  | Raticate           |  0.00130% | 1      |  0.002% | 
 | 209  | Zangoose           |  0.00130% | 1      |  0.002% | 
 | 210  | Arbok              |  0.00130% | 1      |  0.002% | 
 | 211  | Rotom              |  0.00130% | 1      |  0.002% | 
 | 212  | Graveler           |  0.00130% | 1      |  0.002% | 
 | 213  | Monferno           |  0.00130% | 1      |  0.002% | 
 | 214  | Chimchar           |  0.00130% | 1      |  0.002% | 
 | 215  | Flareon            |  0.00130% | 1      |  0.002% | 
 | 216  | Bidoof             |  0.00130% | 1      |  0.002% | 
 | 217  | Cherrim            |  0.00125% | 2      |  0.004% | 
 | 218  | Charmander         |  0.00123% | 1      |  0.002% | 
 | 219  | Glalie             |  0.00104% | 4      |  0.008% | 
 | 220  | Treecko            |  0.00103% | 1      |  0.002% | 
 | 221  | Fearow             |  0.00065% | 1      |  0.002% | 
 | 222  | Walrein            |  0.00062% | 1      |  0.002% | 
 | 223  | Golbat             |  0.00061% | 1      |  0.002% | 
 | 224  | Spinda             |  0.00061% | 1      |  0.002% | 
 | 225  | Magikarp           |  0.00029% | 2      |  0.004% | 
 | 226  | Primeape           |  0.00006% | 6      |  0.012% | 
 | 227  | Mawile             |  0.00001% | 4      |  0.008% | 
 | 228  | Hypno              |  0.00001% | 1      |  0.002% | 
 | 229  | Sharpedo           |  0.00001% | 5      |  0.010% | 
 | 230  | Bellossom          |  0.00000% | 1      |  0.002% | 
 | 231  | Marowak            |  0.00000% | 1      |  0.002% | 
 | 232  | Kricketune         |  0.00000% | 1      |  0.002% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
