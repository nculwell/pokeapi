 hyperoffense..................100.00000%
 weatherless...................100.00000%

 Stalliness (mean: -1.639)
     |##############################
     |
     |
     |
     |##################
 -1.2|
     |
 -1.0|
     |
 -0.8|
     |
 -0.6|
     |
 -0.4|
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  2.09%
