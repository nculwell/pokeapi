 Total leads: 8010
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Azelf              | 10.70046% | 857    | 10.699% | 
 | 2    | Heatran            |  9.75153% | 781    |  9.750% | 
 | 3    | Machamp            |  6.91722% | 554    |  6.916% | 
 | 4    | Metagross          |  4.91947% | 394    |  4.919% | 
 | 5    | Aerodactyl         |  4.45749% | 357    |  4.457% | 
 | 6    | Swampert           |  4.08585% | 328    |  4.095% | 
 | 7    | Tyranitar          |  3.60844% | 289    |  3.608% | 
 | 8    | Smeargle           |  3.39618% | 272    |  3.396% | 
 | 9    | Infernape          |  3.32126% | 266    |  3.321% | 
 | 10   | Hippowdon          |  2.78437% | 223    |  2.784% | 
 | 11   | Dragonite          |  2.72194% | 218    |  2.722% | 
 | 12   | Jirachi            |  2.62205% | 210    |  2.622% | 
 | 13   | Roserade           |  2.60956% | 209    |  2.609% | 
 | 14   | Zapdos             |  2.12261% | 170    |  2.122% | 
 | 15   | Crobat             |  1.91035% | 153    |  1.910% | 
 | 16   | Starmie            |  1.73555% | 139    |  1.735% | 
 | 17   | Empoleon           |  1.71058% | 137    |  1.710% | 
 | 18   | Gliscor            |  1.54826% | 124    |  1.548% | 
 | 19   | Ninjask            |  1.39843% | 112    |  1.398% | 
 | 20   | Skarmory           |  1.36097% | 109    |  1.361% | 
 | 21   | Ambipom            |  1.31103% | 105    |  1.311% | 
 | 22   | Forretress         |  1.17368% | 94     |  1.174% | 
 | 23   | Scizor             |  1.14871% | 92     |  1.149% | 
 | 24   | Froslass           |  0.98639% | 79     |  0.986% | 
 | 25   | Staraptor          |  0.97390% | 78     |  0.974% | 
 | 26   | Breloom            |  0.97390% | 78     |  0.974% | 
 | 27   | Weavile            |  0.93645% | 75     |  0.936% | 
 | 28   | Gengar             |  0.91147% | 73     |  0.911% | 
 | 29   | Gallade            |  0.88650% | 71     |  0.886% | 
 | 30   | Flygon             |  0.84904% | 68     |  0.849% | 
 | 31   | Celebi             |  0.77413% | 62     |  0.774% | 
 | 32   | Abomasnow          |  0.76164% | 61     |  0.762% | 
 | 33   | Nidoqueen          |  0.71170% | 57     |  0.712% | 
 | 34   | Bronzong           |  0.69921% | 56     |  0.699% | 
 | 35   | Blissey            |  0.63678% | 51     |  0.637% | 
 | 36   | Togekiss           |  0.61181% | 49     |  0.612% | 
 | 37   | Yanmega            |  0.54938% | 44     |  0.549% | 
 | 38   | Gyarados           |  0.51192% | 41     |  0.512% | 
 | 39   | Feraligatr         |  0.48695% | 39     |  0.487% | 
 | 40   | Venusaur           |  0.39955% | 32     |  0.400% | 
 | 41   | Rotom-Wash         |  0.39955% | 32     |  0.400% | 
 | 42   | Houndoom           |  0.32463% | 26     |  0.325% | 
 | 43   | Charizard          |  0.31215% | 25     |  0.312% | 
 | 44   | Rhyperior          |  0.28718% | 23     |  0.287% | 
 | 45   | Electivire         |  0.28718% | 23     |  0.287% | 
 | 46   | Uxie               |  0.27469% | 22     |  0.275% | 
 | 47   | Mamoswine          |  0.27469% | 22     |  0.275% | 
 | 48   | Hariyama           |  0.26221% | 21     |  0.262% | 
 | 49   | Jolteon            |  0.24972% | 20     |  0.250% | 
 | 50   | Gardevoir          |  0.24972% | 20     |  0.250% | 
 | 51   | Pikachu            |  0.23723% | 19     |  0.237% | 
 | 52   | Magnezone          |  0.23723% | 19     |  0.237% | 
 | 53   | Snorlax            |  0.21226% | 17     |  0.212% | 
 | 54   | Ludicolo           |  0.21226% | 17     |  0.212% | 
 | 55   | Bibarel            |  0.19978% | 16     |  0.200% | 
 | 56   | Lucario            |  0.18729% | 15     |  0.187% | 
 | 57   | Alakazam           |  0.18729% | 15     |  0.187% | 
 | 58   | Butterfree         |  0.17480% | 14     |  0.175% | 
 | 59   | Lopunny            |  0.17480% | 14     |  0.175% | 
 | 60   | Dugtrio            |  0.17480% | 14     |  0.175% | 
 | 61   | Typhlosion         |  0.16232% | 13     |  0.162% | 
 | 62   | Torterra           |  0.16232% | 13     |  0.162% | 
 | 63   | Claydol            |  0.16232% | 13     |  0.162% | 
 | 64   | Espeon             |  0.16232% | 13     |  0.162% | 
 | 65   | Mesprit            |  0.14983% | 12     |  0.150% | 
 | 66   | Azumarill          |  0.14983% | 12     |  0.150% | 
 | 67   | Blastoise          |  0.13735% | 11     |  0.137% | 
 | 68   | Blaziken           |  0.12486% | 10     |  0.125% | 
 | 69   | Linoone            |  0.12486% | 10     |  0.125% | 
 | 70   | Golduck            |  0.12486% | 10     |  0.125% | 
 | 71   | Umbreon            |  0.12486% | 10     |  0.125% | 
 | 72   | Luxray             |  0.12486% | 10     |  0.125% | 
 | 73   | Chansey            |  0.11237% | 9      |  0.112% | 
 | 74   | Tentacruel         |  0.11237% | 9      |  0.112% | 
 | 75   | Nidoking           |  0.11237% | 9      |  0.112% | 
 | 76   | Clefable           |  0.11237% | 9      |  0.112% | 
 | 77   | Aggron             |  0.09989% | 8      |  0.100% | 
 | 78   | Toxicroak          |  0.09989% | 8      |  0.100% | 
 | 79   | Rotom-Heat         |  0.09989% | 8      |  0.100% | 
 | 80   | Suicune            |  0.09695% | 8      |  0.100% | 
 | 81   | Weezing            |  0.08740% | 7      |  0.087% | 
 | 82   | Swellow            |  0.08740% | 7      |  0.087% | 
 | 83   | Hitmonlee          |  0.08740% | 7      |  0.087% | 
 | 84   | Cloyster           |  0.08740% | 7      |  0.087% | 
 | 85   | Slowbro            |  0.07492% | 6      |  0.075% | 
 | 86   | Arcanine           |  0.07492% | 6      |  0.075% | 
 | 87   | Honchkrow          |  0.07492% | 6      |  0.075% | 
 | 88   | Vaporeon           |  0.07492% | 6      |  0.075% | 
 | 89   | Heracross          |  0.07492% | 6      |  0.075% | 
 | 90   | Hitmontop          |  0.07492% | 6      |  0.075% | 
 | 91   | Banette            |  0.06243% | 5      |  0.062% | 
 | 92   | Registeel          |  0.06243% | 5      |  0.062% | 
 | 93   | Magmar             |  0.04994% | 4      |  0.050% | 
 | 94   | Marowak            |  0.04994% | 4      |  0.050% | 
 | 95   | Dusknoir           |  0.04994% | 4      |  0.050% | 
 | 96   | Gastrodon          |  0.04994% | 4      |  0.050% | 
 | 97   | Jumpluff           |  0.04994% | 4      |  0.050% | 
 | 98   | Drapion            |  0.04994% | 4      |  0.050% | 
 | 99   | Medicham           |  0.04994% | 4      |  0.050% | 
 | 100  | Lickilicky         |  0.04994% | 4      |  0.050% | 
 | 101  | Shedinja           |  0.04994% | 4      |  0.050% | 
 | 102  | Porygon-Z          |  0.03746% | 3      |  0.037% | 
 | 103  | Spiritomb          |  0.03746% | 3      |  0.037% | 
 | 104  | Regirock           |  0.03746% | 3      |  0.037% | 
 | 105  | Milotic            |  0.03746% | 3      |  0.037% | 
 | 106  | Leafeon            |  0.03746% | 3      |  0.037% | 
 | 107  | Tauros             |  0.03746% | 3      |  0.037% | 
 | 108  | Glaceon            |  0.03746% | 3      |  0.037% | 
 | 109  | Donphan            |  0.03746% | 3      |  0.037% | 
 | 110  | Monferno           |  0.03746% | 3      |  0.037% | 
 | 111  | Pidgeot            |  0.03746% | 3      |  0.037% | 
 | 112  | Electrode          |  0.03746% | 3      |  0.037% | 
 | 113  | Quagsire           |  0.03746% | 3      |  0.037% | 
 | 114  | Kingdra            |  0.03746% | 3      |  0.037% | 
 | 115  | Cresselia          |  0.02497% | 2      |  0.025% | 
 | 116  | Rhydon             |  0.02497% | 2      |  0.025% | 
 | 117  | Absol              |  0.02497% | 2      |  0.025% | 
 | 118  | Politoed           |  0.02497% | 2      |  0.025% | 
 | 119  | Kangaskhan         |  0.02497% | 2      |  0.025% | 
 | 120  | Piplup             |  0.02497% | 2      |  0.025% | 
 | 121  | Raichu             |  0.02497% | 2      |  0.025% | 
 | 122  | Meganium           |  0.02497% | 2      |  0.025% | 
 | 123  | Bastiodon          |  0.02497% | 2      |  0.025% | 
 | 124  | Torkoal            |  0.02497% | 2      |  0.025% | 
 | 125  | Rotom-Fan          |  0.02497% | 2      |  0.025% | 
 | 126  | Shuckle            |  0.02497% | 2      |  0.025% | 
 | 127  | Steelix            |  0.02497% | 2      |  0.025% | 
 | 128  | Beedrill           |  0.01249% | 1      |  0.012% | 
 | 129  | Aron               |  0.01249% | 1      |  0.012% | 
 | 130  | Exeggutor          |  0.01249% | 1      |  0.012% | 
 | 131  | Rampardos          |  0.01249% | 1      |  0.012% | 
 | 132  | Altaria            |  0.01249% | 1      |  0.012% | 
 | 133  | Ampharos           |  0.01249% | 1      |  0.012% | 
 | 134  | Articuno           |  0.01249% | 1      |  0.012% | 
 | 135  | Raikou             |  0.01249% | 1      |  0.012% | 
 | 136  | Mismagius          |  0.01249% | 1      |  0.012% | 
 | 137  | Lanturn            |  0.01249% | 1      |  0.012% | 
 | 138  | Slaking            |  0.01249% | 1      |  0.012% | 
 | 139  | Flaaffy            |  0.01249% | 1      |  0.012% | 
 | 140  | Magcargo           |  0.01249% | 1      |  0.012% | 
 | 141  | Slowking           |  0.01249% | 1      |  0.012% | 
 | 142  | Pidgey             |  0.01249% | 1      |  0.012% | 
 | 143  | Hypno              |  0.01249% | 1      |  0.012% | 
 | 144  | Victreebel         |  0.01249% | 1      |  0.012% | 
 | 145  | Furret             |  0.01249% | 1      |  0.012% | 
 | 146  | Lunatone           |  0.01249% | 1      |  0.012% | 
 | 147  | Persian            |  0.01249% | 1      |  0.012% | 
 | 148  | Charmander         |  0.01249% | 1      |  0.012% | 
 | 149  | Moltres            |  0.01249% | 1      |  0.012% | 
 | 150  | Shaymin            |  0.01249% | 1      |  0.012% | 
 | 151  | Jynx               |  0.01249% | 1      |  0.012% | 
 | 152  | Magmortar          |  0.01249% | 1      |  0.012% | 
 | 153  | Qwilfish           |  0.01249% | 1      |  0.012% | 
 | 154  | Cherrim            |  0.01249% | 1      |  0.012% | 
 | 155  | Ninetales          |  0.01249% | 1      |  0.012% | 
 | 156  | Ariados            |  0.01249% | 1      |  0.012% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
