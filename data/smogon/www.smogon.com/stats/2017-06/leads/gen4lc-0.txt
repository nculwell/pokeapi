 Total leads: 26
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Houndour           | 76.92308% | 20     | 76.923% | 
 | 2    | Torchic            | 11.53846% | 3      | 11.538% | 
 | 3    | Cyndaquil          |  7.69231% | 2      |  7.692% | 
 | 4    | Duskull            |  3.84615% | 1      |  3.846% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
