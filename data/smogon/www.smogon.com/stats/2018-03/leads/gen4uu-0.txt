 Total leads: 424
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Dugtrio            | 10.14151% | 43     | 10.142% | 
 | 2    | Swellow            |  7.31132% | 31     |  7.311% | 
 | 3    | Uxie               |  6.83962% | 29     |  6.840% | 
 | 4    | Rhyperior          |  6.36792% | 27     |  6.368% | 
 | 5    | Mesprit            |  5.89623% | 25     |  5.896% | 
 | 6    | Cloyster           |  4.48113% | 19     |  4.481% | 
 | 7    | Wigglytuff         |  3.30189% | 14     |  3.302% | 
 | 8    | Absol              |  3.30189% | 14     |  3.302% | 
 | 9    | Alakazam           |  3.06604% | 13     |  3.066% | 
 | 10   | Venusaur           |  3.06604% | 13     |  3.066% | 
 | 11   | Ambipom            |  2.83019% | 12     |  2.830% | 
 | 12   | Blaziken           |  2.35849% | 10     |  2.358% | 
 | 13   | Mismagius          |  2.35849% | 10     |  2.358% | 
 | 14   | Feraligatr         |  2.12264% | 9      |  2.123% | 
 | 15   | Registeel          |  1.65094% | 7      |  1.651% | 
 | 16   | Leafeon            |  1.65094% | 7      |  1.651% | 
 | 17   | Jynx               |  1.41509% | 6      |  1.415% | 
 | 18   | Drapion            |  1.41509% | 6      |  1.415% | 
 | 19   | Shiftry            |  1.41509% | 6      |  1.415% | 
 | 20   | Omastar            |  1.41509% | 6      |  1.415% | 
 | 21   | Blastoise          |  1.41509% | 6      |  1.415% | 
 | 22   | Houndoom           |  1.17925% | 5      |  1.179% | 
 | 23   | Aggron             |  1.17925% | 5      |  1.179% | 
 | 24   | Spiritomb          |  0.94340% | 4      |  0.943% | 
 | 25   | Arcanine           |  0.94340% | 4      |  0.943% | 
 | 26   | Donphan            |  0.94340% | 4      |  0.943% | 
 | 27   | Milotic            |  0.94340% | 4      |  0.943% | 
 | 28   | Sceptile           |  0.94340% | 4      |  0.943% | 
 | 29   | Electrode          |  0.94340% | 4      |  0.943% | 
 | 30   | Torkoal            |  0.94340% | 4      |  0.943% | 
 | 31   | Hariyama           |  0.70755% | 3      |  0.708% | 
 | 32   | Machoke            |  0.70755% | 3      |  0.708% | 
 | 33   | Steelix            |  0.70755% | 3      |  0.708% | 
 | 34   | Ninetales          |  0.70755% | 3      |  0.708% | 
 | 35   | Altaria            |  0.70755% | 3      |  0.708% | 
 | 36   | Spinda             |  0.47170% | 2      |  0.472% | 
 | 37   | Moltres            |  0.47170% | 2      |  0.472% | 
 | 38   | Golem              |  0.47170% | 2      |  0.472% | 
 | 39   | Miltank            |  0.47170% | 2      |  0.472% | 
 | 40   | Clefable           |  0.47170% | 2      |  0.472% | 
 | 41   | Claydol            |  0.47170% | 2      |  0.472% | 
 | 42   | Lanturn            |  0.47170% | 2      |  0.472% | 
 | 43   | Hippopotas         |  0.47170% | 2      |  0.472% | 
 | 44   | Ariados            |  0.47170% | 2      |  0.472% | 
 | 45   | Persian            |  0.47170% | 2      |  0.472% | 
 | 46   | Toxicroak          |  0.23585% | 1      |  0.236% | 
 | 47   | Gastrodon          |  0.23585% | 1      |  0.236% | 
 | 48   | Banette            |  0.23585% | 1      |  0.236% | 
 | 49   | Piplup             |  0.23585% | 1      |  0.236% | 
 | 50   | Shelgon            |  0.23585% | 1      |  0.236% | 
 | 51   | Snover             |  0.23585% | 1      |  0.236% | 
 | 52   | Chatot             |  0.23585% | 1      |  0.236% | 
 | 53   | Arbok              |  0.23585% | 1      |  0.236% | 
 | 54   | Ursaring           |  0.23585% | 1      |  0.236% | 
 | 55   | Gardevoir          |  0.23585% | 1      |  0.236% | 
 | 56   | Cacturne           |  0.23585% | 1      |  0.236% | 
 | 57   | Whiscash           |  0.23585% | 1      |  0.236% | 
 | 58   | Pikachu            |  0.23585% | 1      |  0.236% | 
 | 59   | Graveler           |  0.23585% | 1      |  0.236% | 
 | 60   | Pidgey             |  0.23585% | 1      |  0.236% | 
 | 61   | Chansey            |  0.23585% | 1      |  0.236% | 
 | 62   | Exeggutor          |  0.23585% | 1      |  0.236% | 
 | 63   | Torterra           |  0.23585% | 1      |  0.236% | 
 | 64   | Qwilfish           |  0.23585% | 1      |  0.236% | 
 | 65   | Rattata            |  0.23585% | 1      |  0.236% | 
 | 66   | Minun              |  0.23585% | 1      |  0.236% | 
 | 67   | Shuckle            |  0.23585% | 1      |  0.236% | 
 | 68   | Pidgeot            |  0.23585% | 1      |  0.236% | 
 | 69   | Probopass          |  0.23585% | 1      |  0.236% | 
 | 70   | Zangoose           |  0.23585% | 1      |  0.236% | 
 | 71   | Azumarill          |  0.23585% | 1      |  0.236% | 
 | 72   | Rampardos          |  0.23585% | 1      |  0.236% | 
 | 73   | Dusclops           |  0.23585% | 1      |  0.236% | 
 | 74   | Typhlosion         |  0.23585% | 1      |  0.236% | 
 | 75   | Shedinja           |  0.23585% | 1      |  0.236% | 
 | 76   | Ampharos           |  0.23585% | 1      |  0.236% | 
 | 77   | Kabutops           |  0.23585% | 1      |  0.236% | 
 | 78   | Scyther            |  0.23585% | 1      |  0.236% | 
 | 79   | Nidoqueen          |  0.23585% | 1      |  0.236% | 
 | 80   | Mr. Mime           |  0.23585% | 1      |  0.236% | 
 | 81   | Primeape           |  0.23585% | 1      |  0.236% | 
 | 82   | Wormadam-Sandy     |  0.23585% | 1      |  0.236% | 
 | 83   | Roselia            |  0.23585% | 1      |  0.236% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
