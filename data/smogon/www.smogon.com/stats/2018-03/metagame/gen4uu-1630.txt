 weatherless...................91.02019%
 offense.......................50.87225%
 balance.......................31.58372%
 hail.......................... 8.97981%
 semistall..................... 8.97981%
 hyperoffense.................. 8.56422%
 monotype...................... 5.83978%
 monofire...................... 5.83978%
 rainoffense................... 0.00000%
 monopoison.................... 0.00000%
 sun........................... 0.00000%
 batonpass..................... 0.00000%
 sand.......................... 0.00000%
 monosteel..................... 0.00000%
 monorock...................... 0.00000%
 rain.......................... 0.00000%
 choice........................ 0.00000%
 mononormal.................... 0.00000%
 monoflying.................... 0.00000%
 hailstall..................... 0.00000%
 sunoffense.................... 0.00000%
 stall......................... 0.00000%
 monodragon.................... 0.00000%

 Stalliness (mean:  0.016)
     |
 -3.0|
     |
 -2.5|
     |
 -2.0|
     |
 -1.5|#####
     |
 -1.0|##
     |
 -0.5|
     |##############################
  0.0|#######
     |####
 +0.5|#####
     |
 +1.0|################
     |
 +1.5|
     |
 more negative = more offensive, more positive = more stall
 one # =  1.46%
