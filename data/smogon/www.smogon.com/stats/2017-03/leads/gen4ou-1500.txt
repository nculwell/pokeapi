 Total leads: 31344
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Azelf              |  8.59141% | 2208   |  7.044% | 
 | 2    | Metagross          |  7.56837% | 2163   |  6.901% | 
 | 3    | Heatran            |  5.47726% | 1368   |  4.364% | 
 | 4    | Machamp            |  5.15281% | 1360   |  4.339% | 
 | 5    | Infernape          |  4.70196% | 1475   |  4.706% | 
 | 6    | Jirachi            |  4.21984% | 1123   |  3.583% | 
 | 7    | Swampert           |  4.20623% | 1317   |  4.202% | 
 | 8    | Empoleon           |  3.94853% | 1174   |  3.746% | 
 | 9    | Aerodactyl         |  3.35053% | 1034   |  3.299% | 
 | 10   | Zapdos             |  3.16159% | 741    |  2.364% | 
 | 11   | Hippowdon          |  3.14027% | 787    |  2.511% | 
 | 12   | Tyranitar          |  2.92533% | 794    |  2.533% | 
 | 13   | Roserade           |  2.83413% | 717    |  2.288% | 
 | 14   | Bronzong           |  2.31235% | 762    |  2.431% | 
 | 15   | Uxie               |  2.20377% | 549    |  1.752% | 
 | 16   | Dragonite          |  2.16474% | 651    |  2.077% | 
 | 17   | Skarmory           |  1.99579% | 681    |  2.173% | 
 | 18   | Forretress         |  1.97482% | 630    |  2.010% | 
 | 19   | Flygon             |  1.92397% | 499    |  1.592% | 
 | 20   | Gliscor            |  1.82026% | 551    |  1.758% | 
 | 21   | Mamoswine          |  1.49462% | 599    |  1.911% | 
 | 22   | Gengar             |  1.46614% | 458    |  1.461% | 
 | 23   | Starmie            |  1.39138% | 431    |  1.375% | 
 | 24   | Breloom            |  1.28324% | 401    |  1.279% | 
 | 25   | Yanmega            |  1.27062% | 369    |  1.177% | 
 | 26   | Smeargle           |  1.12492% | 411    |  1.311% | 
 | 27   | Gyarados           |  1.01102% | 310    |  0.989% | 
 | 28   | Porygon-Z          |  0.97531% | 248    |  0.791% | 
 | 29   | Jolteon            |  0.90410% | 328    |  1.046% | 
 | 30   | Lucario            |  0.84644% | 354    |  1.129% | 
 | 31   | Celebi             |  0.77141% | 230    |  0.734% | 
 | 32   | Crobat             |  0.70814% | 371    |  1.184% | 
 | 33   | Scizor             |  0.58692% | 286    |  0.912% | 
 | 34   | Alakazam           |  0.51454% | 155    |  0.495% | 
 | 35   | Electrode          |  0.50012% | 135    |  0.431% | 
 | 36   | Abomasnow          |  0.45681% | 139    |  0.443% | 
 | 37   | Blissey            |  0.45669% | 171    |  0.546% | 
 | 38   | Staraptor          |  0.41888% | 121    |  0.386% | 
 | 39   | Heracross          |  0.40338% | 179    |  0.571% | 
 | 40   | Raikou             |  0.38812% | 114    |  0.364% | 
 | 41   | Weavile            |  0.36787% | 179    |  0.571% | 
 | 42   | Typhlosion         |  0.35128% | 116    |  0.370% | 
 | 43   | Nidoqueen          |  0.33218% | 81     |  0.258% | 
 | 44   | Electivire         |  0.29181% | 175    |  0.558% | 
 | 45   | Muk                |  0.27401% | 102    |  0.325% | 
 | 46   | Ninjask            |  0.26899% | 271    |  0.865% | 
 | 47   | Magnezone          |  0.26187% | 108    |  0.345% | 
 | 48   | Jumpluff           |  0.24134% | 52     |  0.166% | 
 | 49   | Ursaring           |  0.23517% | 55     |  0.175% | 
 | 50   | Togekiss           |  0.23083% | 186    |  0.593% | 
 | 51   | Gallade            |  0.21992% | 69     |  0.220% | 
 | 52   | Kingdra            |  0.18942% | 80     |  0.255% | 
 | 53   | Rotom-Wash         |  0.18799% | 82     |  0.262% | 
 | 54   | Torterra           |  0.18043% | 122    |  0.389% | 
 | 55   | Vaporeon           |  0.17954% | 65     |  0.207% | 
 | 56   | Cloyster           |  0.16845% | 64     |  0.204% | 
 | 57   | Ambipom            |  0.15892% | 72     |  0.230% | 
 | 58   | Claydol            |  0.15748% | 82     |  0.262% | 
 | 59   | Slaking            |  0.15226% | 46     |  0.147% | 
 | 60   | Umbreon            |  0.15127% | 109    |  0.348% | 
 | 61   | Azumarill          |  0.14766% | 40     |  0.128% | 
 | 62   | Arcanine           |  0.14357% | 66     |  0.211% | 
 | 63   | Rhyperior          |  0.14323% | 68     |  0.217% | 
 | 64   | Qwilfish           |  0.14028% | 45     |  0.144% | 
 | 65   | Drapion            |  0.13963% | 65     |  0.207% | 
 | 66   | Pikachu            |  0.12830% | 106    |  0.338% | 
 | 67   | Dusknoir           |  0.11851% | 65     |  0.207% | 
 | 68   | Clefable           |  0.11773% | 33     |  0.105% | 
 | 69   | Shiftry            |  0.11737% | 32     |  0.102% | 
 | 70   | Shaymin            |  0.11293% | 30     |  0.096% | 
 | 71   | Hariyama           |  0.10769% | 28     |  0.089% | 
 | 72   | Jynx               |  0.10764% | 58     |  0.185% | 
 | 73   | Snorlax            |  0.10725% | 88     |  0.281% | 
 | 74   | Hitmontop          |  0.10377% | 28     |  0.089% | 
 | 75   | Houndoom           |  0.09318% | 29     |  0.093% | 
 | 76   | Tentacruel         |  0.08733% | 57     |  0.182% | 
 | 77   | Dugtrio            |  0.08702% | 44     |  0.140% | 
 | 78   | Suicune            |  0.08538% | 55     |  0.175% | 
 | 79   | Primeape           |  0.07930% | 26     |  0.083% | 
 | 80   | Charizard          |  0.07919% | 55     |  0.175% | 
 | 81   | Absol              |  0.07547% | 65     |  0.207% | 
 | 82   | Aggron             |  0.06340% | 38     |  0.121% | 
 | 83   | Feraligatr         |  0.06091% | 38     |  0.121% | 
 | 84   | Steelix            |  0.05957% | 69     |  0.220% | 
 | 85   | Scyther            |  0.05956% | 22     |  0.070% | 
 | 86   | Luxray             |  0.05904% | 26     |  0.083% | 
 | 87   | Blastoise          |  0.05567% | 32     |  0.102% | 
 | 88   | Drifblim           |  0.05222% | 17     |  0.054% | 
 | 89   | Torkoal            |  0.05169% | 30     |  0.096% | 
 | 90   | Shedinja           |  0.05056% | 18     |  0.057% | 
 | 91   | Blaziken           |  0.05026% | 34     |  0.108% | 
 | 92   | Mismagius          |  0.04900% | 110    |  0.351% | 
 | 93   | Shuckle            |  0.04893% | 33     |  0.105% | 
 | 94   | Golem              |  0.04696% | 19     |  0.061% | 
 | 95   | Slowbro            |  0.04664% | 19     |  0.061% | 
 | 96   | Bastiodon          |  0.04567% | 26     |  0.083% | 
 | 97   | Exeggutor          |  0.04535% | 13     |  0.041% | 
 | 98   | Butterfree         |  0.04217% | 10     |  0.032% | 
 | 99   | Girafarig          |  0.04098% | 28     |  0.089% | 
 | 100  | Persian            |  0.04085% | 19     |  0.061% | 
 | 101  | Swellow            |  0.04035% | 23     |  0.073% | 
 | 102  | Donphan            |  0.03486% | 13     |  0.041% | 
 | 103  | Granbull           |  0.03371% | 12     |  0.038% | 
 | 104  | Kingler            |  0.03351% | 7      |  0.022% | 
 | 105  | Hitmonlee          |  0.03324% | 10     |  0.032% | 
 | 106  | Espeon             |  0.03293% | 23     |  0.073% | 
 | 107  | Venusaur           |  0.03027% | 14     |  0.045% | 
 | 108  | Medicham           |  0.03016% | 16     |  0.051% | 
 | 109  | Moltres            |  0.02978% | 7      |  0.022% | 
 | 110  | Miltank            |  0.02961% | 13     |  0.041% | 
 | 111  | Cresselia          |  0.02905% | 29     |  0.093% | 
 | 112  | Seviper            |  0.02774% | 8      |  0.026% | 
 | 113  | Articuno           |  0.02717% | 10     |  0.032% | 
 | 114  | Honchkrow          |  0.02701% | 12     |  0.038% | 
 | 115  | Lapras             |  0.02654% | 13     |  0.041% | 
 | 116  | Ludicolo           |  0.02643% | 13     |  0.041% | 
 | 117  | Spiritomb          |  0.02620% | 61     |  0.195% | 
 | 118  | Raichu             |  0.02474% | 44     |  0.140% | 
 | 119  | Milotic            |  0.02468% | 16     |  0.051% | 
 | 120  | Furret             |  0.02424% | 8      |  0.026% | 
 | 121  | Linoone            |  0.02361% | 11     |  0.035% | 
 | 122  | Lanturn            |  0.02316% | 56     |  0.179% | 
 | 123  | Magmortar          |  0.02234% | 9      |  0.029% | 
 | 124  | Rhydon             |  0.02149% | 13     |  0.041% | 
 | 125  | Dragonair          |  0.02086% | 22     |  0.070% | 
 | 126  | Rotom-Heat         |  0.02043% | 9      |  0.029% | 
 | 127  | Venomoth           |  0.02036% | 6      |  0.019% | 
 | 128  | Hypno              |  0.01892% | 8      |  0.026% | 
 | 129  | Cradily            |  0.01818% | 35     |  0.112% | 
 | 130  | Noctowl            |  0.01804% | 11     |  0.035% | 
 | 131  | Porygon2           |  0.01791% | 8      |  0.026% | 
 | 132  | Mawile             |  0.01741% | 7      |  0.022% | 
 | 133  | Sceptile           |  0.01717% | 9      |  0.029% | 
 | 134  | Manectric          |  0.01638% | 94     |  0.300% | 
 | 135  | Toxicroak          |  0.01630% | 12     |  0.038% | 
 | 136  | Camerupt           |  0.01583% | 4      |  0.013% | 
 | 137  | Pinsir             |  0.01446% | 10     |  0.032% | 
 | 138  | Rotom-Fan          |  0.01385% | 7      |  0.022% | 
 | 139  | Sharpedo           |  0.01384% | 10     |  0.032% | 
 | 140  | Stantler           |  0.01372% | 5      |  0.016% | 
 | 141  | Floatzel           |  0.01367% | 12     |  0.038% | 
 | 142  | Regigigas          |  0.01363% | 6      |  0.019% | 
 | 143  | Banette            |  0.01341% | 4      |  0.013% | 
 | 144  | Eevee              |  0.01318% | 12     |  0.038% | 
 | 145  | Tangrowth          |  0.01315% | 3      |  0.010% | 
 | 146  | Flareon            |  0.01313% | 12     |  0.038% | 
 | 147  | Registeel          |  0.01250% | 5      |  0.016% | 
 | 148  | Leafeon            |  0.01238% | 7      |  0.022% | 
 | 149  | Lopunny            |  0.01235% | 7      |  0.022% | 
 | 150  | Meganium           |  0.01184% | 12     |  0.038% | 
 | 151  | Vespiquen          |  0.01168% | 4      |  0.013% | 
 | 152  | Weezing            |  0.01074% | 7      |  0.022% | 
 | 153  | Piplup             |  0.01066% | 7      |  0.022% | 
 | 154  | Rotom              |  0.01042% | 13     |  0.041% | 
 | 155  | Politoed           |  0.01015% | 5      |  0.016% | 
 | 156  | Buizel             |  0.01013% | 4      |  0.013% | 
 | 157  | Altaria            |  0.01010% | 5      |  0.016% | 
 | 158  | Glalie             |  0.00984% | 2      |  0.006% | 
 | 159  | Dusclops           |  0.00932% | 4      |  0.013% | 
 | 160  | Magneton           |  0.00927% | 4      |  0.013% | 
 | 161  | Chansey            |  0.00891% | 11     |  0.035% | 
 | 162  | Wailord            |  0.00853% | 6      |  0.019% | 
 | 163  | Rampardos          |  0.00812% | 8      |  0.026% | 
 | 164  | Omastar            |  0.00763% | 4      |  0.013% | 
 | 165  | Slowking           |  0.00736% | 2      |  0.006% | 
 | 166  | Ditto              |  0.00712% | 13     |  0.041% | 
 | 167  | Gardevoir          |  0.00687% | 5      |  0.016% | 
 | 168  | Haunter            |  0.00686% | 2      |  0.006% | 
 | 169  | Charmeleon         |  0.00641% | 5      |  0.016% | 
 | 170  | Rattata            |  0.00628% | 7      |  0.022% | 
 | 171  | Prinplup           |  0.00587% | 4      |  0.013% | 
 | 172  | Dodrio             |  0.00581% | 4      |  0.013% | 
 | 173  | Magikarp           |  0.00572% | 5      |  0.016% | 
 | 174  | Hitmonchan         |  0.00560% | 5      |  0.016% | 
 | 175  | Lunatone           |  0.00543% | 3      |  0.010% | 
 | 176  | Probopass          |  0.00542% | 3      |  0.010% | 
 | 177  | Skuntank           |  0.00537% | 4      |  0.013% | 
 | 178  | Pichu              |  0.00537% | 8      |  0.026% | 
 | 179  | Ninetales          |  0.00530% | 3      |  0.010% | 
 | 180  | Ampharos           |  0.00501% | 32     |  0.102% | 
 | 181  | Cyndaquil          |  0.00498% | 3      |  0.010% | 
 | 182  | Chimchar           |  0.00485% | 3      |  0.010% | 
 | 183  | Misdreavus         |  0.00485% | 3      |  0.010% | 
 | 184  | Sableye            |  0.00472% | 8      |  0.026% | 
 | 185  | Gorebyss           |  0.00453% | 1      |  0.003% | 
 | 186  | Mesprit            |  0.00442% | 5      |  0.016% | 
 | 187  | Solrock            |  0.00435% | 1      |  0.003% | 
 | 188  | Regirock           |  0.00429% | 2      |  0.006% | 
 | 189  | Gligar             |  0.00408% | 6      |  0.019% | 
 | 190  | Rotom-Mow          |  0.00406% | 1      |  0.003% | 
 | 191  | Purugly            |  0.00397% | 3      |  0.010% | 
 | 192  | Gastrodon          |  0.00378% | 3      |  0.010% | 
 | 193  | Ariados            |  0.00372% | 2      |  0.006% | 
 | 194  | Kangaskhan         |  0.00360% | 5      |  0.016% | 
 | 195  | Pidgeot            |  0.00350% | 8      |  0.026% | 
 | 196  | Wormadam-Trash     |  0.00350% | 16     |  0.051% | 
 | 197  | Tauros             |  0.00335% | 2      |  0.006% | 
 | 198  | Aron               |  0.00329% | 2      |  0.006% | 
 | 199  | Marowak            |  0.00321% | 4      |  0.013% | 
 | 200  | Monferno           |  0.00318% | 2      |  0.006% | 
 | 201  | Carnivine          |  0.00316% | 11     |  0.035% | 
 | 202  | Kabutops           |  0.00313% | 3      |  0.010% | 
 | 203  | Poliwrath          |  0.00285% | 2      |  0.006% | 
 | 204  | Swalot             |  0.00284% | 2      |  0.006% | 
 | 205  | Machoke            |  0.00268% | 2      |  0.006% | 
 | 206  | Squirtle           |  0.00258% | 3      |  0.010% | 
 | 207  | Lickilicky         |  0.00249% | 1      |  0.003% | 
 | 208  | Fearow             |  0.00241% | 2      |  0.006% | 
 | 209  | Shelgon            |  0.00236% | 4      |  0.013% | 
 | 210  | Mime Jr.           |  0.00217% | 2      |  0.006% | 
 | 211  | Nidoking           |  0.00208% | 4      |  0.013% | 
 | 212  | Golduck            |  0.00199% | 1      |  0.003% | 
 | 213  | Aipom              |  0.00198% | 1      |  0.003% | 
 | 214  | Castform           |  0.00192% | 2      |  0.006% | 
 | 215  | Rotom-Frost        |  0.00192% | 1      |  0.003% | 
 | 216  | Huntail            |  0.00186% | 1      |  0.003% | 
 | 217  | Sudowoodo          |  0.00184% | 5      |  0.016% | 
 | 218  | Riolu              |  0.00183% | 1      |  0.003% | 
 | 219  | Kirlia             |  0.00183% | 1      |  0.003% | 
 | 220  | Turtwig            |  0.00183% | 1      |  0.003% | 
 | 221  | Regice             |  0.00178% | 1      |  0.003% | 
 | 222  | Metapod            |  0.00165% | 1      |  0.003% | 
 | 223  | Wormadam-Sandy     |  0.00156% | 1      |  0.003% | 
 | 224  | Roselia            |  0.00153% | 4      |  0.013% | 
 | 225  | Wigglytuff         |  0.00152% | 1      |  0.003% | 
 | 226  | Rapidash           |  0.00149% | 12     |  0.038% | 
 | 227  | Munchlax           |  0.00149% | 1      |  0.003% | 
 | 228  | Wormadam           |  0.00138% | 1      |  0.003% | 
 | 229  | Pineco             |  0.00135% | 1      |  0.003% | 
 | 230  | Pidgeotto          |  0.00131% | 3      |  0.010% | 
 | 231  | Sneasel            |  0.00113% | 1      |  0.003% | 
 | 232  | Armaldo            |  0.00041% | 1      |  0.003% | 
 | 233  | Wurmple            |  0.00025% | 1      |  0.003% | 
 | 234  | Charmander         |  0.00019% | 2      |  0.006% | 
 | 235  | Bellossom          |  0.00019% | 5      |  0.016% | 
 | 236  | Bulbasaur          |  0.00014% | 1      |  0.003% | 
 | 237  | Beautifly          |  0.00000% | 1      |  0.003% | 
 | 238  | Happiny            |  0.00000% | 3      |  0.010% | 
 | 239  | Mr. Mime           |  0.00000% | 7      |  0.022% | 
 | 240  | Treecko            |  0.00000% | 1      |  0.003% | 
 | 241  | Raticate           |  0.00000% | 1      |  0.003% | 
 | 242  | Pachirisu          |  0.00000% | 2      |  0.006% | 
 | 243  | Pelipper           |  0.00000% | 1      |  0.003% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
