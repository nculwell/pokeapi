 weatherless...................65.02042%
 offense.......................43.85847%
 sand..........................30.44921%
 hyperoffense..................27.29390%
 balance.......................22.29135%
 sandoffense................... 7.64740%
 semistall..................... 3.91143%
 rain.......................... 2.68313%
 stall......................... 2.64484%
 voltturn...................... 2.56189%
 hail.......................... 1.86639%
 batonpass..................... 1.80577%
 choice........................ 1.47078%
 sandstall..................... 1.43249%
 rainoffense................... 1.38144%
 dragmag....................... 1.29849%
 monotype...................... 1.10069%
 sun........................... 0.80079%
 multiweather.................. 0.74336%
 trickroom..................... 0.57746%
 sunoffense.................... 0.39242%
 monoelectric.................. 0.34456%
 tricksand..................... 0.30947%
 hailstall..................... 0.19461%
 monosteel..................... 0.14038%
 monofighting.................. 0.10528%
 monopsychic................... 0.09252%
 monowater..................... 0.06700%
 monoflying.................... 0.06700%
 monopoison.................... 0.06381%
 monofire...................... 0.05424%
 monoice....................... 0.04148%
 monobug....................... 0.03190%
 monoghost..................... 0.02871%
 mononormal.................... 0.02552%
 monorock...................... 0.02233%
 monoground.................... 0.01914%
 monodark...................... 0.01595%
 tailwind...................... 0.01276%
 hailoffense................... 0.01276%
 monograss..................... 0.00957%
 trapper....................... 0.00638%
 monofairy..................... 0.00319%
 rainstall..................... 0.00319%

 Stalliness (mean: -0.392)
     |#######
 -1.5|###############
     |##########################
 -1.0|###########################
     |############################
 -0.5|##############################
     |#######################
  0.0|#######################
     |################
 +0.5|###########
     |##########
 +1.0|######
     |####
 +1.5|###
     |##
 +2.0|#
     |#
 +2.5|#
 more negative = more offensive, more positive = more stall
 one # =  0.42%
