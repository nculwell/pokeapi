 Total battles: 37
 Avg. weight/team: 0.004
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Bronzor            | 61.25303% | 32     | 43.243% | 27     | 46.552% | 
 | 2    | Slowpoke           | 57.98816% | 7      |  9.459% | 4      |  6.897% | 
 | 3    | Porygon            | 57.98816% | 8      | 10.811% | 7      | 12.069% | 
 | 4    | Cranidos           | 57.98816% | 12     | 16.216% | 6      | 10.345% | 
 | 5    | Wailmer            | 57.98816% | 7      |  9.459% | 3      |  5.172% | 
 | 6    | Cubone             | 57.98816% | 9      | 12.162% | 6      | 10.345% | 
 | 7    | Eevee              | 38.74697% | 6      |  8.108% | 6      | 10.345% | 
 | 8    | Munchlax           | 38.74697% | 18     | 24.324% | 16     | 27.586% | 
 | 9    | Gastly             | 38.74697% | 27     | 36.486% | 24     | 41.379% | 
 | 10   | Diglett            | 38.74697% | 10     | 13.514% | 10     | 17.241% | 
 | 11   | Totodile           | 38.74697% | 6      |  8.108% | 4      |  6.897% | 
 | 12   | Houndour           | 38.74697% | 18     | 24.324% | 14     | 24.138% | 
 | 13   | Voltorb            |  3.26486% | 11     | 14.865% | 7      | 12.069% | 
 | 14   | Drifloon           |  3.26486% | 20     | 27.027% | 18     | 31.034% | 
 | 15   | Buizel             |  3.26486% | 17     | 22.973% | 14     | 24.138% | 
 | 16   | Mantyke            |  3.26486% | 15     | 20.270% | 13     | 22.414% | 
 | 17   | Croagunk           |  3.26486% | 19     | 25.676% | 17     | 29.310% | 
 | 18   | Pichu              |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 19   | Duskull            |  0.00000% | 4      |  5.405% | 2      |  3.448% | 
 | 20   | Chingling          |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 21   | Togepi             |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 22   | Machop             |  0.00000% | 2      |  2.703% | 1      |  1.724% | 
 | 23   | Magby              |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 24   | Ekans              |  0.00000% | 2      |  2.703% | 1      |  1.724% | 
 | 25   | Buneary            |  0.00000% | 3      |  4.054% | 1      |  1.724% | 
 | 26   | Baltoy             |  0.00000% | 3      |  4.054% | 3      |  5.172% | 
 | 27   | Piplup             |  0.00000% | 3      |  4.054% | 3      |  5.172% | 
 | 28   | Bulbasaur          |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 29   | Growlithe          |  0.00000% | 6      |  8.108% | 3      |  5.172% | 
 | 30   | Spheal             |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 31   | Abra               |  0.00000% | 14     | 18.919% | 13     | 22.414% | 
 | 32   | Bidoof             |  0.00000% | 3      |  4.054% | 3      |  5.172% | 
 | 33   | Wynaut             |  0.00000% | 4      |  5.405% | 4      |  6.897% | 
 | 34   | Ponyta             |  0.00000% | 3      |  4.054% | 3      |  5.172% | 
 | 35   | Lickitung          |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 36   | Aron               |  0.00000% | 6      |  8.108% | 3      |  5.172% | 
 | 37   | Poochyena          |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 38   | Clamperl           |  0.00000% | 2      |  2.703% | 1      |  1.724% | 
 | 39   | Glameow            |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 40   | Meditite           |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 41   | Swinub             |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 42   | Aipom              |  0.00000% | 2      |  2.703% | 1      |  1.724% | 
 | 43   | Anorith            |  0.00000% | 3      |  4.054% | 2      |  3.448% | 
 | 44   | Wooper             |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 45   | Gligar             |  0.00000% | 10     | 13.514% | 9      | 15.517% | 
 | 46   | Elekid             |  0.00000% | 15     | 20.270% | 11     | 18.966% | 
 | 47   | Shieldon           |  0.00000% | 2      |  2.703% | 1      |  1.724% | 
 | 48   | Barboach           |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 49   | Taillow            |  0.00000% | 2      |  2.703% | 2      |  3.448% | 
 | 50   | Carvanha           |  0.00000% | 4      |  5.405% | 3      |  5.172% | 
 | 51   | Tentacool          |  0.00000% | 3      |  4.054% | 2      |  3.448% | 
 | 52   | Snover             |  0.00000% | 12     | 16.216% | 11     | 18.966% | 
 | 53   | Magnemite          |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 54   | Gulpin             |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 55   | Snubbull           |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 56   | Chinchou           |  0.00000% | 13     | 17.568% | 9      | 15.517% | 
 | 57   | Mudkip             |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 58   | Exeggcute          |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 59   | Budew              |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 60   | Mime Jr.           |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 61   | Chimchar           |  0.00000% | 10     | 13.514% | 9      | 15.517% | 
 | 62   | Bellsprout         |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 63   | Stunky             |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 64   | Mankey             |  0.00000% | 9      | 12.162% | 7      | 12.069% | 
 | 65   | Dratini            |  0.00000% | 2      |  2.703% | 2      |  3.448% | 
 | 66   | Teddiursa          |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 67   | Onix               |  0.00000% | 5      |  6.757% | 5      |  8.621% | 
 | 68   | Riolu              |  0.00000% | 2      |  2.703% | 1      |  1.724% | 
 | 69   | Bagon              |  0.00000% | 3      |  4.054% | 3      |  5.172% | 
 | 70   | Skorupi            |  0.00000% | 3      |  4.054% | 3      |  5.172% | 
 | 71   | Charmander         |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 72   | Bonsly             |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 73   | Gible              |  0.00000% | 3      |  4.054% | 0      |  0.000% | 
 | 74   | Krabby             |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 75   | Phanpy             |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 76   | Corphish           |  0.00000% | 2      |  2.703% | 1      |  1.724% | 
 | 77   | Hippopotas         |  0.00000% | 1      |  1.351% | 1      |  1.724% | 
 | 78   | Shellos            |  0.00000% | 4      |  5.405% | 2      |  3.448% | 
 | 79   | Surskit            |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 80   | Staryu             |  0.00000% | 1      |  1.351% | 0      |  0.000% | 
 | 81   | Starly             |  0.00000% | 3      |  4.054% | 2      |  3.448% | 
 | 82   | Turtwig            |  0.00000% | 3      |  4.054% | 3      |  5.172% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
