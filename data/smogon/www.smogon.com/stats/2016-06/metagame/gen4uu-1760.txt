 weatherless...................100.00000%
 hyperoffense..................62.62490%
 offense.......................36.42559%
 balance....................... 0.94951%
 semistall..................... 0.00000%
 stall......................... 0.00000%
 sun........................... 0.00000%
 rain.......................... 0.00000%
 choice........................ 0.00000%
 rainoffense................... 0.00000%
 mononormal.................... 0.00000%
 monotype...................... 0.00000%

 Stalliness (mean: -0.838)
     |
 -1.5|
     |
 -1.0|##############################
     |
 -0.5|#################
     |
  0.0|
     |
 +0.5|
     |
 +1.0|
     |
 +1.5|
     |
 more negative = more offensive, more positive = more stall
 one # =  2.09%
