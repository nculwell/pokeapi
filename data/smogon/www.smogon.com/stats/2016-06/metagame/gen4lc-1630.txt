 offense.......................100.00000%
 weatherless...................100.00000%
 sun........................... 0.00000%
 hail.......................... 0.00000%
 hyperoffense.................. 0.00000%
 hailoffense................... 0.00000%

 Stalliness (mean: -0.963)
     |
     |
     |
     |
     |
 -1.2|
     |
 -1.0|##############################
     |
 -0.8|
     |
 -0.6|
     |
 -0.4|
     |
 -0.2|
     |
  0.0|
 more negative = more offensive, more positive = more stall
 one # =  3.33%
