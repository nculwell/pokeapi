 Total leads: 94
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Machop             | 14.90473% | 11     | 11.702% | 
 | 2    | Slowpoke           | 11.30021% | 8      |  8.511% | 
 | 3    | Diglett            |  8.33149% | 6      |  6.383% | 
 | 4    | Glameow            |  6.59211% | 5      |  5.319% | 
 | 5    | Abra               |  6.55369% | 8      |  8.511% | 
 | 6    | Drifloon           |  4.92613% | 3      |  3.191% | 
 | 7    | Houndour           |  4.64204% | 5      |  5.319% | 
 | 8    | Meowth             |  4.00908% | 4      |  4.255% | 
 | 9    | Eevee              |  3.62237% | 5      |  5.319% | 
 | 10   | Onix               |  3.31834% | 4      |  4.255% | 
 | 11   | Chimchar           |  3.05020% | 3      |  3.191% | 
 | 12   | Snover             |  2.87822% | 3      |  3.191% | 
 | 13   | Aipom              |  2.58852% | 3      |  3.191% | 
 | 14   | Hippopotas         |  2.35881% | 2      |  2.128% | 
 | 15   | Gligar             |  2.30993% | 2      |  2.128% | 
 | 16   | Anorith            |  2.11605% | 2      |  2.128% | 
 | 17   | Azurill            |  1.90877% | 2      |  2.128% | 
 | 18   | Omanyte            |  1.87573% | 2      |  2.128% | 
 | 19   | Kabuto             |  1.17819% | 1      |  1.064% | 
 | 20   | Growlithe          |  1.17819% | 1      |  1.064% | 
 | 21   | Bidoof             |  1.06195% | 1      |  1.064% | 
 | 22   | Spinarak           |  0.77578% | 1      |  1.064% | 
 | 23   | Lickitung          |  0.77195% | 1      |  1.064% | 
 | 24   | Elekid             |  0.75510% | 1      |  1.064% | 
 | 25   | Totodile           |  0.71456% | 1      |  1.064% | 
 | 26   | Piplup             |  0.69754% | 1      |  1.064% | 
 | 27   | Munchlax           |  0.69754% | 1      |  1.064% | 
 | 28   | Gastly             |  0.69754% | 1      |  1.064% | 
 | 29   | Cacnea             |  0.69754% | 1      |  1.064% | 
 | 30   | Buizel             |  0.69754% | 1      |  1.064% | 
 | 31   | Beldum             |  0.69754% | 1      |  1.064% | 
 | 32   | Phanpy             |  0.69754% | 1      |  1.064% | 
 | 33   | Bagon              |  0.69754% | 1      |  1.064% | 
 | 34   | Gible              |  0.69754% | 1      |  1.064% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
