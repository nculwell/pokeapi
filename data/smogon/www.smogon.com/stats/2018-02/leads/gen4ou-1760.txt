 Total leads: 44130
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Zapdos             | 13.64233% | 1836   |  4.160% | 
 | 2    | Metagross          |  9.14701% | 2716   |  6.155% | 
 | 3    | Tyranitar          |  8.81788% | 1492   |  3.381% | 
 | 4    | Jirachi            |  7.19301% | 2126   |  4.818% | 
 | 5    | Machamp            |  6.43557% | 2566   |  5.815% | 
 | 6    | Azelf              |  6.35804% | 3775   |  8.554% | 
 | 7    | Hippowdon          |  5.94142% | 1199   |  2.717% | 
 | 8    | Dragonite          |  4.23463% | 880    |  1.994% | 
 | 9    | Heatran            |  4.19208% | 2026   |  4.591% | 
 | 10   | Flygon             |  3.11742% | 1145   |  2.595% | 
 | 11   | Swampert           |  3.00335% | 1012   |  2.293% | 
 | 12   | Empoleon           |  2.86791% | 777    |  1.761% | 
 | 13   | Roserade           |  2.48782% | 1129   |  2.558% | 
 | 14   | Skarmory           |  2.32818% | 1057   |  2.395% | 
 | 15   | Nidoqueen          |  2.05824% | 200    |  0.453% | 
 | 16   | Infernape          |  2.05638% | 1573   |  3.564% | 
 | 17   | Bronzong           |  1.84173% | 1144   |  2.592% | 
 | 18   | Starmie            |  1.65709% | 670    |  1.518% | 
 | 19   | Gliscor            |  1.61188% | 774    |  1.754% | 
 | 20   | Uxie               |  1.59572% | 407    |  0.922% | 
 | 21   | Aerodactyl         |  1.58416% | 1871   |  4.240% | 
 | 22   | Gallade            |  0.87417% | 244    |  0.553% | 
 | 23   | Raikou             |  0.74690% | 258    |  0.585% | 
 | 24   | Gengar             |  0.73702% | 374    |  0.847% | 
 | 25   | Gyarados           |  0.71540% | 218    |  0.494% | 
 | 26   | Donphan            |  0.59847% | 559    |  1.267% | 
 | 27   | Yanmega            |  0.46520% | 319    |  0.723% | 
 | 28   | Forretress         |  0.39441% | 448    |  1.015% | 
 | 29   | Mamoswine          |  0.34495% | 642    |  1.455% | 
 | 30   | Crobat             |  0.31433% | 258    |  0.585% | 
 | 31   | Smeargle           |  0.29836% | 644    |  1.459% | 
 | 32   | Vaporeon           |  0.29106% | 191    |  0.433% | 
 | 33   | Scizor             |  0.25798% | 377    |  0.854% | 
 | 34   | Milotic            |  0.19174% | 22     |  0.050% | 
 | 35   | Breloom            |  0.16432% | 405    |  0.918% | 
 | 36   | Froslass           |  0.16380% | 448    |  1.015% | 
 | 37   | Celebi             |  0.15253% | 211    |  0.478% | 
 | 38   | Alakazam           |  0.13183% | 513    |  1.162% | 
 | 39   | Staraptor          |  0.10353% | 123    |  0.279% | 
 | 40   | Weavile            |  0.10157% | 333    |  0.755% | 
 | 41   | Dusknoir           |  0.08600% | 103    |  0.233% | 
 | 42   | Jolteon            |  0.08356% | 158    |  0.358% | 
 | 43   | Hariyama           |  0.07753% | 93     |  0.211% | 
 | 44   | Clefable           |  0.06677% | 22     |  0.050% | 
 | 45   | Abomasnow          |  0.05516% | 220    |  0.499% | 
 | 46   | Moltres            |  0.05190% | 19     |  0.043% | 
 | 47   | Registeel          |  0.04359% | 25     |  0.057% | 
 | 48   | Porygon-Z          |  0.04275% | 141    |  0.320% | 
 | 49   | Rotom-Wash         |  0.03881% | 108    |  0.245% | 
 | 50   | Snorlax            |  0.03486% | 67     |  0.152% | 
 | 51   | Butterfree         |  0.02366% | 9      |  0.020% | 
 | 52   | Suicune            |  0.02086% | 70     |  0.159% | 
 | 53   | Electivire         |  0.01739% | 149    |  0.338% | 
 | 54   | Togekiss           |  0.01665% | 957    |  2.169% | 
 | 55   | Ninjask            |  0.01299% | 672    |  1.523% | 
 | 56   | Claydol            |  0.01171% | 132    |  0.299% | 
 | 57   | Camerupt           |  0.00934% | 27     |  0.061% | 
 | 58   | Blissey            |  0.00847% | 151    |  0.342% | 
 | 59   | Ambipom            |  0.00776% | 175    |  0.397% | 
 | 60   | Miltank            |  0.00718% | 143    |  0.324% | 
 | 61   | Tentacruel         |  0.00486% | 45     |  0.102% | 
 | 62   | Kingdra            |  0.00433% | 140    |  0.317% | 
 | 63   | Charizard          |  0.00432% | 140    |  0.317% | 
 | 64   | Lucario            |  0.00386% | 121    |  0.274% | 
 | 65   | Honchkrow          |  0.00342% | 82     |  0.186% | 
 | 66   | Torterra           |  0.00331% | 142    |  0.322% | 
 | 67   | Mismagius          |  0.00330% | 14     |  0.032% | 
 | 68   | Probopass          |  0.00327% | 1      |  0.002% | 
 | 69   | Feraligatr         |  0.00311% | 62     |  0.140% | 
 | 70   | Cacturne           |  0.00304% | 25     |  0.057% | 
 | 71   | Shaymin            |  0.00273% | 14     |  0.032% | 
 | 72   | Cloyster           |  0.00224% | 73     |  0.165% | 
 | 73   | Electrode          |  0.00178% | 310    |  0.702% | 
 | 74   | Jynx               |  0.00154% | 30     |  0.068% | 
 | 75   | Drapion            |  0.00147% | 78     |  0.177% | 
 | 76   | Spiritomb          |  0.00126% | 64     |  0.145% | 
 | 77   | Umbreon            |  0.00126% | 144    |  0.326% | 
 | 78   | Pikachu            |  0.00115% | 94     |  0.213% | 
 | 79   | Gardevoir          |  0.00113% | 13     |  0.029% | 
 | 80   | Medicham           |  0.00103% | 88     |  0.199% | 
 | 81   | Magnezone          |  0.00096% | 91     |  0.206% | 
 | 82   | Chimecho           |  0.00094% | 3      |  0.007% | 
 | 83   | Persian            |  0.00088% | 18     |  0.041% | 
 | 84   | Typhlosion         |  0.00087% | 101    |  0.229% | 
 | 85   | Absol              |  0.00081% | 34     |  0.077% | 
 | 86   | Furret             |  0.00079% | 4      |  0.009% | 
 | 87   | Dugtrio            |  0.00071% | 44     |  0.100% | 
 | 88   | Venusaur           |  0.00071% | 26     |  0.059% | 
 | 89   | Heracross          |  0.00065% | 92     |  0.208% | 
 | 90   | Gastrodon          |  0.00053% | 10     |  0.023% | 
 | 91   | Steelix            |  0.00043% | 63     |  0.143% | 
 | 92   | Gabite             |  0.00038% | 1      |  0.002% | 
 | 93   | Sceptile           |  0.00028% | 90     |  0.204% | 
 | 94   | Exeggutor          |  0.00027% | 15     |  0.034% | 
 | 95   | Skuntank           |  0.00022% | 19     |  0.043% | 
 | 96   | Ursaring           |  0.00022% | 26     |  0.059% | 
 | 97   | Aggron             |  0.00017% | 75     |  0.170% | 
 | 98   | Poliwrath          |  0.00015% | 28     |  0.063% | 
 | 99   | Nidoking           |  0.00014% | 9      |  0.020% | 
 | 100  | Lapras             |  0.00012% | 79     |  0.179% | 
 | 101  | Golem              |  0.00011% | 45     |  0.102% | 
 | 102  | Blaziken           |  0.00011% | 46     |  0.104% | 
 | 103  | Rhyperior          |  0.00009% | 53     |  0.120% | 
 | 104  | Shuckle            |  0.00008% | 33     |  0.075% | 
 | 105  | Omastar            |  0.00007% | 37     |  0.084% | 
 | 106  | Flareon            |  0.00007% | 8      |  0.018% | 
 | 107  | Arcanine           |  0.00007% | 55     |  0.125% | 
 | 108  | Bibarel            |  0.00006% | 3      |  0.007% | 
 | 109  | Kabutops           |  0.00006% | 8      |  0.018% | 
 | 110  | Hitmonchan         |  0.00004% | 36     |  0.082% | 
 | 111  | Blastoise          |  0.00003% | 99     |  0.224% | 
 | 112  | Linoone            |  0.00003% | 16     |  0.036% | 
 | 113  | Vileplume          |  0.00003% | 2      |  0.005% | 
 | 114  | Floatzel           |  0.00002% | 7      |  0.016% | 
 | 115  | Meganium           |  0.00002% | 21     |  0.048% | 
 | 116  | Rampardos          |  0.00001% | 18     |  0.041% | 
 | 117  | Magmortar          |  0.00001% | 28     |  0.063% | 
 | 118  | Glalie             |  0.00001% | 6      |  0.014% | 
 | 119  | Azumarill          |  0.00001% | 7      |  0.016% | 
 | 120  | Ditto              |  0.00001% | 7      |  0.016% | 
 | 121  | Tangrowth          |  0.00001% | 7      |  0.016% | 
 | 122  | Relicanth          |  0.00001% | 8      |  0.018% | 
 | 123  | Torkoal            |  0.00000% | 33     |  0.075% | 
 | 124  | Regirock           |  0.00000% | 6      |  0.014% | 
 | 125  | Lopunny            |  0.00000% | 32     |  0.073% | 
 | 126  | Luxray             |  0.00000% | 40     |  0.091% | 
 | 127  | Jumpluff           |  0.00000% | 46     |  0.104% | 
 | 128  | Bellossom          |  0.00000% | 4      |  0.009% | 
 | 129  | Rattata            |  0.00000% | 6      |  0.014% | 
 | 130  | Shedinja           |  0.00000% | 6      |  0.014% | 
 | 131  | Regigigas          |  0.00000% | 5      |  0.011% | 
 | 132  | Ampharos           |  0.00000% | 32     |  0.073% | 
 | 133  | Pidgeot            |  0.00000% | 3      |  0.007% | 
 | 134  | Espeon             |  0.00000% | 11     |  0.025% | 
 | 135  | Charmander         |  0.00000% | 2      |  0.005% | 
 | 136  | Bastiodon          |  0.00000% | 48     |  0.109% | 
 | 137  | Muk                |  0.00000% | 10     |  0.023% | 
 | 138  | Cresselia          |  0.00000% | 32     |  0.073% | 
 | 139  | Pelipper           |  0.00000% | 3      |  0.007% | 
 | 140  | Machoke            |  0.00000% | 2      |  0.005% | 
 | 141  | Mesprit            |  0.00000% | 12     |  0.027% | 
 | 142  | Carnivine          |  0.00000% | 1      |  0.002% | 
 | 143  | Ludicolo           |  0.00000% | 7      |  0.016% | 
 | 144  | Raichu             |  0.00000% | 13     |  0.029% | 
 | 145  | Slowbro            |  0.00000% | 30     |  0.068% | 
 | 146  | Slaking            |  0.00000% | 15     |  0.034% | 
 | 147  | Ninetales          |  0.00000% | 14     |  0.032% | 
 | 148  | Hitmontop          |  0.00000% | 15     |  0.034% | 
 | 149  | Sandslash          |  0.00000% | 2      |  0.005% | 
 | 150  | Croagunk           |  0.00000% | 2      |  0.005% | 
 | 151  | Vespiquen          |  0.00000% | 16     |  0.036% | 
 | 152  | Scyther            |  0.00000% | 2      |  0.005% | 
 | 153  | Cradily            |  0.00000% | 8      |  0.018% | 
 | 154  | Politoed           |  0.00000% | 10     |  0.023% | 
 | 155  | Rotom-Heat         |  0.00000% | 5      |  0.011% | 
 | 156  | Drifblim           |  0.00000% | 17     |  0.039% | 
 | 157  | Hitmonlee          |  0.00000% | 1      |  0.002% | 
 | 158  | Swellow            |  0.00000% | 14     |  0.032% | 
 | 159  | Houndoom           |  0.00000% | 15     |  0.034% | 
 | 160  | Abra               |  0.00000% | 1      |  0.002% | 
 | 161  | Kadabra            |  0.00000% | 1      |  0.002% | 
 | 162  | Solrock            |  0.00000% | 2      |  0.005% | 
 | 163  | Toxicroak          |  0.00000% | 12     |  0.027% | 
 | 164  | Buizel             |  0.00000% | 1      |  0.002% | 
 | 165  | Mightyena          |  0.00000% | 1      |  0.002% | 
 | 166  | Beedrill           |  0.00000% | 1      |  0.002% | 
 | 167  | Aron               |  0.00000% | 3      |  0.007% | 
 | 168  | Grotle             |  0.00000% | 1      |  0.002% | 
 | 169  | Dragonair          |  0.00000% | 1      |  0.002% | 
 | 170  | Rhydon             |  0.00000% | 1      |  0.002% | 
 | 171  | Haunter            |  0.00000% | 4      |  0.009% | 
 | 172  | Zangoose           |  0.00000% | 3      |  0.007% | 
 | 173  | Mantine            |  0.00000% | 1      |  0.002% | 
 | 174  | Altaria            |  0.00000% | 7      |  0.016% | 
 | 175  | Charmeleon         |  0.00000% | 2      |  0.005% | 
 | 176  | Leafeon            |  0.00000% | 9      |  0.020% | 
 | 177  | Banette            |  0.00000% | 1      |  0.002% | 
 | 178  | Piplup             |  0.00000% | 3      |  0.007% | 
 | 179  | Piloswine          |  0.00000% | 4      |  0.009% | 
 | 180  | Arbok              |  0.00000% | 1      |  0.002% | 
 | 181  | Rotom              |  0.00000% | 2      |  0.005% | 
 | 182  | Marowak            |  0.00000% | 2      |  0.005% | 
 | 183  | Glaceon            |  0.00000% | 1      |  0.002% | 
 | 184  | Porygon2           |  0.00000% | 2      |  0.005% | 
 | 185  | Chansey            |  0.00000% | 3      |  0.007% | 
 | 186  | Lanturn            |  0.00000% | 2      |  0.005% | 
 | 187  | Nosepass           |  0.00000% | 2      |  0.005% | 
 | 188  | Kricketot          |  0.00000% | 2      |  0.005% | 
 | 189  | Golduck            |  0.00000% | 1      |  0.002% | 
 | 190  | Walrein            |  0.00000% | 3      |  0.007% | 
 | 191  | Monferno           |  0.00000% | 1      |  0.002% | 
 | 192  | Vigoroth           |  0.00000% | 1      |  0.002% | 
 | 193  | Dunsparce          |  0.00000% | 2      |  0.005% | 
 | 194  | Bayleef            |  0.00000% | 2      |  0.005% | 
 | 195  | Beautifly          |  0.00000% | 2      |  0.005% | 
 | 196  | Slowking           |  0.00000% | 1      |  0.002% | 
 | 197  | Hypno              |  0.00000% | 17     |  0.039% | 
 | 198  | Rotom-Mow          |  0.00000% | 5      |  0.011% | 
 | 199  | Victreebel         |  0.00000% | 1      |  0.002% | 
 | 200  | Regice             |  0.00000% | 2      |  0.005% | 
 | 201  | Rotom-Frost        |  0.00000% | 1      |  0.002% | 
 | 202  | Chimchar           |  0.00000% | 1      |  0.002% | 
 | 203  | Rapidash           |  0.00000% | 3      |  0.007% | 
 | 204  | Sableye            |  0.00000% | 4      |  0.009% | 
 | 205  | Dodrio             |  0.00000% | 1      |  0.002% | 
 | 206  | Articuno           |  0.00000% | 2      |  0.005% | 
 | 207  | Dusclops           |  0.00000% | 2      |  0.005% | 
 | 208  | Rotom-Fan          |  0.00000% | 1      |  0.002% | 
 | 209  | Manectric          |  0.00000% | 1      |  0.002% | 
 | 210  | Magnemite          |  0.00000% | 2      |  0.005% | 
 | 211  | Xatu               |  0.00000% | 2      |  0.005% | 
 | 212  | Glameow            |  0.00000% | 1      |  0.002% | 
 | 213  | Tropius            |  0.00000% | 2      |  0.005% | 
 | 214  | Qwilfish           |  0.00000% | 3      |  0.007% | 
 | 215  | Sudowoodo          |  0.00000% | 1      |  0.002% | 
 | 216  | Entei              |  0.00000% | 2      |  0.005% | 
 | 217  | Armaldo            |  0.00000% | 1      |  0.002% | 
 | 218  | Onix               |  0.00000% | 1      |  0.002% | 
 | 219  | Magneton           |  0.00000% | 1      |  0.002% | 
 | 220  | Venomoth           |  0.00000% | 1      |  0.002% | 
 | 221  | Silcoon            |  0.00000% | 1      |  0.002% | 
 | 222  | Quagsire           |  0.00000% | 1      |  0.002% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
