 Total leads: 30
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Metagross          | 22.16536% | 5      | 16.667% | 
 | 2    | Deoxys-Speed       | 22.11783% | 5      | 16.667% | 
 | 3    | Froslass           | 13.29928% | 3      | 10.000% | 
 | 4    | Cloyster           |  8.86619% | 2      |  6.667% | 
 | 5    | Deoxys-Attack      |  4.43310% | 1      |  3.333% | 
 | 6    | Darkrai            |  4.43309% | 1      |  3.333% | 
 | 7    | Scizor             |  4.43309% | 1      |  3.333% | 
 | 8    | Rayquaza           |  4.41067% | 1      |  3.333% | 
 | 9    | Deoxys-Defense     |  3.68645% | 3      | 10.000% | 
 | 10   | Azelf              |  3.51793% | 2      |  6.667% | 
 | 11   | Garchomp           |  1.90081% | 1      |  3.333% | 
 | 12   | Spiritomb          |  1.89594% | 1      |  3.333% | 
 | 13   | Giratina-Origin    |  1.65450% | 1      |  3.333% | 
 | 14   | Machamp            |  1.58566% | 1      |  3.333% | 
 | 15   | Infernape          |  1.25163% | 1      |  3.333% | 
 | 16   | Torterra           |  0.34847% | 1      |  3.333% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
