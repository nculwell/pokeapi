 +----------------------------------------+ 
 | Dialga                                 | 
 +----------------------------------------+ 
 | Raw count: 13                          | 
 | Avg. weight: 0.0533956209937           | 
 | Viability Ceiling: 79                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 83.838%                       | 
 | Choice Scarf 16.162%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/4/0/252/0/252 68.722%          | 
 | Timid:0/0/0/252/4/252 10.169%          | 
 | Modest:0/0/0/252/4/252 10.027%         | 
 | Modest:4/0/0/252/0/252  6.135%         | 
 | Other  4.947%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Draco Meteor 100.000%                  | 
 | Stealth Rock 83.838%                   | 
 | Flamethrower 77.610%                   | 
 | Outrage 68.722%                        | 
 | Thunder 31.278%                        | 
 | Fire Blast 10.027%                     | 
 | Sleep Talk 10.027%                     | 
 | Other 18.498%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Scizor +3.326%                         | 
 | Palkia +3.316%                         | 
 | Rayquaza +3.314%                       | 
 | Groudon +3.056%                        | 
 | Mewtwo +3.056%                         | 
 | Jirachi +0.773%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Scizor                                 | 
 +----------------------------------------+ 
 | Raw count: 6                           | 
 | Avg. weight: 0.094134173604            | 
 | Viability Ceiling: 79                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Technician 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 84.459%                      | 
 | Life Orb 10.924%                       | 
 | Other  4.617%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Careful:244/20/76/0/168/0 84.459%      | 
 | Adamant:248/252/0/0/0/8 10.924%        | 
 | Other  4.617%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Bullet Punch 100.000%                  | 
 | Roost 100.000%                         | 
 | U-turn 89.076%                         | 
 | Superpower 84.459%                     | 
 | Bug Bite 15.541%                       | 
 | Other 10.924%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Mewtwo +12.657%                        | 
 | Groudon +12.657%                       | 
 | Rayquaza +6.688%                       | 
 | Palkia +6.555%                         | 
 | Dialga +4.087%                         | 
 | Darkrai +1.335%                        | 
 | Deoxys-Attack +1.064%                  | 
 | Skarmory +1.014%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Palkia                                 | 
 +----------------------------------------+ 
 | Raw count: 8                           | 
 | Avg. weight: 0.0704758261963           | 
 | Viability Ceiling: 79                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 92.281%                   | 
 | Lustrous Orb  7.719%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/4/0/252/0/252 84.608%          | 
 | Timid:0/0/0/252/4/252 15.392%          | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Spacial Rend 100.000%                  | 
 | Fire Blast 92.333%                     | 
 | Surf 84.608%                           | 
 | Outrage 84.608%                        | 
 | Thunder 15.392%                        | 
 | Hydro Pump 15.392%                     | 
 | Other  7.667%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Rayquaza +14.505%                      | 
 | Mewtwo +12.812%                        | 
 | Groudon +12.812%                       | 
 | Scizor +6.567%                         | 
 | Dialga +4.082%                         | 
 | Froslass +1.706%                       | 
 | Weavile +1.706%                        | 
 | Cloyster +1.694%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Rayquaza                               | 
 +----------------------------------------+ 
 | Raw count: 8                           | 
 | Avg. weight: 0.0703553660103           | 
 | Viability Ceiling: 79                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Air Lock 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 100.000%                      | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Naive:0/40/0/252/0/216 92.320%         | 
 | Adamant:0/252/0/0/4/252  7.680%        | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Extreme Speed 100.000%                 | 
 | Fire Blast 92.320%                     | 
 | Draco Meteor 92.320%                   | 
 | Brick Break 84.753%                    | 
 | Earthquake 15.247%                     | 
 | Other 15.361%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Mewtwo +20.518%                        | 
 | Groudon +20.518%                       | 
 | Palkia +14.530%                        | 
 | Scizor +6.712%                         | 
 | Dialga +4.087%                         | 
 | Cloyster +1.707%                       | 
 | Lugia +1.682%                          | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Mewtwo                                 | 
 +----------------------------------------+ 
 | Raw count: 7                           | 
 | Avg. weight: 0.0742349519797           | 
 | Viability Ceiling: 79                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 99.994%                       | 
 | Other  0.006%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/24/0/252/0/232 91.799%         | 
 | Modest:4/0/0/252/0/252  8.196%         | 
 | Other  0.006%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Ice Beam 100.000%                      | 
 | Aura Sphere 100.000%                   | 
 | Self-Destruct 91.799%                  | 
 | Flamethrower 91.799%                   | 
 | Other 16.403%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Groudon +28.198%                       | 
 | Rayquaza +22.224%                      | 
 | Palkia +13.901%                        | 
 | Scizor +13.757%                        | 
 | Dialga +4.082%                         | 
 | Lugia +2.311%                          | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Groudon                                | 
 +----------------------------------------+ 
 | Raw count: 7                           | 
 | Avg. weight: 0.0742349519797           | 
 | Viability Ceiling: 79                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drought 100.000%                       | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 91.799%                      | 
 | Lum Berry  8.196%                      | 
 | Other  0.006%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:200/112/108/0/0/88 91.799%     | 
 | Adamant:64/252/0/0/0/136  8.196%       | 
 | Other  0.006%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Earthquake 100.000%                    | 
 | Swords Dance 99.994%                   | 
 | Stone Edge 91.804%                     | 
 | Thunder Wave 91.799%                   | 
 | Other 16.403%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Mewtwo +28.198%                        | 
 | Rayquaza +22.224%                      | 
 | Palkia +13.901%                        | 
 | Scizor +13.757%                        | 
 | Dialga +4.082%                         | 
 | Lugia +2.311%                          | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Jirachi                                | 
 +----------------------------------------+ 
 | Raw count: 8                           | 
 | Avg. weight: 0.0164125181045           | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Serene Grace 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 53.009%                      | 
 | Choice Scarf 46.991%                   | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 46.991%          | 
 | Careful:248/8/0/0/252/0 33.147%        | 
 | Careful:248/0/0/0/252/8 19.862%        | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Iron Head 100.000%                     | 
 | U-turn 80.138%                         | 
 | Thunder Wave 53.009%                   | 
 | Stealth Rock 53.009%                   | 
 | Ice Punch 46.991%                      | 
 | Trick 46.991%                          | 
 | Other 19.862%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Latias +50.641%                        | 
 | Weavile +27.133%                       | 
 | Froslass +27.133%                      | 
 | Kyogre +23.050%                        | 
 | Darkrai +21.408%                       | 
 | Garchomp +17.325%                      | 
 | Deoxys-Attack +17.057%                 | 
 | Skarmory +16.259%                      | 
 | Wobbuffet +11.084%                     | 
 | Giratina-Origin +10.282%               | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Latias                                 | 
 +----------------------------------------+ 
 | Raw count: 4                           | 
 | Avg. weight: 0.0293322183735           | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Soul Dew 100.000%                      | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:112/0/0/144/0/252 100.000%       | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Calm Mind 100.000%                     | 
 | Recover 100.000%                       | 
 | Draco Meteor 77.773%                   | 
 | Surf 74.814%                           | 
 | Thunder 25.186%                        | 
 | Dragon Pulse 22.227%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Jirachi +56.671%                       | 
 | Garchomp +45.626%                      | 
 | Darkrai +24.523%                       | 
 | Starmie +21.103%                       | 
 | Manaphy +21.103%                       | 
 | Deoxys-Attack +19.538%                 | 
 | Skarmory +18.624%                      | 
 | Metagross +15.219%                     | 
 | Kyogre +15.090%                        | 
 | Wobbuffet +13.565%                     | 
 | Giratina-Origin +12.647%               | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Kyogre                                 | 
 +----------------------------------------+ 
 | Raw count: 8                           | 
 | Avg. weight: 0.00913414108509          | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Drizzle 100.000%                       | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Specs 100.000%                  | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Modest:0/0/0/252/4/252 59.560%         | 
 | Timid:0/0/0/252/4/252 40.440%          | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Thunder 100.000%                       | 
 | Ice Beam 100.000%                      | 
 | Water Spout 100.000%                   | 
 | Hydro Pump 59.560%                     | 
 | Surf 40.440%                           | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Weavile +53.546%                       | 
 | Froslass +53.546%                      | 
 | Jirachi +41.417%                       | 
 | Starmie +36.357%                       | 
 | Manaphy +36.357%                       | 
 | Garchomp +31.612%                      | 
 | Metagross +30.472%                     | 
 | Latias +24.228%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Metagross                              | 
 +----------------------------------------+ 
 | Raw count: 6                           | 
 | Avg. weight: 0.0120232664667           | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Clear Body 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Lum Berry 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Adamant:252/252/0/0/4/0 40.964%        | 
 | Adamant:128/252/0/0/124/0 29.891%      | 
 | Adamant:252/252/4/0/0/0 29.144%        | 
 | Other  0.001%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Earthquake 100.000%                    | 
 | Bullet Punch 99.999%                   | 
 | Stealth Rock 99.999%                   | 
 | Explosion 59.036%                      | 
 | Meteor Mash 40.964%                    | 
 | Other  0.002%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Lugia +53.151%                         | 
 | Starmie +36.880%                       | 
 | Manaphy +36.880%                       | 
 | Garchomp +32.136%                      | 
 | Kyogre +30.867%                        | 
 | Latias +24.752%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Wobbuffet                              | 
 +----------------------------------------+ 
 | Raw count: 3                           | 
 | Avg. weight: 0.0235289305508           | 
 | Viability Ceiling: 73                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Shadow Tag 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Custap Berry 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Calm:248/0/8/0/252/0 100.000%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Destiny Bond 100.000%                  | 
 | Counter 100.000%                       | 
 | Mirror Coat 100.000%                   | 
 | Encore 100.000%                        | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Cloyster +55.267%                      | 
 | Giratina-Origin +51.659%               | 
 | Deoxys-Attack +34.980%                 | 
 | Latias +22.548%                        | 
 | Jirachi +20.618%                       | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Giratina-Origin                        | 
 +----------------------------------------+ 
 | Raw count: 9                           | 
 | Avg. weight: 0.00770405317437          | 
 | Viability Ceiling: 73                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Levitate 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Griseous Orb 100.000%                  | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Naive:0/116/0/140/0/252 62.344%        | 
 | Rash:0/112/0/252/0/144 37.612%         | 
 | Other  0.043%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Shadow Sneak 100.000%                  | 
 | Draco Meteor 100.000%                  | 
 | Thunder 99.957%                        | 
 | Earthquake 99.957%                     | 
 | Other  0.087%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Cloyster +56.371%                      | 
 | Wobbuffet +52.591%                     | 
 | Skarmory +34.009%                      | 
 | Latias +21.401%                        | 
 | Jirachi +19.470%                       | 
 | Dialga +4.045%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Garchomp                               | 
 +----------------------------------------+ 
 | Raw count: 12                          | 
 | Avg. weight: 0.00532417309861          | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Sand Veil 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Scarf 100.000%                  | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 100.000%         | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Sleep Talk 100.000%                    | 
 | Outrage 100.000%                       | 
 | Dragon Claw 100.000%                   | 
 | Earthquake 100.000%                    | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Latias +83.788%                        | 
 | Darkrai +49.002%                       | 
 | Manaphy +42.170%                       | 
 | Starmie +42.170%                       | 
 | Metagross +36.285%                     | 
 | Kyogre +36.156%                        | 
 | Jirachi +35.605%                       | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Froslass                               | 
 +----------------------------------------+ 
 | Raw count: 3                           | 
 | Avg. weight: 0.0145076091171           | 
 | Viability Ceiling: 73                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Snow Cloak 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Focus Sash 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 99.999%          | 
 | Other  0.001%                          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Icy Wind 100.000%                      | 
 | Spikes 100.000%                        | 
 | Shadow Ball 100.000%                   | 
 | Taunt 99.999%                          | 
 | Other  0.001%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Weavile +93.985%                       | 
 | Kyogre +89.902%                        | 
 | Jirachi +81.856%                       | 
 | Palkia +22.096%                        | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Weavile                                | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: 0.0217611000057           | 
 | Viability Ceiling: 73                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Choice Band 100.000%                   | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/0/0/4/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Low Kick 100.000%                      | 
 | Ice Punch 100.000%                     | 
 | Ice Shard 100.000%                     | 
 | Pursuit 100.000%                       | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Froslass +93.986%                      | 
 | Kyogre +89.903%                        | 
 | Jirachi +81.858%                       | 
 | Palkia +22.096%                        | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Cloyster                               | 
 +----------------------------------------+ 
 | Raw count: 2                           | 
 | Avg. weight: 0.021613580576            | 
 | Viability Ceiling: 73                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Shell Armor 100.000%                   | 
 +----------------------------------------+ 
 | Items                                  | 
 | Focus Sash 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Jolly:0/252/4/0/0/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Payback 100.000%                       | 
 | Ice Shard 100.000%                     | 
 | Spikes 100.000%                        | 
 | Rapid Spin 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Giratina-Origin +90.419%               | 
 | Wobbuffet +90.247%                     | 
 | Rayquaza +22.230%                      | 
 | Palkia +22.096%                        | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Lugia                                  | 
 +----------------------------------------+ 
 | Raw count: 4                           | 
 | Avg. weight: 0.0106470106978           | 
 | Viability Ceiling: 71                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Calm:252/0/4/0/252/0 100.000%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Toxic 100.000%                         | 
 | Roost 100.000%                         | 
 | Ice Beam 100.000%                      | 
 | Reflect 50.632%                        | 
 | Psycho Boost 49.368%                   | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Metagross +90.032%                     | 
 | Groudon +28.198%                       | 
 | Mewtwo +28.198%                        | 
 | Rayquaza +22.230%                      | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Darkrai                                | 
 +----------------------------------------+ 
 | Raw count: 4                           | 
 | Avg. weight: 0.00858478712856          | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Bad Dreams 100.000%                    | 
 +----------------------------------------+ 
 | Items                                  | 
 | Chople Berry 100.000%                  | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:0/0/0/252/4/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Dark Void 100.000%                     | 
 | Dark Pulse 100.000%                    | 
 | Thunder 100.000%                       | 
 | Focus Blast 100.000%                   | 
 | Other  0.000%                          | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Garchomp +91.172%                      | 
 | Latias +83.788%                        | 
 | Jirachi +81.858%                       | 
 | Scizor +21.958%                        | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Starmie                                | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 0.0295509286692           | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Natural Cure 100.000%                  | 
 +----------------------------------------+ 
 | Items                                  | 
 | Life Orb 100.000%                      | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:40/0/0/252/0/216 100.000%        | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Hydro Pump 100.000%                    | 
 | Ice Beam 100.000%                      | 
 | Recover 100.000%                       | 
 | Rapid Spin 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Manaphy +95.917%                       | 
 | Garchomp +91.172%                      | 
 | Metagross +90.032%                     | 
 | Kyogre +89.903%                        | 
 | Latias +83.788%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Manaphy                                | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 0.0295509286692           | 
 | Viability Ceiling: 74                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Hydration 100.000%                     | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Timid:252/0/40/0/0/216 100.000%        | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Surf 100.000%                          | 
 | Calm Mind 100.000%                     | 
 | Rest 100.000%                          | 
 | Ice Beam 100.000%                      | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Starmie +95.917%                       | 
 | Garchomp +91.172%                      | 
 | Metagross +90.032%                     | 
 | Kyogre +89.903%                        | 
 | Latias +83.788%                        | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Deoxys-Attack                          | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 0.0273596305005           | 
 | Viability Ceiling: 73                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Pressure 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Focus Sash 100.000%                    | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Hasty:0/252/0/4/0/252 100.000%         | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Shadow Ball 100.000%                   | 
 | Spikes 100.000%                        | 
 | Extreme Speed 100.000%                 | 
 | Superpower 100.000%                    | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Wobbuffet +90.247%                     | 
 | Latias +83.788%                        | 
 | Jirachi +81.858%                       | 
 | Scizor +21.958%                        | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
 +----------------------------------------+ 
 | Skarmory                               | 
 +----------------------------------------+ 
 | Raw count: 1                           | 
 | Avg. weight: 0.02607916581             | 
 | Viability Ceiling: 73                  | 
 +----------------------------------------+ 
 | Abilities                              | 
 | Keen Eye 100.000%                      | 
 +----------------------------------------+ 
 | Items                                  | 
 | Leftovers 100.000%                     | 
 +----------------------------------------+ 
 | Spreads                                | 
 | Bold:248/0/0/0/252/8 100.000%          | 
 +----------------------------------------+ 
 | Moves                                  | 
 | Toxic 100.000%                         | 
 | Spikes 100.000%                        | 
 | Roost 100.000%                         | 
 | Whirlwind 100.000%                     | 
 +----------------------------------------+ 
 | Teammates                              | 
 | Giratina-Origin +90.419%               | 
 | Latias +83.788%                        | 
 | Jirachi +81.858%                       | 
 | Scizor +21.958%                        | 
 | Dialga +4.087%                         | 
 +----------------------------------------+ 
 | Checks and Counters                    | 
 +----------------------------------------+ 
