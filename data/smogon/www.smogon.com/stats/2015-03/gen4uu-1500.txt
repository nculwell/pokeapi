 Total battles: 5
 Avg. weight/team: 0.5
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Arcanine           | 39.22060% | 4      | 40.000% | 4      | 42.105% | 
 | 2    | Milotic            | 34.87509% | 4      | 40.000% | 4      | 42.105% | 
 | 3    | Registeel          | 32.56246% | 3      | 30.000% | 3      | 31.579% | 
 | 4    | Venusaur           | 32.56246% | 3      | 30.000% | 3      | 31.579% | 
 | 5    | Rhyperior          | 32.56246% | 3      | 30.000% | 2      | 21.053% | 
 | 6    | Rotom              | 25.12491% | 2      | 20.000% | 2      | 21.053% | 
 | 7    | Exeggutor          | 24.34551% | 2      | 20.000% | 2      | 21.053% | 
 | 8    | Scyther            | 24.34551% | 2      | 20.000% | 2      | 21.053% | 
 | 9    | Toxicroak          | 20.00000% | 2      | 20.000% | 1      | 10.526% | 
 | 10   | Claydol            | 20.00000% | 2      | 20.000% | 2      | 21.053% | 
 | 11   | Blaziken           | 20.00000% | 2      | 20.000% | 2      | 21.053% | 
 | 12   | Alakazam           | 20.00000% | 2      | 20.000% | 2      | 21.053% | 
 | 13   | Spiritomb          | 15.65449% | 2      | 20.000% | 2      | 21.053% | 
 | 14   | Moltres            | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 15   | Swellow            | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 16   | Torterra           | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 17   | Donphan            | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 18   | Dugtrio            | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 19   | Slowbro            | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 20   | Qwilfish           | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 21   | Blastoise          | 12.56246% | 1      | 10.000% | 1      | 10.526% | 
 | 22   | Jumpluff           | 11.78306% | 1      | 10.000% | 1      | 10.526% | 
 | 23   | Tangrowth          | 11.78306% | 1      | 10.000% | 1      | 10.526% | 
 | 24   | Typhlosion         | 11.78306% | 1      | 10.000% | 1      | 10.526% | 
 | 25   | Victreebel         |  8.21694% | 1      | 10.000% | 1      | 10.526% | 
 | 26   | Vileplume          |  8.21694% | 1      | 10.000% | 0      |  0.000% | 
 | 27   | Ludicolo           |  8.21694% | 1      | 10.000% | 1      | 10.526% | 
 | 28   | Espeon             |  8.21694% | 1      | 10.000% | 1      | 10.526% | 
 | 29   | Omastar            |  8.21694% | 1      | 10.000% | 1      | 10.526% | 
 | 30   | Shuckle            |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 31   | Kangaskhan         |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 32   | Poliwrath          |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 33   | Absol              |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 34   | Torkoal            |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 35   | Primeape           |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 36   | Sceptile           |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 37   | Rampardos          |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 38   | Dodrio             |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 39   | Nidoking           |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 | 40   | Electrode          |  7.43754% | 1      | 10.000% | 1      | 10.526% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
