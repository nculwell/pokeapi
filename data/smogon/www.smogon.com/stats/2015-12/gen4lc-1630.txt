 Total battles: 1
 Avg. weight/team: 0.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Weedle             |  0.00000% | 2      | 100.000% | 2      | 120.000% | 
 | 2    | Kricketot          |  0.00000% | 1      | 50.000% | 1      | 60.000% | 
 | 3    | Bonsly             |  0.00000% | 1      | 50.000% | 1      | 60.000% | 
 | 4    | Gastly             |  0.00000% | 1      | 50.000% | 1      | 60.000% | 
 | 5    | Combee             |  0.00000% | 1      | 50.000% | 1      | 60.000% | 
 | 6    | Caterpie           |  0.00000% | 2      | 100.000% | 1      | 60.000% | 
 | 7    | Burmy              |  0.00000% | 1      | 50.000% | 1      | 60.000% | 
 | 8    | Wurmple            |  0.00000% | 2      | 100.000% | 1      | 60.000% | 
 | 9    | Taillow            |  0.00000% | 1      | 50.000% | 1      | 60.000% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
