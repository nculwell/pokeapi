 Total battles: 61
 Avg. weight/team: 0.007
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Venusaur           | 50.87353% | 41     | 33.607% | 35     | 34.370% | 
 | 2    | Rhyperior          | 44.42151% | 34     | 27.869% | 29     | 28.478% | 
 | 3    | Blaziken           | 43.76717% | 22     | 18.033% | 17     | 16.694% | 
 | 4    | Absol              | 42.16770% | 12     |  9.836% | 10     |  9.820% | 
 | 5    | Milotic            | 42.02240% | 27     | 22.131% | 21     | 20.622% | 
 | 6    | Kabutops           | 34.73719% | 9      |  7.377% | 7      |  6.874% | 
 | 7    | Registeel          | 33.51083% | 24     | 19.672% | 22     | 21.604% | 
 | 8    | Ludicolo           | 33.41089% | 7      |  5.738% | 7      |  6.874% | 
 | 9    | Omastar            | 32.55389% | 13     | 10.656% | 12     | 11.784% | 
 | 10   | Rotom              | 32.24112% | 20     | 16.393% | 13     | 12.766% | 
 | 11   | Mesprit            | 30.95442% | 5      |  4.098% | 4      |  3.928% | 
 | 12   | Alakazam           | 29.64649% | 40     | 32.787% | 33     | 32.406% | 
 | 13   | Scyther            | 14.03399% | 14     | 11.475% | 12     | 11.784% | 
 | 14   | Blastoise          | 13.95298% | 21     | 17.213% | 17     | 16.694% | 
 | 15   | Donphan            | 13.55040% | 11     |  9.016% | 11     | 10.802% | 
 | 16   | Moltres            | 12.46267% | 15     | 12.295% | 9      |  8.838% | 
 | 17   | Toxicroak          |  7.24940% | 8      |  6.557% | 8      |  7.856% | 
 | 18   | Mismagius          |  7.18717% | 18     | 14.754% | 13     | 12.766% | 
 | 19   | Hariyama           |  6.79599% | 7      |  5.738% | 4      |  3.928% | 
 | 20   | Houndoom           |  6.79599% | 8      |  6.557% | 7      |  6.874% | 
 | 21   | Spiritomb          |  6.70358% | 17     | 13.934% | 14     | 13.748% | 
 | 22   | Qwilfish           |  6.70358% | 14     | 11.475% | 14     | 13.748% | 
 | 23   | Uxie               |  6.49444% | 14     | 11.475% | 14     | 13.748% | 
 | 24   | Swellow            |  6.41263% | 13     | 10.656% | 11     | 10.802% | 
 | 25   | Dugtrio            |  6.01086% | 6      |  4.918% | 4      |  3.928% | 
 | 26   | Electrode          |  6.01086% | 7      |  5.738% | 7      |  6.874% | 
 | 27   | Tangrowth          |  5.75908% | 2      |  1.639% | 2      |  1.964% | 
 | 28   | Tauros             |  5.75908% | 1      |  0.820% | 1      |  0.982% | 
 | 29   | Clefable           |  3.71751% | 10     |  8.197% | 9      |  8.838% | 
 | 30   | Azumarill          |  3.48685% | 7      |  5.738% | 7      |  6.874% | 
 | 31   | Ambipom            |  3.30021% | 27     | 22.131% | 23     | 22.586% | 
 | 32   | Arcanine           |  2.81662% | 25     | 20.492% | 20     | 19.640% | 
 | 33   | Sceptile           |  1.65344% | 11     |  9.016% | 9      |  8.838% | 
 | 34   | Ursaring           |  1.59948% | 9      |  7.377% | 7      |  6.874% | 
 | 35   | Lanturn            |  0.48359% | 11     |  9.016% | 9      |  8.838% | 
 | 36   | Drapion            |  0.26307% | 11     |  9.016% | 11     | 10.802% | 
 | 37   | Altaria            |  0.16313% | 11     |  9.016% | 8      |  7.856% | 
 | 38   | Feraligatr         |  0.16313% | 3      |  2.459% | 3      |  2.946% | 
 | 39   | Steelix            |  0.16313% | 3      |  2.459% | 3      |  2.946% | 
 | 40   | Weezing            |  0.00000% | 5      |  4.098% | 3      |  2.946% | 
 | 41   | Quagsire           |  0.00000% | 2      |  1.639% | 1      |  0.982% | 
 | 42   | Seedot             |  0.00000% | 3      |  2.459% | 2      |  1.964% | 
 | 43   | Sandslash          |  0.00000% | 3      |  2.459% | 3      |  2.946% | 
 | 44   | Kangaskhan         |  0.00000% | 6      |  4.918% | 5      |  4.910% | 
 | 45   | Regirock           |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 46   | Hitmonlee          |  0.00000% | 2      |  1.639% | 2      |  1.964% | 
 | 47   | Poliwrath          |  0.00000% | 2      |  1.639% | 2      |  1.964% | 
 | 48   | Raichu             |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 49   | Gligar             |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 50   | Exeggutor          |  0.00000% | 3      |  2.459% | 1      |  0.982% | 
 | 51   | Parasect           |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 52   | Magikarp           |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 53   | Koffing            |  0.00000% | 3      |  2.459% | 2      |  1.964% | 
 | 54   | Nidoqueen          |  0.00000% | 7      |  5.738% | 7      |  6.874% | 
 | 55   | Aggron             |  0.00000% | 3      |  2.459% | 2      |  1.964% | 
 | 56   | Slowbro            |  0.00000% | 6      |  4.918% | 5      |  4.910% | 
 | 57   | Igglybuff          |  0.00000% | 3      |  2.459% | 3      |  2.946% | 
 | 58   | Walrein            |  0.00000% | 6      |  4.918% | 6      |  5.892% | 
 | 59   | Articuno           |  0.00000% | 1      |  0.820% | 0      |  0.000% | 
 | 60   | Pinsir             |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 61   | Cloyster           |  0.00000% | 4      |  3.279% | 2      |  1.964% | 
 | 62   | Claydol            |  0.00000% | 2      |  1.639% | 2      |  1.964% | 
 | 63   | Graveler           |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 64   | Chansey            |  0.00000% | 14     | 11.475% | 12     | 11.784% | 
 | 65   | Skuntank           |  0.00000% | 4      |  3.279% | 4      |  3.928% | 
 | 66   | Politoed           |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 67   | Granbull           |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 68   | Primeape           |  0.00000% | 3      |  2.459% | 2      |  1.964% | 
 | 69   | Diglett            |  0.00000% | 2      |  1.639% | 2      |  1.964% | 
 | 70   | Slowking           |  0.00000% | 11     |  9.016% | 10     |  9.820% | 
 | 71   | Drifblim           |  0.00000% | 2      |  1.639% | 2      |  1.964% | 
 | 72   | Hypno              |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 73   | Jumpluff           |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 74   | Beldum             |  0.00000% | 3      |  2.459% | 3      |  2.946% | 
 | 75   | Victreebel         |  0.00000% | 4      |  3.279% | 4      |  3.928% | 
 | 76   | Pidgeot            |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 77   | Typhlosion         |  0.00000% | 3      |  2.459% | 3      |  2.946% | 
 | 78   | Piloswine          |  0.00000% | 2      |  1.639% | 2      |  1.964% | 
 | 79   | Nidoking           |  0.00000% | 3      |  2.459% | 3      |  2.946% | 
 | 80   | Flareon            |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 81   | Manectric          |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 82   | Charizard          |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 83   | Miltank            |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 84   | Hitmonchan         |  0.00000% | 3      |  2.459% | 3      |  2.946% | 
 | 85   | Luxray             |  0.00000% | 3      |  2.459% | 3      |  2.946% | 
 | 86   | Torterra           |  0.00000% | 5      |  4.098% | 4      |  3.928% | 
 | 87   | Snover             |  0.00000% | 5      |  4.098% | 5      |  4.910% | 
 | 88   | Baltoy             |  0.00000% | 3      |  2.459% | 2      |  1.964% | 
 | 89   | Entei              |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 90   | Espeon             |  0.00000% | 1      |  0.820% | 1      |  0.982% | 
 | 91   | Onix               |  0.00000% | 3      |  2.459% | 2      |  1.964% | 
 | 92   | Hitmontop          |  0.00000% | 15     | 12.295% | 12     | 11.784% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
