 Total battles: 18
 Avg. weight/team: 0.02
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Registeel          | 100.00000% | 3      |  8.333% | 2      |  7.229% | 
 | 2    | Sceptile           | 100.00000% | 4      | 11.111% | 2      |  7.229% | 
 | 3    | Milotic            | 100.00000% | 12     | 33.333% | 11     | 39.759% | 
 | 4    | Houndoom           | 100.00000% | 6      | 16.667% | 4      | 14.458% | 
 | 5    | Nidoking           | 100.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 6    | Rhyperior          | 100.00000% | 7      | 19.444% | 6      | 21.687% | 
 | 7    | Pichu              |  0.00000% | 2      |  5.556% | 1      |  3.614% | 
 | 8    | Toxicroak          |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 9    | Gastrodon          |  0.00000% | 2      |  5.556% | 1      |  3.614% | 
 | 10   | Weezing            |  0.00000% | 6      | 16.667% | 3      | 10.843% | 
 | 11   | Moltres            |  0.00000% | 2      |  5.556% | 1      |  3.614% | 
 | 12   | Flareon            |  0.00000% | 1      |  2.778% | 0      |  0.000% | 
 | 13   | Cloyster           |  0.00000% | 1      |  2.778% | 0      |  0.000% | 
 | 14   | Kangaskhan         |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 15   | Tauros             |  0.00000% | 3      |  8.333% | 2      |  7.229% | 
 | 16   | Spiritomb          |  0.00000% | 4      | 11.111% | 2      |  7.229% | 
 | 17   | Arcanine           |  0.00000% | 4      | 11.111% | 2      |  7.229% | 
 | 18   | Shelgon            |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 19   | Manectric          |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 20   | Uxie               |  0.00000% | 3      |  8.333% | 2      |  7.229% | 
 | 21   | Swellow            |  0.00000% | 7      | 19.444% | 6      | 21.687% | 
 | 22   | Beedrill           |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 23   | Ambipom            |  0.00000% | 5      | 13.889% | 5      | 18.072% | 
 | 24   | Hitmonlee          |  0.00000% | 1      |  2.778% | 0      |  0.000% | 
 | 25   | Mesprit            |  0.00000% | 4      | 11.111% | 3      | 10.843% | 
 | 26   | Azumarill          |  0.00000% | 6      | 16.667% | 5      | 18.072% | 
 | 27   | Rotom              |  0.00000% | 1      |  2.778% | 0      |  0.000% | 
 | 28   | Poliwrath          |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 29   | Lopunny            |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 30   | Octillery          |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 31   | Donphan            |  0.00000% | 7      | 19.444% | 4      | 14.458% | 
 | 32   | Raichu             |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 33   | Charizard          |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 34   | Porygon2           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 35   | Magcargo           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 36   | Miltank            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 37   | Drifblim           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 38   | Blastoise          |  0.00000% | 2      |  5.556% | 0      |  0.000% | 
 | 39   | Blaziken           |  0.00000% | 5      | 13.889% | 4      | 14.458% | 
 | 40   | Rhydon             |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 41   | Chansey            |  0.00000% | 3      |  8.333% | 1      |  3.614% | 
 | 42   | Absol              |  0.00000% | 4      | 11.111% | 2      |  7.229% | 
 | 43   | Jumpluff           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 44   | Luxray             |  0.00000% | 4      | 11.111% | 4      | 14.458% | 
 | 45   | Clefable           |  0.00000% | 2      |  5.556% | 1      |  3.614% | 
 | 46   | Alakazam           |  0.00000% | 7      | 19.444% | 5      | 18.072% | 
 | 47   | Drapion            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 48   | Torterra           |  0.00000% | 6      | 16.667% | 6      | 21.687% | 
 | 49   | Torkoal            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 50   | Mismagius          |  0.00000% | 3      |  8.333% | 3      | 10.843% | 
 | 51   | Castform           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 52   | Claydol            |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 53   | Ditto              |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 54   | Exploud            |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 55   | Shuckle            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 56   | Haunter            |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 57   | Staravia           |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 58   | Gardevoir          |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 59   | Tangrowth          |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 60   | Medicham           |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 61   | Cradily            |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 62   | Espeon             |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 63   | Ninetales          |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 64   | Venusaur           |  0.00000% | 4      | 11.111% | 2      |  7.229% | 
 | 65   | Steelix            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 66   | Lanturn            |  0.00000% | 4      | 11.111% | 3      | 10.843% | 
 | 67   | Glalie             |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 68   | Altaria            |  0.00000% | 3      |  8.333% | 2      |  7.229% | 
 | 69   | Dugtrio            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 70   | Typhlosion         |  0.00000% | 3      |  8.333% | 2      |  7.229% | 
 | 71   | Leafeon            |  0.00000% | 2      |  5.556% | 1      |  3.614% | 
 | 72   | Aggron             |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 73   | Scyther            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 74   | Shedinja           |  0.00000% | 1      |  2.778% | 0      |  0.000% | 
 | 75   | Delibird           |  0.00000% | 3      |  8.333% | 2      |  7.229% | 
 | 76   | Qwilfish           |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 77   | Ampharos           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 78   | Flaaffy            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 79   | Sneasel            |  0.00000% | 2      |  5.556% | 2      |  7.229% | 
 | 80   | Politoed           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 | 81   | Meganium           |  0.00000% | 3      |  8.333% | 3      | 10.843% | 
 | 82   | Hitmontop          |  0.00000% | 4      | 11.111% | 2      |  7.229% | 
 | 83   | Ledian             |  0.00000% | 1      |  2.778% | 0      |  0.000% | 
 | 84   | Articuno           |  0.00000% | 1      |  2.778% | 1      |  3.614% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
