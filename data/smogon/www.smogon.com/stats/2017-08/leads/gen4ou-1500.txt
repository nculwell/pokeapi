 Total leads: 31740
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Azelf              |  8.06278% | 2474   |  7.795% | 
 | 2    | Metagross          |  6.62121% | 1589   |  5.006% | 
 | 3    | Aerodactyl         |  5.68604% | 1558   |  4.909% | 
 | 4    | Jirachi            |  5.43807% | 1189   |  3.746% | 
 | 5    | Machamp            |  5.33299% | 1451   |  4.572% | 
 | 6    | Zapdos             |  4.76603% | 1083   |  3.412% | 
 | 7    | Heatran            |  4.70133% | 1275   |  4.017% | 
 | 8    | Infernape          |  3.52279% | 1439   |  4.534% | 
 | 9    | Swampert           |  2.98065% | 821    |  2.587% | 
 | 10   | Skarmory           |  2.79257% | 946    |  2.980% | 
 | 11   | Empoleon           |  2.68381% | 880    |  2.773% | 
 | 12   | Gliscor            |  2.63676% | 916    |  2.886% | 
 | 13   | Starmie            |  2.27001% | 534    |  1.682% | 
 | 14   | Gengar             |  2.22084% | 698    |  2.199% | 
 | 15   | Dragonite          |  2.09929% | 554    |  1.745% | 
 | 16   | Roserade           |  2.07194% | 716    |  2.256% | 
 | 17   | Ninjask            |  2.06435% | 881    |  2.776% | 
 | 18   | Tyranitar          |  1.97593% | 587    |  1.849% | 
 | 19   | Forretress         |  1.95199% | 642    |  2.023% | 
 | 20   | Bronzong           |  1.93439% | 581    |  1.830% | 
 | 21   | Hippowdon          |  1.62461% | 404    |  1.273% | 
 | 22   | Smeargle           |  1.60366% | 456    |  1.437% | 
 | 23   | Flygon             |  1.52455% | 401    |  1.263% | 
 | 24   | Uxie               |  1.48766% | 579    |  1.824% | 
 | 25   | Gyarados           |  1.21627% | 360    |  1.134% | 
 | 26   | Weavile            |  1.18916% | 423    |  1.333% | 
 | 27   | Scizor             |  1.09118% | 348    |  1.096% | 
 | 28   | Froslass           |  0.90895% | 272    |  0.857% | 
 | 29   | Breloom            |  0.89907% | 322    |  1.014% | 
 | 30   | Celebi             |  0.87806% | 389    |  1.226% | 
 | 31   | Crobat             |  0.83307% | 252    |  0.794% | 
 | 32   | Rotom-Wash         |  0.75865% | 227    |  0.715% | 
 | 33   | Mamoswine          |  0.57092% | 177    |  0.558% | 
 | 34   | Porygon-Z          |  0.56345% | 143    |  0.451% | 
 | 35   | Magmortar          |  0.56113% | 131    |  0.413% | 
 | 36   | Lucario            |  0.51891% | 191    |  0.602% | 
 | 37   | Yanmega            |  0.50358% | 146    |  0.460% | 
 | 38   | Alakazam           |  0.49142% | 133    |  0.419% | 
 | 39   | Dusknoir           |  0.48413% | 148    |  0.466% | 
 | 40   | Abomasnow          |  0.46561% | 151    |  0.476% | 
 | 41   | Ambipom            |  0.42646% | 165    |  0.520% | 
 | 42   | Shaymin            |  0.41834% | 80     |  0.252% | 
 | 43   | Staraptor          |  0.31023% | 247    |  0.778% | 
 | 44   | Blissey            |  0.30886% | 150    |  0.473% | 
 | 45   | Vaporeon           |  0.30350% | 87     |  0.274% | 
 | 46   | Blaziken           |  0.30090% | 154    |  0.485% | 
 | 47   | Medicham           |  0.30074% | 96     |  0.302% | 
 | 48   | Arcanine           |  0.25815% | 82     |  0.258% | 
 | 49   | Qwilfish           |  0.25561% | 49     |  0.154% | 
 | 50   | Hariyama           |  0.23873% | 52     |  0.164% | 
 | 51   | Charizard          |  0.22470% | 152    |  0.479% | 
 | 52   | Blastoise          |  0.22249% | 67     |  0.211% | 
 | 53   | Snorlax            |  0.21060% | 85     |  0.268% | 
 | 54   | Nidoqueen          |  0.21025% | 52     |  0.164% | 
 | 55   | Togekiss           |  0.20595% | 98     |  0.309% | 
 | 56   | Jolteon            |  0.20514% | 316    |  0.996% | 
 | 57   | Dugtrio            |  0.19716% | 43     |  0.135% | 
 | 58   | Tentacruel         |  0.18913% | 68     |  0.214% | 
 | 59   | Electivire         |  0.18683% | 98     |  0.309% | 
 | 60   | Heracross          |  0.18273% | 94     |  0.296% | 
 | 61   | Gallade            |  0.18157% | 116    |  0.365% | 
 | 62   | Primeape           |  0.17625% | 80     |  0.252% | 
 | 63   | Solrock            |  0.17472% | 37     |  0.117% | 
 | 64   | Torterra           |  0.16889% | 116    |  0.365% | 
 | 65   | Magnezone          |  0.14751% | 66     |  0.208% | 
 | 66   | Kingdra            |  0.14504% | 51     |  0.161% | 
 | 67   | Spiritomb          |  0.14320% | 93     |  0.293% | 
 | 68   | Umbreon            |  0.13276% | 125    |  0.394% | 
 | 69   | Pikachu            |  0.13185% | 84     |  0.265% | 
 | 70   | Skuntank           |  0.12918% | 35     |  0.110% | 
 | 71   | Electrode          |  0.12019% | 60     |  0.189% | 
 | 72   | Typhlosion         |  0.11975% | 85     |  0.268% | 
 | 73   | Honchkrow          |  0.10577% | 32     |  0.101% | 
 | 74   | Dunsparce          |  0.10266% | 21     |  0.066% | 
 | 75   | Moltres            |  0.09165% | 28     |  0.088% | 
 | 76   | Steelix            |  0.09105% | 26     |  0.082% | 
 | 77   | Linoone            |  0.08904% | 21     |  0.066% | 
 | 78   | Aggron             |  0.07995% | 49     |  0.154% | 
 | 79   | Nidoking           |  0.07817% | 66     |  0.208% | 
 | 80   | Sceptile           |  0.07313% | 29     |  0.091% | 
 | 81   | Rhyperior          |  0.06476% | 31     |  0.098% | 
 | 82   | Registeel          |  0.06389% | 15     |  0.047% | 
 | 83   | Donphan            |  0.06205% | 23     |  0.072% | 
 | 84   | Espeon             |  0.06070% | 29     |  0.091% | 
 | 85   | Chansey            |  0.05894% | 23     |  0.072% | 
 | 86   | Hitmontop          |  0.05401% | 11     |  0.035% | 
 | 87   | Rotom-Heat         |  0.05288% | 12     |  0.038% | 
 | 88   | Cresselia          |  0.05199% | 40     |  0.126% | 
 | 89   | Swellow            |  0.05061% | 72     |  0.227% | 
 | 90   | Cloyster           |  0.04925% | 27     |  0.085% | 
 | 91   | Clefable           |  0.04883% | 15     |  0.047% | 
 | 92   | Rampardos          |  0.04718% | 57     |  0.180% | 
 | 93   | Raikou             |  0.04707% | 11     |  0.035% | 
 | 94   | Exeggutor          |  0.04379% | 25     |  0.079% | 
 | 95   | Drapion            |  0.04332% | 20     |  0.063% | 
 | 96   | Feraligatr         |  0.04229% | 39     |  0.123% | 
 | 97   | Ninetales          |  0.04192% | 9      |  0.028% | 
 | 98   | Omastar            |  0.04184% | 18     |  0.057% | 
 | 99   | Gardevoir          |  0.04125% | 16     |  0.050% | 
 | 100  | Suicune            |  0.04069% | 31     |  0.098% | 
 | 101  | Slaking            |  0.03552% | 29     |  0.091% | 
 | 102  | Lopunny            |  0.03500% | 17     |  0.054% | 
 | 103  | Shuckle            |  0.03460% | 48     |  0.151% | 
 | 104  | Aipom              |  0.03403% | 91     |  0.287% | 
 | 105  | Manectric          |  0.03316% | 8      |  0.025% | 
 | 106  | Venusaur           |  0.03245% | 37     |  0.117% | 
 | 107  | Pidgeot            |  0.03090% | 34     |  0.107% | 
 | 108  | Regirock           |  0.02999% | 10     |  0.032% | 
 | 109  | Mesprit            |  0.02884% | 20     |  0.063% | 
 | 110  | Granbull           |  0.02869% | 6      |  0.019% | 
 | 111  | Lapras             |  0.02864% | 24     |  0.076% | 
 | 112  | Milotic            |  0.02709% | 111    |  0.350% | 
 | 113  | Camerupt           |  0.02669% | 11     |  0.035% | 
 | 114  | Plusle             |  0.02654% | 5      |  0.016% | 
 | 115  | Gastrodon          |  0.02640% | 12     |  0.038% | 
 | 116  | Kangaskhan         |  0.02630% | 5      |  0.016% | 
 | 117  | Slowbro            |  0.02518% | 10     |  0.032% | 
 | 118  | Magneton           |  0.02475% | 8      |  0.025% | 
 | 119  | Weezing            |  0.02422% | 19     |  0.060% | 
 | 120  | Relicanth          |  0.02380% | 10     |  0.032% | 
 | 121  | Absol              |  0.02365% | 11     |  0.035% | 
 | 122  | Houndoom           |  0.02361% | 12     |  0.038% | 
 | 123  | Pinsir             |  0.02218% | 7      |  0.022% | 
 | 124  | Rotom-Mow          |  0.02124% | 6      |  0.019% | 
 | 125  | Wigglytuff         |  0.02090% | 4      |  0.013% | 
 | 126  | Rattata            |  0.02058% | 12     |  0.038% | 
 | 127  | Ampharos           |  0.02027% | 32     |  0.101% | 
 | 128  | Scyther            |  0.01983% | 49     |  0.154% | 
 | 129  | Mawile             |  0.01910% | 13     |  0.041% | 
 | 130  | Azumarill          |  0.01773% | 11     |  0.035% | 
 | 131  | Tauros             |  0.01745% | 24     |  0.076% | 
 | 132  | Shedinja           |  0.01662% | 21     |  0.066% | 
 | 133  | Dragonair          |  0.01625% | 6      |  0.019% | 
 | 134  | Claydol            |  0.01580% | 9      |  0.028% | 
 | 135  | Meganium           |  0.01575% | 27     |  0.085% | 
 | 136  | Banette            |  0.01568% | 3      |  0.009% | 
 | 137  | Ludicolo           |  0.01544% | 9      |  0.028% | 
 | 138  | Tangrowth          |  0.01485% | 6      |  0.019% | 
 | 139  | Floatzel           |  0.01484% | 15     |  0.047% | 
 | 140  | Entei              |  0.01356% | 6      |  0.019% | 
 | 141  | Regigigas          |  0.01300% | 12     |  0.038% | 
 | 142  | Jumpluff           |  0.01268% | 10     |  0.032% | 
 | 143  | Ursaring           |  0.01216% | 9      |  0.028% | 
 | 144  | Mismagius          |  0.01188% | 13     |  0.041% | 
 | 145  | Kabutops           |  0.01080% | 3      |  0.009% | 
 | 146  | Luxray             |  0.01068% | 29     |  0.091% | 
 | 147  | Armaldo            |  0.01046% | 6      |  0.019% | 
 | 148  | Stantler           |  0.01032% | 7      |  0.022% | 
 | 149  | Leafeon            |  0.01002% | 5      |  0.016% | 
 | 150  | Rapidash           |  0.00994% | 11     |  0.035% | 
 | 151  | Golduck            |  0.00963% | 20     |  0.063% | 
 | 152  | Kingler            |  0.00952% | 6      |  0.019% | 
 | 153  | Zangoose           |  0.00909% | 11     |  0.035% | 
 | 154  | Marowak            |  0.00879% | 4      |  0.013% | 
 | 155  | Drifblim           |  0.00858% | 55     |  0.173% | 
 | 156  | Articuno           |  0.00817% | 4      |  0.013% | 
 | 157  | Glaceon            |  0.00794% | 13     |  0.041% | 
 | 158  | Kecleon            |  0.00782% | 3      |  0.009% | 
 | 159  | Rhydon             |  0.00763% | 3      |  0.009% | 
 | 160  | Crawdaunt          |  0.00669% | 6      |  0.019% | 
 | 161  | Poliwrath          |  0.00641% | 4      |  0.013% | 
 | 162  | Cacturne           |  0.00624% | 4      |  0.013% | 
 | 163  | Geodude            |  0.00574% | 2      |  0.006% | 
 | 164  | Pachirisu          |  0.00574% | 60     |  0.189% | 
 | 165  | Muk                |  0.00570% | 3      |  0.009% | 
 | 166  | Miltank            |  0.00569% | 7      |  0.022% | 
 | 167  | Sableye            |  0.00562% | 6      |  0.019% | 
 | 168  | Grumpig            |  0.00561% | 4      |  0.013% | 
 | 169  | Persian            |  0.00554% | 10     |  0.032% | 
 | 170  | Lumineon           |  0.00549% | 5      |  0.016% | 
 | 171  | Hitmonchan         |  0.00547% | 3      |  0.009% | 
 | 172  | Vileplume          |  0.00541% | 9      |  0.028% | 
 | 173  | Regice             |  0.00532% | 3      |  0.009% | 
 | 174  | Jigglypuff         |  0.00522% | 4      |  0.013% | 
 | 175  | Charmeleon         |  0.00521% | 3      |  0.009% | 
 | 176  | Mightyena          |  0.00520% | 4      |  0.013% | 
 | 177  | Vespiquen          |  0.00520% | 4      |  0.013% | 
 | 178  | Sudowoodo          |  0.00512% | 2      |  0.006% | 
 | 179  | Cradily            |  0.00501% | 1      |  0.003% | 
 | 180  | Sharpedo           |  0.00457% | 3      |  0.009% | 
 | 181  | Lanturn            |  0.00452% | 7      |  0.022% | 
 | 182  | Golem              |  0.00448% | 3      |  0.009% | 
 | 183  | Flareon            |  0.00432% | 8      |  0.025% | 
 | 184  | Shiftry            |  0.00431% | 2      |  0.006% | 
 | 185  | Quagsire           |  0.00420% | 5      |  0.016% | 
 | 186  | Rotom-Frost        |  0.00417% | 1      |  0.003% | 
 | 187  | Sneasel            |  0.00397% | 2      |  0.006% | 
 | 188  | Girafarig          |  0.00396% | 3      |  0.009% | 
 | 189  | Furret             |  0.00381% | 3      |  0.009% | 
 | 190  | Slowking           |  0.00370% | 3      |  0.009% | 
 | 191  | Arbok              |  0.00364% | 2      |  0.006% | 
 | 192  | Quilava            |  0.00346% | 1      |  0.003% | 
 | 193  | Altaria            |  0.00319% | 2      |  0.006% | 
 | 194  | Delcatty           |  0.00311% | 2      |  0.006% | 
 | 195  | Whiscash           |  0.00310% | 2      |  0.006% | 
 | 196  | Gible              |  0.00305% | 2      |  0.006% | 
 | 197  | Sunflora           |  0.00286% | 2      |  0.006% | 
 | 198  | Onix               |  0.00278% | 25     |  0.079% | 
 | 199  | Raichu             |  0.00260% | 8      |  0.025% | 
 | 200  | Mothim             |  0.00257% | 1      |  0.003% | 
 | 201  | Chimchar           |  0.00246% | 2      |  0.006% | 
 | 202  | Combusken          |  0.00228% | 1      |  0.003% | 
 | 203  | Mantine            |  0.00224% | 2      |  0.006% | 
 | 204  | Cubone             |  0.00208% | 1      |  0.003% | 
 | 205  | Magikarp           |  0.00206% | 1      |  0.003% | 
 | 206  | Beedrill           |  0.00204% | 1      |  0.003% | 
 | 207  | Eevee              |  0.00198% | 1      |  0.003% | 
 | 208  | Walrein            |  0.00198% | 1      |  0.003% | 
 | 209  | Croconaw           |  0.00198% | 1      |  0.003% | 
 | 210  | Gabite             |  0.00198% | 1      |  0.003% | 
 | 211  | Chingling          |  0.00198% | 1      |  0.003% | 
 | 212  | Toxicroak          |  0.00197% | 1      |  0.003% | 
 | 213  | Roselia            |  0.00196% | 1      |  0.003% | 
 | 214  | Bastiodon          |  0.00194% | 1      |  0.003% | 
 | 215  | Politoed           |  0.00187% | 1      |  0.003% | 
 | 216  | Spinda             |  0.00178% | 1      |  0.003% | 
 | 217  | Shelgon            |  0.00168% | 1      |  0.003% | 
 | 218  | Tropius            |  0.00151% | 1      |  0.003% | 
 | 219  | Probopass          |  0.00137% | 4      |  0.013% | 
 | 220  | Pelipper           |  0.00106% | 4      |  0.013% | 
 | 221  | Piloswine          |  0.00085% | 4      |  0.013% | 
 | 222  | Magnemite          |  0.00082% | 1      |  0.003% | 
 | 223  | Rotom              |  0.00072% | 1      |  0.003% | 
 | 224  | Grovyle            |  0.00017% | 1      |  0.003% | 
 | 225  | Lickitung          |  0.00005% | 1      |  0.003% | 
 | 226  | Dodrio             |  0.00004% | 1      |  0.003% | 
 | 227  | Bulbasaur          |  0.00004% | 1      |  0.003% | 
 | 228  | Piplup             |  0.00001% | 3      |  0.009% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
