 Total leads: 102
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Electrode          | 23.18795% | 14     | 13.725% | 
 | 2    | Ambipom            | 22.23855% | 20     | 19.608% | 
 | 3    | Registeel          |  8.50908% | 10     |  9.804% | 
 | 4    | Uxie               |  4.46919% | 5      |  4.902% | 
 | 5    | Omastar            |  3.52184% | 4      |  3.922% | 
 | 6    | Aggron             |  3.51919% | 4      |  3.922% | 
 | 7    | Milotic            |  2.83501% | 3      |  2.941% | 
 | 8    | Nidoking           |  2.68905% | 2      |  1.961% | 
 | 9    | Ursaring           |  1.84402% | 3      |  2.941% | 
 | 10   | Mesprit            |  1.71134% | 2      |  1.961% | 
 | 11   | Cloyster           |  1.68265% | 2      |  1.961% | 
 | 12   | Venusaur           |  1.65894% | 2      |  1.961% | 
 | 13   | Nidoqueen          |  1.65213% | 1      |  0.980% | 
 | 14   | Spiritomb          |  1.54927% | 3      |  2.941% | 
 | 15   | Butterfree         |  1.34845% | 1      |  0.980% | 
 | 16   | Jynx               |  1.30858% | 1      |  0.980% | 
 | 17   | Absol              |  1.28720% | 2      |  1.961% | 
 | 18   | Torterra           |  1.27978% | 2      |  1.961% | 
 | 19   | Drapion            |  1.25775% | 2      |  1.961% | 
 | 20   | Staravia           |  1.16024% | 2      |  1.961% | 
 | 21   | Snover             |  1.13529% | 2      |  1.961% | 
 | 22   | Hitmonchan         |  1.05691% | 1      |  0.980% | 
 | 23   | Espeon             |  1.05691% | 1      |  0.980% | 
 | 24   | Sandslash          |  0.76354% | 1      |  0.980% | 
 | 25   | Claydol            |  0.76140% | 1      |  0.980% | 
 | 26   | Slowbro            |  0.76140% | 1      |  0.980% | 
 | 27   | Arcanine           |  0.74436% | 1      |  0.980% | 
 | 28   | Aipom              |  0.62574% | 1      |  0.980% | 
 | 29   | Linoone            |  0.62574% | 1      |  0.980% | 
 | 30   | Hippopotas         |  0.62574% | 1      |  0.980% | 
 | 31   | Alakazam           |  0.62574% | 1      |  0.980% | 
 | 32   | Kabutops           |  0.62574% | 1      |  0.980% | 
 | 33   | Blastoise          |  0.62574% | 1      |  0.980% | 
 | 34   | Sceptile           |  0.60204% | 1      |  0.980% | 
 | 35   | Wigglytuff         |  0.41759% | 1      |  0.980% | 
 | 36   | Xatu               |  0.23591% | 1      |  0.980% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
