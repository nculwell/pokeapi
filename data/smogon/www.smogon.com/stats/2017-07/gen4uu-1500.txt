 Total battles: 40
 Avg. weight/team: 0.585
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Ludicolo           | 44.43344% | 28     | 35.000% | 23     | 33.659% | 
 | 2    | Arcanine           | 36.10205% | 28     | 35.000% | 24     | 35.122% | 
 | 3    | Feraligatr         | 34.81812% | 24     | 30.000% | 22     | 32.195% | 
 | 4    | Espeon             | 32.08052% | 22     | 27.500% | 19     | 27.805% | 
 | 5    | Ambipom            | 29.47388% | 21     | 26.250% | 21     | 30.732% | 
 | 6    | Steelix            | 28.13113% | 20     | 25.000% | 18     | 26.341% | 
 | 7    | Alakazam           | 19.13795% | 17     | 21.250% | 14     | 20.488% | 
 | 8    | Kabutops           | 18.72769% | 11     | 13.750% | 8      | 11.707% | 
 | 9    | Uxie               | 16.29903% | 9      | 11.250% | 8      | 11.707% | 
 | 10   | Lanturn            | 13.58314% | 7      |  8.750% | 4      |  5.854% | 
 | 11   | Gorebyss           | 12.78817% | 6      |  7.500% | 4      |  5.854% | 
 | 12   | Electrode          | 12.78817% | 6      |  7.500% | 6      |  8.780% | 
 | 13   | Cloyster           | 12.58541% | 13     | 16.250% | 12     | 17.561% | 
 | 14   | Hitmonlee          | 12.08135% | 10     | 12.500% | 7      | 10.244% | 
 | 15   | Milotic            | 11.31425% | 10     | 12.500% | 8      | 11.707% | 
 | 16   | Spiritomb          |  9.71866% | 8      | 10.000% | 7      | 10.244% | 
 | 17   | Mismagius          |  9.53141% | 8      | 10.000% | 7      | 10.244% | 
 | 18   | Houndoom           |  9.25554% | 8      | 10.000% | 6      |  8.780% | 
 | 19   | Rhyperior          |  9.16380% | 8      | 10.000% | 8      | 11.707% | 
 | 20   | Blaziken           |  8.64775% | 9      | 11.250% | 9      | 13.171% | 
 | 21   | Aggron             |  8.39142% | 6      |  7.500% | 5      |  7.317% | 
 | 22   | Absol              |  6.23790% | 5      |  6.250% | 5      |  7.317% | 
 | 23   | Chansey            |  5.97051% | 7      |  8.750% | 7      | 10.244% | 
 | 24   | Azumarill          |  5.95982% | 4      |  5.000% | 3      |  4.390% | 
 | 25   | Registeel          |  5.81120% | 5      |  6.250% | 5      |  7.317% | 
 | 26   | Leafeon            |  5.79962% | 4      |  5.000% | 4      |  5.854% | 
 | 27   | Swellow            |  5.79749% | 4      |  5.000% | 3      |  4.390% | 
 | 28   | Slowbro            |  5.60661% | 4      |  5.000% | 3      |  4.390% | 
 | 29   | Scyther            |  5.26964% | 5      |  6.250% | 4      |  5.854% | 
 | 30   | Venusaur           |  5.10188% | 5      |  6.250% | 4      |  5.854% | 
 | 31   | Mesprit            |  5.06155% | 5      |  6.250% | 5      |  7.317% | 
 | 32   | Carnivine          |  4.64796% | 3      |  3.750% | 2      |  2.927% | 
 | 33   | Dugtrio            |  4.34573% | 4      |  5.000% | 4      |  5.854% | 
 | 34   | Altaria            |  4.14406% | 4      |  5.000% | 4      |  5.854% | 
 | 35   | Donphan            |  4.02774% | 3      |  3.750% | 3      |  4.390% | 
 | 36   | Moltres            |  4.02107% | 3      |  3.750% | 3      |  4.390% | 
 | 37   | Exeggutor          |  3.76494% | 3      |  3.750% | 3      |  4.390% | 
 | 38   | Primeape           |  3.71264% | 3      |  3.750% | 2      |  2.927% | 
 | 39   | Drapion            |  3.70890% | 4      |  5.000% | 3      |  4.390% | 
 | 40   | Claydol            |  3.63587% | 3      |  3.750% | 3      |  4.390% | 
 | 41   | Poliwrath          |  3.24384% | 3      |  3.750% | 3      |  4.390% | 
 | 42   | Sceptile           |  3.20656% | 4      |  5.000% | 4      |  5.854% | 
 | 43   | Toxicroak          |  3.20656% | 4      |  5.000% | 4      |  5.854% | 
 | 44   | Blastoise          |  3.14035% | 3      |  3.750% | 1      |  1.463% | 
 | 45   | Gardevoir          |  3.08160% | 2      |  2.500% | 0      |  0.000% | 
 | 46   | Tangrowth          |  3.07619% | 3      |  3.750% | 3      |  4.390% | 
 | 47   | Clefable           |  3.06734% | 3      |  3.750% | 2      |  2.927% | 
 | 48   | Metang             |  3.05773% | 2      |  2.500% | 1      |  1.463% | 
 | 49   | Kangaskhan         |  2.99812% | 3      |  3.750% | 3      |  4.390% | 
 | 50   | Weezing            |  2.95698% | 3      |  3.750% | 2      |  2.927% | 
 | 51   | Lickilicky         |  2.93261% | 3      |  3.750% | 3      |  4.390% | 
 | 52   | Hitmontop          |  2.68550% | 2      |  2.500% | 2      |  2.927% | 
 | 53   | Torterra           |  2.62939% | 3      |  3.750% | 3      |  4.390% | 
 | 54   | Onix               |  2.48206% | 2      |  2.500% | 2      |  2.927% | 
 | 55   | Omastar            |  2.46790% | 2      |  2.500% | 2      |  2.927% | 
 | 56   | Pikachu            |  2.43912% | 4      |  5.000% | 4      |  5.854% | 
 | 57   | Slowking           |  2.42219% | 2      |  2.500% | 1      |  1.463% | 
 | 58   | Luxray             |  2.41061% | 2      |  2.500% | 2      |  2.927% | 
 | 59   | Butterfree         |  2.23060% | 2      |  2.500% | 1      |  1.463% | 
 | 60   | Qwilfish           |  2.03882% | 2      |  2.500% | 2      |  2.927% | 
 | 61   | Cradily            |  2.01203% | 3      |  3.750% | 3      |  4.390% | 
 | 62   | Purugly            |  1.53310% | 1      |  1.250% | 1      |  1.463% | 
 | 63   | Tangela            |  1.53310% | 1      |  1.250% | 1      |  1.463% | 
 | 64   | Victreebel         |  1.53310% | 1      |  1.250% | 1      |  1.463% | 
 | 65   | Rapidash           |  1.53310% | 1      |  1.250% | 0      |  0.000% | 
 | 66   | Meganium           |  1.52463% | 1      |  1.250% | 1      |  1.463% | 
 | 67   | Floatzel           |  1.52463% | 1      |  1.250% | 1      |  1.463% | 
 | 68   | Gastrodon          |  1.52463% | 1      |  1.250% | 1      |  1.463% | 
 | 69   | Manectric          |  1.52463% | 1      |  1.250% | 1      |  1.463% | 
 | 70   | Shedinja           |  1.37314% | 1      |  1.250% | 1      |  1.463% | 
 | 71   | Ursaring           |  1.34275% | 1      |  1.250% | 0      |  0.000% | 
 | 72   | Articuno           |  1.33284% | 1      |  1.250% | 1      |  1.463% | 
 | 73   | Slaking            |  1.33284% | 1      |  1.250% | 1      |  1.463% | 
 | 74   | Seviper            |  1.33284% | 1      |  1.250% | 1      |  1.463% | 
 | 75   | Marowak            |  1.19038% | 2      |  2.500% | 1      |  1.463% | 
 | 76   | Hypno              |  1.09148% | 2      |  2.500% | 1      |  1.463% | 
 | 77   | Relicanth          |  0.94897% | 1      |  1.250% | 1      |  1.463% | 
 | 78   | Bastiodon          |  0.94897% | 1      |  1.250% | 1      |  1.463% | 
 | 79   | Cherrim            |  0.94597% | 1      |  1.250% | 1      |  1.463% | 
 | 80   | Torkoal            |  0.90584% | 2      |  2.500% | 1      |  1.463% | 
 | 81   | Charizard          |  0.82645% | 1      |  1.250% | 1      |  1.463% | 
 | 82   | Magneton           |  0.82645% | 1      |  1.250% | 0      |  0.000% | 
 | 83   | Sharpedo           |  0.82165% | 1      |  1.250% | 1      |  1.463% | 
 | 84   | Tauros             |  0.82165% | 1      |  1.250% | 1      |  1.463% | 
 | 85   | Muk                |  0.82165% | 1      |  1.250% | 1      |  1.463% | 
 | 86   | Ampharos           |  0.82165% | 1      |  1.250% | 1      |  1.463% | 
 | 87   | Piloswine          |  0.82165% | 1      |  1.250% | 1      |  1.463% | 
 | 88   | Minun              |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 89   | Shelgon            |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 90   | Wigglytuff         |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 91   | Nidoking           |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 92   | Snorunt            |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 93   | Mothim             |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 94   | Poliwhirl          |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 95   | Jumpluff           |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 96   | Pidgeot            |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 97   | Entei              |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 98   | Glalie             |  0.79497% | 1      |  1.250% | 1      |  1.463% | 
 | 99   | Grotle             |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 100  | Houndour           |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 101  | Duskull            |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 102  | Castform           |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 103  | Shuppet            |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 104  | Togepi             |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 105  | Rotom              |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 106  | Cyndaquil          |  0.78612% | 1      |  1.250% | 1      |  1.463% | 
 | 107  | Golem              |  0.70597% | 1      |  1.250% | 1      |  1.463% | 
 | 108  | Beedrill           |  0.70597% | 1      |  1.250% | 1      |  1.463% | 
 | 109  | Furret             |  0.70597% | 1      |  1.250% | 1      |  1.463% | 
 | 110  | Octillery          |  0.70597% | 1      |  1.250% | 1      |  1.463% | 
 | 111  | Walrein            |  0.69607% | 1      |  1.250% | 1      |  1.463% | 
 | 112  | Probopass          |  0.69607% | 1      |  1.250% | 1      |  1.463% | 
 | 113  | Dragonair          |  0.51043% | 1      |  1.250% | 1      |  1.463% | 
 | 114  | Haunter            |  0.51043% | 1      |  1.250% | 1      |  1.463% | 
 | 115  | Wailord            |  0.39541% | 1      |  1.250% | 1      |  1.463% | 
 | 116  | Dusclops           |  0.39541% | 1      |  1.250% | 1      |  1.463% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
