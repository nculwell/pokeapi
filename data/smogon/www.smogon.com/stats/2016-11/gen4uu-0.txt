 Total battles: 31
 Avg. weight/team: 1.0
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | Real   | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
 | 1    | Ambipom            | 43.54839% | 27     | 43.548% | 22     | 42.997% | 
 | 2    | Venusaur           | 29.03226% | 18     | 29.032% | 18     | 35.179% | 
 | 3    | Alakazam           | 24.19355% | 15     | 24.194% | 14     | 27.362% | 
 | 4    | Blaziken           | 22.58065% | 14     | 22.581% | 7      | 13.681% | 
 | 5    | Donphan            | 22.58065% | 14     | 22.581% | 12     | 23.453% | 
 | 6    | Milotic            | 19.35484% | 12     | 19.355% | 11     | 21.498% | 
 | 7    | Toxicroak          | 19.35484% | 12     | 19.355% | 10     | 19.544% | 
 | 8    | Mesprit            | 14.51613% | 9      | 14.516% | 9      | 17.590% | 
 | 9    | Registeel          | 14.51613% | 9      | 14.516% | 9      | 17.590% | 
 | 10   | Arcanine           | 12.90323% | 8      | 12.903% | 6      | 11.726% | 
 | 11   | Kabutops           | 12.90323% | 8      | 12.903% | 7      | 13.681% | 
 | 12   | Slowbro            | 11.29032% | 7      | 11.290% | 7      | 13.681% | 
 | 13   | Gastrodon          | 11.29032% | 7      | 11.290% | 7      | 13.681% | 
 | 14   | Miltank            | 11.29032% | 7      | 11.290% | 5      |  9.772% | 
 | 15   | Clefable           | 11.29032% | 7      | 11.290% | 6      | 11.726% | 
 | 16   | Chansey            |  9.67742% | 6      |  9.677% | 5      |  9.772% | 
 | 17   | Steelix            |  9.67742% | 6      |  9.677% | 5      |  9.772% | 
 | 18   | Feraligatr         |  9.67742% | 6      |  9.677% | 6      | 11.726% | 
 | 19   | Bastiodon          |  9.67742% | 6      |  9.677% | 6      | 11.726% | 
 | 20   | Magmortar          |  9.67742% | 6      |  9.677% | 4      |  7.818% | 
 | 21   | Houndoom           |  9.67742% | 6      |  9.677% | 5      |  9.772% | 
 | 22   | Azumarill          |  9.67742% | 6      |  9.677% | 5      |  9.772% | 
 | 23   | Uxie               |  8.06452% | 5      |  8.065% | 5      |  9.772% | 
 | 24   | Absol              |  8.06452% | 5      |  8.065% | 4      |  7.818% | 
 | 25   | Altaria            |  8.06452% | 5      |  8.065% | 3      |  5.863% | 
 | 26   | Blastoise          |  8.06452% | 5      |  8.065% | 3      |  5.863% | 
 | 27   | Nidoking           |  8.06452% | 5      |  8.065% | 4      |  7.818% | 
 | 28   | Medicham           |  8.06452% | 5      |  8.065% | 4      |  7.818% | 
 | 29   | Muk                |  8.06452% | 5      |  8.065% | 4      |  7.818% | 
 | 30   | Tangrowth          |  8.06452% | 5      |  8.065% | 5      |  9.772% | 
 | 31   | Sceptile           |  6.45161% | 4      |  6.452% | 3      |  5.863% | 
 | 32   | Mismagius          |  6.45161% | 4      |  6.452% | 4      |  7.818% | 
 | 33   | Ampharos           |  6.45161% | 4      |  6.452% | 3      |  5.863% | 
 | 34   | Torterra           |  6.45161% | 4      |  6.452% | 3      |  5.863% | 
 | 35   | Ludicolo           |  6.45161% | 4      |  6.452% | 4      |  7.818% | 
 | 36   | Weezing            |  4.83871% | 3      |  4.839% | 2      |  3.909% | 
 | 37   | Rampardos          |  4.83871% | 3      |  4.839% | 2      |  3.909% | 
 | 38   | Rotom              |  4.83871% | 3      |  4.839% | 3      |  5.863% | 
 | 39   | Ursaring           |  4.83871% | 3      |  4.839% | 3      |  5.863% | 
 | 40   | Victreebel         |  4.83871% | 3      |  4.839% | 3      |  5.863% | 
 | 41   | Typhlosion         |  4.83871% | 3      |  4.839% | 3      |  5.863% | 
 | 42   | Claydol            |  4.83871% | 3      |  4.839% | 3      |  5.863% | 
 | 43   | Espeon             |  4.83871% | 3      |  4.839% | 0      |  0.000% | 
 | 44   | Hitmontop          |  4.83871% | 3      |  4.839% | 3      |  5.863% | 
 | 45   | Spiritomb          |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 46   | Sandslash          |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 47   | Regirock           |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 48   | Hitmonlee          |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 49   | Exeggutor          |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 50   | Omastar            |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 51   | Articuno           |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 52   | Politoed           |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 53   | Slaking            |  3.22581% | 2      |  3.226% | 0      |  0.000% | 
 | 54   | Swalot             |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 55   | Meganium           |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 56   | Gardevoir          |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 57   | Slowking           |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 58   | Lopunny            |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 59   | Drapion            |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 60   | Shiftry            |  3.22581% | 2      |  3.226% | 1      |  1.954% | 
 | 61   | Ninetales          |  3.22581% | 2      |  3.226% | 2      |  3.909% | 
 | 62   | Swellow            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 63   | Hitmonchan         |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 64   | Leafeon            |  1.61290% | 1      |  1.613% | 0      |  0.000% | 
 | 65   | Aggron             |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 66   | Pinsir             |  1.61290% | 1      |  1.613% | 0      |  0.000% | 
 | 67   | Banette            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 68   | Vespiquen          |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 69   | Cloyster           |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 70   | Pachirisu          |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 71   | Fearow             |  1.61290% | 1      |  1.613% | 0      |  0.000% | 
 | 72   | Primeape           |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 73   | Bellossom          |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 74   | Scyther            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 75   | Togetic            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 76   | Monferno           |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 77   | Rhyperior          |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 78   | Wailord            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 79   | Masquerain         |  1.61290% | 1      |  1.613% | 0      |  0.000% | 
 | 80   | Jumpluff           |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 81   | Drifloon           |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 82   | Regice             |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 83   | Sableye            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 84   | Dugtrio            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 85   | Floatzel           |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 86   | Electrode          |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 87   | Moltres            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 88   | Flareon            |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 89   | Qwilfish           |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 90   | Entei              |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 | 91   | Glalie             |  1.61290% | 1      |  1.613% | 1      |  1.954% | 
 + ---- + ------------------ + --------- + ------ + ------- + ------ + ------- + 
