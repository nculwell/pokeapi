 Total leads: 62
 + ---- + ------------------ + --------- + ------ + ------- + 
 | Rank | Pokemon            | Usage %   | Raw    | %       | 
 + ---- + ------------------ + --------- + ------ + ------- + 
 | 1    | Mesprit            | 72.37340% | 3      |  4.839% | 
 | 2    | Ambipom            | 27.62660% | 16     | 25.806% | 
 | 3    | Wailord            |  0.00000% | 1      |  1.613% | 
 | 4    | Claydol            |  0.00000% | 2      |  3.226% | 
 | 5    | Uxie               |  0.00000% | 1      |  1.613% | 
 | 6    | Registeel          |  0.00000% | 5      |  8.065% | 
 | 7    | Gardevoir          |  0.00000% | 1      |  1.613% | 
 | 8    | Blastoise          |  0.00000% | 1      |  1.613% | 
 | 9    | Absol              |  0.00000% | 1      |  1.613% | 
 | 10   | Jumpluff           |  0.00000% | 1      |  1.613% | 
 | 11   | Bastiodon          |  0.00000% | 4      |  6.452% | 
 | 12   | Drapion            |  0.00000% | 1      |  1.613% | 
 | 13   | Torterra           |  0.00000% | 1      |  1.613% | 
 | 14   | Donphan            |  0.00000% | 1      |  1.613% | 
 | 15   | Victreebel         |  0.00000% | 3      |  4.839% | 
 | 16   | Primeape           |  0.00000% | 1      |  1.613% | 
 | 17   | Tangrowth          |  0.00000% | 2      |  3.226% | 
 | 18   | Houndoom           |  0.00000% | 1      |  1.613% | 
 | 19   | Ninetales          |  0.00000% | 1      |  1.613% | 
 | 20   | Rampardos          |  0.00000% | 1      |  1.613% | 
 | 21   | Glalie             |  0.00000% | 1      |  1.613% | 
 | 22   | Altaria            |  0.00000% | 1      |  1.613% | 
 | 23   | Dugtrio            |  0.00000% | 1      |  1.613% | 
 | 24   | Typhlosion         |  0.00000% | 1      |  1.613% | 
 | 25   | Swalot             |  0.00000% | 1      |  1.613% | 
 | 26   | Alakazam           |  0.00000% | 3      |  4.839% | 
 | 27   | Vespiquen          |  0.00000% | 1      |  1.613% | 
 | 28   | Meganium           |  0.00000% | 2      |  3.226% | 
 | 29   | Venusaur           |  0.00000% | 1      |  1.613% | 
 | 30   | Floatzel           |  0.00000% | 1      |  1.613% | 
 | 31   | Rhyperior          |  0.00000% | 1      |  1.613% | 
 + ---- + ------------------ + --------- + ------ + ------- + 
